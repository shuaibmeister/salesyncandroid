package com.thinknsync.salesync.location;

import com.thinknsync.logger.Logger;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.positionmanager.TrackingData;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.messageBroker.BrokerTopics;
import com.thinknsync.salesync.messageBroker.PushPullManager;
import com.thinknsync.salesync.messageBroker.pushPullContents.BrokerPublishContent;

public class TrackingListenerDefaultAction extends TypedObserverImpl<TrackingData> {

    private ObjectConstructor objectConstructor;
    private Logger logger;

    private final float MIN_LOCATION_ACCURACY = 50;

    public TrackingListenerDefaultAction(ObjectConstructor objectConstructor){
        this.objectConstructor = objectConstructor;
        this.logger = objectConstructor.getLogger();
    }

    @Override
    public void update(TrackingData trackingData) {
        logger.debugLog("location", trackingData.toString());
        if(trackingData.getAccuracy() < MIN_LOCATION_ACCURACY) {
            PushPullManager pushPullContainer = objectConstructor.getMasterPushPullContainer().getPushPullManager();
            BrokerTopics topics = objectConstructor.getBrokerTopics();

            pushPullContainer.publish(new BrokerPublishContent(objectConstructor,
                    topics.getLocationUpdateTopic(), trackingData.toString()));
        }
    }
}
