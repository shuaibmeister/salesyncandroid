package com.thinknsync.salesync.location;

import android.content.Intent;

import com.google.gson.Gson;
import com.thinknsync.positionmanager.BaseLocationTrackerService;
import com.thinknsync.salesync.factory.GetNewObject;
import com.thinknsync.salesync.factory.ObjectConstructor;

public class EmployeeTracking extends BaseLocationTrackerService {

    private ObjectConstructor objectConstructor;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        long srMinTrackingTime = Integer.parseInt(intent.getExtras().getString(key_min_time)) * 1000 * 60;
        objectConstructor = new Gson().fromJson(intent.getExtras().getString(ObjectConstructor.tag), GetNewObject.class);
//        addToObservers(new TrackingListenerDefaultAction(objectConstructor, contextWrapper));
        objectConstructor.getLogger().debugLog(key_min_time, String.valueOf(srMinTrackingTime));
        setMinTime(srMinTrackingTime);
        return super.onStartCommand(intent, flags, startId);
    }
}
