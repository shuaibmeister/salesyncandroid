package com.thinknsync.salesync.uiElements.formElements.inputs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.thinknsync.inputvalidation.InputValidation;
import com.thinknsync.inputvalidation.errors.ValidationError;
import com.thinknsync.salesync.commons.osSpecifics.imageCapture.ImageCapture;
import com.thinknsync.salesync.commons.osSpecifics.imageCapture.ImageCaptureImpl;
import com.thinknsync.salesync.commons.utils.ImageUtils;
import com.thinknsync.salesync.factory.ObjectConstructor;

@SuppressLint("AppCompatCustomView")
public class ImageHolder extends ImageView implements InputValidationContainer<String>, ImageHolderPolicy, TextInputChangeObserverApplier{

    private InputValidation inputValidation;
    private InputValidation.ValidationTypes validationType;
    private InputChangeListener<String> inputChangeListener;

    public ImageHolder(Context context) {
        super(context);
    }

    public ImageHolder(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageHolder(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initialize(ObjectConstructor objectConstructor, InputValidation.ValidationTypes validationType) {
        this.inputValidation = objectConstructor.getInputValidator();
        this.validationType = validationType;
    }

    @Override
    public boolean isValid() {
        if(getDrawable() != null) {
            return inputValidation.validateField(getInput(), validationType);
        }
        return false;
    }

    @Override
    public void setImageBitmap(Bitmap bitmap){
        super.setImageBitmap(bitmap);
        if(inputChangeListener != null) {
            inputChangeListener.onInputChanged(getInput());
        }
    }

    @Override
    public String getInput() {
        if(getBitmapFromDrawable() != null) {
            return new ImageUtils().encodeToBase64String(getBitmapFromDrawable());
        }
        return "";
    }

    @Override
    public String getScaledInput(){
        if(getBitmapFromDrawable() != null) {
            ImageUtils imageUtils = new ImageUtils();
            Bitmap scaled = imageUtils.getScaledImage(getBitmapFromDrawable(), 150, 150);
            return imageUtils.encodeToBase64String(scaled);
        }
        return "";
    }

    private Bitmap getBitmapFromDrawable(){
        if(getDrawable() != null) {
            BitmapDrawable drawable = (BitmapDrawable) getDrawable();
            return drawable.getBitmap();
        }
        return null;
    }
    @Override
    public ValidationError.ErrorEnums getValidationError() {
        return inputValidation.getOccurredError();
    }

    @Override
    public void showError() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public InputValidationType getValidationContainerType() {
        return InputValidationType.TEXT;
    }

    @Override
    public void setFocusChangeObserver(InputChangeListener<String> changeListener) {
        this.inputChangeListener = changeListener;
    }

    @Override
    public void setTextChangeObserver(InputChangeListener<String> changeListener) {

    }
}