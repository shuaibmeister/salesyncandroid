package com.thinknsync.salesync.uiElements.formElements.inputs;

public interface InputChangeListener<T> {
    void onInputChanged(T input);
}
