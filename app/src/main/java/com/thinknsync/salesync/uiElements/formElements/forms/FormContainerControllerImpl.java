package com.thinknsync.salesync.uiElements.formElements.forms;

import com.thinknsync.inputvalidation.errors.ValidationError;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.salesync.uiElements.formElements.inputs.BaseOnFocusChangeObserver;
import com.thinknsync.salesync.uiElements.formElements.inputs.BaseOnTextChangeObserver;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputChangeListener;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputValidationContainer;
import com.thinknsync.salesync.uiElements.formElements.inputs.TextInputChangeObserverApplier;

import java.util.HashMap;
import java.util.List;

public class FormContainerControllerImpl implements FormContainerContract.Controller {

    private FormContainerContract.Initializer formContainer;
    private final HashMap<InputValidationContainer, ValidationError.ErrorEnums> errorMap = new HashMap<>();


    public FormContainerControllerImpl(FormContainerContract.Initializer formContainer) {
        this.formContainer = formContainer;
    }

    @Override
    public boolean validateForm(final List<InputValidationContainer> formInputs) {
        errorMap.clear();
        for(InputValidationContainer inputStringProcessor : formInputs){
            if(!inputStringProcessor.isValid()){
                errorMap.put(inputStringProcessor, inputStringProcessor.getValidationError());
            }
        }

        return errorMap.size() == 0;
    }

    private InputChangeListener getFocusChangeListener(final InputValidationContainer inputField) {
        BaseOnFocusChangeObserver onFocusChange = new BaseOnFocusChangeObserver();
        onFocusChange.addToObservers(new TypedObserverImpl<String>() {
            @Override
            public void update(String s) {
                if(!inputField.isValid()){
                    inputField.showError();
                }
            }
        });
        return onFocusChange;
    }

    private InputChangeListener getTextChangeListener() {
        BaseOnTextChangeObserver textWatcher = new BaseOnTextChangeObserver();
        textWatcher.addToObservers(new TypedObserverImpl<String>() {
            @Override
            public void update(String s) {
                formContainer.onValidationResult(formContainer.doFormValidation());
            }
        });
        return textWatcher;
    }

//    private <T extends MasterData> InputChangeListener getSelectionChangeListener(InputValidationContainer validationContainer) {
//        BaseOnItemSelectedObserver<T> selectedChangeListener = new BaseOnItemSelectedObserver<>();
//        selectedChangeListener.addToObservers(new TypedObserverImpl<T>() {
//            @Override
//            public void update(T s) {
//                formContainer.onValidationResult(formContainer.doFormValidation());
//            }
//        });
//        return selectedChangeListener;
//    }

    @Override
    public HashMap<InputValidationContainer, ValidationError.ErrorEnums> getErrors() {
        return errorMap;
    }

    @Override
    public void setValueChangeObserver(InputValidationContainer validationContainer) {
        if(validationContainer.getValidationContainerType() == InputValidationContainer.InputValidationType.TEXT ||
                validationContainer.getValidationContainerType() == InputValidationContainer.InputValidationType.SINGLE_SELECTION){
            ((TextInputChangeObserverApplier)validationContainer).setFocusChangeObserver(getFocusChangeListener(validationContainer));
            ((TextInputChangeObserverApplier)validationContainer).setTextChangeObserver(getTextChangeListener());
        }
//        else if(validationContainer.getValidationContainerType() == InputValidationContainer.InputValidationType.SINGLE_SELECTION){
//            ((SingleSelectionInputChangeObserverApplier)validationContainer).setSelectionChangeObserver(getSelectionChangeListener(validationContainer));
//        }
    }
}
