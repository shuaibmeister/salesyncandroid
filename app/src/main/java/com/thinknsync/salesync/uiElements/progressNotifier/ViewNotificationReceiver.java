package com.thinknsync.salesync.uiElements.progressNotifier;

public interface ViewNotificationReceiver<T> {
    void setViews();
    void notifyViewUpdate(T... updateValue);
}
