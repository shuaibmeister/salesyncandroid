package com.thinknsync.salesync.uiElements.formElements.inputs;

public interface TextInputChangeObserverApplier {
    void setFocusChangeObserver(InputChangeListener<String> changeListener);
    void setTextChangeObserver(InputChangeListener<String> changeListener);
}
