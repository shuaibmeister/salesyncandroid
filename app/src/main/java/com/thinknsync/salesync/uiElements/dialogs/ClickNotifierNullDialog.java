package com.thinknsync.salesync.uiElements.dialogs;

import android.content.Context;

import com.thinknsync.dialogs.BaseDialog;
import com.thinknsync.mapslib.frameworkWrappers.MarketInfoDialog;
import com.thinknsync.observerhost.TypedObserver;

public class ClickNotifierNullDialog extends BaseDialog<Object> implements MarketInfoDialog<Object> {

    public ClickNotifierNullDialog(Context context) {
        super(context);
    }

    @Override
    public void buildDialog() {

    }

    @Override
    public void showDialog() {
        dialogResponse.onNegativeClicked(null);
    }

    @Override
    public void onDismiss(TypedObserver typedObserver) {

    }

    @Override
    public void adjustDialogPosition(int[] ints) {

    }

    @Override
    public void showDialog(String... strings) {
        showDialog();
    }

    @Override
    public void hideDialog(){

    }

    @Override
    public Object getFrameworkObject() {
        return null;
    }

    @Override
    public void setFrameworkObject(Object o) {

    }
}
