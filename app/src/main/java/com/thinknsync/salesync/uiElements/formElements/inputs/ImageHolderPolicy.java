package com.thinknsync.salesync.uiElements.formElements.inputs;

public interface ImageHolderPolicy {
    String getScaledInput();
}
