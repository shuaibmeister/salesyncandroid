package com.thinknsync.salesync.uiElements.expandableView;

import android.graphics.Color;

import com.thinknsync.salesync.commons.commonInterfaces.HasIdentifier;
import com.thinknsync.salesync.commons.commonInterfaces.HasName;

import java.util.HashMap;
import java.util.List;

public interface ExpandableSingleSelectionView<T extends HasIdentifier & HasName> {
    int[] default_colors = new int[]{Color.parseColor("#66000000"), Color.BLACK};

    void setDefaultColors(int activeColor, int passiveColor);
    void swapActiveColors();
    T getSelectedItem();
    void setOptions(HashMap<String, List<T>> options);
}
