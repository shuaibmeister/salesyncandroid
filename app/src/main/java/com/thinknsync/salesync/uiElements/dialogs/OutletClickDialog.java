package com.thinknsync.salesync.uiElements.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.thinknsync.dialogs.BaseDialog;
import com.thinknsync.mapslib.frameworkWrappers.MarketInfoDialog;
import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.base.viewInterfaces.InteractiveView;
import com.thinknsync.salesync.base.viewInterfaces.ViewInitialization;
import com.thinknsync.salesync.commons.utils.ImageUtils;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;


public class OutletClickDialog extends BaseDialog<OutletInterface> implements MarketInfoDialog<Dialog>,
        ViewInitialization.InitView, ViewInitialization.InitValues, InteractiveView {

    private Dialog alertDialog;
    private TextView outletName, outletAddress;
    private ImageView outletImageView;
    private View openOutletButton, getDirectionButton;

    public OutletClickDialog(Context context, OutletInterface outlet) {
        super(context);
        setInformationObject(outlet);
    }

    @Override
    public void showDialog(String... strings) {
        showDialog();
    }

    @Override
    public void showDialog() {
        if(alertDialog == null){
            prepare();
        }
        alertDialog.show();
    }

    @Override
    public void buildDialog() {
//        alertDialog = new Dialog(context);
//        alertDialog.setContentView(R.layout.outlet_map_click_dialog);
//        setFrameworkObject(alertDialog);
    }

    private void prepare(){
        alertDialog = new Dialog(context);
        alertDialog.setContentView(R.layout.outlet_map_click_dialog);
        setFrameworkObject(alertDialog);

        initView();
        initValues();
        initViewListeners();
    }

    @Override
    public void hideDialog() {
        if(alertDialog != null) {
            alertDialog.cancel();
        }
    }

    @Override
    public Dialog getFrameworkObject() {
        return alertDialog;
    }

    @Override
    public void setFrameworkObject(Dialog alertDialog) {
        this.alertDialog = alertDialog;
    }

    @Override
    public void initValues() {
        ImageUtils imageUtils = new ImageUtils();
        outletName.setText(infoObject.getName());
        outletAddress.setText(infoObject.getOutletAddress());
        outletImageView.setImageBitmap(imageUtils.getBimapFromBase64Image(infoObject.getOutletImage()));
    }

    @Override
    public void initViewListeners() {
        getDirectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogResponse.onNegativeClicked(infoObject);
            }
        });

        openOutletButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogResponse.onPositiveClicked(infoObject);
            }
        });
    }

    @Override
    public void initView() {
        outletName = alertDialog.findViewById(R.id.outlet_title);
        outletAddress = alertDialog.findViewById(R.id.outlet_address);
        outletImageView = alertDialog.findViewById(R.id.outlet_image);
        getDirectionButton = alertDialog.findViewById(R.id.get_direction_button);
        openOutletButton = alertDialog.findViewById(R.id.open_outlet_activities_button);
    }

    @Override
    public void onDismiss(final TypedObserver typedObserver) {
        if(alertDialog != null) {
            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    typedObserver.update(null);
                }
            });
        }
    }

    @Override
    public void adjustDialogPosition(int[] ints) {
        if(alertDialog == null) {
            prepare();
        }
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams layoutParams = window.getAttributes();
        window.setDimAmount(0);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DisplayMetrics displayMetrics = new DisplayMetrics();
        window.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        layoutParams.x = ints[0] - displayMetrics.widthPixels / 2;
        layoutParams.y = ints[1] - displayMetrics.heightPixels / 2 + 150;
    }
}
