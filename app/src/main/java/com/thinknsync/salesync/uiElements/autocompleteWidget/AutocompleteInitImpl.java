package com.thinknsync.salesync.uiElements.autocompleteWidget;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.thinknsync.listviewadapterhelper.searchable.SearchableObject;
import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.commons.commonInterfaces.HasIdentifier;
import com.thinknsync.salesync.commons.commonInterfaces.HasName;

public class AutocompleteInitImpl<T extends SearchableObject & HasIdentifier & HasName> extends ObserverHostImpl<T>
        implements AutocompleteInitializer<T> {

    private EditText searchView;
    private ListView suggestionList;

    public AutocompleteInitImpl(View autocompleteFullView){
        searchView = autocompleteFullView.findViewById(R.id.search_text);
        suggestionList = autocompleteFullView.findViewById(R.id.search_result_list);
    }

    @Override
    public void setupAutocompleteWithAdapter(final ArrayAdapter<T> autocompleteDataAdapter){
        suggestionList.setAdapter(autocompleteDataAdapter);
        suggestionList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                T objectClicked = (T)parent.getAdapter().getItem(position);
                searchView.setText(objectClicked.getName());
                suggestionList.setVisibility(View.GONE);
                searchView.clearFocus();
                notifyObservers(objectClicked);
            }
        });

        searchView.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 0){
                    suggestionList.setVisibility(View.GONE);
                } else {
                    suggestionList.setVisibility(View.VISIBLE);
                    autocompleteDataAdapter.getFilter().filter(s);
                }
            }

            public void afterTextChanged(Editable s) {
            }
        });
    }
}
