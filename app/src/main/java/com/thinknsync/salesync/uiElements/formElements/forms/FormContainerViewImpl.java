package com.thinknsync.salesync.uiElements.formElements.forms;

import com.thinknsync.inputvalidation.InputValidation;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.uiElements.formElements.inputs.ActionView;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputValidationContainer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FormContainerViewImpl implements FormContainerContract.Initializer {

    private HashMap<InputValidationContainer, InputValidation.ValidationTypes> inputFieldList;
    private ActionView actionView;

    private ObjectConstructor objectConstructor;
    protected FormContainerContract.Controller validationHelper;

    public FormContainerViewImpl(HashMap<InputValidationContainer, InputValidation.ValidationTypes> inputFieldList,
                                 ObjectConstructor objectConstructor, ActionView actionView) {
        this.inputFieldList = inputFieldList;
        this.objectConstructor = objectConstructor;
        this.actionView = actionView;
        validationHelper = new FormContainerControllerImpl(this);
    }

    @Override
    public void initForm() {
        boolean isFormValid = true;
        for(Map.Entry<InputValidationContainer, InputValidation.ValidationTypes> entry : inputFieldList.entrySet()){
            entry.getKey().initialize(objectConstructor, entry.getValue());
            validationHelper.setValueChangeObserver(entry.getKey());
            if(isFormValid && !entry.getKey().isValid()){
                isFormValid = false;
            }
        }
        actionView.setActionable(isFormValid);
    }

    @Override
    public boolean doFormValidation() {
        return validationHelper.validateForm(new ArrayList<>(inputFieldList.keySet()));
    }

    @Override
    public void onValidationResult(boolean validationSuccess) {
        actionView.setActionable(validationSuccess);
    }
}
