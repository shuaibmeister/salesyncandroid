package com.thinknsync.salesync.uiElements.formElements.inputs;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;

public class ActionText extends AppCompatTextView implements ActionView {
    public ActionText(Context context) {
        super(context);
    }

    public ActionText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ActionText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setActionable(boolean actionable) {
        setEnabled(actionable);
    }
}
