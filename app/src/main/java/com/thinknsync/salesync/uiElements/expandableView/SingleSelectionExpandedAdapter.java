package com.thinknsync.salesync.uiElements.expandableView;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.thinknsync.salesync.R;
import com.thinknsync.salesync.commons.commonInterfaces.HasIdentifier;
import com.thinknsync.salesync.commons.commonInterfaces.HasName;

import java.util.HashMap;
import java.util.List;

public class SingleSelectionExpandedAdapter<T extends HasIdentifier & HasName> extends SingleSelectionExpandableAdapter<T> {

    public SingleSelectionExpandedAdapter(Context context, HashMap<String, List<T>> listChildData, T... selectedItem) {
        super(context, listChildData, selectedItem);
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        convertView = super.getGroupView(groupPosition, isExpanded, convertView, parent);
        convertView.findViewById(R.id.arrow_image).setVisibility(View.INVISIBLE);
        return convertView;
    }

    @Override
    public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long l) {
        selectedObject = getChild(groupPosition, childPosition);
        notifyDataSetChanged();
        return false;
    }
}
