package com.thinknsync.salesync.uiElements.formElements.inputs;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;

public class ActionButton extends AppCompatButton implements ActionView {
    public ActionButton(Context context) {
        super(context);
    }

    public ActionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setActionable(boolean actionable) {
        setEnabled(actionable);
    }
}
