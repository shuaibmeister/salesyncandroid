package com.thinknsync.salesync.uiElements.formElements.inputs;

import android.content.Context;
import android.text.TextWatcher;
import android.util.AttributeSet;

import com.google.android.material.textfield.TextInputEditText;
import com.thinknsync.inputvalidation.InputValidation;
import com.thinknsync.inputvalidation.errors.ValidationError;
import com.thinknsync.salesync.factory.ObjectConstructor;

public class InputField extends TextInputEditText implements InputValidationContainer<String>, TextInputChangeObserverApplier {

    private InputValidation inputValidation;
    private InputValidation.ValidationTypes validationType;

    public InputField(Context context) {
        super(context);
    }

    public InputField(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InputField(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initialize(ObjectConstructor objectConstructor, InputValidation.ValidationTypes validationType){
        this.inputValidation = objectConstructor.getInputValidator();
        this.validationType = validationType;
    }

    @Override
    public boolean isValid() {
        return inputValidation.validateField(getInput(), validationType);
    }

    @Override
    public String getInput() {
        return getText() == null ? "" : getText().toString();
    }

    @Override
    public ValidationError.ErrorEnums getValidationError() {
        return inputValidation.getOccurredError();
    }

    @Override
    public void showError() {
        setError(getValidationError().getErrorText());
    }

    @Override
    public void showError(String message) {
        setError(message);
    }

    @Override
    public InputValidationType getValidationContainerType() {
        return InputValidationType.TEXT;
    }

    @Override
    public void setFocusChangeObserver(InputChangeListener changeListener) {
        setOnFocusChangeListener((OnFocusChangeListener) changeListener);
    }

    @Override
    public void setTextChangeObserver(InputChangeListener changeListener) {
        addTextChangedListener((TextWatcher) changeListener);
    }
}