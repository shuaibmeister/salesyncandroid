package com.thinknsync.salesync.uiElements.formElements.inputs;

import android.content.Context;
import android.util.AttributeSet;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ActionFloatingButton extends FloatingActionButton implements ActionView {
    public ActionFloatingButton(Context context) {
        super(context);
    }

    public ActionFloatingButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ActionFloatingButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setActionable(boolean actionable) {
        setEnabled(actionable);
    }
}
