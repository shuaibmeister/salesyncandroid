package com.thinknsync.salesync.uiElements.formElements.inputs;

import com.thinknsync.inputvalidation.InputValidation;
import com.thinknsync.inputvalidation.errors.ValidationError;
import com.thinknsync.salesync.factory.ObjectConstructor;

public interface InputValidationContainer<T> {
    void initialize(ObjectConstructor objectConstructor, InputValidation.ValidationTypes validationType);
    boolean isValid();
    T getInput();
    ValidationError.ErrorEnums getValidationError();
    void showError();
    void showError(String message);
    InputValidationType getValidationContainerType();

    enum InputValidationType {
        TEXT, SINGLE_SELECTION
    }
}
