package com.thinknsync.salesync.uiElements.formElements.forms;

import com.thinknsync.inputvalidation.InputValidation;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.uiElements.formElements.inputs.ActionView;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputValidationContainer;

import java.util.HashMap;
import java.util.List;

public class FormValidationViewImplIncludingRetypedFields extends FormContainerViewImpl {

    public FormValidationViewImplIncludingRetypedFields(HashMap<InputValidationContainer, InputValidation.ValidationTypes> inputFieldList,
                                                        ObjectConstructor objectConstructor, ActionView actionView,
                                                        List<InputValidationContainer[]> pairOfInputsToMatch) {
        super(inputFieldList, objectConstructor, actionView);
        validationHelper = new FormControllerRetypedValidation(this);
        ((FormContainerContract.RetypedPassMatchContainer)validationHelper).setFormTypeAndRetypePairs(pairOfInputsToMatch);
    }
}
