package com.thinknsync.salesync.uiElements.autocompleteWidget;

import android.widget.ArrayAdapter;

import com.thinknsync.listviewadapterhelper.searchable.SearchableObject;
import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.salesync.commons.commonInterfaces.HasIdentifier;
import com.thinknsync.salesync.commons.commonInterfaces.HasName;

public interface AutocompleteInitializer<T extends SearchableObject & HasIdentifier & HasName> extends ObserverHost<T> {
    void setupAutocompleteWithAdapter(final ArrayAdapter<T> autocompleteDataAdapter);
}
