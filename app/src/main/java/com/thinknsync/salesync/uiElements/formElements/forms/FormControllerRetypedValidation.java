package com.thinknsync.salesync.uiElements.formElements.forms;

import com.thinknsync.inputvalidation.errors.ValidationError;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputValidationContainer;

import java.util.ArrayList;
import java.util.List;

class FormControllerRetypedValidation extends FormContainerControllerImpl implements FormContainerContract.RetypedPassMatchContainer {

    private List<InputValidationContainer[]> pairOfInputsToMatch = new ArrayList<>();

    public FormControllerRetypedValidation(FormContainerContract.Initializer formContainer) {
        super(formContainer);
    }

    @Override
    public boolean validateForm(final List<InputValidationContainer> formInputs) {
        boolean validationResult = super.validateForm(formInputs);
        for(InputValidationContainer[] inputPair : pairOfInputsToMatch){
            if(inputPair.length == 2 && !inputPair[0].getInput().equals(inputPair[1].getInput())){
                validationResult = false;
                getErrors().put(inputPair[1], ValidationError.ErrorEnums.TEXTS_DONT_MATCH);
//                inputPair[1].showError();
            }
        }

        return validationResult;
    }

    @Override
    public void setFormTypeAndRetypePairs(List<InputValidationContainer[]> pairOfInputsToMatch) {
        this.pairOfInputsToMatch = pairOfInputsToMatch;
    }
}
