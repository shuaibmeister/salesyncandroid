package com.thinknsync.salesync.uiElements.formElements.inputs;

public interface ActionView {
    void setActionable(boolean actionable);
}
