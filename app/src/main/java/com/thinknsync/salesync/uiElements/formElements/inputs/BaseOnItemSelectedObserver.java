package com.thinknsync.salesync.uiElements.formElements.inputs;

import android.view.View;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;

import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.salesync.database.records.masterData.MasterData;

public class BaseOnItemSelectedObserver<T extends MasterData> extends ObserverHostImpl<T> implements
        ExpandableListView.OnChildClickListener, InputChangeListener<T> {

    @Override
    public void onInputChanged(T inputString) {
        notifyObservers(inputString);
    }

    @Override
    public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long id) {
        notifyObservers((T)((BaseExpandableListAdapter)expandableListView.getAdapter()).getChild(groupPosition, childPosition));
        return false;
    }
}
