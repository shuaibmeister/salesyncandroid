package com.thinknsync.salesync.uiElements.expandableView;

import com.thinknsync.salesync.commons.commonInterfaces.HasIdentifier;
import com.thinknsync.salesync.commons.commonInterfaces.HasName;

public enum BooleanEnum implements HasIdentifier, HasName {
    TRUE(1, "Yes"),
    FALSE(0, "No")
    ;

    private int id;
    private String name;

    BooleanEnum(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getDataId() {
        return id;
    }

    @Override
    public void setDataId(int id) {
        this.id = id;
    }
}
