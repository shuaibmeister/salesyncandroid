package com.thinknsync.salesync.uiElements.progressNotifier;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.BaseFrameworkWrapper;
import com.thinknsync.objectwrappers.ProgressDialogWrapper;
import com.thinknsync.salesync.R;

public class ProgressNotifierWrapper extends BaseFrameworkWrapper<Dialog> implements ProgressDialogWrapper<Dialog>, ViewNotificationReceiver<Integer> {

    private boolean dialogShown = false;

    private TextView syncMessage, progressValueText;
    private ProgressBar progressBar;
    private AndroidContextWrapper contextWrapper;

    public ProgressNotifierWrapper(AndroidContextWrapper contextWrapper){
        this.contextWrapper = contextWrapper;
        Dialog dialog = new Dialog(contextWrapper.getFrameworkObject());
        dialog.setContentView(R.layout.sync_progress_dialog);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        setFrameworkObject(dialog);
        setViews();
    }

    @Override
    public void setViews() {
        syncMessage = getFrameworkObject().findViewById(R.id.sync_message);
        progressValueText = getFrameworkObject().findViewById(R.id.progress_value);
        progressBar = getFrameworkObject().findViewById(R.id.sync_progressbar);
    }

    @Override
    public void showProgress() {
        if(!dialogShown) {
            getFrameworkObject().show();
            dialogShown = true;
        }
    }

    @Override
    public void hideProgress() {
        if(getFrameworkObject() != null && dialogShown){
            getFrameworkObject().dismiss();
            dialogShown = false;
        }
    }

    @Override
    public void hideProgressWithDelay(int delay) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideProgress();
            }
        }, delay);
    }

    @SuppressLint("StringFormatInvalid")
    @Override
    public void setProgressValue(int value){
        progressBar.setProgress(value);
        progressValueText.setText(String.format(contextWrapper.getFrameworkObject().getString(R.string.percentage_value_text),value));
    }

    @Override
    public void setCancelable(boolean cancelable) {
        if(getFrameworkObject() != null) {
            getFrameworkObject().setCancelable(cancelable);
        }
    }

    @Override
    public void setMessage(String message) {
        syncMessage.setText(message);
    }

    @SuppressLint("StringFormatInvalid")
    @Override
    public void notifyViewUpdate(Integer... updateValue) {
        if(updateValue.length > 0) {
            setProgressValue(updateValue[0]);
        }
    }
}
