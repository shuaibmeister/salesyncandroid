package com.thinknsync.salesync.uiElements.formElements.inputs;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;

import com.thinknsync.inputvalidation.InputValidation;
import com.thinknsync.inputvalidation.errors.ValidationError;
import com.thinknsync.salesync.commons.utils.ImageUtils;
import com.thinknsync.salesync.factory.ObjectConstructor;

import de.hdodenhof.circleimageview.CircleImageView;

public class CircleImageHolder extends CircleImageView implements InputValidationContainer<String>, ImageHolderPolicy,
        TextInputChangeObserverApplier{

    private ImageHolder imageHolder;

    public CircleImageHolder(Context context) {
        super(context);
        initImageHolder();
    }

    public CircleImageHolder(Context context, AttributeSet attrs) {
        super(context, attrs);
        initImageHolder();
    }

    public CircleImageHolder(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initImageHolder();
    }

    private void initImageHolder(){
        imageHolder = new ImageHolder(getContext());
    }

    @Override
    public void initialize(ObjectConstructor objectConstructor, InputValidation.ValidationTypes validationType) {
        imageHolder.initialize(objectConstructor, validationType);
    }

    @Override
    public boolean isValid() {
        return imageHolder.isValid();
    }

    @Override
    public String getInput() {
        return imageHolder.getInput();
    }

    @Override
    public ValidationError.ErrorEnums getValidationError() {
        return imageHolder.getValidationError();
    }

    @Override
    public void showError() {
        imageHolder.showError();
    }

    @Override
    public void showError(String message) {
        imageHolder.showError(message);
    }

    @Override
    public InputValidationType getValidationContainerType() {
        return imageHolder.getValidationContainerType();
    }

    @Override
    public void setImageBitmap(Bitmap bitmap){
        super.setImageBitmap(bitmap);
        imageHolder.setImageBitmap(bitmap);
    }

    @Override
    public void setFocusChangeObserver(InputChangeListener<String> changeListener) {
        imageHolder.setFocusChangeObserver(changeListener);
    }

    @Override
    public void setTextChangeObserver(InputChangeListener<String> changeListener) {
        imageHolder.setTextChangeObserver(changeListener);
    }

    @Override
    public String getScaledInput() {
        return imageHolder.getScaledInput();
    }
}
