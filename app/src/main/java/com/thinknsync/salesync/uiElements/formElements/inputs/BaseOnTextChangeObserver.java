package com.thinknsync.salesync.uiElements.formElements.inputs;

import android.text.Editable;
import android.text.TextWatcher;

import com.thinknsync.observerhost.ObserverHostImpl;

public class BaseOnTextChangeObserver extends ObserverHostImpl<String> implements TextWatcher, InputChangeListener<String> {

    @Override
    public void onInputChanged(String inputString) {
        notifyObservers(inputString);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        onInputChanged(charSequence.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
