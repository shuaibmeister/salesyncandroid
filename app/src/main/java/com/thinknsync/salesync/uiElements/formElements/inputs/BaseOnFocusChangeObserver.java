package com.thinknsync.salesync.uiElements.formElements.inputs;

import android.view.View;
import android.widget.EditText;

import com.thinknsync.observerhost.ObserverHostImpl;

public class BaseOnFocusChangeObserver extends ObserverHostImpl<String> implements View.OnFocusChangeListener, InputChangeListener<String> {

    @Override
    public void onFocusChange(View view, boolean b) {
        if(!b){
            onInputChanged(((EditText)view).getText().toString());
        }
    }

    @Override
    public void onInputChanged(String inputString) {
        notifyObservers(inputString);
    }
}
