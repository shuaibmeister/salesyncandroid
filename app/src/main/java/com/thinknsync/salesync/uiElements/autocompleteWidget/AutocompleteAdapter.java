package com.thinknsync.salesync.uiElements.autocompleteWidget;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.thinknsync.listviewadapterhelper.searchable.BaseSearchableListViewAdapterImpl;
import com.thinknsync.listviewadapterhelper.searchable.SearchProcessImpl;
import com.thinknsync.listviewadapterhelper.searchable.SearchableObject;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.commons.commonInterfaces.HasIdentifier;
import com.thinknsync.salesync.commons.commonInterfaces.HasName;

import java.util.List;

public class AutocompleteAdapter<T extends SearchableObject & HasIdentifier & HasName> extends BaseSearchableListViewAdapterImpl<T> {

    private int layoutId;

    public AutocompleteAdapter(Context context, List<T> objects, int... layoutId) {
        super(context, objects, new SearchProcessImpl<>(objects));
        if(layoutId.length > 0) {
            this.layoutId = layoutId[0];
        }
    }

    @Override
    protected void setValues(View view, T object) {
        TextView nameLabel = view.findViewById(R.id.outlet_name);
        nameLabel.setText(object.getName());
    }

    @Override
    protected int getView() {
        return layoutId == 0 ? R.layout.basic_search_adapter_item : layoutId;
    }
}
