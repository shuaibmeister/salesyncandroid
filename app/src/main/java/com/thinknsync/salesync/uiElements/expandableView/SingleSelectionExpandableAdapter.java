package com.thinknsync.salesync.uiElements.expandableView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.thinknsync.inputvalidation.InputValidation;
import com.thinknsync.inputvalidation.errors.ValidationError;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.commons.commonInterfaces.HasIdentifier;
import com.thinknsync.salesync.commons.commonInterfaces.HasName;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputChangeListener;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputValidationContainer;
import com.thinknsync.salesync.uiElements.formElements.inputs.TextInputChangeObserverApplier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SingleSelectionExpandableAdapter<T extends HasIdentifier & HasName> extends BaseExpandableListAdapter
        implements InputValidationContainer<T>, ExpandableListView.OnChildClickListener,
        ExpandableSingleSelectionView<T>, TextInputChangeObserverApplier {

    protected Context context;
    protected List<String> listDataHeader;
    protected HashMap<String, List<T>> listDataChild;
    protected int[] textColors;

    private InputValidation inputValidation;
    private InputValidation.ValidationTypes validationType;
    private InputChangeListener<String> inputChangeListener;

    protected T selectedObject;

    private View groupViewLayout;

    public SingleSelectionExpandableAdapter(Context context, HashMap<String, List<T>> listChildData, T... selectedItem) {
        this.context = context;
        this.listDataHeader = new ArrayList<>(listChildData.keySet());
        setOptions(listChildData);
        if(selectedItem.length > 0){
            selectedObject = selectedItem[0];
        }
        textColors = default_colors;
    }

    @Override
    public T getChild(int groupPosition, int childPosititon) {
        return listDataChild.get(getGroup(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final T childText = getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.expandable_listview_item, null);
        }

        TextView txtListChild = convertView.findViewById(R.id.expandable_value_other);
        txtListChild.setText(childText.getName());
        if(selectedObject != null && childText.getDataId() == selectedObject.getDataId()){
            convertView.findViewById(R.id.checked_checkbox).setVisibility(View.VISIBLE);
        } else {
            convertView.findViewById(R.id.checked_checkbox).setVisibility(View.GONE);
        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.listDataChild.get(this.listDataHeader.get(groupPosition)).size();
    }

    @Override
    public String getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.expandable_listview_group, null);
        }

        TextView label = convertView.findViewById(R.id.expandable_label_text);
        label.setText(headerTitle);
        label.setTextColor(textColors[1]);
        groupViewLayout = convertView.findViewById(R.id.group_view_layout);
        if(selectedObject != null) {
            TextView value = convertView.findViewById(R.id.expandable_value_text);
            value.setText(selectedObject.getName());
            value.setTextColor(textColors[0]);
            inputChangeListener.onInputChanged(selectedObject.getName());
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public void setDefaultColors(int activeColor, int passiveColor) {
        textColors = new int[]{passiveColor, activeColor};
    }

    @Override
    public void swapActiveColors(){
        int a = textColors[0];
        textColors[0] = textColors[1];
        textColors[1] = a;
    }

    @Override
    public T getSelectedItem() {
        return getInput();
    }

    @Override
    public void setOptions(HashMap<String, List<T>> options) {
        this.listDataChild = options;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public void initialize(ObjectConstructor objectConstructor, InputValidation.ValidationTypes validationType) {
        this.inputValidation = objectConstructor.getInputValidator();
        this.validationType = validationType;
    }

    @Override
    public boolean isValid() {
        if(selectedObject != null) {
            return inputValidation.validateField(selectedObject.getName(), validationType);
        }
        return false;
    }

    @Override
    public T getInput() {
        return selectedObject;
    }

    @Override
    public ValidationError.ErrorEnums getValidationError() {
        return inputValidation.getOccurredError();
    }

    @Override
    public void showError() {
        groupViewLayout.setBackgroundColor(context.getResources().getColor(R.color.colorErrorRed));
    }

    @Override
    public void showError(String message) {

    }

    @Override
    public InputValidationType getValidationContainerType() {
        return InputValidationType.SINGLE_SELECTION;
    }

    @Override
    public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long l) {
        selectedObject = getChild(groupPosition, childPosition);
        expandableListView.collapseGroup(groupPosition);
        return false;
    }

    @Override
    public void setFocusChangeObserver(InputChangeListener<String> changeListener) {
    }

    @Override
    public void setTextChangeObserver(InputChangeListener<String> changeListener) {
        this.inputChangeListener = changeListener;
    }
}