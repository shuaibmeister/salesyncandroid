package com.thinknsync.salesync.uiElements.formElements.forms;

import com.thinknsync.inputvalidation.InputValidation;
import com.thinknsync.inputvalidation.errors.ValidationError;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.uiElements.formElements.inputs.ActionView;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputChangeListener;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputValidationContainer;

import java.util.HashMap;
import java.util.List;

public interface FormContainerContract {

    interface Initializer {
        void initForm();
        boolean doFormValidation();
        void onValidationResult(boolean validationSuccess);
    }

    interface Controller {
        boolean validateForm(final List<InputValidationContainer> formInputs);
        HashMap<InputValidationContainer, ValidationError.ErrorEnums> getErrors();
        void setValueChangeObserver(InputValidationContainer validationContainer);
    }

    interface FormActivity {
        void setupForm(HashMap<InputValidationContainer, InputValidation.ValidationTypes> inputAndTypeMap, ActionView actionView);
    }

    interface RetypedPassMatchContainer {
        void setFormTypeAndRetypePairs(List<InputValidationContainer[]> pairOfInputsToMatch);
    }
}
