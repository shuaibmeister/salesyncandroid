package com.thinknsync.salesync.uiElements.progressNotifier;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.R;

public class CircularProgressbar implements ViewNotificationReceiver<Integer> {

    private CircularProgressBar progressBar;
    private TextView progressValueText;

    private View layoutView;
    private Context context;

    public CircularProgressbar(View progressbarView) {
        this.context = progressbarView.getContext();
        layoutView = progressbarView;
//        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        layoutView = inflater.inflate(R.layout.circular_progressbar, null);
        setViews();
    }

    @Override
    public void setViews() {
        this.progressValueText = layoutView.findViewById(R.id.progress_value);
        this.progressBar = layoutView.findViewById(R.id.circular_progress);
    }

    @SuppressLint("StringFormatInvalid")
    @Override
    public void notifyViewUpdate(Integer... updateValue) {
        if(updateValue.length > 0) {
            progressBar.setProgress(updateValue[0]);
            progressValueText.setText(String.format(context.getString(R.string.percentage_value_text), updateValue[0]));
        }
    }
}