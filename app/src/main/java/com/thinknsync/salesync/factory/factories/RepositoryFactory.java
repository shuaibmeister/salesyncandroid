package com.thinknsync.salesync.factory.factories;

import com.thinknsync.salesync.database.repositories.RepositoryPolicy;
import com.thinknsync.salesync.database.repositories.config.ConfigRepository;
import com.thinknsync.salesync.database.repositories.masterData.MasterDataRepository;
import com.thinknsync.salesync.database.repositories.order.OrderRepository;
import com.thinknsync.salesync.database.repositories.outlet.OutletRepository;
import com.thinknsync.salesync.database.repositories.promotion.PromotionRepository;
import com.thinknsync.salesync.database.repositories.route.RouteRepository;
import com.thinknsync.salesync.database.repositories.sku.SkuRepository;
import com.thinknsync.salesync.database.repositories.user.UserRepository;
import com.thinknsync.salesync.factory.FactoryStringConstraints;

import java.util.Map;

/**
 * Created by shuaib on 5/13/17.
 */

public class RepositoryFactory implements AbstractFactory<RepositoryPolicy> {
    @Override
    public RepositoryPolicy getObject(String objectType, Map objectArgs) {
        switch (objectType) {
            case FactoryStringConstraints.RepositoryObjectType.UserRepository:
                return new UserRepository();
            case FactoryStringConstraints.RepositoryObjectType.RouteRepository:
                return new RouteRepository();
            case FactoryStringConstraints.RepositoryObjectType.OutletRepository:
                return new OutletRepository();
            case FactoryStringConstraints.RepositoryObjectType.ConfigRepository:
                return new ConfigRepository();
            case FactoryStringConstraints.RepositoryObjectType.SkuRepository:
                return new SkuRepository();
            case FactoryStringConstraints.RepositoryObjectType.PromotionRepository:
                return new PromotionRepository();
            case FactoryStringConstraints.RepositoryObjectType.MasterDataRepository:
                return new MasterDataRepository();
            case FactoryStringConstraints.RepositoryObjectType.OrderRepository:
                return new OrderRepository();
            default:
                return null;
        }
    }
}
