package com.thinknsync.salesync.factory;

import com.thinknsync.androidpermissions.PermissionManager;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.dialogs.DialogInterface;
import com.thinknsync.inputvalidation.InputValidation;
import com.thinknsync.logger.Logger;
import com.thinknsync.mapslib.mapWorks.MapsManager;
import com.thinknsync.networkutils.NetworkStatusReporter;
import com.thinknsync.objectwrappers.AndroidViewWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.positionmanager.GpsPositionManager;
import com.thinknsync.salesync.commons.loginAndSessions.LoginLogoutActions;
import com.thinknsync.salesync.commons.osSpecifics.broadcastReceivers.BroadcastListener;
import com.thinknsync.salesync.commons.osSpecifics.broadcastReceivers.BroadcastListenerContainer;
import com.thinknsync.salesync.commons.osSpecifics.frameworkHelper.ApplicationInfo;
import com.thinknsync.salesync.commons.utils.animation.Animations;
import com.thinknsync.salesync.commons.utils.dateTime.DateTimePicker;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.repositories.config.ConfigRepositoryInterface;
import com.thinknsync.salesync.database.repositories.masterData.MasterDataRepositoryInterface;
import com.thinknsync.salesync.database.repositories.order.OrderRepositoryInterface;
import com.thinknsync.salesync.database.repositories.outlet.OutletRepositoryInterface;
import com.thinknsync.salesync.database.repositories.promotion.PromotionRepositoryInterface;
import com.thinknsync.salesync.database.repositories.route.RouteRepositoryInterface;
import com.thinknsync.salesync.database.repositories.sku.SkuRepositoryInterface;
import com.thinknsync.salesync.database.repositories.user.UserRepositoryInterface;
import com.thinknsync.salesync.home.bottomSlidingView.AttendanceManager;
import com.thinknsync.salesync.messageBroker.BrokerTopics;
import com.thinknsync.salesync.messageBroker.PushPullManager;
import com.thinknsync.salesync.messageBroker.PushPullManagerContainer;

import java.util.HashMap;

/**
 * Created by shuaib on 5/11/17.
 */

public interface ObjectConstructor {
    String tag = "objectConstructor";

    void initLogger(HashMap<String, Object> objectArgs);
    Logger getLogger();
    PermissionManager getPermissionHandler(HashMap<String, Object> objectArgs);
    GpsPositionManager getLocationManager(HashMap<String, Object> objectArgs);
//    GpsPositionManager getSrTrackingService(HashMap<String, Object> objectArgs);
//    SyncManager getSyncManager(HashMap<String, Object> objectArgs);
//    DateTimeComparator getHandsetDateTimeManager(HashMap<String, Object> argsHashmap);
//    NotifyUser getAndroidNotification(HashMap<String, Object> argsHashmap);
    BroadcastListener getInternetBroadcastListener();
    BroadcastListener getScreenOnOffBroadcastListener();
    NetworkStatusReporter getNetworkStatusReport(HashMap<String, Object> argsHashmap);
    InputValidation getInputValidator();
    BroadcastListenerContainer getBroadcastListenerContainer();
    ApplicationInfo getAndroidFrameworkHelper(HashMap<String, Object> argsHashmap);
    LoginLogoutActions.Initialization getLoginEssentialActionManager(HashMap<String, Object> argsHashmap);
    LoginLogoutActions.Termination getLogoutEssentialActionManager(HashMap<String, Object> argsHashmap);
//    MapCenterCoordinateFetcher getMapCenterCoordinateFetcher(HashMap<String, Object> objectArgs);
    MapsManager getGoogleMapsManager(HashMap<String, Object> objectArgs);
    DateTimePicker getAndroidDateTimePicker(HashMap<String, Object> argsHashmap);

    PushPullManager getMqttBrokerClient(HashMap<String, Object> argsHashmap);
    BrokerTopics getBrokerTopics(HashMap<String, Object> argsHashmap);
    BrokerTopics getBrokerTopics();
    PushPullManagerContainer getMasterPushPullContainer();
    AttendanceManager getAttendanceManager(HashMap<String, Object> argsHashmap);

    FrameworkWrapper getBundleWrapper(HashMap<String, Object> argsHashmap);

    Animations getAnimationImpl(HashMap<String, Object> objectArgs);

    FrameworkWrapper getAndroidContextWrapper(HashMap<String, Object> argsHashmap);

    AndroidViewWrapper getViewWrapper(HashMap<String, Object> argsHashmap);

    FrameworkWrapper getMqttActionListenerWrapper(HashMap<String, Object> argsHashmap);
    FrameworkWrapper getMqttJsonMessageListenerWrapper(HashMap<String, Object> argsHashmap);
    FrameworkWrapper getBitmapWrapper(HashMap<String, Object> argsHashmap);
    FrameworkWrapper getProgressDialogWrapper(HashMap<String, Object> argsHashmap);
    FrameworkWrapper getNotifiableProgressDialogWrapper(HashMap<String, Object> argsHashmap);
    FrameworkWrapper getAlertDialogWrapper(HashMap<String, Object> argsHashmap);

    DialogInterface getBasicMessageDialog(HashMap<String, Object> argsHashmap);
    DialogInterface<OutletInterface> getOutletInfoDialog(HashMap<String, Object> argsHashmap);

    DialogInterface<Object> getClickNotificationDialog(HashMap<String, Object> argsHashmap);

    DialogInterface<Boolean> getBooleanDialog(HashMap<String, Object> argsHashmap);

    UserRepositoryInterface getUserRepository();
    RouteRepositoryInterface getRouteRepository();
    OutletRepositoryInterface getOutletRepository();
    ConfigRepositoryInterface getConfigRepository();
    SkuRepositoryInterface getSkuRepository();
    PromotionRepositoryInterface getPromotionRepository();
//    OrderRepositoryInterface getOrderRepository();
    MasterDataRepositoryInterface getMasterDataRepository();
    OrderRepositoryInterface getSalesOrderRepository();

    //
    ApiRequest getLoginApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest getTempPinGenerationApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest getTempPinVerificationApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest getPinResetApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest getTodaysRouteApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest getOtherRoutesApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest getSkuApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest getExceptionReasonsApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest getOutletMasterDataApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest getPaymentMethodsApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest getSkuStockApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest getPromotionDataApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest getTargetVsAchievementApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest getSrRunRateApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest getHasAttendanceTodayApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest submitAttendanceApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest punchOutApiObject(HashMap<String, Object> argsHashmap);

    ApiRequest getBulkOrderSendApiObject(HashMap<String, Object> argsHashmap);

    ApiRequest getExceptionSendApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest getOutletImageFetchApiObject(HashMap<String, Object> argsHashmap);

    ApiRequest getProductImageFetchApiObject(HashMap<String, Object> argsHashmap);

    ApiRequest getSkuImageFetchApiObject(HashMap<String, Object> argsHashmap);

    ApiRequest getOutletCreateApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest getOutletUpdateApiObject(HashMap<String, Object> argsHashmap);
    ApiRequest getOrderSendApiObject(HashMap<String, Object> argsHashmap);
}