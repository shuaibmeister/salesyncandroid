package com.thinknsync.salesync.factory;

/**
 * Created by shuaib on 5/10/17.
 */

public interface FactoryStringConstraints {

    interface ObjectType {
        String ObjectConstructor = "objectConstructor";
        String Logger = "logger";
        String PermissionHandler = "permissionHandler";
        String GpsPositionManager = "gpsPositionManager";
        String SrTrackingService = "srTrackingService";
        String SyncManager = "syncManager";
        String RepositoryFactory = "repositoryFactory";
        String HandsetDateTimeManager = "handsetDateTimeManager";
        String AndroidDateTimePicker = "androidDateTimePicker";
        String AndroidNotification = "androidNotification";
        String InternetBroadcastListener = "internetBroadcastListener";
        String ScreenOnOffBroadcastListener = "screenOnOffBroadcastListener";
        String NetworkStatusReporter = "networkStatusReporter";
        String InputValidator = "inputValidator";
        String BroadcastListenerContainer = "broadcastListenerContainer";
        String MqttBrokerClient = "mqttBrokerClient";
        String MessageBrokerTopics = "messageBrokerTopics";
        String PushPullClientContainer = "pushPullClientContainer";
        String FrameworkHelper = "frameworkHelper";
        String LoginActionManager = "loginActionManager";
        String LogoutActionManager = "logoutActionManager";
        String MapCenterCoordinateFetcher = "mapCenterCoordinateFetcher";
        String GoogleMapsManager = "googleMapsManager";
        String AttendanceManager = "attendanceManager";
        String AnimationImpl = "animationImpl";
    }

    interface FactoryObjectType {
        String ApiCallFactory = "apiCallFactory";
        String DialogFactory = "dialogFactory";
        String FrameworkWrapperFactory = "frameworkWrapperFactory ";
    }

    interface DialogObjectType{
        String BasicNotificationDialog = "basicNotificationDialog";
        String OutletInfoDialog = "outletInfoDialog";
        String ClickNotificationDialog = "clickNotificationDialog";
        String BooleanYesNoDialog = "booleanYesNoDialog";
    }

    interface ViewControllerObjectType {
        String LoginViewController = "loginViewController";
        String ForgotPasswordViewController = "forgotPasswordViewController";
        String TempPinVerificationViewController = "tempPinVerificationViewController";
        String ResetPinViewController = "resetPinViewController";
        String PinLoginViewController = "pinLoginViewController";
        String HomeViewController = "homeViewController";
        String HomeBottomSheetController = "homeBottomSheetController";
        String OutletViewController = "outletViewController";
        String OutletBottomViewController = "outletBottomViewController";
        String LocationPickerViewController = "locationPickerViewController";
        String NewOutletViewController = "newOutletViewController";
        String UnverifiedOutletsViewController = "unverifiedOutletsViewController";
        String SkuParentSelectionViewController = "skuParentSelectionViewController";
        String SkuSelectionViewController = "skuSelectionViewController";
        String SkuInputBottomViewController = "skuInputBottomViewController";
        String CartOrderViewController = "cartOrderViewController";
        String CartOrderBottomViewController = "cartOrderBottomViewController";
        String CartOrderSubmitBottomViewController = "cartOrderSubmitBottomViewController";
    }

    interface ApiCallObjectType {
        String LoginApiCallObject = "loginApiCallObject";
        String TempPinGenerateApiCallObject = "tempPinGenerateApiCallObject";
        String TempPinValidateApiCallObject = "tempPinValidateApiCallObject";
        String PinResetApiCallObject = "pinResetApiCallObject";
        String TodaysRouteApiCallObject = "todaysRouteApiCallObject";
        String OtherRoutesApiCallObject = "otherRoutesApiCallObject";
        String SkuApiCallObject = "skuApiCallObject";
        String ExceptionReasonsApiCallObject = "exceptionReasonsApiCallObject";
        String TargetVsAchievementApiCallObject = "targetVsAchievementApiCallObject";
        String CmInfoApiCallObject = "cmInfoApiCallObject";
        String SendOrderApiCallObject = "sendOrderApiCallObject";
        String SendBulkOrderApiCallObject = "sendBulkOrderApiCallObject";
        String SendExceptionApiCallObject = "sendExceptionApiCallObject";
        String UpdateApiCallObject = "updateApiCallObject";
        String ProductApiCallObject = "productApiCallObject";
        String ChangePasswordApiCallObject = "changePasswordApiCallObject";
        String OutletMasterDataApiObject = "outletMasterDataApiObject";
        String PaymentMethodsApiObject = "paymentMethodsApiObject";
        String SkuStockApiObject = "skuStockApiObject";
        String PromotionDataApiObject = "promotionDataApiObject";
        String RunRateApiObject = "runRateApiObject";
        String NewOutletApiObject = "newOutletApiObject";
        String VerifyOutletApiObject = "VerifyOutletApiObject";
        String ServerDateTimeApiObject = "serverDateTimeApiObject";
        String SrLocationSendApiObject = "srLocationSendApiObject";
        String ConfigurationApiObject = "configurationApiObject";
        String OutletImageApiObject = "outletImageApiObject";
        String ProductParentImageApiObject = "productParentImageApiObject";
        String SkuImageApiObject = "skuImageApiObject";
        String OutletUpdateApiObject = "outletUpdateApiObject";
        String OutletCreateApiObject = "outletCreateApiObject";

        String AttendanceCheckApiObject = "attendanceCheckApiObject";
        String SubmitAttendanceApiObject = "submitAttendanceApiObject";
        String PunchOutApiObject = "punchOutApiObject";

        String DayWiseSalesReport = "dayWiseSalesReport";
        String RouteWiseSalesReport = "routeWiseSalesReport";
        String TargetVsAchievementReport = "targetVsAchievementReport";
        String DssReport = "dssReport";
    }

    interface FrameworkWrapperObjectType {
        String ContextWrapper = "contextWrapper";
        String BundleWrapper = "bundleWrapper";
        String BitmapWrapper = "bitmapWrapper";
        String ViewWrapper = "viewWrapper";
        String MqttActionListenerWrapper = "mqttActionListenerWrapper";
        String MqttMessageListenerWrapper = "mqttMessageListenerWrapper";
        String ProgressDialogWrapper = "progressDialogWrapper";
        String NotifiableProgressDialog = "notifiableProgressDialog";
        String AlertDialogWrapper = "alertDialogWrapper";
    }

    interface RepositoryObjectType {
        String UserRepository = "userRepository";
        String RouteRepository = "routeRepository";
        String OutletRepository = "outletRepository";
        String ConfigRepository = "configRepository";
        String SkuRepository = "skuRepository";
        String PromotionRepository = "promotionRepository";
        String OrderRepository = "orderRepository";
        String MasterDataRepository = "masterDataRepository";
    }

    interface ObserverObjectType {

    }
}
