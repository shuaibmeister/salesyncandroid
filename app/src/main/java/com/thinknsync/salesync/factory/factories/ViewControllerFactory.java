package com.thinknsync.salesync.factory.factories;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.base.viewInterfaces.Interactors;
import com.thinknsync.salesync.cart.order.CartOrderContract;
import com.thinknsync.salesync.cart.order.CartOrderController;
import com.thinknsync.salesync.cart.order.bottomView.orderConfirm.OrderSubmitBottomController;
import com.thinknsync.salesync.cart.order.bottomView.orderConfirm.OrderSubmitBottomSheetContract;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.home.HomeContract;
import com.thinknsync.salesync.home.bottomSlidingView.HomeBottomSheetController;
import com.thinknsync.salesync.home.bottomSlidingView.HomeBottomSlidingSheetContract;
import com.thinknsync.salesync.home.homeController.HomeController;
import com.thinknsync.salesync.home.outletCreate.NewOutletContract;
import com.thinknsync.salesync.home.outletCreate.NewOutletController;
import com.thinknsync.salesync.home.unverifiedOutlets.UnverifiedOutletsContract;
import com.thinknsync.salesync.home.unverifiedOutlets.UnverifiedOutletsController;
import com.thinknsync.salesync.login.forgotPin.forgot.ForgotPasswordContract;
import com.thinknsync.salesync.login.forgotPin.forgot.ForgotPasswordController;
import com.thinknsync.salesync.login.forgotPin.pinReset.PinResetContact;
import com.thinknsync.salesync.login.forgotPin.pinReset.ResetPinController;
import com.thinknsync.salesync.login.forgotPin.pinVerification.TempPinVerificationContact;
import com.thinknsync.salesync.login.forgotPin.pinVerification.TempPinVerificationController;
import com.thinknsync.salesync.login.login.LoginContract;
import com.thinknsync.salesync.login.login.LoginController;
import com.thinknsync.salesync.login.pinLogin.PinLoginController;
import com.thinknsync.salesync.outlet.OutletContract;
import com.thinknsync.salesync.outlet.OutletController;
import com.thinknsync.salesync.outlet.actions.order.skuParentView.SkuParentSelectionContract;
import com.thinknsync.salesync.outlet.actions.order.skuParentView.SkuParentSelectionController;
import com.thinknsync.salesync.outlet.actions.order.skuView.SkuSelectionContract;
import com.thinknsync.salesync.outlet.actions.order.skuView.SkuSelectionController;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.SkuInputBottomSheetController;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.SkuInputContract;
import com.thinknsync.salesync.outlet.bottomSlidingView.OutletBottomSheetContract;
import com.thinknsync.salesync.outlet.bottomSlidingView.OutletBottomSheetController;
import com.thinknsync.salesync.outlet.locationChange.LocationPickerContract;
import com.thinknsync.salesync.outlet.locationChange.LocationPickerController;

import java.util.Map;

import static com.thinknsync.salesync.factory.FactoryStringConstraints.ViewControllerObjectType;

public class ViewControllerFactory implements AbstractFactory<Interactors> {
    @Override
    public Interactors getObject(String objectType, Map objectArgs) {
        switch (objectType){
            case ViewControllerObjectType.LoginViewController:
                return new LoginController((LoginContract.View) objectArgs.get(LoginContract.View.tag));
            case ViewControllerObjectType.ForgotPasswordViewController:
                return new ForgotPasswordController((ForgotPasswordContract.View) objectArgs.get(ForgotPasswordContract.View.tag));
            case ViewControllerObjectType.TempPinVerificationViewController:
                return new TempPinVerificationController((TempPinVerificationContact.View) objectArgs.get(TempPinVerificationContact.View.tag));
            case ViewControllerObjectType.ResetPinViewController:
                return new ResetPinController((PinResetContact.View) objectArgs.get(PinResetContact.View.tag));
            case ViewControllerObjectType.PinLoginViewController:
                return new PinLoginController((LoginContract.View) objectArgs.get(LoginContract.View.tag));
            case ViewControllerObjectType.HomeViewController:
                return new HomeController((HomeContract.View) objectArgs.get(HomeContract.View.tag));
            case ViewControllerObjectType.HomeBottomSheetController:
                return new HomeBottomSheetController((ObjectConstructor) objectArgs.get(ObjectConstructor.tag),
                        (AndroidContextWrapper)objectArgs.get(AndroidContextWrapper.tag),
                        (HomeBottomSlidingSheetContract.HomeBottomSheetView) objectArgs.get(
                                HomeBottomSlidingSheetContract.HomeBottomSheetView.tag));
            case ViewControllerObjectType.OutletViewController:
                return new OutletController((OutletContract.View) objectArgs.get(OutletContract.View.tag));
            case ViewControllerObjectType.OutletBottomViewController:
                return new OutletBottomSheetController((ObjectConstructor) objectArgs.get(ObjectConstructor.tag),
                        (AndroidContextWrapper)objectArgs.get(AndroidContextWrapper.tag),
                        (OutletInterface) objectArgs.get(OutletInterface.tag),
                        (OutletBottomSheetContract.View) objectArgs.get(OutletBottomSheetContract.View.tag));
            case ViewControllerObjectType.LocationPickerViewController:
                return new LocationPickerController((LocationPickerContract.View) objectArgs.get(LocationPickerContract.View.tag));
            case ViewControllerObjectType.NewOutletViewController:
                return new NewOutletController((NewOutletContract.View) objectArgs.get(NewOutletContract.View.tag));
            case ViewControllerObjectType.UnverifiedOutletsViewController:
                return new UnverifiedOutletsController((UnverifiedOutletsContract.View) objectArgs.get(UnverifiedOutletsContract.View.tag));
            case ViewControllerObjectType.SkuParentSelectionViewController:
                return new SkuParentSelectionController((SkuParentSelectionContract.View) objectArgs.get(SkuParentSelectionContract.View.tag));
            case ViewControllerObjectType.SkuSelectionViewController:
                return new SkuSelectionController((SkuSelectionContract.View) objectArgs.get(SkuSelectionContract.View.tag));
            case ViewControllerObjectType.SkuInputBottomViewController:
                return new SkuInputBottomSheetController((SkuInputContract.View) objectArgs.get(SkuInputContract.View.tag),
                        (ObjectConstructor) objectArgs.get(ObjectConstructor.tag),
                        (OutletInterface) objectArgs.get(OutletInterface.tag),
                        (SkuInterface) objectArgs.get(SkuInterface.tag),
                        (long) objectArgs.get(SalesOrderInterface.key_in_date_time));
            case ViewControllerObjectType.CartOrderViewController:
                return new CartOrderController((CartOrderContract.View) objectArgs.get("ascac"));
            case ViewControllerObjectType.CartOrderBottomViewController:
                return new CartOrderController((CartOrderContract.View) objectArgs.get(CartOrderContract.View.tag));
            case ViewControllerObjectType.CartOrderSubmitBottomViewController:
                return new OrderSubmitBottomController((OrderSubmitBottomSheetContract.View) objectArgs.get(
                        OrderSubmitBottomSheetContract.View.tag),
                        (SalesOrderInterface) objectArgs.get(SalesOrderInterface.tag));
        }
        return null;
    }
}
