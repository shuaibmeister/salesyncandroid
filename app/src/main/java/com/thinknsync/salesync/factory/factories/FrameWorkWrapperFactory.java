package com.thinknsync.salesync.factory.factories;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;

import com.thinknsync.objectwrappers.AlertDialogWrapper;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.AndroidViewWrapper;
import com.thinknsync.objectwrappers.BitmapWrapper;
import com.thinknsync.objectwrappers.BundleWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.objectwrappers.ProgressDialogWrapper;
import com.thinknsync.objectwrappers.ProgressDialogWrapperImpl;
import com.thinknsync.salesync.factory.FactoryStringConstraints;
import com.thinknsync.salesync.messageBroker.actionListeners.PushPullActionListenerWrapper;
import com.thinknsync.salesync.messageBroker.actionListeners.PushPullMessageListenerWrapper;
import com.thinknsync.salesync.uiElements.progressNotifier.ProgressNotifierWrapper;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;

import java.util.Map;

/**
 * Created by shuaib on 5/13/17.
 */

public class FrameWorkWrapperFactory implements AbstractFactory<FrameworkWrapper> {
    @Override
    public FrameworkWrapper getObject(String objectType, Map objectArgs) {
        switch (objectType) {
            case FactoryStringConstraints.FrameworkWrapperObjectType.ContextWrapper:
                return new AndroidContextWrapper((Context) objectArgs.get(AndroidContextWrapper.tag));
            case FactoryStringConstraints.FrameworkWrapperObjectType.BundleWrapper:
                return new BundleWrapper((Bundle) objectArgs.get(BundleWrapper.tag));
            case FactoryStringConstraints.FrameworkWrapperObjectType.BitmapWrapper:
                return new BitmapWrapper((Bitmap) objectArgs.get(BitmapWrapper.tag));
            case FactoryStringConstraints.FrameworkWrapperObjectType.ViewWrapper:
                return new AndroidViewWrapper((View) objectArgs.get(AndroidViewWrapper.tag));
            case FactoryStringConstraints.FrameworkWrapperObjectType.MqttActionListenerWrapper:
                return new PushPullActionListenerWrapper((IMqttActionListener) objectArgs.get(PushPullActionListenerWrapper.tag));
            case FactoryStringConstraints.FrameworkWrapperObjectType.MqttMessageListenerWrapper:
                return new PushPullMessageListenerWrapper((IMqttMessageListener) objectArgs.get(PushPullMessageListenerWrapper.tag));
            case FactoryStringConstraints.FrameworkWrapperObjectType.ProgressDialogWrapper:
                return new ProgressDialogWrapperImpl((ProgressDialog) objectArgs.get(ProgressDialogWrapper.tag));
            case FactoryStringConstraints.FrameworkWrapperObjectType.NotifiableProgressDialog:
                return new ProgressNotifierWrapper((AndroidContextWrapper) objectArgs.get(AndroidContextWrapper.tag));
            case FactoryStringConstraints.FrameworkWrapperObjectType.AlertDialogWrapper:
                return new AlertDialogWrapper((AndroidContextWrapper) objectArgs.get(AndroidContextWrapper.tag));
        }

        return null;
    }
}
