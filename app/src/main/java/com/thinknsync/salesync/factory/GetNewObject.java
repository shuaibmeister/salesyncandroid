package com.thinknsync.salesync.factory;

import com.thinknsync.androidpermissions.PermissionManager;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.dialogs.DialogInterface;
import com.thinknsync.inputvalidation.InputValidation;
import com.thinknsync.logger.Logger;
import com.thinknsync.mapslib.mapWorks.MapsManager;
import com.thinknsync.networkutils.NetworkStatusReporter;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.AndroidViewWrapper;
import com.thinknsync.objectwrappers.BitmapWrapper;
import com.thinknsync.objectwrappers.BundleWrapper;
import com.thinknsync.objectwrappers.DialogWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.objectwrappers.ProgressDialogWrapper;
import com.thinknsync.positionmanager.GpsPositionManager;
import com.thinknsync.salesync.commons.osSpecifics.broadcastReceivers.BroadcastListenerContainer;
import com.thinknsync.salesync.commons.utils.animation.Animations;
import com.thinknsync.salesync.commons.utils.dateTime.DateTimePicker;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.repositories.config.ConfigRepositoryInterface;
import com.thinknsync.salesync.database.repositories.masterData.MasterDataRepositoryInterface;
import com.thinknsync.salesync.database.repositories.order.OrderRepositoryInterface;
import com.thinknsync.salesync.database.repositories.outlet.OutletRepositoryInterface;
import com.thinknsync.salesync.database.repositories.promotion.PromotionRepositoryInterface;
import com.thinknsync.salesync.database.repositories.route.RouteRepositoryInterface;
import com.thinknsync.salesync.database.repositories.sku.SkuRepositoryInterface;
import com.thinknsync.salesync.database.repositories.user.UserRepositoryInterface;
import com.thinknsync.salesync.factory.factories.RepositoryFactory;
import com.thinknsync.salesync.home.bottomSlidingView.AttendanceManager;
import com.thinknsync.salesync.messageBroker.BrokerTopics;
import com.thinknsync.salesync.messageBroker.PushPullManager;
import com.thinknsync.salesync.messageBroker.PushPullManagerContainer;
import com.thinknsync.salesync.messageBroker.actionListeners.PushPullActionListenerWrapper;
import com.thinknsync.salesync.messageBroker.actionListeners.PushPullMessageListenerWrapper;
import com.thinknsync.salesync.factory.factories.AbstractFactory;
import com.thinknsync.salesync.factory.factories.AbstractFactoryImpl;
import com.thinknsync.salesync.commons.osSpecifics.broadcastReceivers.BroadcastListener;
import com.thinknsync.salesync.commons.osSpecifics.frameworkHelper.ApplicationInfo;
import com.thinknsync.salesync.commons.loginAndSessions.LoginLogoutActions;

import java.util.HashMap;

/**
 * Created by shuaib on 5/11/17.
 */

public class GetNewObject implements ObjectConstructor {
    @Override
    public void initLogger(HashMap<String, Object> objectArgs) {
        getFactory().getObject(FactoryStringConstraints.ObjectType.Logger, objectArgs);
    }

    @Override
    public Logger getLogger() {
        return (Logger) getFactory().getObject(FactoryStringConstraints.ObjectType.Logger, null);
    }

    @Override
    public PermissionManager getPermissionHandler(HashMap<String, Object> objectArgs) {
        return (PermissionManager) getFactory().getObject(FactoryStringConstraints.ObjectType.PermissionHandler, objectArgs);
    }

    @Override
    public GpsPositionManager getLocationManager(HashMap<String, Object> objectArgs) {
        return (GpsPositionManager) getFactory().getObject(FactoryStringConstraints.ObjectType.GpsPositionManager, objectArgs);
    }
//
//    @Override
//    public GpsPositionManager getSrTrackingService(HashMap<String, Object> objectArgs) {
//        return (GpsPositionManager) getFactory().getObject(FactoryStringConstraints.ObjectType
//                .SrTrackingService, getObjectConstructorIncludedMap(objectArgs));
//    }
//
//    @Override
//    public SyncManager getSyncManager(HashMap<String, Object> objectArgs) {
//        return (SyncManager) getFactory().getObject(FactoryStringConstraints.ObjectType.SyncManager,
//                getObjectConstructorIncludedMap(objectArgs));
//    }
//
//    @Override
//    public DateTimeComparator getHandsetDateTimeManager(HashMap<String, Object> argsHashmap) {
//        return (DateTimeComparator)getFactory().getObject(
//                FactoryStringConstraints.ObjectType.HandsetDateTimeManager,
//                getObjectConstructorIncludedMap(argsHashmap));
//    }
    @Override
    public DateTimePicker getAndroidDateTimePicker(HashMap<String, Object> argsHashmap) {
        return (DateTimePicker)getFactory().getObject(FactoryStringConstraints.ObjectType.AndroidDateTimePicker, argsHashmap);
    }
//
//    @Override
//    public NotifyUser getAndroidNotification(HashMap<String, Object> argsHashmap) {
//        return (NotifyUser) getFactory().getObject(FactoryStringConstraints.ObjectType.AndroidNotification,
//                getObjectConstructorIncludedMap(argsHashmap));
//    }

    @Override
    public NetworkStatusReporter getNetworkStatusReport(HashMap<String, Object> argsHashmap) {
        return (NetworkStatusReporter) getFactory().getObject(FactoryStringConstraints.ObjectType.NetworkStatusReporter,
                getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public InputValidation getInputValidator() {
        return (InputValidation) getFactory().getObject(FactoryStringConstraints.ObjectType.InputValidator, null);
    }

    @Override
    public ApplicationInfo getAndroidFrameworkHelper(HashMap<String, Object> argsHashmap) {
        return (ApplicationInfo) getFactory().getObject(FactoryStringConstraints.ObjectType.FrameworkHelper, argsHashmap);
    }

    @Override
    public BroadcastListenerContainer getBroadcastListenerContainer() {
        return (BroadcastListenerContainer) getFactory().getObject(FactoryStringConstraints.ObjectType.BroadcastListenerContainer, null);
    }

    @Override
    public BroadcastListener getInternetBroadcastListener() {
        return (BroadcastListener) getFactory().getObject(FactoryStringConstraints.ObjectType.InternetBroadcastListener,
                getObjectConstructorIncludedMap(new HashMap()));
    }

    @Override
    public BroadcastListener getScreenOnOffBroadcastListener() {
        return (BroadcastListener) getFactory().getObject(FactoryStringConstraints.ObjectType.ScreenOnOffBroadcastListener, null);
    }

    @Override
    public PushPullManager getMqttBrokerClient(HashMap<String, Object> argsHashmap) {
        return (PushPullManager) getFactory().getObject(FactoryStringConstraints.ObjectType.MqttBrokerClient,
                getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public BrokerTopics getBrokerTopics(HashMap<String, Object> argsHashmap) {
        return (BrokerTopics) getFactory().getObject(FactoryStringConstraints.ObjectType.MessageBrokerTopics, argsHashmap);
    }

    @Override
    public BrokerTopics getBrokerTopics() {
        return (BrokerTopics) getFactory().getObject(FactoryStringConstraints.ObjectType.MessageBrokerTopics, null);
    }

    @Override
    public PushPullManagerContainer getMasterPushPullContainer() {
        return (PushPullManagerContainer) getFactory().getObject(FactoryStringConstraints.ObjectType.PushPullClientContainer, null);
    }

    @Override
    public AttendanceManager getAttendanceManager(HashMap<String, Object> argsHashmap) {
        return (AttendanceManager) getFactory().getObject(FactoryStringConstraints.ObjectType.AttendanceManager,
                getObjectConstructorIncludedMap(argsHashmap));

    }

    @Override
    public LoginLogoutActions.Initialization getLoginEssentialActionManager(HashMap<String, Object> argsHashmap) {
        return (LoginLogoutActions.Initialization) getFactory().getObject(FactoryStringConstraints.ObjectType.LoginActionManager, argsHashmap);
    }

    @Override
    public LoginLogoutActions.Termination getLogoutEssentialActionManager(HashMap<String, Object> argsHashmap) {
        return (LoginLogoutActions.Termination) getFactory().getObject(FactoryStringConstraints.ObjectType.LogoutActionManager, argsHashmap);
    }

//    @Override
//    public MapCenterCoordinateFetcher getMapCenterCoordinateFetcher(HashMap<String, Object> objectArgs) {
//        return (MapCenterCoordinateFetcher) getFactory().getObject(FactoryStringConstraints.ObjectType.MapCenterCoordinateFetcher,
//                getObjectConstructorIncludedMap(objectArgs));
//    }
//
    @Override
    public MapsManager getGoogleMapsManager(HashMap<String, Object> objectArgs) {
        return (MapsManager) getFactory().getObject(FactoryStringConstraints.ObjectType.GoogleMapsManager, getObjectConstructorIncludedMap(objectArgs));
    }

    @Override
    public Animations getAnimationImpl(HashMap<String, Object> objectArgs) {
        return (Animations) getFactory().getObject(FactoryStringConstraints.ObjectType.AnimationImpl, getObjectConstructorIncludedMap(objectArgs));
    }

    @Override
    public AndroidContextWrapper getAndroidContextWrapper(HashMap<String, Object> argsHashmap) {
        return (AndroidContextWrapper)getFrameworkWrapperFactory().getObject(
                FactoryStringConstraints.FrameworkWrapperObjectType.ContextWrapper, argsHashmap);
    }

    @Override
    public BundleWrapper getBundleWrapper(HashMap<String, Object> argsHashmap) {
        return (BundleWrapper)getFrameworkWrapperFactory().getObject(
                FactoryStringConstraints.FrameworkWrapperObjectType.BundleWrapper, argsHashmap);
    }

    @Override
    public BitmapWrapper getBitmapWrapper(HashMap<String, Object> argsHashmap) {
        return (BitmapWrapper)getFrameworkWrapperFactory().getObject(
                FactoryStringConstraints.FrameworkWrapperObjectType.BitmapWrapper, argsHashmap);
    }

    @Override
    public AndroidViewWrapper getViewWrapper(HashMap<String, Object> argsHashmap) {
        return (AndroidViewWrapper)getFrameworkWrapperFactory().getObject(
                FactoryStringConstraints.FrameworkWrapperObjectType.ViewWrapper, argsHashmap);
    }

    @Override
    public PushPullActionListenerWrapper getMqttActionListenerWrapper(HashMap<String, Object> argsHashmap) {
        return (PushPullActionListenerWrapper)getFrameworkWrapperFactory().getObject(
                FactoryStringConstraints.FrameworkWrapperObjectType.MqttActionListenerWrapper, argsHashmap);
    }

    @Override
    public PushPullMessageListenerWrapper getMqttJsonMessageListenerWrapper(HashMap<String, Object> argsHashmap) {
        return (PushPullMessageListenerWrapper)getFrameworkWrapperFactory().getObject(
                FactoryStringConstraints.FrameworkWrapperObjectType.MqttMessageListenerWrapper, argsHashmap);
    }

    @Override
    public ProgressDialogWrapper getProgressDialogWrapper(HashMap<String, Object> argsHashmap) {
        return (ProgressDialogWrapper) getFrameworkWrapperFactory().getObject(
                FactoryStringConstraints.FrameworkWrapperObjectType.ProgressDialogWrapper, argsHashmap);
    }

    @Override
    public ProgressDialogWrapper getNotifiableProgressDialogWrapper(HashMap<String, Object> argsHashmap) {
        return (ProgressDialogWrapper) getFrameworkWrapperFactory().getObject(
                FactoryStringConstraints.FrameworkWrapperObjectType.NotifiableProgressDialog, argsHashmap);
    }

    @Override
    public DialogWrapper getAlertDialogWrapper(HashMap<String, Object> argsHashmap) {
        return (DialogWrapper) getFrameworkWrapperFactory().getObject(
                FactoryStringConstraints.FrameworkWrapperObjectType.AlertDialogWrapper, argsHashmap);
    }


    @Override
    public DialogInterface<Void> getBasicMessageDialog(HashMap<String, Object> argsHashmap) {
        return getDialogFactory().getObject(FactoryStringConstraints.DialogObjectType.BasicNotificationDialog, argsHashmap) ;
    }

    @Override
    public DialogInterface<OutletInterface> getOutletInfoDialog(HashMap<String, Object> argsHashmap) {
        return getDialogFactory().getObject(FactoryStringConstraints.DialogObjectType.OutletInfoDialog, argsHashmap) ;
    }

    @Override
    public DialogInterface<Object> getClickNotificationDialog(HashMap<String, Object> argsHashmap) {
        return getDialogFactory().getObject(FactoryStringConstraints.DialogObjectType.ClickNotificationDialog, argsHashmap) ;
    }

    @Override
    public DialogInterface<Boolean> getBooleanDialog(HashMap<String, Object> argsHashmap) {
        return getDialogFactory().getObject(FactoryStringConstraints.DialogObjectType.BooleanYesNoDialog, argsHashmap) ;
    }


    @Override
    public ApiRequest getLoginApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.LoginApiCallObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getTempPinGenerationApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.TempPinGenerateApiCallObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getTempPinVerificationApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.TempPinValidateApiCallObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getPinResetApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.PinResetApiCallObject, getObjectConstructorIncludedMap(argsHashmap));
    }
//
//    @Override
//    public ApiRequest getUpdateApiObject(HashMap<String, Object> argsHashmap) {
//        return getApiFactory().getObject(
//                FactoryStringConstraints.ApiCallObjectType.UpdateApiCallObject, getObjectConstructorIncludedMap(argsHashmap));
//    }
//
    @Override
    public ApiRequest getTodaysRouteApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.TodaysRouteApiCallObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getOtherRoutesApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.OtherRoutesApiCallObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getSkuApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.SkuApiCallObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getExceptionReasonsApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.ExceptionReasonsApiCallObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getOutletMasterDataApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.OutletMasterDataApiObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getPaymentMethodsApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.PaymentMethodsApiObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getSkuStockApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.SkuStockApiObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getPromotionDataApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.PromotionDataApiObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getTargetVsAchievementApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.TargetVsAchievementApiCallObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getSrRunRateApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.RunRateApiObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getHasAttendanceTodayApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.AttendanceCheckApiObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest submitAttendanceApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.SubmitAttendanceApiObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest punchOutApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.PunchOutApiObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getOutletImageFetchApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.OutletImageApiObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getProductImageFetchApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.ProductParentImageApiObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getSkuImageFetchApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.SkuImageApiObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getOutletCreateApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.OutletCreateApiObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getOutletUpdateApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.OutletUpdateApiObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getOrderSendApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.SendOrderApiCallObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getBulkOrderSendApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.SendBulkOrderApiCallObject, getObjectConstructorIncludedMap(argsHashmap));
    }

    @Override
    public ApiRequest getExceptionSendApiObject(HashMap<String, Object> argsHashmap) {
        return getApiFactory().getObject(
                FactoryStringConstraints.ApiCallObjectType.SendExceptionApiCallObject, getObjectConstructorIncludedMap(argsHashmap));
    }
//
//    @Override
//    public ApiRequest getProductApiObject(HashMap<String, Object> argsHashmap) {
//        return getApiFactory().getObject(
//                FactoryStringConstraints.ApiCallObjectType.ProductApiCallObject, getObjectConstructorIncludedMap(argsHashmap));
//    }
//
//
//
//    @Override
//    public ApiRequest getCmInfoApiObject(HashMap<String, Object> argsHashmap) {
//        return getApiFactory().getObject(
//                FactoryStringConstraints.ApiCallObjectType.CmInfoApiCallObject, getObjectConstructorIncludedMap(argsHashmap));
//    }
//
//    @Override
//    public ApiRequest getChangePasswordApiObject(HashMap<String, Object> argsHashmap) {
//        return getApiFactory().getObject(
//                FactoryStringConstraints.ApiCallObjectType.ChangePasswordApiCallObject, getObjectConstructorIncludedMap(argsHashmap));
//    }
//
//    @Override
//    public ApiRequest getDayWiseSalesReportApiObject(HashMap<String, Object> argsHashmap) {
//        return getApiFactory().getObject(
//                FactoryStringConstraints.ApiCallObjectType.DayWiseSalesReport, getObjectConstructorIncludedMap(argsHashmap));
//    }
//
//    @Override
//    public ApiRequest getRouteWiseSalesReportApiObject(HashMap<String, Object> argsHashmap) {
//        return getApiFactory().getObject(
//                FactoryStringConstraints.ApiCallObjectType.RouteWiseSalesReport, getObjectConstructorIncludedMap(argsHashmap));
//    }
//
//    @Override
//    public ApiRequest getTargetVsAchievementReportApiObject(HashMap<String, Object> argsHashmap) {
//        return getApiFactory().getObject(
//                FactoryStringConstraints.ApiCallObjectType.TargetVsAchievementReport, getObjectConstructorIncludedMap(argsHashmap));
//    }
//
//
//    @Override
//    public ApiRequest getDssReportApiObject(HashMap<String, Object> argsHashmap) {
//        return getApiFactory().getObject(
//                FactoryStringConstraints.ApiCallObjectType.DssReport, getObjectConstructorIncludedMap(argsHashmap));
//    }
//
//    @Override
//    public ApiRequest getSendOrderApiObject(HashMap<String, Object> argsHashmap) {
//        return getApiFactory().getObject(
//                FactoryStringConstraints.ApiCallObjectType.SendOrderApiCallObject, getObjectConstructorIncludedMap(argsHashmap));
//    }
//
//    @Override
//    public ApiRequest getSendExceptionApiObject(HashMap<String, Object> argsHashmap) {
//        return getApiFactory().getObject(
//                FactoryStringConstraints.ApiCallObjectType.SendExceptionApiCallObject, getObjectConstructorIncludedMap(argsHashmap));
//    }
//
//    @Override
//    public ApiRequest getSendNewOutletApiObject(HashMap<String, Object> argsHashmap) {
//        return getApiFactory().getObject(
//                FactoryStringConstraints.ApiCallObjectType.NewOutletApiObject, getObjectConstructorIncludedMap(argsHashmap));
//    }
//
//    @Override
//    public ApiRequest getSendVerifiedOutletApiObject(HashMap<String, Object> argsHashmap) {
//        return getApiFactory().getObject(
//                FactoryStringConstraints.ApiCallObjectType.VerifyOutletApiObject, getObjectConstructorIncludedMap(argsHashmap));
//    }
//
//    @Override
//    public ApiRequest getServerDateTime(HashMap<String, Object> argsHashmap) {
//        return getApiFactory().getObject(
//                FactoryStringConstraints.ApiCallObjectType.ServerDateTimeApiObject, getObjectConstructorIncludedMap(argsHashmap));
//    }
//
//    @Override
//    public ApiRequest getSrLocationSendApiObject(HashMap<String, Object> argsHashmap) {
//        return getApiFactory().getObject(
//                FactoryStringConstraints.ApiCallObjectType.SrLocationSendApiObject, getObjectConstructorIncludedMap(argsHashmap));
//    }
//
//    @Override
//    public ApiRequest getConfigurationApiObject(HashMap<String, Object> argsHashmap) {
//        return getApiFactory().getObject(
//                FactoryStringConstraints.ApiCallObjectType.ConfigurationApiObject, getObjectConstructorIncludedMap(argsHashmap));
//    }



    @Override
    public UserRepositoryInterface getUserRepository() {
        return (UserRepositoryInterface) getRepositoryFactory().getObject(
                FactoryStringConstraints.RepositoryObjectType.UserRepository, null);
    }
//
    @Override
    public RouteRepositoryInterface getRouteRepository() {
        return (RouteRepositoryInterface) getRepositoryFactory().getObject(
                FactoryStringConstraints.RepositoryObjectType.RouteRepository, null);
    }

    @Override
    public OutletRepositoryInterface getOutletRepository() {
        return (OutletRepositoryInterface) getRepositoryFactory().getObject(
                FactoryStringConstraints.RepositoryObjectType.OutletRepository, null);
    }

    @Override
    public ConfigRepositoryInterface getConfigRepository(){
        return (ConfigRepositoryInterface) getRepositoryFactory().getObject(
                FactoryStringConstraints.RepositoryObjectType.ConfigRepository, null);
    }

    @Override
    public SkuRepositoryInterface getSkuRepository() {
        return (SkuRepositoryInterface) getRepositoryFactory().getObject(
                FactoryStringConstraints.RepositoryObjectType.SkuRepository, null);
    }

    @Override
    public PromotionRepositoryInterface getPromotionRepository() {
        return (PromotionRepositoryInterface) getRepositoryFactory().getObject(
                FactoryStringConstraints.RepositoryObjectType.PromotionRepository, null);
    }

    @Override
    public MasterDataRepositoryInterface getMasterDataRepository() {
        return (MasterDataRepositoryInterface) getRepositoryFactory().getObject(
                FactoryStringConstraints.RepositoryObjectType.MasterDataRepository, null);
    }

    @Override
    public OrderRepositoryInterface getSalesOrderRepository() {
        return (OrderRepositoryInterface) getRepositoryFactory().getObject(
                FactoryStringConstraints.RepositoryObjectType.OrderRepository, null);
    }
//
//    @Override
//    public OrderRepositoryInterface getOrderRepository() {
//        return (OrderRepositoryInterface) getRepositoryFactory().getObject(
//                FactoryStringConstraints.RepositoryObjectType.OrderRepository, null);
//    }
//



    private AbstractFactory<Object> getFactory() {
        return new AbstractFactoryImpl();
    }

    private AbstractFactory<ApiRequest> getApiFactory() {
        return (AbstractFactory<ApiRequest>) getFactory().getObject(FactoryStringConstraints.FactoryObjectType.ApiCallFactory, null);
    }

    private AbstractFactory<DialogInterface> getDialogFactory() {
        return (AbstractFactory<DialogInterface>) getFactory().getObject(FactoryStringConstraints.FactoryObjectType.DialogFactory, null);
    }

    private AbstractFactory<RepositoryFactory> getRepositoryFactory() {
        return (AbstractFactory) getFactory().getObject(FactoryStringConstraints.ObjectType.RepositoryFactory, null);
    }

    private AbstractFactory<FrameworkWrapper> getFrameworkWrapperFactory() {
        return (AbstractFactory<FrameworkWrapper>) getFactory().getObject(FactoryStringConstraints.FactoryObjectType.FrameworkWrapperFactory, null);
    }

    private HashMap getObjectConstructorIncludedMap(HashMap argsMap){
        argsMap.put(ObjectConstructor.tag, GetNewObject.this);
        return argsMap;
    }
}
