package com.thinknsync.salesync.factory.factories;

import android.content.Context;

import com.thinknsync.dialogs.BooleanDialog;
import com.thinknsync.dialogs.DialogInterface;
import com.thinknsync.dialogs.MessageWithDismiss;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.uiElements.dialogs.ClickNotifierNullDialog;
import com.thinknsync.salesync.factory.FactoryStringConstraints;
import com.thinknsync.salesync.uiElements.dialogs.OutletClickDialog;

import java.util.Map;

/**
 * Created by shuaib on 5/13/17.
 */

public class DialogFactory implements AbstractFactory<DialogInterface> {
    @Override
    public DialogInterface getObject(String objectType, Map objectArgs) {
        switch (objectType) {
            case FactoryStringConstraints.DialogObjectType.BasicNotificationDialog:
                return new MessageWithDismiss((Context)objectArgs.get(AndroidContextWrapper.tag));
            case FactoryStringConstraints.DialogObjectType.OutletInfoDialog:
                return new OutletClickDialog((Context)objectArgs.get(AndroidContextWrapper.tag),
                        (OutletInterface)objectArgs.get(OutletInterface.tag));
            case FactoryStringConstraints.DialogObjectType.ClickNotificationDialog:
                return new ClickNotifierNullDialog((Context)objectArgs.get(AndroidContextWrapper.tag));
            case FactoryStringConstraints.DialogObjectType.BooleanYesNoDialog:
                return new BooleanDialog((Context)objectArgs.get(AndroidContextWrapper.tag));
            default:
                return null;
        }
    }
}
