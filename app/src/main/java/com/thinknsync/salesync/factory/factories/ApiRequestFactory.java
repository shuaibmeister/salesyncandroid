package com.thinknsync.salesync.factory.factories;

import androidx.annotation.NonNull;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiRequestImpl;
import com.thinknsync.apilib.ApiRequestImplOdoo;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.apilib.RequestMethods;
import com.thinknsync.mapslib.Utils;
import com.thinknsync.mapslib.apiCall.GoogelGeoCodeWaypointApiRequestImpl;
import com.thinknsync.mapslib.mapsDrawer.PathDataFetcher;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.positionmanager.TrackingData;
import com.thinknsync.salesync.api.ApiUrls;
import com.thinknsync.salesync.factory.FactoryStringConstraints;

import java.util.Map;

/**
 * Created by shuaib on 5/13/17.
 */

public class ApiRequestFactory implements AbstractFactory<ApiRequest> {
    @Override
    public ApiRequest getObject(String objectType, Map objectArgs) {
        switch (objectType) {
            case FactoryStringConstraints.ApiCallObjectType.LoginApiCallObject:
                return getApiRequest(objectArgs, ApiUrls.LOGIN_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.TempPinGenerateApiCallObject:
                return getApiRequest(objectArgs, ApiUrls.TEMP_PIN_GENERATION_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.TempPinValidateApiCallObject:
                return getApiRequest(objectArgs, ApiUrls.TEMP_PIN_VALIDATE_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.PinResetApiCallObject:
                return getApiRequest(objectArgs, ApiUrls.PIN_RESET_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.TodaysRouteApiCallObject:
                return getApiRequest(objectArgs, ApiUrls.TODAYS_ROUTE_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.OtherRoutesApiCallObject:
                return getApiRequest(objectArgs, ApiUrls.OTHER_ROUTES_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.SkuApiCallObject:
                return getApiRequest(objectArgs, ApiUrls.SKU_FETCH_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.ExceptionReasonsApiCallObject:
                return getApiRequest(objectArgs, ApiUrls.EXCEPTION_REASONS_FETCH_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.OutletMasterDataApiObject:
                return getApiRequest(objectArgs, ApiUrls.OUTLET_MASTER_DATA_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.PaymentMethodsApiObject:
                return getApiRequest(objectArgs, ApiUrls.PAYMENT_METHODS_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.SkuStockApiObject:
                return getApiRequest(objectArgs, ApiUrls.SKU_STOCK_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.PromotionDataApiObject:
                return getApiRequest(objectArgs, ApiUrls.PROMOTION_DATA_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.TargetVsAchievementApiCallObject:
                return getApiRequest(objectArgs, ApiUrls.TARGET_VS_ACHIEVEMENT_FETCH_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.RunRateApiObject:
                return getApiRequest(objectArgs, ApiUrls.RUN_RATE_DARA_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.AttendanceCheckApiObject:
                return getApiRequest(objectArgs, ApiUrls.ATTENDANCE_CHECK_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.SubmitAttendanceApiObject:
                return getApiRequest(objectArgs, ApiUrls.ATTENDANCE_SUBMIT_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.PunchOutApiObject:
                return getApiRequest(objectArgs, ApiUrls.ATTENDANCE_OUT_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.OutletImageApiObject:
                return getApiRequest(objectArgs, ApiUrls.GET_OUTLET_IMAGES_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.ProductParentImageApiObject:
                return getApiRequest(objectArgs, ApiUrls.GET_PRODUCT_IMAGES_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.SkuImageApiObject:
                return getApiRequest(objectArgs, ApiUrls.GET_SKU_IMAGES_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.OutletCreateApiObject:
                return getApiRequest(objectArgs, ApiUrls.NEW_OUTLET_CREATE_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.OutletUpdateApiObject:
                return getApiRequest(objectArgs, ApiUrls.OUTLET_VERIFY_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.SendOrderApiCallObject:
                return getApiRequest(objectArgs, ApiUrls.SEND_ORDER_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.SendBulkOrderApiCallObject:
                return getApiRequest(objectArgs, ApiUrls.SEND_ORDER_BULK_URL, RequestMethods.METHOD_POST);
            case FactoryStringConstraints.ApiCallObjectType.SendExceptionApiCallObject:
                return getApiRequest(objectArgs, ApiUrls.SEND_EXCEPTION_URL, RequestMethods.METHOD_POST);

            default:
                return null;
        }
    }

    @NonNull
    private ApiRequestImpl getApiRequest(Map objectArgs, String url, int requestMethod) {
        return new ApiRequestImplOdoo(
                (AndroidContextWrapper) objectArgs.get(AndroidContextWrapper.tag),
                url,
                (ApiDataObject) objectArgs.get(ApiDataObject.tag),
                requestMethod,
                (ApiResponseActions) objectArgs.get(ApiResponseActions.tag),
                (int) objectArgs.get(ApiRequest.ApiFields.REQ_ID));
    }
}
