package com.thinknsync.salesync.factory.factories;

import android.content.Context;

import com.thinknsync.androidpermissions.PermissionHandler;
import com.thinknsync.inputvalidation.ValidateField;
import com.thinknsync.logger.Logger;
import com.thinknsync.mapslib.MapsOperation;
import com.thinknsync.mapslib.mapWorks.GoogleMapsManager;
import com.thinknsync.networkutils.NetworkUtil;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.positionmanager.LocationTracker;
import com.thinknsync.salesync.base.ApplicationBase;
import com.thinknsync.salesync.commons.osSpecifics.broadcastReceivers.ScreenOnOffBroadcastReceiver;
import com.thinknsync.salesync.commons.utils.animation.AnimationHelper;
import com.thinknsync.salesync.commons.utils.dateTime.AndroidDateTimePicker;
import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.factory.FactoryStringConstraints;
import com.thinknsync.salesync.factory.GetNewObject;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.home.bottomSlidingView.AttendanceHelper;
import com.thinknsync.salesync.messageBroker.MessageBrokerClient;
import com.thinknsync.salesync.messageBroker.Topics;
import com.thinknsync.salesync.commons.osSpecifics.broadcastReceivers.InternetBroadcastReceiver;
import com.thinknsync.salesync.commons.osSpecifics.frameworkHelper.AndroidHelper;
import com.thinknsync.salesync.commons.loginAndSessions.LoginActions;
import com.thinknsync.salesync.commons.loginAndSessions.LogoutActions;

import java.util.Map;

/**
 * Created by shuaib on 5/10/17.
 */

public class AbstractFactoryImpl implements AbstractFactory<Object> {
    @Override
    public Object getObject(String objectType, Map objectArgs) {
        switch (objectType) {
            case FactoryStringConstraints.ObjectType.Logger:
                if (objectArgs == null) {
                    return Logger.getInstance();
                } else {
                    return Logger.getInstance((boolean) objectArgs.get(Logger.tag));
                }
            case FactoryStringConstraints.ObjectType.ObjectConstructor:
                return new GetNewObject();
            case FactoryStringConstraints.FactoryObjectType.ApiCallFactory:
                return new ApiRequestFactory();
            case FactoryStringConstraints.FactoryObjectType.DialogFactory:
                return new DialogFactory();
            case FactoryStringConstraints.FactoryObjectType.FrameworkWrapperFactory:
                return new FrameWorkWrapperFactory();
            case FactoryStringConstraints.ObjectType.PermissionHandler:
                return new PermissionHandler((AndroidContextWrapper) objectArgs.get(AndroidContextWrapper.tag));
            case FactoryStringConstraints.ObjectType.NetworkStatusReporter:
                return new NetworkUtil((AndroidContextWrapper) objectArgs.get(AndroidContextWrapper.tag));
            case FactoryStringConstraints.ObjectType.InternetBroadcastListener:
                return new InternetBroadcastReceiver((ObjectConstructor)objectArgs.get(ObjectConstructor.tag));
            case FactoryStringConstraints.ObjectType.ScreenOnOffBroadcastListener:
                return new ScreenOnOffBroadcastReceiver();
            case FactoryStringConstraints.ObjectType.MqttBrokerClient:
                return MessageBrokerClient.getClient((ObjectConstructor)objectArgs.get(ObjectConstructor.tag),
                        (AndroidContextWrapper) objectArgs.get(AndroidContextWrapper.tag));
            case FactoryStringConstraints.ObjectType.PushPullClientContainer:
            case FactoryStringConstraints.ObjectType.BroadcastListenerContainer:
                return ApplicationBase.getInstance();
            case FactoryStringConstraints.ObjectType.MessageBrokerTopics:
                if(objectArgs != null) {
                    return Topics.getInstance((UserInterface) objectArgs.get(UserInterface.tag));
                } else {
                    return Topics.getInstance(null);
                }
            case FactoryStringConstraints.ObjectType.InputValidator:
                return new ValidateField();
            case FactoryStringConstraints.ObjectType.RepositoryFactory:
                return new RepositoryFactory();
            case FactoryStringConstraints.ObjectType.FrameworkHelper:
                return new AndroidHelper((AndroidContextWrapper) objectArgs.get(AndroidContextWrapper.tag));
            case FactoryStringConstraints.ObjectType.LoginActionManager:
                return new LoginActions((ObjectConstructor)objectArgs.get(ObjectConstructor.tag),
                        (AndroidContextWrapper) objectArgs.get(AndroidContextWrapper.tag));
            case FactoryStringConstraints.ObjectType.LogoutActionManager:
                return new LogoutActions();
            case FactoryStringConstraints.ObjectType.GoogleMapsManager:
                return new GoogleMapsManager((MapsOperation) objectArgs.get(MapsOperation.tag), (AndroidContextWrapper)objectArgs.get(AndroidContextWrapper.tag));
            case FactoryStringConstraints.ObjectType.GpsPositionManager:
                return LocationTracker.getInstance((AndroidContextWrapper)objectArgs.get(AndroidContextWrapper.tag));
            case FactoryStringConstraints.ObjectType.AndroidDateTimePicker:
                return new AndroidDateTimePicker((Context) objectArgs.get(AndroidContextWrapper.tag));
            case FactoryStringConstraints.ObjectType.AttendanceManager:
                return new AttendanceHelper((ObjectConstructor) objectArgs.get(ObjectConstructor.tag),
                        (AndroidContextWrapper)objectArgs.get(AndroidContextWrapper.tag));
            case FactoryStringConstraints.ObjectType.AnimationImpl:
                return new AnimationHelper((Context) objectArgs.get(AndroidContextWrapper.tag));

            default:
                return null;
        }
    }
}
