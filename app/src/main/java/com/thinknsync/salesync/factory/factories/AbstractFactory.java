package com.thinknsync.salesync.factory.factories;

import java.util.Map;

/**
 * Created by shuaib on 5/10/17.
 */

public interface AbstractFactory<T extends Object> {
    T getObject(String objectType, Map objectArgs);
}
