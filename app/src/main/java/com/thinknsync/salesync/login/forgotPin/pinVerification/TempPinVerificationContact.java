package com.thinknsync.salesync.login.forgotPin.pinVerification;

import com.thinknsync.apilib.ApiResponseView;
import com.thinknsync.salesync.base.BaseController;
import com.thinknsync.salesync.base.BaseView;
import com.thinknsync.salesync.base.viewInterfaces.ViewInitialization;
import com.thinknsync.salesync.base.viewInterfaces.InteractiveView;

public interface TempPinVerificationContact {
    interface View extends BaseView, ApiResponseView, InteractiveView, ViewInitialization.InitValues {
        String tag = "TempPinVerificationView";

        void updateOtpWaitingTimer(String remainingTime);
        void updateVerifyingDot(Long timeElapsed);
    }

    interface Controller extends BaseController<View> {
        void sendTempLoginRequestToServer(String userName, String tempPass, String deviceIdentifier);
        void startOtpResendTimer();
        void startVerifyingDotUpdateTimer(int dotCountLoop);
        void stopVerifyingDotTimer();
        void sendRequestForOtpCode();
    }
}
