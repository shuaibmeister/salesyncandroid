package com.thinknsync.salesync.login.pinLogin;

import com.thinknsync.salesync.base.BaseController;
import com.thinknsync.salesync.login.login.LoginContract;

public interface PinLoginContact {
    interface Controller extends BaseController<LoginContract.View> {
        void loginWithPin(String pin, String deviceId);
    }
}
