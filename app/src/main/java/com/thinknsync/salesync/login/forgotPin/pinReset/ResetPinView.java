package com.thinknsync.salesync.login.forgotPin.pinReset;

import android.os.Bundle;
import android.view.View;

import com.thinknsync.inputvalidation.InputValidation;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.api.ApiCallArgumentKeys;
import com.thinknsync.salesync.login.DeviceIdentifierFetchFormActivity;
import com.thinknsync.salesync.uiElements.formElements.forms.FormContainerContract;
import com.thinknsync.salesync.uiElements.formElements.forms.FormValidationViewImplIncludingRetypedFields;
import com.thinknsync.salesync.uiElements.formElements.inputs.ActionButton;
import com.thinknsync.salesync.uiElements.formElements.inputs.ActionView;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputField;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputValidationContainer;

import java.util.ArrayList;
import java.util.HashMap;

public class ResetPinView extends DeviceIdentifierFetchFormActivity implements PinResetContact.View {

    private PinResetContact.Controller controller;

    private InputField newPin, retypedNewPin;
    private ActionButton actionButton;

    private String userName, otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewListeners();
        initValues();
    }

    @Override
    public void initValues() {
        if(getIntent().getExtras() != null){
            userName = getIntent().getExtras().getString(ApiCallArgumentKeys.LoginAndRegApiKeys.USERNAME);
            otp = getIntent().getExtras().getString(ApiCallArgumentKeys.LoginAndRegApiKeys.OTP);
        }
    }

    @Override
    protected void initForm() {
        setupForm(new HashMap
                <InputValidationContainer, InputValidation.ValidationTypes>(){{
            put(newPin, InputValidation.ValidationTypes.TYPE_PIN);
            put(retypedNewPin, InputValidation.ValidationTypes.TYPE_PIN);
        }}, actionButton);
    }

    @Override
    public void setupForm(HashMap<InputValidationContainer, InputValidation.ValidationTypes> inputAndTypeMap, ActionView actionView) {
        final InputValidationContainer[] inputValidationContainers = new InputValidationContainer[]{newPin, retypedNewPin};
        FormContainerContract.Initializer formContainer = new FormValidationViewImplIncludingRetypedFields(inputAndTypeMap,
                getObjectConstructorInstance(), actionView,
                new ArrayList<InputValidationContainer[]>(){{add(inputValidationContainers);}});
        formContainer.initForm();
    }

    @Override
    public void initView() {
        newPin = findViewById(R.id.input_box_pin);
        retypedNewPin = findViewById(R.id.input_box_pin_retype);
        actionButton = findViewById(R.id.reset_pin_button);
    }

    @Override
    public void initController() {
        controller = new ResetPinController(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_pin_reset;
    }

    @Override
    public AndroidContextWrapper getContextWrapper() {
        return (AndroidContextWrapper) getObjectConstructorInstance().getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, ResetPinView.this);
        }});
    }

    @Override
    public void initViewListeners() {
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.sendResetPinRequestToServer(userName, otp, newPin.getInput(), getDeviceIdentifierText());
            }
        });
    }

    @Override
    public void onApiCallSuccess(String... strings) {
        finish();
    }

    @Override
    public void onApiCallFailure(String... strings) {
        showShortMessage(strings[0]);
    }

    @Override
    public void onApiCallError(String... strings) {
        onApiCallFailure(strings);
    }
}
