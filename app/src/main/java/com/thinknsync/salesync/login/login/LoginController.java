package com.thinknsync.salesync.login.login;


import com.thinknsync.androidpermissions.PermissionManager;
import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiDataObjectImpl;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.apilib.ErrorWrapper;
import com.thinknsync.convertible.Convertible;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.positionmanager.TrackingData;
import com.thinknsync.salesync.api.ApiCallArgumentKeys;
import com.thinknsync.salesync.commons.utils.DateTimeUtils;
import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.database.repositories.user.UserRepositoryInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.commons.loginAndSessions.LoginLogoutActions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginController implements LoginContract.Controller {

    protected LoginContract.View loginView;

    protected UserRepositoryInterface userRepository;
    private ObjectConstructor objectConstructor;
    private PermissionManager permissionManager;
    private TrackingData locationData;

    public LoginController(final LoginContract.View loginView){
        this.objectConstructor = loginView.getObjectConstructorInstance();
        this.userRepository = this.objectConstructor.getUserRepository();
        this.permissionManager = this.objectConstructor.getPermissionHandler(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, loginView.getContextWrapper());
        }});
        setViewReference(loginView);
    }

    @Override
    public void setViewReference(LoginContract.View view) {
        this.loginView = view;
    }

    @Override
    public void login(final String userName, final String pass, final String deviceId, final String userImage) {
        final ApiDataObject apiDataObject = new ApiDataObjectImpl(new HashMap<String, String>(){{
//            put(ApiCallArgumentKeys.LoginAndRegApiKeys.USERNAME, "ah@gm.c");
//            put(ApiCallArgumentKeys.LoginAndRegApiKeys.PASSWORD, "258520");
            put(ApiCallArgumentKeys.LoginAndRegApiKeys.DEVICE_IDENTIFIER, "34e84a3c3b658127");
            put(ApiCallArgumentKeys.LoginAndRegApiKeys.USERNAME, userName);
            put(ApiCallArgumentKeys.LoginAndRegApiKeys.PASSWORD, pass);
//            put(ApiCallArgumentKeys.LoginAndRegApiKeys.DEVICE_IDENTIFIER, deviceId);
        }});

        final ApiResponseActions responseActions = getLoginResponseAction(userImage, pass);

        ApiRequest apiRequest = objectConstructor.getLoginApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, loginView.getContextWrapper());
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, responseActions);
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});

        apiRequest.sendRequest();
    }

    @Override
    public boolean hasLocationPermission() {
        return permissionManager.showLocationPermissionMessageIfNeeded();
    }

//    @Override
//    public void obtainLastLocation() {
//        GpsPositionManager positionManager = objectConstructor.getLocationManager(new HashMap<String, Object>(){{
//            put(AndroidContextWrapper.tag, loginView.getContextWrapper());
//        }});
//
//        positionManager.getLastLocation(new Observer() {
//            @Override
//            public void update(Observable o, Object arg) {
//                locationData = (TrackingData)arg;
//                loginView.onLocationObtained();
//            }
//        });
//    }

    private ApiResponseActions getLoginResponseAction(final String userImage, final String pass){
        return new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException {
                JSONObject userJson = responseJson.getJSONObject(ApiRequest.ApiFields.KEY_DATA);
                final UserInterface user = saveLoginUser(userJson);
                executePostLoginActions(user);
            }

            private UserInterface saveLoginUser(JSONObject userJson) throws JSONException {
                UserInterface user = ((Convertible<UserInterface>)userRepository.getLoggedInUser()).fromJson(userJson);
                user.setPassword(pass);
                user.setIsLoggedIn(true);
                user.setImage(userImage);
                user.setLastLoginTime(new DateTimeUtils().getDefaultDateTimeNow());
                userRepository.save(user);
                return user;
            }

            @Override
            public void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException {
//                loginView.loginFailure();
                loginView.showShortMessage(responseJson.getString(ApiRequest.ApiFields.KEY_MESSAGE));
            }

            @Override
            public void onApiCallError(int reqId, ErrorWrapper error) {
                super.onApiCallError(reqId, error);
                loginView.showShortMessage("Server Error. Please Try Again Later");
            }
        };
    }

    protected void executePostLoginActions(final UserInterface user) {
        LoginLogoutActions.Initialization loginActionManager = objectConstructor.getLoginEssentialActionManager(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, loginView.getContextWrapper());
            put(ObjectConstructor.tag, loginView.getObjectConstructorInstance());
        }});
        loginActionManager.addToObservers(new TypedObserverImpl() {
            @Override
            public void update(Object o) {
                loginView.onApiCallSuccess();
            }
        });
        loginActionManager.onSuccessfulLogin(user);
    }
}
