package com.thinknsync.salesync.login.pinLogin;

import android.view.View;

import com.thinknsync.inputvalidation.InputValidation;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.login.login.LoginView;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputField;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputValidationContainer;

import java.util.HashMap;

public class PinLoginView extends LoginView {

//    private PinLoginContact.Controller controller;

    private InputField pin;

    @Override
    protected void initForm() {
        setupForm(new HashMap<InputValidationContainer, InputValidation.ValidationTypes>(){{
            put(pin, InputValidation.ValidationTypes.TYPE_PIN);
        }}, loginButton);
    }

    @Override
    public void initView() {
        super.initView();
        pin = findViewById(R.id.input_box_pin);
        loginButton = findViewById(R.id.enter_button);
    }

    @Override
    public void initController() {
        loginController = new PinLoginController(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_pin_login;
    }

    @Override
    public AndroidContextWrapper getContextWrapper() {
        return (AndroidContextWrapper) getObjectConstructorInstance().getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, PinLoginView.this);
        }});
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onApiCallSuccess(String... strings) {
        if(!isTaskRoot()) {
            finish();
        } else {
            super.onApiCallSuccess(strings);
        }
    }

    @Override
    public void onApiCallFailure(String... strings) {
        pin.showError(getString(R.string.pin_incorrect_text));
    }


    @Override
    public void initViewListeners() {
        super.initViewListeners();
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((PinLoginContact.Controller)loginController).loginWithPin(pin.getInput(), getDeviceIdentifierText());
            }
        });
    }
}
