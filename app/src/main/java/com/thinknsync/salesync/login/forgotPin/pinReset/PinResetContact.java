package com.thinknsync.salesync.login.forgotPin.pinReset;

import com.thinknsync.apilib.ApiResponseView;
import com.thinknsync.salesync.base.BaseController;
import com.thinknsync.salesync.base.BaseView;
import com.thinknsync.salesync.base.viewInterfaces.ViewInitialization;
import com.thinknsync.salesync.base.viewInterfaces.InteractiveView;
import com.thinknsync.salesync.uiElements.formElements.forms.FormContainerContract;

public interface PinResetContact {
    interface View extends BaseView, ApiResponseView, FormContainerContract.FormActivity, InteractiveView, ViewInitialization.InitValues {
        String tag = "PinResetView";
    }

    interface Controller extends BaseController<View> {
        void sendResetPinRequestToServer(String userName, String otp, String newPin, String deviceId);
    }
}
