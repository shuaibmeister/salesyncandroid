package com.thinknsync.salesync.login.pinLogin;

import com.thinknsync.networkutils.NetworkStatusReporter;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.commons.utils.DateTimeUtils;
import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.database.repositories.user.UserRepositoryInterface;
import com.thinknsync.salesync.login.login.LoginContract;
import com.thinknsync.salesync.login.login.LoginController;

import java.util.HashMap;

public class PinLoginController extends LoginController implements PinLoginContact.Controller{

    public PinLoginController(LoginContract.View view){
        super(view);
    }

    @Override
    public void loginWithPin(final String pin, final String deviceId) {
        final UserInterface loggedInUser = userRepository.getUser();
        if(loggedInUser.getId() != 0) {
            NetworkStatusReporter networkStatus = loginView.getObjectConstructorInstance().getNetworkStatusReport(new HashMap<String, Object>() {{
                put(AndroidContextWrapper.tag, loginView.getContextWrapper());
            }});
            if (networkStatus.isConnected(false)) {
                super.login(loggedInUser.getUserId(), pin, deviceId, loggedInUser.getImage());
            } else {
                loginLocally(loggedInUser.getUserId(), pin);
            }
        }
    }

    private void loginLocally(final String userName, final String pass){
        UserRepositoryInterface userRepository = loginView.getObjectConstructorInstance().getUserRepository();
        UserInterface validatedUser = userRepository.getUserIfValid(userName, pass);
        if(validatedUser != null){
            validatedUser.setIsLoggedIn(true);
            validatedUser.setLastLoginTime(new DateTimeUtils().getDefaultDateTimeNow());
            userRepository.save(validatedUser);
            executePostLoginActions(validatedUser);
        } else {
            loginView.onApiCallFailure();
        }
    }
}
