package com.thinknsync.salesync.login.forgotPin.pinReset;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiDataObjectImpl;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.api.ApiCallArgumentKeys;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ResetPinController implements PinResetContact.Controller {

    private PinResetContact.View view;

    public ResetPinController(PinResetContact.View view){
        setViewReference(view);
    }

    @Override
    public void setViewReference(PinResetContact.View view) {
        this.view = view;
    }

    @Override
    public void sendResetPinRequestToServer(final String userName, final String otp, final String newPin, final String deviceId) {
        final ApiDataObject apiDataObject = new ApiDataObjectImpl(new HashMap<String, String>(){{
            put(ApiCallArgumentKeys.LoginAndRegApiKeys.USERNAME, userName);
            put(ApiCallArgumentKeys.LoginAndRegApiKeys.OTP, otp);
            put(ApiCallArgumentKeys.LoginAndRegApiKeys.NEW_PIN, newPin);
            put(ApiCallArgumentKeys.LoginAndRegApiKeys.DEVICE_IDENTIFIER, deviceId);
        }});

        final ApiResponseActions responseActions = getPinResetResponseAction();

        ApiRequest apiRequest = view.getObjectConstructorInstance().getPinResetApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, view.getContextWrapper());
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, responseActions);
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});

        apiRequest.sendRequest();
    }

    private ApiResponseActions getPinResetResponseAction() {
        return new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int i, JSONObject jsonObject) throws JSONException {
                view.onApiCallSuccess();
            }

            @Override
            public void onApiCallFailure(int i, JSONObject jsonObject) throws JSONException {
                view.onApiCallFailure(jsonObject.getString(ApiRequest.ApiFields.KEY_MESSAGE));
            }
        };
    }
}
