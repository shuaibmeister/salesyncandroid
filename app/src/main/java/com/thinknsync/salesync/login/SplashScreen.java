package com.thinknsync.salesync.login;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.base.views.BaseActivity;
import com.thinknsync.salesync.base.viewInterfaces.InteractiveView;
import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.home.HomeView;
import com.thinknsync.salesync.login.login.LoginView;
import com.thinknsync.salesync.login.pinLogin.PinLoginView;
import com.thinknsync.salesync.commons.loginAndSessions.LoginLogoutActions;

import java.util.HashMap;

public class SplashScreen extends BaseActivity implements InteractiveView {

    private Button getStartedButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showFullScreen();
        goToMainFlowIfLoggedIn();
    }

    private void goToMainFlowIfLoggedIn() {
        UserInterface user = getObjectConstructorInstance().getUserRepository().getUser();
        if(user.getId() == 0){
            initViewListeners();
        } else if(!user.isLoggedIn()){
            startActivityAndClearCurrent(PinLoginView.class);
        } else {
//            final Bundle b = new Bundle();
//            b.putInt(UserInterface.key_id, user.getSrId());
//            b.putBoolean(HomeContract.key_fetch_data, false);
//            b.putString(UserInterface.key_name, user.getName());
            LoginLogoutActions.Initialization loginActionManager = getObjectConstructorInstance().getLoginEssentialActionManager(new HashMap<String, Object>(){{
                put(AndroidContextWrapper.tag, getContextWrapper());
                put(ObjectConstructor.tag, getObjectConstructorInstance());
            }});
            loginActionManager.addToObservers(new TypedObserverImpl() {
                @Override
                public void update(Object o) {
                    startActivityAndClearCurrent(HomeView.class);
                }
            });
            loginActionManager.onSuccessfulLogin(user);
        }
    }

    @Override
    public void initView() {
        getStartedButton = findViewById(R.id.get_started_btn);
    }

    @Override
    public void initController() {
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash_screen;
    }

    @Override
    public AndroidContextWrapper getContextWrapper() {
        return (AndroidContextWrapper) getObjectConstructorInstance().getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, SplashScreen.this);
        }});
    }

    @Override
    public void initViewListeners() {
        getStartedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(LoginView.class);
            }
        });
    }
}
