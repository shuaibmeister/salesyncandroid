package com.thinknsync.salesync.login.login;

import com.thinknsync.apilib.ApiResponseView;
import com.thinknsync.objectwrappers.BaseFrameworkWrapper;
import com.thinknsync.salesync.base.BaseController;
import com.thinknsync.salesync.base.BaseView;
import com.thinknsync.salesync.base.viewInterfaces.InteractiveView;
import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.uiElements.formElements.forms.FormContainerContract;

public interface LoginContract {
    interface View extends BaseView, ApiResponseView, InteractiveView, FormContainerContract.FormActivity {
        String tag = "LoginView";
    }

    interface Controller extends BaseController<View> {
        void login(String username, String pass, String deviceId, String userImage);

        boolean hasLocationPermission();
    }
}
