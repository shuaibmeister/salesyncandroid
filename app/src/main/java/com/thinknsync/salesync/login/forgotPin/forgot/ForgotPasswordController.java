package com.thinknsync.salesync.login.forgotPin.forgot;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiDataObjectImpl;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.api.ApiCallArgumentKeys;
import com.thinknsync.salesync.database.records.user.UserInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ForgotPasswordController implements ForgotPasswordContract.Controller {

    private ForgotPasswordContract.View forgotPinView;

    public ForgotPasswordController(ForgotPasswordContract.View view){
        setViewReference(view);
    }

    @Override
    public void setViewReference(ForgotPasswordContract.View view) {
        this.forgotPinView = view;
    }

    @Override
    public void sendTemporaryPinRequestToServer(final String userName, final String deviceId) {
        final ApiDataObject apiDataObject = new ApiDataObjectImpl(new HashMap<String, String>(){{
            put(ApiCallArgumentKeys.LoginAndRegApiKeys.USERNAME, userName);
            put(ApiCallArgumentKeys.LoginAndRegApiKeys.DEVICE_IDENTIFIER, deviceId);
        }});

        final ApiResponseActions responseActions = getPinGenerateResponseAction();

        ApiRequest apiRequest = forgotPinView.getObjectConstructorInstance().getTempPinGenerationApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, forgotPinView.getContextWrapper());
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, responseActions);
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});

        apiRequest.sendRequest();;
    }

    private ApiResponseActions getPinGenerateResponseAction() {
        return new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int i, JSONObject jsonObject) throws JSONException {
                JSONObject data = jsonObject.getJSONObject(ApiRequest.ApiFields.KEY_DATA);
                forgotPinView.onApiCallSuccess(data.getString(ApiCallArgumentKeys.LoginAndRegApiKeys.OTP),
                        data.getString(UserInterface.key_phone));
            }

            @Override
            public void onApiCallFailure(int i, JSONObject jsonObject) throws JSONException {
                forgotPinView.onApiCallFailure(jsonObject.getString(ApiRequest.ApiFields.KEY_MESSAGE));
            }
        };
    }
}
