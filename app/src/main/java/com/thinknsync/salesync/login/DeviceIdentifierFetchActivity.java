package com.thinknsync.salesync.login;

import android.widget.TextView;

import com.thinknsync.salesync.R;
import com.thinknsync.salesync.base.views.BaseActivity;
import com.thinknsync.salesync.commons.osSpecifics.deviceIdFetcher.DeviceIdentifierFetchImpl;
import com.thinknsync.salesync.commons.osSpecifics.deviceIdFetcher.DeviceIdentifierFetchManager;

public abstract class DeviceIdentifierFetchActivity extends BaseActivity {

    private TextView deviceIdentifier;
    private DeviceIdentifierFetchManager deviceIdentifierManager;

    @Override
    protected void onResume() {
        super.onResume();
        deviceIdentifier = findViewById(R.id.imei_text);
        deviceIdentifierManager = new DeviceIdentifierFetchImpl(getObjectConstructorInstance(), getContextWrapper());
        setupDeviceIdentifier();
        showFullScreen();
    }

    private void setupDeviceIdentifier() {
        deviceIdentifierManager.setupDeviceIdentifier();
        deviceIdentifier.setText(String.format(getString(R.string.imei_text), deviceIdentifierManager.getDeviceIdentifierText()));
    }

    protected String getDeviceIdentifierText(){
        return deviceIdentifierManager.getDeviceIdentifierText();
    }

}
