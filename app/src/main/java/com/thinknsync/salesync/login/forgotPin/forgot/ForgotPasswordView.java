package com.thinknsync.salesync.login.forgotPin.forgot;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.thinknsync.inputvalidation.InputValidation;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.BundleWrapper;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.api.ApiCallArgumentKeys;
import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.login.DeviceIdentifierFetchFormActivity;
import com.thinknsync.salesync.login.forgotPin.pinVerification.TempPinVerificationView;
import com.thinknsync.salesync.uiElements.formElements.inputs.ActionButton;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputField;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputValidationContainer;

import java.util.HashMap;

public class ForgotPasswordView extends DeviceIdentifierFetchFormActivity implements ForgotPasswordContract.View  {

    private ForgotPasswordContract.Controller controller;

    private InputField userId;
    private TextView backToLogin;
    private ActionButton requestTempPin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewListeners();
    }

    @Override
    public void initView() {
        userId = findViewById(R.id.input_box_user_id);
        backToLogin = findViewById(R.id.back_to_login);
        requestTempPin = findViewById(R.id.request_temp_pin_button);
    }

    @Override
    public void initController() {
        controller = new ForgotPasswordController(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_forgot_pin;
    }

    @Override
    public AndroidContextWrapper getContextWrapper() {
        return (AndroidContextWrapper) getObjectConstructorInstance().getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, ForgotPasswordView.this);
        }});
    }

    @Override
    public void initForm() {
        setupForm(new HashMap<InputValidationContainer, InputValidation.ValidationTypes>(){{
            put(userId, InputValidation.ValidationTypes.TYPE_MANDATORY);
        }}, requestTempPin);
    }


    @Override
    public void initViewListeners() {
        requestTempPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.sendTemporaryPinRequestToServer(userId.getInput(), getDeviceIdentifierText());
//                onTempPinGenerationSuccess();
            }
        });

        backToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onApiCallSuccess(String... strings) {
        final Bundle b = new Bundle();
        b.putString(ApiCallArgumentKeys.LoginAndRegApiKeys.USERNAME, userId.getInput());
        b.putString(ApiCallArgumentKeys.LoginAndRegApiKeys.OTP, strings[0]);
        b.putString(UserInterface.key_phone, strings[1]);
        startActivityAndClearCurrent(TempPinVerificationView.class, (BundleWrapper) getObjectConstructorInstance().getBundleWrapper(new HashMap<String, Object>(){{
            put(BundleWrapper.tag, b);
        }}));
    }

    @Override
    public void onApiCallFailure(String... strings) {
        userId.showError(getString(R.string.user_id_incorrect_text));
    }

    @Override
    public void onApiCallError(String... strings) {
        onApiCallFailure(strings);
    }
}
