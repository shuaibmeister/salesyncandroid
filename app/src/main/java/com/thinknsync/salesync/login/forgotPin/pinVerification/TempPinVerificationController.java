package com.thinknsync.salesync.login.forgotPin.pinVerification;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiDataObjectImpl;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.salesync.api.ApiCallArgumentKeys;
import com.thinknsync.salesync.commons.utils.timer.RunTimer;
import com.thinknsync.salesync.commons.utils.timer.TimerImpl;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class TempPinVerificationController implements TempPinVerificationContact.Controller {

    private TempPinVerificationContact.View view;
    private RunTimer timer;

    public TempPinVerificationController(TempPinVerificationContact.View view){
        setViewReference(view);
    }

    @Override
    public void setViewReference(TempPinVerificationContact.View view) {
        this.view = view;
    }

    @Override
    public void sendTempLoginRequestToServer(final String userName, final String tempPass, final String deviceIdentifier) {
        final ApiDataObject apiDataObject = new ApiDataObjectImpl(new HashMap<String, String>(){{
            put(ApiCallArgumentKeys.LoginAndRegApiKeys.USERNAME, userName);
            put(ApiCallArgumentKeys.LoginAndRegApiKeys.OTP, tempPass);
            put(ApiCallArgumentKeys.LoginAndRegApiKeys.DEVICE_IDENTIFIER, deviceIdentifier);
        }});

        final ApiResponseActions responseActions = getPinVerificationResponseAction();

        ApiRequest apiRequest = view.getObjectConstructorInstance().getTempPinVerificationApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, view.getContextWrapper());
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, responseActions);
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});

        apiRequest.setProgressDialog(null);
        apiRequest.sendRequest();;
    }

    private ApiResponseActions getPinVerificationResponseAction() {
        return new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int i, JSONObject jsonObject) throws JSONException {
                view.onApiCallSuccess();
            }

            @Override
            public void onApiCallFailure(int i, JSONObject jsonObject) throws JSONException {
                view.onApiCallFailure(jsonObject.getString(ApiRequest.ApiFields.KEY_MESSAGE));
            }
        };
    }

    @Override
    public void startOtpResendTimer() {
        timer = new TimerImpl(30, 1);
        timer.setTimerStartDelay(1);
        timer.addToObservers(new TypedObserverImpl<Long>() {
            @Override
            public void update(Long aLong) {
                view.updateOtpWaitingTimer(String.valueOf(timer.getRemainingTime()));
            }
        });
        timer.startTimer();
    }

    @Override
    public void startVerifyingDotUpdateTimer(int dotCountLoop) {
        timer = new TimerImpl(dotCountLoop, 1);
        timer.addToObservers(new TypedObserverImpl<Long>() {
            @Override
            public void update(Long timeElapsed) {
                view.updateVerifyingDot(timeElapsed);
            }
        });
        timer.startTimer();
    }

    @Override
    public void stopVerifyingDotTimer(){
        timer.stopTimer();
    }

    @Override
    public void sendRequestForOtpCode() {

    }
}
