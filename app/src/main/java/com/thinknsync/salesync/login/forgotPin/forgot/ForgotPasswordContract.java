package com.thinknsync.salesync.login.forgotPin.forgot;

import com.thinknsync.apilib.ApiResponseView;
import com.thinknsync.salesync.base.BaseController;
import com.thinknsync.salesync.base.BaseView;
import com.thinknsync.salesync.uiElements.formElements.forms.FormContainerContract;
import com.thinknsync.salesync.base.viewInterfaces.InteractiveView;

public interface ForgotPasswordContract {
    interface View extends BaseView, ApiResponseView, FormContainerContract.FormActivity, InteractiveView {
        String tag = "ForgotPasswordView";
    }

    interface Controller extends BaseController<View> {
        void sendTemporaryPinRequestToServer(String userName, String deviceId);
    }
}
