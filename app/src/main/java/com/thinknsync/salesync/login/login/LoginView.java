package com.thinknsync.salesync.login.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.thinknsync.inputvalidation.InputValidation;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.BitmapWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.commons.osSpecifics.imageCapture.ImageCapture;
import com.thinknsync.salesync.commons.osSpecifics.imageCapture.ImageCaptureImpl;
import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.home.HomeView;
import com.thinknsync.salesync.login.DeviceIdentifierFetchFormActivity;
import com.thinknsync.salesync.login.forgotPin.forgot.ForgotPasswordView;
import com.thinknsync.salesync.uiElements.formElements.inputs.ActionButton;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputField;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputValidationContainer;

import java.util.HashMap;

public class LoginView extends DeviceIdentifierFetchFormActivity implements LoginContract.View {

    protected LoginContract.Controller loginController;
    private InputField username, pin;
    private TextView forgotPass;
    protected ActionButton loginButton;

    private ImageCapture imageCaptureImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageCaptureImpl = new ImageCaptureImpl(this);
        initViewListeners();
    }

    @Override
    protected void initForm() {
        setupForm(new HashMap<InputValidationContainer, InputValidation.ValidationTypes>(){{
            put(username, InputValidation.ValidationTypes.TYPE_MANDATORY);
            put(pin, InputValidation.ValidationTypes.TYPE_PIN);
        }}, loginButton);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loginController.hasLocationPermission();
    }

    @Override
    public void initViewListeners() {
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                loginController.loginButton(username.getInput(), pin.getInput(), deviceIdentifier.getText().toString());
                imageCaptureImpl.openCamera();
            }
        });

        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(ForgotPasswordView.class);
            }
        });
    }

    @Override
    public void initView() {
        username = findViewById(R.id.input_box_user_id);
        pin = findViewById(R.id.input_box_pin);
        loginButton = findViewById(R.id.start_button);
        forgotPass = findViewById(R.id.forgot_pass_text);
    }

    @Override
    public void initController() {
        loginController = new LoginController(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public AndroidContextWrapper getContextWrapper() {
        return (AndroidContextWrapper) getObjectConstructorInstance().getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, LoginView.this);
        }});
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null) {
            if (requestCode == ImageCapture.ImageCaptureCode) {
                FrameworkWrapper bitmapWrapper = getObjectConstructorInstance().getBitmapWrapper(new HashMap<String, Object>(){{
                    put(BitmapWrapper.tag, data.getExtras().get("data"));
                }});

                imageCaptureImpl.processCapturedImage(bitmapWrapper);
                loginController.login(username.getInput(), pin.getInput(), getDeviceIdentifierText(),
                        imageCaptureImpl.getCapturedImage());
            }
        }
    }

    @Override
    public void onApiCallSuccess(String... strings) {
        startActivityAndClearAll(HomeView.class);
    }

    @Override
    public void onApiCallFailure(String... strings) {
        username.showError(getString(R.string.user_id_incorrect_text));
        pin.showError(getString(R.string.pin_incorrect_text));
    }

    @Override
    public void onApiCallError(String... strings) {
        onApiCallFailure(strings);
    }
}
