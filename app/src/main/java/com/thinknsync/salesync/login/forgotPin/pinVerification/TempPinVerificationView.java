package com.thinknsync.salesync.login.forgotPin.pinVerification;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.BundleWrapper;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.api.ApiCallArgumentKeys;
import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.login.DeviceIdentifierFetchActivity;
import com.thinknsync.salesync.login.forgotPin.pinReset.ResetPinView;
import com.thinknsync.salesync.uiElements.formElements.inputs.BaseOnTextChangeObserver;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputChangeListener;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputField;

import java.util.HashMap;

public class TempPinVerificationView extends DeviceIdentifierFetchActivity implements TempPinVerificationContact.View {

    private TempPinVerificationContact.Controller controller;

    private InputField tempPinBox1, tempPinBox2, tempPinBox3, tempPinBox4, tempPinBox5, tempPinBox6;
    private TextView timerView, resendCode, verifyingText;

    private InputField[] otpFields;
    private TextView[] interactiveSectionViews;

    private String userName, actualVerificationPin, pinManuallyInput, userPhone;
    private final int verifyingDotCount = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        otpFields = new InputField[]{tempPinBox1, tempPinBox2, tempPinBox3, tempPinBox4, tempPinBox5, tempPinBox6};
        interactiveSectionViews = new TextView[]{timerView, resendCode, verifyingText};
        startOtpWaitingTimer();
        initViewListeners();
        initValues();
    }

    @Override
    public void initValues() {
        if(getIntent().getExtras() != null){
            userName = getIntent().getExtras().getString(ApiCallArgumentKeys.LoginAndRegApiKeys.USERNAME);
            actualVerificationPin = getIntent().getExtras().getString(ApiCallArgumentKeys.LoginAndRegApiKeys.OTP);
            userPhone = getIntent().getExtras().getString(UserInterface.key_phone);
        }
    }

    private void startOtpWaitingTimer() {
        setVisibilityOfInteraction(timerView);
        controller.startOtpResendTimer();
    }

    @Override
    public void initView() {
        tempPinBox1 = findViewById(R.id.temp_pin_edittext_1);
        tempPinBox2 = findViewById(R.id.temp_pin_edittext_2);
        tempPinBox3 = findViewById(R.id.temp_pin_edittext_3);
        tempPinBox4 = findViewById(R.id.temp_pin_edittext_4);
        tempPinBox5 = findViewById(R.id.temp_pin_edittext_5);
        tempPinBox6 = findViewById(R.id.temp_pin_edittext_6);

        timerView = findViewById(R.id.timer_text);
        resendCode = findViewById(R.id.resend_code_text);
        verifyingText = findViewById(R.id.verifying_text);
    }

    @Override
    public void initViewListeners() {
        resendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.sendRequestForOtpCode();
//                startOtpWaitingTimer();
            }
        });

        InputChangeListener inputChangeListener = getOtpTextChangeAction();
        for (InputField inputField : otpFields) {
            inputField.setTextChangeObserver(inputChangeListener);
        }
    }

    private InputChangeListener getOtpTextChangeAction() {
        BaseOnTextChangeObserver inputChangeListener = new BaseOnTextChangeObserver();
        inputChangeListener.addToObservers(new TypedObserverImpl<String>() {
            @Override
            public void update(String s) {
                String otp = getOtpFieldValues();
                focusOnNextField();
                if(otp.length() == otpFields.length){
                    pinManuallyInput = otp;
                    controller.sendTempLoginRequestToServer(userName, pinManuallyInput, getDeviceIdentifierText());
                    setVisibilityOfInteraction(verifyingText);
                    controller.startVerifyingDotUpdateTimer(verifyingDotCount);
                }
            }

            private String getOtpFieldValues() {
                StringBuilder otp = new StringBuilder();
                for (InputField otpField : otpFields) {
                    if (otpField.getInput().equals("")) {
                        break;
                    }
                    otp.append(otpField.getInput());
                }
                return otp.toString();
            }

            private void focusOnNextField() {
                for (int i = 0; i < otpFields.length; i++) {
                    if(otpFields[i].hasFocus() && (i +1) != otpFields.length){
                        otpFields[i + 1].requestFocus();
                        break;
                    }
                }
            }
        });

        return inputChangeListener;
    }

    private void setVisibilityOfInteraction(TextView visibleText){
        for (TextView textView : interactiveSectionViews) {
            if (textView.getId() != visibleText.getId()) {
                textView.setVisibility(View.GONE);
            } else {
                textView.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void initController() {
        controller = new TempPinVerificationController(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_pin_verification;
    }

    @Override
    public AndroidContextWrapper getContextWrapper() {
        return (AndroidContextWrapper) getObjectConstructorInstance().getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, TempPinVerificationView.this);
        }});
    }

    @Override
    public void updateOtpWaitingTimer(final String remainingTime) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(Integer.valueOf(remainingTime) != 0) {
                    timerView.setText(String.format(getString(R.string.time_countdown_text), remainingTime));
                } else {
                    setVisibilityOfInteraction(resendCode);
                }
            }
        });
    }

    @Override
    public void updateVerifyingDot(final Long timeElapsed) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(timeElapsed != verifyingDotCount){
                    verifyingText.setText(verifyingText.getText().toString() + ".");
                } else {
                    verifyingText.setText(getString(R.string.verifying_text));
                    controller.startVerifyingDotUpdateTimer(verifyingDotCount);
                }
            }
        });
    }

    @Override
    public void onApiCallSuccess(String... strings) {
        final Bundle b = new Bundle();
        b.putString(ApiCallArgumentKeys.LoginAndRegApiKeys.USERNAME, userName);
        b.putString(ApiCallArgumentKeys.LoginAndRegApiKeys.OTP, pinManuallyInput);
        controller.stopVerifyingDotTimer();
        startActivityAndClearCurrent(ResetPinView.class, (BundleWrapper) getObjectConstructorInstance().getBundleWrapper(new HashMap<String, Object>(){{
            put(BundleWrapper.tag, b);
        }}));
    }

    @Override
    public void onApiCallFailure(String... strings) {
        showShortMessage(strings[0]);
        for (InputField otpField : otpFields) {
            otpField.setText("");
        }
        otpFields[0].requestFocus();
        setVisibilityOfInteraction(resendCode);
        controller.stopVerifyingDotTimer();
    }

    @Override
    public void onApiCallError(String... strings) {
        onApiCallFailure(strings);
    }
}
