package com.thinknsync.salesync.messageBroker;

public interface PushPullFailedManager extends PushPullFailedContentHolder {
    void setPushPullManager(PushPullManager brokerManager);
    void proceedWithFailedAction();
}
