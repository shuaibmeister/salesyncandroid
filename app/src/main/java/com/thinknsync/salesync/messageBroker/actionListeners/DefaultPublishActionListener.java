package com.thinknsync.salesync.messageBroker.actionListeners;

import com.thinknsync.logger.Logger;
import com.thinknsync.salesync.messageBroker.pushPullContents.BrokerPublishContent;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;

public class DefaultPublishActionListener implements IMqttActionListener {

    private String Tag = "mqtt-publish-action-listener";
    private Logger logger;
    private BrokerPublishContent messageObject;

    public DefaultPublishActionListener(Logger logger, BrokerPublishContent messageObject){
        this.logger = logger;
        this.messageObject = messageObject;
    }

    @Override
    public void onSuccess(IMqttToken asyncActionToken) {
        logger.debugLog(Tag, "published-topic:" + messageObject.getTopic() + ", message:" + messageObject.getMessage());
        messageObject.notifyFailedActionComplete();
    }

    @Override
    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
        logger.debugLog(Tag, exception.getLocalizedMessage());
        messageObject.addToFailedQueue();
    }
}
