package com.thinknsync.salesync.messageBroker.actionListeners.messageListenerActions;

import com.thinknsync.observerhost.TypedObserverImpl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class BaseJsonMessageObserver extends TypedObserverImpl<String> {

    protected JSONArray convertToJsonArray(String jsonArrayString){
        JSONArray jsonArray = new JSONArray();
        try {
            jsonArray = new JSONArray(jsonArrayString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    protected JSONObject convertToJsonObject(String jsonObjectString){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(jsonObjectString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
