package com.thinknsync.salesync.messageBroker.actionListeners;

import com.thinknsync.logger.Logger;
import com.thinknsync.salesync.messageBroker.pushPullContents.SubscribeContent;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;

public class DefaultSubscribeActionListener implements IMqttActionListener {

    private String Tag = "mqtt-subscribe-action-listener";
    private Logger logger;
    private SubscribeContent subscribeContent;

    public DefaultSubscribeActionListener(Logger logger, SubscribeContent subscribeContent){
        this.logger = logger;
        this.subscribeContent = subscribeContent;
    }

    @Override
    public void onSuccess(IMqttToken asyncActionToken) {
        logger.debugLog(Tag, "subscribed:" + subscribeContent.getTopic());
        subscribeContent.notifyFailedActionComplete();
    }

    @Override
    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
        logger.debugLog(Tag, exception.getLocalizedMessage());
        subscribeContent.addToFailedQueue();
    }
}
