package com.thinknsync.salesync.messageBroker.pushPullContents;

public interface PushPullFaliedManagerContainer {
    void notifyPublishComplete(PublishContent[] publishContent);
    void notifyPublishComplete(PublishContent publishContent);
}
