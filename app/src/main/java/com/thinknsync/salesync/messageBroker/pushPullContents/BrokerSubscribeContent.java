package com.thinknsync.salesync.messageBroker.pushPullContents;

import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.messageBroker.PushPullManager;
import com.thinknsync.salesync.messageBroker.actionListeners.DefaultSubscribeActionListener;
import com.thinknsync.salesync.messageBroker.actionListeners.DefaultSubscribeMessageListener;
import com.thinknsync.salesync.messageBroker.actionListeners.PushPullActionListenerWrapper;
import com.thinknsync.salesync.messageBroker.actionListeners.PushPullMessageListenerWrapper;

import java.util.HashMap;

public class BrokerSubscribeContent extends BrokerContentBase implements SubscribeContent {

    private PushPullMessageListenerWrapper<DefaultSubscribeMessageListener> messageListenerWrapper;

    public BrokerSubscribeContent(ObjectConstructor objectConstructor, String topic) {
        super(objectConstructor, topic);
        setDefaultMessageListener(objectConstructor);
    }

    public BrokerSubscribeContent(ObjectConstructor objectConstructor, String topic, PushPullManager.Qos qos) {
        super(objectConstructor, topic, qos);
        setDefaultMessageListener(objectConstructor);
    }

    @Override
    protected void setDefaultActionListener(final ObjectConstructor objectConstructor){
        setActionListener(objectConstructor.getMqttActionListenerWrapper(new HashMap<String, Object>(){{
            put(PushPullActionListenerWrapper.tag, new DefaultSubscribeActionListener(
                    objectConstructor.getLogger(), BrokerSubscribeContent.this));
        }}));
    }

    private void setDefaultMessageListener(final ObjectConstructor objectConstructor){
        setMessageListener(objectConstructor.getMqttJsonMessageListenerWrapper(new HashMap<String, Object>(){{
            put(PushPullMessageListenerWrapper.tag, new DefaultSubscribeMessageListener(objectConstructor.getLogger()));
        }}));
    }

    @Override
    public PushPullMessageListenerWrapper<DefaultSubscribeMessageListener> getMessageListenerWrapper() {
        return messageListenerWrapper;
    }

    @Override
    public void addMessageListenerObserver(TypedObserver<String> messageObserver) {
        ((ObserverHost<String>)getMessageListenerWrapper().getFrameworkObject()).addToObservers(messageObserver);
    }

    @Override
    public void setMessageListener(FrameworkWrapper messageListenerWrapper) {
        this.messageListenerWrapper = (PushPullMessageListenerWrapper<DefaultSubscribeMessageListener>) messageListenerWrapper;
    }

    @Override
    public void addToFailedQueue() {
        pushPullFailedManager.addToSubscribeList(this);
    }

    @Override
    public void notifyFailedActionComplete() {
        pushPullFailedManager.removeFromSubscribeList(this);
    }
}
