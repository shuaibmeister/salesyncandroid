package com.thinknsync.salesync.messageBroker.pushPullContents;

import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.observerhost.TypedObserver;

public interface SubscribeContent extends PushPullContentBase {
    FrameworkWrapper getMessageListenerWrapper();
    void addMessageListenerObserver(TypedObserver<String> messageObserver);
    void setMessageListener(FrameworkWrapper messageListenerWrapper);
}
