package com.thinknsync.salesync.messageBroker.actionListeners;

import com.thinknsync.logger.Logger;
import com.thinknsync.salesync.messageBroker.pushPullContents.PushPullContentBase;
import com.thinknsync.salesync.messageBroker.pushPullContents.SubscribeContent;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;

public class DefaultUnsubscribeActionListener implements IMqttActionListener {

    private String Tag = "mqtt-unsubscribe-action-listener";
    private Logger logger;
    private PushPullContentBase unsubscribeContent;

    public DefaultUnsubscribeActionListener(Logger logger, PushPullContentBase unsubscribeContent){
        this.logger = logger;
        this.unsubscribeContent = unsubscribeContent;
    }

    @Override
    public void onSuccess(IMqttToken asyncActionToken) {
        logger.debugLog(Tag, "unsubscribed:" + unsubscribeContent.getTopic());
        unsubscribeContent.notifyFailedActionComplete();
    }

    @Override
    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
        logger.debugLog(Tag, exception.getLocalizedMessage());
        unsubscribeContent.addToFailedQueue();
    }
}
