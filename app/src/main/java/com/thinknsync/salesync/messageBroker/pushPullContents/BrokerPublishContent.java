package com.thinknsync.salesync.messageBroker.pushPullContents;

import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.messageBroker.PushPullManager;
import com.thinknsync.salesync.messageBroker.actionListeners.DefaultPublishActionListener;
import com.thinknsync.salesync.messageBroker.actionListeners.PushPullActionListenerWrapper;

import java.util.HashMap;

public class BrokerPublishContent extends BrokerContentBase implements PublishContent {
    private String message;
    private boolean retained = false;

    public BrokerPublishContent(ObjectConstructor objectConstructor, String topic, String message) {
        super(objectConstructor, topic);
        this.message = message;
        setDefaultActionListener(objectConstructor);
    }

    public BrokerPublishContent(ObjectConstructor objectConstructor, String topic, String message, PushPullManager.Qos qos, boolean retained) {
        super(objectConstructor, topic, qos);
        this.message = message;
        this.retained = retained;
        setDefaultActionListener(objectConstructor);
    }

    @Override
    protected void setDefaultActionListener(final ObjectConstructor objectConstructor){
        setActionListener((PushPullActionListenerWrapper)objectConstructor.getMqttActionListenerWrapper(new HashMap<String, Object>(){{
            put(PushPullActionListenerWrapper.tag, new DefaultPublishActionListener(
                    objectConstructor.getLogger(), BrokerPublishContent.this));
        }}));
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void setRetained(boolean retained) {
        this.retained = retained;
    }

    @Override
    public boolean isRetained() {
        return retained;
    }

    @Override
    public void addToFailedQueue(){
        pushPullFailedManager.addToPublishList(this);
    }

    @Override
    public void notifyFailedActionComplete(){
        pushPullFailedManager.removeFromPublishList(this);
    }
}
