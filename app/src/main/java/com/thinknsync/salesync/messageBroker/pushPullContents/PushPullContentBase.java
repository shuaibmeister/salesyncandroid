package com.thinknsync.salesync.messageBroker.pushPullContents;

import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.salesync.messageBroker.PushPullManager;

public interface PushPullContentBase {
    String getTopic();
    void setTopic(String topic);
    void setQos(PushPullManager.Qos qos);
    PushPullManager.Qos getQos();
    int getQosInt();
    FrameworkWrapper getActionListenerWrapper();
    void setActionListener(FrameworkWrapper actionListenerWrapper);
    void addToFailedQueue();
    void notifyFailedActionComplete();
}
