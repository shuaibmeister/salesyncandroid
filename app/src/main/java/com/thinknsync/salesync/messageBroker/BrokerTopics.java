package com.thinknsync.salesync.messageBroker;

import java.util.List;

public interface BrokerTopics {
    String baseTopic = "salesync/";
    String configurationTopic = baseTopic + "configuration";

    String getLocationUpdateTopic();
    List<String> getAllTopics();
    String[] getInitialSubscribeList();
}
