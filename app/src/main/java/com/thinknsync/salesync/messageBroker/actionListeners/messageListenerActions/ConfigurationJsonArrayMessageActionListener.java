package com.thinknsync.salesync.messageBroker.actionListeners.messageListenerActions;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.commons.configActions.ConfigWiseActions;
import com.thinknsync.salesync.database.records.config.Config;
import com.thinknsync.salesync.database.records.config.ConfigInterface;
import com.thinknsync.salesync.database.repositories.config.ConfigRepositoryInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.commons.configActions.ConfigAction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ConfigurationJsonArrayMessageActionListener extends BaseJsonMessageObserver {

    private ObjectConstructor objectConstructor;
    private AndroidContextWrapper contextWrapper;

    public ConfigurationJsonArrayMessageActionListener(ObjectConstructor objectConstructor, AndroidContextWrapper contextWrapper) {
        this.objectConstructor = objectConstructor;
        this.contextWrapper = contextWrapper;
    }

    @Override
    public void update(String jsonArrayString){
        JSONArray configArray = convertToJsonArray(jsonArrayString);
        saveConfigs(configArray);
        triggerAction();
    }

    private void saveConfigs(JSONArray configArray) {
        ConfigInterface[] configs = new ConfigInterface[configArray.length()];
        ConfigRepositoryInterface configRepository = objectConstructor.getConfigRepository();
        try {
            for (int i = 0; i < configArray.length(); i++) {
                JSONObject configJson = configArray.getJSONObject(i);
                String jsonKey = configJson.keys().next();
                Config config = new Config();
                config.setConfigType(ConfigInterface.ConfigType.getConfigTypeFromValue(jsonKey));
                config.setValue(configJson.getString(jsonKey));
                configs[i] = config;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        configRepository.insertOrUpdateConfigs(configs);
    }

    private void triggerAction() {
        ConfigWiseActions configAction = new ConfigAction(contextWrapper, objectConstructor);
        configAction.doAction();
    }
}
