package com.thinknsync.salesync.messageBroker;

import com.thinknsync.salesync.messageBroker.pushPullContents.PublishContent;
import com.thinknsync.salesync.messageBroker.pushPullContents.PushPullContentBase;
import com.thinknsync.salesync.messageBroker.pushPullContents.SubscribeContent;

import java.util.List;

/**
 * Created by shuaib on 5/20/17.
 */

public interface PushPullManager {
    String tag = "pushPullManager";

//    void setInitialSubscribeTopics(List<String> initSubList);
    void addToInitialSubscribeTopics(String[] initSubList);
    void addToInitialSubscribeTopics(SubscribeContent[] initSubList);
    void setInitialSubscribeTopics(SubscribeContent[] initSubList);
    void connect();
    void disconnect();
    void publish(PublishContent message);
    void publish(PublishContent[] messages);
    void subscribe(SubscribeContent pushPullContent);
    void subscribe(SubscribeContent[] pushPullContent);
    void unsubscribe(PushPullContentBase topics);
    void unsubscribe(PushPullContentBase[] topic);
    void clearFailedPublishQueue();
    void clearFailedSubscribeQueue();
    void clearFailedUnsubscribeQueue();
    boolean isConnected();

    enum Qos {
        QOS_AT_MOST_ONCE(0),
        QOS_AT_LEAST_ONCE(1),
        QOS_EXACTLY_ONCE(2);

        private int qos;

        Qos(int qos) {
            this.qos = qos;
        }

        public int getQosInt(){
            return qos;
        }

        public static Qos getDefaultQos(){
            return QOS_AT_LEAST_ONCE;
        }

        public static int getDefaultQosInt(){
            return QOS_AT_LEAST_ONCE.getQosInt();
        }
    }
}