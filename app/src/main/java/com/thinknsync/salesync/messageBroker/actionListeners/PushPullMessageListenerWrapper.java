package com.thinknsync.salesync.messageBroker.actionListeners;

import com.thinknsync.objectwrappers.BaseFrameworkWrapper;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;

public class PushPullMessageListenerWrapper<T extends IMqttMessageListener> extends BaseFrameworkWrapper<T> {
    public static final String tag = "mqttMessageListener";

    public PushPullMessageListenerWrapper(T messageListener) {
        setFrameworkObject(messageListener);
    }
}
