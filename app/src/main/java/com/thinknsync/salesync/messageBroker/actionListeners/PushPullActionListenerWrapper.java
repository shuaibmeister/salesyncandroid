package com.thinknsync.salesync.messageBroker.actionListeners;

import com.thinknsync.objectwrappers.BaseFrameworkWrapper;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;

public class PushPullActionListenerWrapper extends BaseFrameworkWrapper<IMqttActionListener> {
    public static final String tag = "mqttActionListener";

    public PushPullActionListenerWrapper(IMqttActionListener actionListener) {
        setFrameworkObject(actionListener);
    }
}
