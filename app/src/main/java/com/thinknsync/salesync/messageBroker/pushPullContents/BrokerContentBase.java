package com.thinknsync.salesync.messageBroker.pushPullContents;

import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.messageBroker.PushPullFailedManager;
import com.thinknsync.salesync.messageBroker.PushPullManager;
import com.thinknsync.salesync.messageBroker.RetryPushPullFailedManager;
import com.thinknsync.salesync.messageBroker.actionListeners.DefaultSubscribeActionListener;
import com.thinknsync.salesync.messageBroker.actionListeners.PushPullActionListenerWrapper;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;

import java.util.HashMap;

public abstract class BrokerContentBase implements PushPullContentBase {
    private String topic;
    private PushPullManager.Qos qos = PushPullManager.Qos.getDefaultQos();
    private PushPullActionListenerWrapper actionListener;
    protected PushPullFailedManager pushPullFailedManager;


    public BrokerContentBase(ObjectConstructor objectConstructor, String topic) {
        this.topic = topic;
        setDefaultActionListener(objectConstructor);
        this.pushPullFailedManager = RetryPushPullFailedManager.getInstance();
    }

    public BrokerContentBase(ObjectConstructor objectConstructor, String topic, PushPullManager.Qos qos) {
        this.topic = topic;
        this.qos = qos;
        setDefaultActionListener(objectConstructor);
    }

    protected abstract void setDefaultActionListener(final ObjectConstructor objectConstructor);

    @Override
    public String getTopic() {
        return topic;
    }

    @Override
    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public void setQos(PushPullManager.Qos qos) {
        this.qos = qos;
    }

    @Override
    public PushPullManager.Qos getQos() {
        return qos;
    }

    @Override
    public int getQosInt() {
        return qos.getQosInt();
    }

    @Override
    public PushPullActionListenerWrapper getActionListenerWrapper() {
        return actionListener;
    }

    @Override
    public void setActionListener(FrameworkWrapper actionListenerWrapper) {
        this.actionListener = (PushPullActionListenerWrapper)actionListenerWrapper;
    }
}
