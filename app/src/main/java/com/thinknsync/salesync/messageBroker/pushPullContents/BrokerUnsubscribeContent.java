package com.thinknsync.salesync.messageBroker.pushPullContents;

import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.messageBroker.actionListeners.DefaultUnsubscribeActionListener;
import com.thinknsync.salesync.messageBroker.actionListeners.PushPullActionListenerWrapper;

import java.util.HashMap;

public class BrokerUnsubscribeContent extends BrokerContentBase {

    public BrokerUnsubscribeContent(ObjectConstructor objectConstructor, String topic) {
        super(objectConstructor, topic);
    }

    @Override
    protected void setDefaultActionListener(final ObjectConstructor objectConstructor){
        setActionListener((PushPullActionListenerWrapper)objectConstructor.getMqttActionListenerWrapper(new HashMap<String, Object>(){{
            put(PushPullActionListenerWrapper.tag, new DefaultUnsubscribeActionListener(
                    objectConstructor.getLogger(), BrokerUnsubscribeContent.this));
        }}));
    }

    @Override
    public void addToFailedQueue() {
        pushPullFailedManager.addToUnsubscribeList(this);
    }

    @Override
    public void notifyFailedActionComplete() {
        pushPullFailedManager.removeFromUnsubscribeList(this);
    }
}
