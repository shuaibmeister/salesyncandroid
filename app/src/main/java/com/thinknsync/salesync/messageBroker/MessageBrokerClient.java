package com.thinknsync.salesync.messageBroker;

import android.content.Context;

import com.thinknsync.logger.Logger;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.salesync.commons.osSpecifics.broadcastReceivers.BroadcastListenerContainer;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.messageBroker.pushPullContents.BrokerSubscribeContent;
import com.thinknsync.salesync.messageBroker.pushPullContents.PublishContent;
import com.thinknsync.salesync.messageBroker.pushPullContents.PushPullContentBase;
import com.thinknsync.salesync.messageBroker.pushPullContents.SubscribeContent;
import com.thinknsync.salesync.commons.osSpecifics.broadcastReceivers.BroadcastListener;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MessageBrokerClient implements PushPullManager {

    private static PushPullManager brokerClient;
    private Context context;

    private final String BROKER_USERNAME = "salesync";
    private final String BROKER_PASSWORD = "mqttSalesync";
    private final String BROKER_URL = "ssl://broker.salesync.thinknsync.com";
    private final int BROKER_PORT = 8883;
    private final String BROKER_FULL_URL = BROKER_URL + ":" + BROKER_PORT;

    private ObjectConstructor objectConstructor;
    private Logger logger;
    private MqttAndroidClient client;
    private String clientId;
//    private List<String> initSubscribeList;
    private SubscribeContent[] initSubscribeList;
    private PushPullFailedManager pushPullFailedManager;

    private boolean connectionInProgress = false;
    private boolean shouldConnect = false;

    private final String TAG = "mqtt-message-broker";

    private MessageBrokerClient(ObjectConstructor objectConstructor, final AndroidContextWrapper contextWrapper){
        clientId = MqttClient.generateClientId();
        context = contextWrapper.getFrameworkObject();
        this.objectConstructor = objectConstructor;
//        this.networkStatusReporter = objectConstructor.getNetworkStatusReport(new HashMap<String, Object>(){{
//            put(AndroidContextWrapper.tag, contextWrapper);
//        }});
        logger = objectConstructor.getLogger();
        initSubscribeList = new SubscribeContent[0];
        pushPullFailedManager = RetryPushPullFailedManager.getInstance(this);
        setupInternetBroadcastReceiver();
    }

    private void setupInternetBroadcastReceiver() {
        BroadcastListenerContainer broadcastListenerContainer = objectConstructor.getBroadcastListenerContainer();
        broadcastListenerContainer.addObserverToListener(BroadcastListener.BroadcastListenerType.INTERNET_BROADCAST,
                new TypedObserverImpl<Boolean>() {
                    @Override
                    public void update(Boolean connectivity) {
                        if(connectivity && !isConnected() && shouldConnect){
                            logger.debugLog(TAG, "connecting from broadcast receiver");
                            connect();
                        }
                    }
        });
    }

    public static PushPullManager getClient(ObjectConstructor objectConstructor, AndroidContextWrapper contextWrapper){
        if(brokerClient == null){
            brokerClient = new MessageBrokerClient(objectConstructor, contextWrapper);
        }
        return brokerClient;
    }

    public static PushPullManager getClient(){
        if(brokerClient == null){
            throw new NullPointerException(
                    "Broker Manager is Null. call getInstance(ObjectConstructor, androidContextWrapper) first");
        }
        return brokerClient;
    }

    @Override
    public void addToInitialSubscribeTopics(String[] initSubList){
        SubscribeContent[] subscribeContentList = getSubscribeContentsFromTopics(initSubList);
        addToInitialSubscribeTopics(subscribeContentList);
    }

    @Override
    public void addToInitialSubscribeTopics(SubscribeContent[] initSubList){
        if(initSubscribeList.length == 0){
            initSubscribeList = new SubscribeContent[initSubList.length];
        }
        initSubscribeList = joinSubscribeContentsToInitSubscribeArray(initSubList);
        subscribe(initSubscribeList);
        logger.debugLog(TAG, Arrays.toString(initSubscribeList));
    }

    @Override
    public void setInitialSubscribeTopics(SubscribeContent[] initSubList){
        this.initSubscribeList = initSubList;
        subscribe(initSubscribeList);
    }

    private SubscribeContent[] joinSubscribeContentsToInitSubscribeArray(SubscribeContent[] arr2){
        List<SubscribeContent> list1 = new ArrayList<>(Arrays.asList(initSubscribeList));
        list1.addAll(Arrays.asList(arr2));
        return list1.toArray(new SubscribeContent[0]);
    }

    @Override
    public void connect() {
        if(!connectionInProgress) {
            try {
                connectionInProgress = true;
                MqttConnectOptions mqttConnectOptions = getMqttClientOptions();
                client = new MqttAndroidClient(context, BROKER_FULL_URL, clientId);
                client.setCallback(new MqttCallback() {
                    @Override
                    public void connectionLost(Throwable cause) {
                        logger.debugLog(TAG, "connection lost. trying to reconnect");
                        client.unregisterResources();
                        client = null;
                        connect();
                    }

                    @Override
                    public void messageArrived(String topic, MqttMessage message) throws Exception {
                        logger.debugLog(TAG, "topic: " + topic + ",message:" + message.toString());
                    }

                    @Override
                    public void deliveryComplete(IMqttDeliveryToken token) {
                        logger.debugLog(TAG, token.toString());
                    }
                });

                client.connect(mqttConnectOptions).setActionCallback(new IMqttActionListener() {
                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        logger.debugLog(TAG, "mqtt connect successful");
                        pushPullFailedManager.proceedWithFailedAction();
                        connectionInProgress = false;
                        shouldConnect = true;
                        subscribe(initSubscribeList);
//                        publish(new BrokerPublishContent(objectConstructor, "/location_updates", "test"));
                    }

                    @Override
                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                        exception.printStackTrace();
                        logger.debugLog(TAG, "mqtt connect failure");
                    }
                });;
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }

    private SubscribeContent[] getSubscribeContentsFromTopics(String[] subscribeTopics) {
        SubscribeContent[] subContents = new SubscribeContent[subscribeTopics.length];
        for (int i = 0; i < subscribeTopics.length; i++) {
            subContents[i] = new BrokerSubscribeContent(objectConstructor, subscribeTopics[i]);
        }
        return subContents;
    }

    private MqttConnectOptions getMqttClientOptions() {
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setCleanSession(false);
//        mqttConnectOptions.setAutomaticReconnect(true);
//            mqttConnectOptions.setWill(Constants.PUBLISH_TOPIC, "I am going offline".getBytes(), 1, true);
        mqttConnectOptions.setUserName(BROKER_USERNAME);
        mqttConnectOptions.setPassword(BROKER_PASSWORD.toCharArray());
//        mqttConnectOptions.setConnectionTimeout(2);
        return mqttConnectOptions;
    }

    @Override
    public void disconnect() {
        try {
            client.disconnect().setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    logger.debugLog(TAG, "mqtt disconnect successful");
                    shouldConnect = false;
                    client = null;
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    logger.debugLog(TAG, "mqtt disconnect failure");
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void publish(PublishContent publishContent) {
        try {
            byte[] encodedPayload = publishContent.getMessage().getBytes("UTF-8");
            client.publish(publishContent.getTopic(), encodedPayload, publishContent.getQosInt(), publishContent.isRetained())
                    .setActionCallback((IMqttActionListener) publishContent.getActionListenerWrapper().getFrameworkObject());
        } catch (UnsupportedEncodingException | MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void publish(PublishContent[] publishContents) {
        for (PublishContent publishContent : publishContents) {
            publish(publishContent);
        }
    }

    @Override
    public void clearFailedPublishQueue() {
        pushPullFailedManager.clearFailedPublishQueue();
    }

    @Override
    public void subscribe(SubscribeContent subscribeContent) {
        subscribe(new SubscribeContent[]{subscribeContent});
    }

    @Override
    public void subscribe(SubscribeContent[] subscribeContents) {
        if(isConnected()) {
            String[] topics = new String[subscribeContents.length];
            int[] qosArray = new int[subscribeContents.length];
            IMqttMessageListener[] messageListenerArray = new IMqttMessageListener[subscribeContents.length];

            for (int i = 0; i < subscribeContents.length; i++) {
                topics[i] = subscribeContents[i].getTopic();
                qosArray[i] = subscribeContents[i].getQosInt();
                messageListenerArray[i] = (IMqttMessageListener) subscribeContents[i].getMessageListenerWrapper().getFrameworkObject();
            }

            try {
                if (subscribeContents.length > 0) {
                    IMqttToken token = client.subscribe(topics, qosArray, messageListenerArray);
                    if (token != null) {
                        token.setActionCallback((IMqttActionListener) subscribeContents[0].getActionListenerWrapper().getFrameworkObject());
                    }
                }
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void clearFailedSubscribeQueue() {
        pushPullFailedManager.clearFailedSubscribeQueue();
    }

    @Override
    public void clearFailedUnsubscribeQueue() {
        pushPullFailedManager.clearFailedUnSubscribeQueue();
    }

    @Override
    public void unsubscribe(PushPullContentBase brokerUnsubscribeContent) {
        unsubscribe(new PushPullContentBase[]{brokerUnsubscribeContent});
    }

    @Override
    public void unsubscribe(PushPullContentBase[] unsubscribeContents) {
        if(isConnected()) {
            String[] topics = new String[unsubscribeContents.length];
            for (int i = 0; i < topics.length; i++) {
                topics[i] = unsubscribeContents[i].getTopic();
            }
            try {
                if (unsubscribeContents.length > 0) {
                    client.unsubscribe(topics).setActionCallback((IMqttActionListener) unsubscribeContents[0].getActionListenerWrapper().getFrameworkObject());
                }
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean isConnected() {
        if(client != null) {
            return client.isConnected();
        }
        return false;
    }
}
