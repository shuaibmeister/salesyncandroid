package com.thinknsync.salesync.messageBroker.pushPullContents;

public interface PublishContent extends PushPullContentBase {
    String getMessage();
    void setMessage(String message);
    void setRetained(boolean retained);
    boolean isRetained();
}
