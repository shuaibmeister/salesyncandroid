package com.thinknsync.salesync.messageBroker;

import com.thinknsync.salesync.database.records.user.UserInterface;

import java.util.ArrayList;
import java.util.List;

public final class Topics implements BrokerTopics {
    private String baseUserRoleTopic;
//    private String baseUserRoleTopic="";

    private static Topics topics;
    private UserInterface loggedInUser;

    private Topics(UserInterface userObject) {
        loggedInUser = userObject;
        baseUserRoleTopic = baseTopic + userObject.getDesignation() + "/";
    }

    public static BrokerTopics getInstance(UserInterface userObject){
        if(topics == null){
            topics = new Topics(userObject);
        }
        return topics;
    }

    @Override
    public String getLocationUpdateTopic(){
        return baseUserRoleTopic + "location_updates/" + loggedInUser.getDataId();
//        return baseUserRoleTopic + "location-update/";
    }

    @Override
    public List<String> getAllTopics() {
        return new ArrayList<String>() {{add(getLocationUpdateTopic());}};
    }

    @Override
    public String[] getInitialSubscribeList() {
        return new String[] {getLocationUpdateTopic()};
    }
}
