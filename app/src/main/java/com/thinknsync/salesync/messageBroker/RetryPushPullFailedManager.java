package com.thinknsync.salesync.messageBroker;

import com.thinknsync.salesync.messageBroker.pushPullContents.PublishContent;
import com.thinknsync.salesync.messageBroker.pushPullContents.PushPullContentBase;
import com.thinknsync.salesync.messageBroker.pushPullContents.SubscribeContent;

import java.util.ArrayList;
import java.util.List;

public class RetryPushPullFailedManager implements PushPullFailedManager {

    private PushPullManager brokerManager;

    private List<PublishContent> publishContentList;
    private List<SubscribeContent> subscribeContentList;
    private List<PushPullContentBase> unsubscribeContentList;

    private static PushPullFailedManager retryPushPullFailedManager;

    private RetryPushPullFailedManager(PushPullManager brokerManager) {
        setPushPullManager(brokerManager);
        publishContentList = new ArrayList<>();
        subscribeContentList = new ArrayList<>();
        unsubscribeContentList = new ArrayList<>();
    }

    public static PushPullFailedManager getInstance(PushPullManager brokerManager){
        if(retryPushPullFailedManager == null){
            retryPushPullFailedManager = new RetryPushPullFailedManager(brokerManager);
        }
        return retryPushPullFailedManager;
    }

    public static PushPullFailedManager getInstance(){
        if(retryPushPullFailedManager == null){
            throw new NullPointerException("Broker Manager is Null. call getInstance(brokerManager) first");
        }
        return retryPushPullFailedManager;
    }

    @Override
    public void addToPublishList(PublishContent[] pubContents) {
        for(PublishContent pubContent : pubContents) {
            addToPublishList(pubContent);
        }
    }

    @Override
    public void addToPublishList(PublishContent pubContent) {
        if (!getPubFailedTopics().contains(pubContent)) {
            publishContentList.add(pubContent);
        }
    }

    @Override
    public void addToSubscribeList(SubscribeContent[] subContents) {
        for(SubscribeContent subContent : subContents) {
            addToSubscribeList(subContent);
        }
    }

    @Override
    public void addToSubscribeList(SubscribeContent subContent) {
        if (!getSubFailedTopics().contains(subContent)) {
            subscribeContentList.add(subContent);
        }
    }

    @Override
    public void addToUnsubscribeList(PushPullContentBase topic) {
        if(!getUnsubscribeFailedTopics().contains(topic)){
            unsubscribeContentList.add(topic);
        }
    }

    @Override
    public void removeFromPublishList(PublishContent pubDetails) {
        getPubFailedTopics().remove(pubDetails);
    }

    @Override
    public void removeFromSubscribeList(SubscribeContent subDetails) {
        getSubFailedTopics().remove(subDetails);
    }

    @Override
    public void removeFromUnsubscribeList(PushPullContentBase topic) {
        getUnsubscribeFailedTopics().remove(topic);
    }

    @Override
    public List<PublishContent> getPubFailedTopics() {
        return publishContentList;
    }

    @Override
    public List<SubscribeContent> getSubFailedTopics() {
        return subscribeContentList;
    }

    @Override
    public List<PushPullContentBase> getUnsubscribeFailedTopics() {
        return unsubscribeContentList;
    }

    @Override
    public void clearFailedPublishQueue() {
        publishContentList.clear();
    }

    @Override
    public void clearFailedSubscribeQueue() {
        subscribeContentList.clear();
    }

    @Override
    public void clearFailedUnSubscribeQueue() {
        unsubscribeContentList.clear();
    }

    @Override
    public void setPushPullManager(PushPullManager brokerManager){
        this.brokerManager = brokerManager;
    }

    @Override
    public void proceedWithFailedAction() {
        brokerManager.subscribe(subscribeContentList.toArray(new SubscribeContent[0]));
        brokerManager.publish(publishContentList.toArray(new PublishContent[0]));
        brokerManager.unsubscribe(unsubscribeContentList.toArray(new PushPullContentBase[0]));
    }
}
