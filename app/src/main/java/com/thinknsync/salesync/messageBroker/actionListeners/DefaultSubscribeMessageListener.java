package com.thinknsync.salesync.messageBroker.actionListeners;

import com.thinknsync.logger.Logger;
import com.thinknsync.observerhost.ObserverHostImpl;

import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

public class DefaultSubscribeMessageListener extends ObserverHostImpl<String> implements IMqttMessageListener {

    private String Tag = "mqtt-message";
    private Logger logger;

    public DefaultSubscribeMessageListener(Logger logger){
        this.logger = logger;
    }

    @Override
    public void messageArrived(String topic, MqttMessage message){
        logger.debugLog(Tag, "topic:" + topic + ",message:" + message.toString());
        notifyObservers(message.toString());
    }
}
