package com.thinknsync.salesync.messageBroker;

import com.thinknsync.salesync.messageBroker.pushPullContents.PublishContent;
import com.thinknsync.salesync.messageBroker.pushPullContents.PushPullContentBase;
import com.thinknsync.salesync.messageBroker.pushPullContents.SubscribeContent;

import java.util.List;

/**
 * Created by shuaib on 7/18/17.
 */

public interface PushPullFailedContentHolder {
    void addToPublishList(PublishContent[] pubContents);
    void addToPublishList(PublishContent pubContent);
    void addToSubscribeList(SubscribeContent[] subContents);
    void addToSubscribeList(SubscribeContent subContent);
    void addToUnsubscribeList(PushPullContentBase topic);
    void removeFromPublishList(PublishContent pubDetails);
    void removeFromSubscribeList(SubscribeContent subDetails);
    void removeFromUnsubscribeList(PushPullContentBase topic);
    List<PublishContent> getPubFailedTopics();
    List<SubscribeContent> getSubFailedTopics();
    List<PushPullContentBase> getUnsubscribeFailedTopics();
    void clearFailedPublishQueue();
    void clearFailedSubscribeQueue();
    void clearFailedUnSubscribeQueue();
}
