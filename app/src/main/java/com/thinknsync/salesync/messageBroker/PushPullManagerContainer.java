package com.thinknsync.salesync.messageBroker;

public interface PushPullManagerContainer {
    void setupPushPullManager();
    PushPullManager getPushPullManager();
}
