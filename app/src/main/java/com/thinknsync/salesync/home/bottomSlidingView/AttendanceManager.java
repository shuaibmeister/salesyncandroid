package com.thinknsync.salesync.home.bottomSlidingView;

import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.positionmanager.TrackingData;

public interface AttendanceManager {
    AttendanceStatus getAttendanceStatusOfUser();
    void sendAttendanceSubmitRequest(TrackingData currentLocation, ApiResponseActions responseActions);
    void sendAttendanceOutRequest(TrackingData currentLocation, ApiResponseActions responseActions);
    void isSignInRequiredToday(TypedObserver<Boolean> responseObserver);

    enum AttendanceStatus {
        NOT_SUBMITTED(0, "Not Submitted"),
        SUBMITTED(1, "Submitted"),
        SIGNED_OUT_AFTER_SIGN_IN(2, "Signed Out"),
        NULL(-1, "UNDEFINED");

        private int id;
        private String text;

        AttendanceStatus(int id, String text) {
            this.id = id;
            this.text = text;
        }

        public int getId() {
            return id;
        }

        public String getText() {
            return text;
        }

        public static AttendanceStatus getStatusFromId(int id) {
            for (AttendanceManager.AttendanceStatus statusType : AttendanceManager.AttendanceStatus.values()) {
                if (statusType.getId() == id) {
                    return statusType;
                }
            }
            return NULL;
        }
    }
}
