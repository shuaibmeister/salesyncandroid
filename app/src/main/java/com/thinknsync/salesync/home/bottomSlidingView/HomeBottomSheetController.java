package com.thinknsync.salesync.home.bottomSlidingView;

import android.content.Context;

import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.dialogs.DialogInterface;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.positionmanager.GpsPositionManager;
import com.thinknsync.positionmanager.TrackingData;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.commons.utils.DateTimeUtils;
import com.thinknsync.salesync.commons.utils.timer.RunTimer;
import com.thinknsync.salesync.commons.utils.timer.TimerImpl;
import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class HomeBottomSheetController implements HomeBottomSlidingSheetContract.HomeBottomSheetController {

    private HomeBottomSlidingSheetContract.HomeBottomSheetView homeBottomSheetView;
    private UserInterface loggedInUser;
    private ObjectConstructor objectConstructor;
    private AndroidContextWrapper contextWrapper;
    private AttendanceManager attendanceManager;
    private RunTimer timer;

    public HomeBottomSheetController(final ObjectConstructor objectConstructor, final AndroidContextWrapper contextWrapper,
                                     HomeBottomSlidingSheetContract.HomeBottomSheetView homeBottomSheetView){
        setViewReference(homeBottomSheetView);
        this.objectConstructor = objectConstructor;
        this.contextWrapper = contextWrapper;
        loggedInUser = objectConstructor.getUserRepository().getLoggedInUser();
        attendanceManager = objectConstructor.getAttendanceManager(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
        }});
    }

    @Override
    public void setAttendanceCard() {
        performActionsBasedOnStatus(attendanceManager.getAttendanceStatusOfUser());
    }

    private void performActionsBasedOnStatus(AttendanceManager.AttendanceStatus attendanceStatus){
        switch (attendanceStatus){
            case NOT_SUBMITTED:
                homeBottomSheetView.setAttendanceNotSubmitted();
                homeBottomSheetView.setDateTimeNow(new DateTimeUtils().getDefaultDateTimeNow());
                break;
            case SUBMITTED:
                homeBottomSheetView.setAttendanceSubmitted();
                startAttendanceTimer();
                break;
            case SIGNED_OUT_AFTER_SIGN_IN:
                homeBottomSheetView.setAttendanceSignedOut();
                long attendanceDurationSec = (loggedInUser.getAttendanceOutDateTime().getTime() -
                        loggedInUser.getAttendanceInDateTime().getTime()) / 1000;
                homeBottomSheetView.setAttendanceTimerDateTime(attendanceDurationSec);
                break;
        }
//        homeBottomSheetView.notifyObservers(attendanceStatus);
    }

    @Override
    public void setTargetVsAchievementCard() {
        homeBottomSheetView.setTargetVsAchievement(loggedInUser.getTarget(), loggedInUser.getAchievement());
        homeBottomSheetView.setTargetVsAchievementProgressbar((int)(loggedInUser.getAchievement() / loggedInUser.getTarget()) * 100);
    }

    @Override
    public String getUserName() {
        return loggedInUser.getName();
    }

    @Override
    public void showAttendanceDialog() {
        Context context = contextWrapper.getFrameworkObject();
        String message = "";
        final AttendanceManager.AttendanceStatus attendanceStatus  = attendanceManager.getAttendanceStatusOfUser();

        switch (attendanceStatus){
            case NOT_SUBMITTED:
                message = context.getString(R.string.attendance_confirmation_message);
                break;
            case SUBMITTED:
                message = context.getString(R.string.attendance_out_confirmation_message);
                break;
            case SIGNED_OUT_AFTER_SIGN_IN:
                return;
        }

        showAttendanceInOutDialog(new TypedObserverImpl<Boolean>() {
            @Override
            public void update(Boolean aBoolean) {
                if (aBoolean) {
                    punchInOutAttendance(attendanceStatus == AttendanceManager.AttendanceStatus.SUBMITTED);
                }
            }
        }, message);
    }

    @Override
    public void startAttendanceTimer() {
        if(timer == null || !timer.isTimerRunning()) {
            DateTimeUtils dateTimeUtils = new DateTimeUtils();
            final long datetimeNow = dateTimeUtils.getDefaultDateTimeNow().getTime();
            long remainingTime = (dateTimeUtils.get11thHourDateTimeNow().getTime() - datetimeNow) / 1000;
            timer = new TimerImpl(remainingTime, 1);
            timer.setTimerStartDelay(1);
            timer.addToObservers(new TypedObserverImpl<Long>() {
                @Override
                public void update(Long aLong) {
                    long elapsedTimeSec = (datetimeNow - loggedInUser.getAttendanceInDateTime().getTime()) / 1000;
                    homeBottomSheetView.setAttendanceTimerDateTime(elapsedTimeSec + aLong);
                }
            });
            timer.startTimer();
        }
    }

    @Override
    public void stopAttendanceTimer() {
        if(timer != null){
            timer.stopTimer();
            timer = null;
        }
    }

    private void showAttendanceInOutDialog(final TypedObserver<Boolean> positiveActionObserver, String message){
        DialogInterface<Boolean> attendanceConfirmationDialog = objectConstructor.getBooleanDialog(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper.getFrameworkObject());
        }});
        attendanceConfirmationDialog.setDialogResponse(new DialogInterface.DialogResponse<Boolean>() {
            @Override
            public void onPositiveClicked(Boolean infoObject) {
                positiveActionObserver.update(infoObject);
            }

            @Override
            public void onNegativeClicked(Boolean infoObject) {
                positiveActionObserver.update(infoObject);
            }
        });
        attendanceConfirmationDialog.setMessage(message);
        attendanceConfirmationDialog.showDialog();
    }

    private void punchInOutAttendance(final boolean isAttendanceSubmitted){
        homeBottomSheetView.attendanceSubmitInProgress();
        GpsPositionManager positionManager = objectConstructor.getLocationManager(new HashMap<String, Object>() {{
            put(AndroidContextWrapper.tag, contextWrapper);
        }});

        positionManager.getLastLocation(new TypedObserverImpl<TrackingData>() {
            @Override
            public void update(TrackingData trackingData) {
                if(!isAttendanceSubmitted) {
                    sendAttendanceToServer(trackingData);
                } else {
                    submitAttendanceOutToServer(trackingData);
                }
            }
        });
    }

    private void sendAttendanceToServer(final TrackingData currentLocation) {
        attendanceManager.sendAttendanceSubmitRequest(currentLocation, new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException {
                homeBottomSheetView.onApiCallSuccess();
                performActionsBasedOnStatus(AttendanceManager.AttendanceStatus.SUBMITTED);
            }

            @Override
            public void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException {
                homeBottomSheetView.onApiCallFailure(responseJson.getString(ApiRequest.ApiFields.KEY_MESSAGE));
            }
        });
    }

    private void submitAttendanceOutToServer(final TrackingData currentLocation) {
        attendanceManager.sendAttendanceOutRequest(currentLocation, new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException {
                stopAttendanceTimer();
                performActionsBasedOnStatus(AttendanceManager.AttendanceStatus.SIGNED_OUT_AFTER_SIGN_IN);
            }

            @Override
            public void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException {
//                homeBottomSheetView.onApiCallFailure(responseJson.getString(ApiRequest.ApiFields.KEY_MESSAGE));
            }
        });
    }

    @Override
    public void setViewReference(HomeBottomSlidingSheetContract.HomeBottomSheetView bottomSheetView) {
        this.homeBottomSheetView = bottomSheetView;
    }
}
