package com.thinknsync.salesync.home.homeController;

import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.route.RouteInterface;
import com.thinknsync.salesync.database.repositories.route.RouteRepositoryInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import java.util.ArrayList;
import java.util.List;


public class RouteHelper {

    private RouteRepositoryInterface routeRepository;

    private List<RouteInterface> allRoutes;
    private List<RouteInterface> todaysRoutes;
    private RouteInterface selectedRoute;

    public RouteHelper(ObjectConstructor objectConstructor) {
        this.routeRepository = objectConstructor.getRouteRepository();
    }

    RouteInterface getSelectedRoute(){
        return selectedRoute;
    }

    void setSelectedRoute(RouteInterface route){
        this.selectedRoute = route;
    }

    List<RouteInterface> getAllRoutes() {
        if(allRoutes == null) {
            allRoutes = routeRepository.getAll();
        }
        return allRoutes;
    }

    private List<RouteInterface> getTodaysRoutes() {
        if(todaysRoutes == null) {
            todaysRoutes = routeRepository.getRouteWithCondition(true);
        }
        return todaysRoutes;
    }

    RouteInterface getTodaysFirstRoute() {
        try {
            return getTodaysRoutes().get(0);
        } catch (IndexOutOfBoundsException e){
            e.printStackTrace();
            return routeRepository.getNewEntity();
        }
    }

    List<OutletInterface> getAllOutletsOfAllRoutes() {
        List<OutletInterface> outletsOfRoutes = new ArrayList<>();
        for (RouteInterface route : getAllRoutes()) {
            outletsOfRoutes.addAll(route.getOutletsOfRoute());
        }
        return outletsOfRoutes;
    }
}
