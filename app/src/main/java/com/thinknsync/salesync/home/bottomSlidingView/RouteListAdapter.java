package com.thinknsync.salesync.home.bottomSlidingView;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.thinknsync.listviewadapterhelper.BaseListViewAdapter;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.database.records.route.RouteInterface;

import java.util.List;

public class RouteListAdapter extends BaseListViewAdapter<RouteInterface> {

    public RouteListAdapter(Context context, List<RouteInterface> objects) {
        super(context, objects);
    }

    @Override
    protected void setValues(View view, RouteInterface routeInterface) {
        TextView titleText = view.findViewById(R.id.adapter_item_title);
        titleText.setText(routeInterface.getName());
    }

    @Override
    protected int getView() {
        return R.layout.basic_adapter_layout;
    }
}
