package com.thinknsync.salesync.home.unverifiedOutlets;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.thinknsync.listviewadapterhelper.searchable.BaseSearchableListViewAdapterImpl;
import com.thinknsync.listviewadapterhelper.searchable.SearchProcessImpl;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;

import java.util.List;

public class UnverifiedOutletsAdapter extends BaseSearchableListViewAdapterImpl<OutletInterface> {
    public UnverifiedOutletsAdapter(Context context, List<OutletInterface> objects) {
        super(context, objects, new SearchProcessImpl<>(objects));
    }

    @Override
    protected void setValues(View view, OutletInterface outletInterface) {
        TextView title = view.findViewById(R.id.title_text);
        TextView subtitle = view.findViewById(R.id.subtitle_text);
        title.setText(outletInterface.getName());
        subtitle.setText(outletInterface.getOutletAddress());
    }

    @Override
    protected int getView() {
        return R.layout.listview_item_default;
    }
}
