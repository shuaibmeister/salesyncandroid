package com.thinknsync.salesync.home.outletCreate;

import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.positionmanager.GpsPositionManager;
import com.thinknsync.positionmanager.TrackingData;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.commons.offlineSupport.SendOutletToServer;
import com.thinknsync.salesync.commons.offlineSupport.SendSingleToServer;
import com.thinknsync.salesync.commons.utils.dateTime.DateTimePicker;
import com.thinknsync.salesync.database.records.masterData.MasterData;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.route.RouteInterface;
import com.thinknsync.salesync.database.repositories.masterData.MasterDataRepositoryInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class NewOutletController implements NewOutletContract.Controller {

    private NewOutletContract.View view;
    private MasterDataRepositoryInterface masterDataRepository;
    private TrackingData outletLocation;

    public NewOutletController(NewOutletContract.View view) {
        setViewReference(view);
        masterDataRepository = view.getObjectConstructorInstance().getMasterDataRepository();
    }

    @Override
    public List<MasterData> getAllOutletTypes() {
        return masterDataRepository.getMasterDataOfType(MasterData.MasterDataType.OUTLET_TYPE);
    }

    @Override
    public List<MasterData> getAllOutletClassifications() {
        return masterDataRepository.getMasterDataOfType(MasterData.MasterDataType.OUTLET_CLASSIFICATION);
    }

    @Override
    public void sendNewOutletToServer(String ownerName, String ownerPhone, String ownerNid, String ownerDob,
                                      String ownerMarriageDateInput, String outletName, String outletAddress,
                                      MasterData outletType, MasterData outletClassification, String outletPic, String ownerPic) {
        if(outletLocation != null) {
            SendSingleToServer<OutletInterface> sendOutletToServer = new SendOutletToServer(view.getObjectConstructorInstance());
            sendOutletToServer.setData(getNewOutlet(ownerName, ownerPhone, ownerNid, ownerDob, ownerMarriageDateInput,
                    outletName, outletAddress, outletType, outletClassification, outletPic, ownerPic));
            sendOutletToServer.sendData(view.getContextWrapper(), new BaseResponseActions() {
                @Override
                public void onApiCallSuccess(int i, JSONObject jsonObject) throws JSONException {
                    view.onApiCallSuccess();
                }

                @Override
                public void onApiCallFailure(int i, JSONObject jsonObject) throws JSONException {
                    view.onApiCallFailure(jsonObject.getString(ApiRequest.ApiFields.KEY_MESSAGE));
                }
            });
        } else {
            view.onApiCallFailure(((AndroidContextWrapper)view.getContextWrapper()).getFrameworkObject().getString(R.string.waiting_for_location_message));
        }
    }

    private OutletInterface getNewOutlet(String ownerName, String ownerPhone, String ownerNid, String ownerDob,
                                         String ownerMarriageDateInput, String outletName, String outletAddress,
                                         MasterData outletType, MasterData outletClassification, String outletPic, String ownerPic) {
        OutletInterface newOutlet = view.getObjectConstructorInstance().getOutletRepository().getNewEntity();
        RouteInterface todaysRoute = view.getObjectConstructorInstance().getRouteRepository().getRouteWithCondition(true).get(0);
        newOutlet.setName(outletName);
        newOutlet.setOutletAddress(outletAddress);
        newOutlet.setParentRoute(todaysRoute);
        newOutlet.setOwnerName(ownerName);
        newOutlet.setOwnerPhone(ownerPhone);
        newOutlet.setOwnerNid(ownerNid);
        newOutlet.setOwnerDob(ownerDob);
        newOutlet.setOwnerMarriageDate(ownerMarriageDateInput);
        newOutlet.setOutletTypeId(outletType.getDataId());
        newOutlet.setOutletClassificationId(outletClassification.getDataId());
        newOutlet.setOwnerImage(ownerPic);
        newOutlet.setOutletImage(outletPic);
        newOutlet.setLat(outletLocation.getLat());
        newOutlet.setLon(outletLocation.getLon());
        return newOutlet;
    }

    @Override
    public void setViewReference(NewOutletContract.View view) {
        this.view = view;
    }

    @Override
    public void showTimePicker(TypedObserver<String> observer) {
        DateTimePicker dateTimePicker = view.getObjectConstructorInstance().getAndroidDateTimePicker(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, view.getContextWrapper().getFrameworkObject());
        }});
        dateTimePicker.setDateSeparator("-");
        dateTimePicker.allowFutureDates(false);
        dateTimePicker.showDatePicker(observer);
    }

    @Override
    public void initLocationService() {
        GpsPositionManager positionManager = view.getObjectConstructorInstance().getLocationManager(new HashMap<String, Object>() {{
            put(AndroidContextWrapper.tag, view.getContextWrapper());
        }});

        positionManager.getLastLocation(new TypedObserverImpl<TrackingData>() {
            @Override
            public void update(TrackingData trackingData) {
                outletLocation = trackingData;
            }
        });
    }

}
