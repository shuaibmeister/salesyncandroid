package com.thinknsync.salesync.home.unverifiedOutlets;

import com.thinknsync.salesync.base.BaseController;
import com.thinknsync.salesync.base.BaseView;
import com.thinknsync.salesync.base.viewInterfaces.InteractiveView;
import com.thinknsync.salesync.base.viewInterfaces.ViewInitialization;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;

import java.util.List;

public interface UnverifiedOutletsContract {
    interface View extends BaseView, InteractiveView, ViewInitialization.InitValues {
        String tag = "UnverifiedOutletsView";
    }

    interface Controller extends BaseController<View> {
        List<OutletInterface> getUnverifiedOutlets(int routeId);
    }
}
