package com.thinknsync.salesync.home;

import com.thinknsync.mapslib.mapWorks.MapsUiInterface;
import com.thinknsync.salesync.base.BaseController;
import com.thinknsync.salesync.base.BaseView;
import com.thinknsync.salesync.base.viewInterfaces.SearchableView;
import com.thinknsync.salesync.base.viewInterfaces.ViewInitialization;
import com.thinknsync.salesync.base.viewInterfaces.InteractiveView;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.route.RouteInterface;

import java.util.List;

public interface HomeContract {
    interface View extends BaseView, InteractiveView, MapsUiInterface.View, ViewInitialization.InitValues, SearchableView<OutletInterface> {
        void onMarkerClicked();
        void openOutletActivity(OutletInterface outlet);
        void outletClickedNoAttendance();
    }

    interface Controller extends BaseController<View>, MapsUiInterface.Controller<View> {
        void onOutletSelected(OutletInterface outlet);
        void gotToSelectedRoute(RouteInterface route);
        List<RouteInterface> getAllOtherRoutes();
        void plotRouteOutlets();
        void setMapZoomLevel();
        void redrawOutlet(OutletInterface outletClicked);
        String getUserName();
        void addOutletClickDialog();
        void removeOutletClickDialog();
        RouteInterface getTodaysRoute();
        void enableDisableMapInteraction(boolean isInteractionEnabled);
        void removeAllOutletsExceptRouteOutlets(RouteInterface selectedRoute);
        void setSelectedRoute(RouteInterface route);
        public RouteInterface getSelectedRoute();
    }
}
