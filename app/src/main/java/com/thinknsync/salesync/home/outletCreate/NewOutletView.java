package com.thinknsync.salesync.home.outletCreate;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;

import com.thinknsync.inputvalidation.InputValidation;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.base.views.BaseBottomNavigation;
import com.thinknsync.salesync.commons.osSpecifics.imageCapture.ImageCapture;
import com.thinknsync.salesync.commons.osSpecifics.imageCapture.ImageCaptureImpl;
import com.thinknsync.salesync.database.records.masterData.MasterData;
import com.thinknsync.salesync.uiElements.expandableView.SingleSelectionExpandableAdapter;
import com.thinknsync.salesync.uiElements.expandableView.SingleSelectionExpandedAdapter;
import com.thinknsync.salesync.uiElements.formElements.forms.FormContainerContract;
import com.thinknsync.salesync.uiElements.formElements.forms.FormContainerViewImpl;
import com.thinknsync.salesync.uiElements.formElements.inputs.ActionButton;
import com.thinknsync.salesync.uiElements.formElements.inputs.ActionView;
import com.thinknsync.salesync.uiElements.formElements.inputs.CircleImageHolder;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputField;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputValidationContainer;

import java.util.HashMap;
import java.util.List;

public class NewOutletView extends BaseBottomNavigation implements NewOutletContract.View {

    private InputField ownerName, ownerMobile, ownerNid, ownerDob, ownerMarriageDate, outletName, outletAddress;
    private CircleImageHolder outletImage, ownerImage, activeImageHolder;
    private ActionButton createOutletButton;
    private ExpandableListView outletTypeDropDown, outletClassificationDropDown;
    private SingleSelectionExpandableAdapter<MasterData> outletTypeSelectionAdapter, outletClassificationAdapter;

    private NewOutletContract.Controller controller;
    private ImageCapture imageCaptureImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewListeners();
        initValues();
        imageCaptureImpl = new ImageCaptureImpl(this);
        setupForm(new HashMap<InputValidationContainer, InputValidation.ValidationTypes>(){{
            put(outletName, InputValidation.ValidationTypes.TYPE_MANDATORY);
            put(outletAddress, InputValidation.ValidationTypes.TYPE_MANDATORY);
            put(ownerName, InputValidation.ValidationTypes.TYPE_MANDATORY);
            put(ownerMobile, InputValidation.ValidationTypes.TYPE_PHONE);
            put(ownerNid, InputValidation.ValidationTypes.TYPE_MANDATORY);
            put(ownerDob, InputValidation.ValidationTypes.TYPE_DATE);
            put(ownerMarriageDate, InputValidation.ValidationTypes.TYPE_DATE);
            put(outletTypeSelectionAdapter, InputValidation.ValidationTypes.TYPE_MANDATORY);
            put(outletClassificationAdapter, InputValidation.ValidationTypes.TYPE_MANDATORY);
            put(outletImage, InputValidation.ValidationTypes.TYPE_MANDATORY);
            put(ownerImage, InputValidation.ValidationTypes.TYPE_MANDATORY);
        }}, createOutletButton);
        controller.initLocationService();
    }

    @Override
    public void initValues() {
        initOutletTypes();
        initOutletClassifications();
    }

    private void initOutletTypes() {
        final List<MasterData> outletTypeMasterData = controller.getAllOutletTypes();
        HashMap<String, List<MasterData>> adapterMasterData = new HashMap<String, List<MasterData>>(){{
            put(getString(R.string.outlet_type_label), outletTypeMasterData);
        }};
        outletTypeSelectionAdapter = new SingleSelectionExpandedAdapter<>(this, adapterMasterData);
        outletTypeDropDown.setOnChildClickListener(outletTypeSelectionAdapter);
        outletTypeDropDown.setAdapter(outletTypeSelectionAdapter);
        outletTypeDropDown.expandGroup(0);
        outletTypeDropDown.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });
    }

    private void initOutletClassifications() {
        final List<MasterData> outletClassificationData = controller.getAllOutletClassifications();
        HashMap<String, List<MasterData>> adapterMasterData = new HashMap<String, List<MasterData>>(){{
            put(getString(R.string.outlet_classification_label), outletClassificationData);
        }};
        outletClassificationAdapter = new SingleSelectionExpandedAdapter<>(this, adapterMasterData);
        outletClassificationDropDown.setOnChildClickListener(outletClassificationAdapter);
        outletClassificationDropDown.setAdapter(outletClassificationAdapter);
        outletClassificationDropDown.expandGroup(0);
        outletClassificationDropDown.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_new_outlet;
    }

    @Override
    public AndroidContextWrapper getContextWrapper() {
        return (AndroidContextWrapper) getObjectConstructorInstance().getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, NewOutletView.this);
        }});
    }

    @Override
    public void initController() {
        controller = new NewOutletController(this);
    }

    @Override
    public void initViewListeners() {
        ownerDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.showTimePicker(new TypedObserverImpl<String>() {
                    @Override
                    public void update(String s) {
                        ownerDob.setText(s);
                    }
                });
            }
        });

        ownerMarriageDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.showTimePicker(new TypedObserverImpl<String>() {
                    @Override
                    public void update(String s) {
                        ownerMarriageDate.setText(s);
                    }
                });
            }
        });

        createOutletButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.sendNewOutletToServer(ownerName.getInput(), ownerMobile.getInput(), ownerNid.getInput(),
                        ownerDob.getInput(), ownerMarriageDate.getInput(), outletName.getInput(), outletAddress.getInput(),
                        outletTypeSelectionAdapter.getSelectedItem(), outletClassificationAdapter.getSelectedItem(),
                        outletImage.getScaledInput(), ownerImage.getScaledInput());
            }
        });

        outletImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeImageHolder = outletImage;
                imageCaptureImpl.openCamera();
            }
        });

        ownerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeImageHolder = ownerImage;
                imageCaptureImpl.openCamera();
            }
        });
    }

    @Override
    public void initView() {
        ownerName = findViewById(R.id.owner_name_edit);
        ownerMobile = findViewById(R.id.owner_mobile_edit);
        ownerNid = findViewById(R.id.owner_nid_edit);
        ownerDob = findViewById(R.id.owner_dob_edit);
        ownerMarriageDate = findViewById(R.id.owner_marriage_date_edit);
        outletName = findViewById(R.id.outlet_name_edit);
        outletAddress = findViewById(R.id.outlet_address_edit);

        outletTypeDropDown = findViewById(R.id.outlet_type_dropdown);
        outletClassificationDropDown = findViewById(R.id.outlet_classification_dropdown);

        outletImage = findViewById(R.id.outlet_image);
        ownerImage = findViewById(R.id.owner_image);

        createOutletButton = findViewById(R.id.create_outlet_button);
    }

    @Override
    public void setupForm(HashMap<InputValidationContainer, InputValidation.ValidationTypes> inputAndTypeMap, ActionView actionView) {
        FormContainerContract.Initializer formContainer = new FormContainerViewImpl(inputAndTypeMap, getObjectConstructorInstance(), actionView);
        formContainer.initForm();
    }

    @Override
    public void onApiCallSuccess(String... strings) {
        onBackPressed();
    }

    @Override
    public void onApiCallFailure(String... strings) {
        showShortMessage(strings[0]);
    }

    @Override
    public void onApiCallError(String... strings) {
        onApiCallFailure(strings);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == ImageCapture.ImageCaptureCode) {
                activeImageHolder.setImageBitmap((Bitmap) data.getExtras().get("data"));
            }
        }
    }
}
