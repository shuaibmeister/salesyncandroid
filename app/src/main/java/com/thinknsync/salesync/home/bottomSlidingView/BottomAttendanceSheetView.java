package com.thinknsync.salesync.home.bottomSlidingView;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.AndroidViewWrapper;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.commons.utils.DateTimeUtils;
import com.thinknsync.salesync.commons.utils.animation.Animations;
import com.thinknsync.salesync.factory.ObjectConstructor;

import java.util.Date;
import java.util.HashMap;

public class BottomAttendanceSheetView implements HomeBottomSlidingSheetContract.AttendanceSheetView{

    private View attendanceCardView, attendanceSheetIcon, attendanceProgress, clockNow;
    private TextView dateText, timeText, attendanceDuration;
    private Context context;

    private ObjectConstructor objectConstructor;

    public BottomAttendanceSheetView(View attendanceCardView, ObjectConstructor objectConstructor){
        this.attendanceCardView = attendanceCardView;
        this.objectConstructor = objectConstructor;
        this.context = attendanceCardView.getContext();
        initView();
    }

    @Override
    public void attendanceSubmitInProgress() {
        attendanceSheetIcon.setVisibility(View.GONE);
        attendanceProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void setAttendanceSubmitted() {
        attendanceCardView.setBackgroundColor(context.getResources().getColor(R.color.colorBlue4));

       setTickMarkChecked();

        TextView attendanceText = attendanceCardView.findViewById(R.id.attendance_text);
        attendanceText.setTextColor(Color.WHITE);
        dateText.setTextColor(context.getResources().getColor(R.color.colorWhite_90Opacity));
        timeText.setTextColor(context.getResources().getColor(R.color.colorWhite_90Opacity));

        attendanceSheetIcon.setVisibility(View.VISIBLE);
        attendanceProgress.setVisibility(View.GONE);

        clockNow.setVisibility(View.GONE);
        attendanceDuration.setVisibility(View.VISIBLE);
    }

    @Override
    public void setAttendanceNotSubmitted() {
//        setCardToInitialState();
//        attendanceCardView.setBackgroundColor(context.getResources().getColor(R.color.colorBlue4));
//        attendanceCardView.findViewById(R.id.attendance_check_icon).setBackground(
//                context.getResources().getDrawable(R.drawable.shape_circle_fill_white));
//
//        ImageView tickMark = attendanceCardView.findViewById(R.id.checkbox_tick_mark);
//        tickMark.setImageResource(R.drawable.ic_check_blue);
//        TextView attendanceText = attendanceCardView.findViewById(R.id.attendance_text);
//        TextView dateText = attendanceCardView.findViewById(R.id.date_attendance);
//        TextView timeText = attendanceCardView.findViewById(R.id.time_attendance);
//        attendanceText.setTextColor(Color.WHITE);
//        dateText.setTextColor(context.getResources().getColor(R.color.colorWhite_90Opacity));
//        timeText.setTextColor(context.getResources().getColor(R.color.colorWhite_90Opacity));
    }

    private void setTickMarkChecked(){
        View attendanceCheckIcon = attendanceCardView.findViewById(R.id.attendance_check_icon);
        attendanceCheckIcon.setBackground(context.getResources().getDrawable(R.drawable.shape_circle_fill_white));
        ImageView tickMark = attendanceCardView.findViewById(R.id.checkbox_tick_mark);
        tickMark.setImageResource(R.drawable.ic_check_blue);
    }

    @Override
    public void setAttendanceSignedOut() {
        attendanceCardView.setBackgroundColor(Color.WHITE);
        clockNow.setVisibility(View.GONE);
        attendanceDuration.setVisibility(View.VISIBLE);
        setTickMarkChecked();
        attendanceSheetIcon.setVisibility(View.VISIBLE);
        attendanceProgress.setVisibility(View.GONE);
    }

    @Override
    public void setDateTimeNow(Date dateTime) {
        DateTimeUtils dateTimeUtils = new DateTimeUtils();
        String dateTimeString = dateTimeUtils.getDefaultDateTimeStringFromDate(dateTime);
        int[] dateParts = dateTimeUtils.getDayMonthYearArray(dateTimeString);

        dateText.setText(String.format(context.getString(R.string.date_text),
                dateParts[2], dateTimeUtils.getMonthNameFromDateString(dateTimeString), dateParts[0]));
        timeText.setText(dateTimeUtils.getAmPmFormattedTime(dateTime));
    }

    @Override
    public void setAttendanceTimerDateTime(long durationSec) {
        long hour = durationSec / (60 * 60);
        long min = durationSec / 60 - hour * 60;
        long sec = durationSec % 60;
        attendanceDuration.setText(String.format(context.getString(R.string.timer_text), hour, min, sec));
    }

    @Override
    public void showCardsAnimated() {
        final Animations animations = objectConstructor.getAnimationImpl(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, context);
        }});
        animations.setDefaultAnimationDuration(HomeBottomSlidingSheetContract.Animation.FADE_IN_DURATION);
        AndroidViewWrapper cardView = objectConstructor.getViewWrapper(new HashMap<String, Object>(){{
            put(AndroidViewWrapper.tag, attendanceCardView);
        }});
        animations.fadeInElement(cardView);
    }

    @Override
    public void initView() {
        attendanceSheetIcon = attendanceCardView.findViewById(R.id.attendance_status_icon);
        attendanceProgress = attendanceCardView.findViewById(R.id.attendance_submit_progress);
        dateText = attendanceCardView.findViewById(R.id.date_attendance);
        timeText = attendanceCardView.findViewById(R.id.time_attendance);
        clockNow = attendanceCardView.findViewById(R.id.clock_now_layout);
        attendanceDuration = attendanceCardView.findViewById(R.id.attendance_duration_clock);
    }
}
