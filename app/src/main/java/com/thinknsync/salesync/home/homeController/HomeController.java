package com.thinknsync.salesync.home.homeController;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.commons.osSpecifics.frameworkHelper.ApplicationInfo;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.route.RouteInterface;
import com.thinknsync.salesync.home.HomeContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class HomeController implements HomeContract.Controller {

    private HomeContract.View view;
    private List<OutletInterface> currentOutlets = new ArrayList<>();
    private RouteHelper routesHelper;
    private MapsHelper mapsHelper;

    public HomeController(final HomeContract.View view) {
        setViewReference(view);
        routesHelper = new RouteHelper(view.getObjectConstructorInstance());
        mapsHelper = new MapsHelper(view);
    }

    @Override
    public String getUserName(){
        return view.getObjectConstructorInstance().getUserRepository().getLoggedInUser().getName();
    }

    @Override
    public void addOutletClickDialog() {
        mapsHelper.addOutletClickDialog();
    }

    @Override
    public void removeOutletClickDialog() {
        mapsHelper.removeOutletClickDialog();
    }

    @Override
    public RouteInterface getTodaysRoute() {
        return routesHelper.getTodaysFirstRoute();
    }

    @Override
    public RouteInterface getSelectedRoute(){
        return routesHelper.getSelectedRoute();
    }

    @Override
    public void onOutletSelected(final OutletInterface outlet) {
        ApplicationInfo applicationInfo = view.getObjectConstructorInstance().getAndroidFrameworkHelper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, view.getContextWrapper());
        }});
        applicationInfo.hideSoftKeyboard();
        if(outlet.isLocationValid()) {
            mapsHelper.setMapZoomLevel(new ArrayList<OutletInterface>() {{
                add(outlet);
            }});
            mapsHelper.clickOnMarker(outlet);
        }
    }

    @Override
    public void gotToSelectedRoute(RouteInterface route) {
        mapsHelper.gotToSelectedRoute(route);
    }

    @Override
    public List<RouteInterface> getAllOtherRoutes(){
        List<RouteInterface> allRoutes = routesHelper.getAllRoutes();
        for (Iterator<RouteInterface> iterator = allRoutes.iterator(); iterator.hasNext();) {
            if(iterator.next().isTodaysRoute()) {
                iterator.remove();
            }
        }
        return allRoutes;
    }

    @Override
    public void plotRouteOutlets() {
        currentOutlets = routesHelper.getAllOutletsOfAllRoutes();
        mapsHelper.plotRouteOutlets(currentOutlets);
    }

    @Override
    public void setMapZoomLevel() {
        if(currentOutlets.size() > 0) {
            mapsHelper.setMapZoomLevel(currentOutlets);
        }
    }

    @Override
    public void redrawOutlet(OutletInterface outletClicked) {
        mapsHelper.redrawOutlet(outletClicked);
    }

    @Override
    public void setViewReference(HomeContract.View view) {
        this.view = view;
    }

    @Override
    public void initMapsManager() {
        mapsHelper.initMapsManager(view);
    }

    @Override
    public void enableDisableMapInteraction(boolean isInteractionEnabled) {
        mapsHelper.enableDisableMapInteraction(isInteractionEnabled);
    }

    @Override
    public void removeAllOutletsExceptRouteOutlets(RouteInterface selectedRoute) {
//        mapsHelper.removeMarkers(selectedRoute.getOutletsOfRoute());
        mapsHelper.clearMap();
        mapsHelper.plotRouteOutlets(selectedRoute.getOutletsOfRoute());
    }

    @Override
    public void setSelectedRoute(RouteInterface route) {
        routesHelper.setSelectedRoute(route);
    }
}
