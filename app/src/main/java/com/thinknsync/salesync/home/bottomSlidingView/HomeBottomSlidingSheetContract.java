package com.thinknsync.salesync.home.bottomSlidingView;

import com.thinknsync.apilib.ApiResponseView;
import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.salesync.base.viewInterfaces.BaseBottomSheetContract;
import com.thinknsync.salesync.base.viewInterfaces.InteractiveView;
import com.thinknsync.salesync.base.viewInterfaces.ViewInitialization;
import com.thinknsync.salesync.commons.utils.animation.Animations;

import java.util.Date;

public interface HomeBottomSlidingSheetContract {

    interface AttendanceSheetView extends ViewInitialization.InitView{
        void setAttendanceNotSubmitted();
        void attendanceSubmitInProgress();
        void setAttendanceSubmitted();
        void setAttendanceSignedOut();
        void setDateTimeNow(Date dateTime);
        void setAttendanceTimerDateTime(long durationSec);
        void showCardsAnimated();
    }

    interface TargetVsAchievementSheetView{
        void setTargetVsAchievement(double target, double achievement);
        void setTargetVsAchievementProgressbar(int progress);
        void showCardsAnimated();
    }

    interface HomeBottomSheetView extends BaseBottomSheetContract.BottomSheetView, ApiResponseView, AttendanceSheetView,
            TargetVsAchievementSheetView, InteractiveView, ObserverHost<AttendanceManager.AttendanceStatus> {
        String tag = "HomeBottomSlidingSheetContractView";

        void setTitleMessageAnimated(String message);
        void setupAttendanceCard();
        void stopAttendanceTimer();
    }

    interface HomeBottomSheetController extends BaseBottomSheetContract.BottomSheetController<HomeBottomSheetView> {
        void setAttendanceCard();
        void setTargetVsAchievementCard();
        String getUserName();
        void showAttendanceDialog();
        void startAttendanceTimer();
        void stopAttendanceTimer();
    }

    interface Animation {
        int FADE_IN_DURATION = 500;
    }
}
