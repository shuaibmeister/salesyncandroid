package com.thinknsync.salesync.home.homeController;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.thinknsync.mapslib.frameworkWrappers.MarketInfoDialog;
import com.thinknsync.mapslib.mapWorks.MapsManager;
import com.thinknsync.mapslib.mapWorks.markers.MarkerInformation;
import com.thinknsync.mapslib.mapWorks.markers.MarkerObject;
import com.thinknsync.mapslib.mapWorks.markers.markerDrawables.MarkerDrawableFromDynamicView;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.positionmanager.TrackingData;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.commons.utils.ImageUtils;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.repositories.outlet.OutletRepositoryInterface;
import com.thinknsync.salesync.home.HomeContract;

import java.util.List;

public class MapsDrawHelper {

    private OutletRepositoryInterface outletRepository;
    private MapsManager mapsManager;
    private OutletDialogHelper outletDialogHelper;
    private AndroidContextWrapper contextWrapper;

    MapsDrawHelper(MapsManager mapsManager, HomeContract.View view){
        this.mapsManager = mapsManager;
        this.contextWrapper = (AndroidContextWrapper) view.getContextWrapper();
        outletRepository = view.getObjectConstructorInstance().getOutletRepository();
        outletDialogHelper = new OutletDialogHelper(view);
    }

    void redrawOutlet(OutletInterface outletClicked){
        if(mapsManager != null) {
            OutletInterface modifiedOutlet = outletRepository.getOutletByOutletId(outletClicked.getDataId());
            MarkerInformation<OutletInterface> outletMarker = getOutletMarker(modifiedOutlet);
            outletMarker.setOnclickInfoDialog(outletDialogHelper.getOutletInfoDialog(modifiedOutlet, mapsManager));
            mapsManager.removeMarkerIfExists(outletMarker.getId());
            mapsManager.setPin(outletMarker);
        }
    }

    private MarkerInformation<OutletInterface> getOutletMarker(final OutletInterface outlet) {
        MarkerInformation<OutletInterface> marker = new MarkerObject<>(new TrackingData(outlet.getLat(), outlet.getLon()));
        marker.setId(outlet.getDataId());
        marker.setMarkerObject(outlet);
        marker.setSelectedDrawable(new MarkerDrawableFromDynamicView(getOutletMarkerView(outlet.getOwnerImage(), R.layout.map_marker_layout)));
        marker.setUnselectedDrawable(new MarkerDrawableFromDynamicView(getOutletMarkerView(outlet.getOwnerImage(), R.layout.map_marker_layout_without_image)));
//        MarketInfoDialog clickDialog = outletDialogHelper.getOutletClickDialog(outlet, mapsManager);
//        marker.setOnclickInfoDialog(clickDialog);
        return marker;
    }

    private View getOutletMarkerView(String centerImage, int resourceId) {
        View markerView = ((LayoutInflater)((Context) contextWrapper.getFrameworkObject())
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(resourceId, null);
        ImageView ownerImageView = markerView.findViewById(R.id.owner_image);

        if(ownerImageView != null){
            ownerImageView.setImageBitmap(new ImageUtils().getBimapFromBase64Image(centerImage));
        }
        return markerView;
    }

    void addOutletClickDialog() {
        List<MarkerInformation> allMarkers = mapsManager.getAllMarkers();
        for(MarkerInformation marker: allMarkers){
            marker.setOnclickInfoDialog(outletDialogHelper.getOutletInfoDialog((OutletInterface) marker.getMarkerObject(), mapsManager));
        }
    }

    void removeOutletClickDialog() {
        List<MarkerInformation> allMarkers = mapsManager.getAllMarkers();
        for(MarkerInformation marker: allMarkers){
            marker.setOnclickInfoDialog(outletDialogHelper.getOutletClickNotifierDialog());
        }
    }

    void drawOutletMarkers(OutletInterface[] outlets){
        for (OutletInterface outlet : outlets) {
            MarkerInformation<OutletInterface> marker = getOutletMarker(outlet);
            if(!mapsManager.doesMarkerExist(outlet.getDataId())) {
                mapsManager.setPin(marker);
            }
        }
    }

    void plotRouteOutlets(List<OutletInterface> currentOutlets) {
        drawOutletMarkers(currentOutlets.toArray(new OutletInterface[0]));
    }

    public void removeMarkers(List<Integer> outletsToKeep) {
        List<MarkerInformation> allMarkers = mapsManager.getAllMarkers();
        for(MarkerInformation marker: allMarkers){
            if(!outletsToKeep.contains(marker.getId())) {
                mapsManager.removeMarkerIfExists(marker.getId());
            }
        }
    }
}
