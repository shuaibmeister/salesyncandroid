package com.thinknsync.salesync.home.bottomSlidingView;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.AndroidViewWrapper;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.commons.utils.animation.Animations;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.uiElements.progressNotifier.CircularProgressbar;

import java.util.HashMap;

public class BottomTargetVsAchievementSheetView implements HomeBottomSlidingSheetContract.TargetVsAchievementSheetView{

    private View targetVsAchievementCard;
    private Context context;
    private ObjectConstructor objectConstructor;

    public BottomTargetVsAchievementSheetView(View targetVsAchievementCard, ObjectConstructor objectConstructor){
        this.targetVsAchievementCard = targetVsAchievementCard;
        this.context = targetVsAchievementCard.getContext();
        this.objectConstructor = objectConstructor;
    }

    @Override
    public void setTargetVsAchievement(double target, double achievement) {
        TextView tarVsAchTv = targetVsAchievementCard.findViewById(R.id.taregt_vs_achievement_value);
        tarVsAchTv.setText(String.format(context.getString(R.string.sale_target_value), String.valueOf(achievement), String.valueOf(target)));
    }

    @Override
    public void setTargetVsAchievementProgressbar(int progress) {
        CircularProgressbar progressbar = new CircularProgressbar(targetVsAchievementCard.findViewById(R.id.circular_progressbar));
        progressbar.notifyViewUpdate(progress);
    }

    @Override
    public void showCardsAnimated() {
        final Animations animations = objectConstructor.getAnimationImpl(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, context);
        }});
        animations.setDefaultAnimationDuration(HomeBottomSlidingSheetContract.Animation.FADE_IN_DURATION);

        AndroidViewWrapper cardView = objectConstructor.getViewWrapper(new HashMap<String, Object>(){{
            put(AndroidViewWrapper.tag, targetVsAchievementCard);
        }});
        animations.fadeInElement(cardView);
    }
}
