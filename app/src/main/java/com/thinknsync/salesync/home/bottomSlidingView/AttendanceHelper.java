package com.thinknsync.salesync.home.bottomSlidingView;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiDataObjectImpl;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.positionmanager.TrackingData;
import com.thinknsync.salesync.api.ApiCallArgumentKeys;
import com.thinknsync.salesync.commons.utils.DateTimeUtils;
import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AttendanceHelper implements AttendanceManager {

    private ObjectConstructor objectConstructor;
    private AndroidContextWrapper contextWrapper;
    private UserInterface loggedInUser;

    public AttendanceHelper(ObjectConstructor objectConstructor, AndroidContextWrapper contextWrapper){
        this.objectConstructor = objectConstructor;
        this.contextWrapper = contextWrapper;
        this.loggedInUser = objectConstructor.getUserRepository().getLoggedInUser();
    }

    @Override
    public AttendanceStatus getAttendanceStatusOfUser() {
        if(loggedInUser.getAttendanceInDateTime() == null){
            return AttendanceManager.AttendanceStatus.NOT_SUBMITTED;
        }
        DateTimeUtils dateUtils = new DateTimeUtils();
        int dateIntNow = dateUtils.getDateIntFromDefaultDate(dateUtils.getDefaultDateTimeNow());
        int dateIntAttendance = dateUtils.getDateIntFromDefaultDate(loggedInUser.getAttendanceInDateTime());
        if(dateIntNow != dateIntAttendance){
            return AttendanceManager.AttendanceStatus.NOT_SUBMITTED;
        }
        if(loggedInUser.getAttendanceOutDateTime() == null){
            return AttendanceManager.AttendanceStatus.SUBMITTED;
        }

        int dateOutAttendance = dateUtils.getDateIntFromDefaultDate(loggedInUser.getAttendanceOutDateTime());
        if(dateIntNow != dateOutAttendance){
            return AttendanceManager.AttendanceStatus.SUBMITTED;
        }

        return AttendanceManager.AttendanceStatus.SIGNED_OUT_AFTER_SIGN_IN;
    }

    @Override
    public void sendAttendanceSubmitRequest(final TrackingData currentLocation, final ApiResponseActions responseActions) {
        final ApiDataObject apiDataObject = new ApiDataObjectImpl(new HashMap<String, String>() {{
            put(ApiCallArgumentKeys.CommonArgs.USER_ID, String.valueOf(loggedInUser.getDataId()));
            put(ApiCallArgumentKeys.ImageArgs.USER_IMAGE, loggedInUser.getImage());
            put(ApiCallArgumentKeys.LocationApiKeys.LAT, String.valueOf(currentLocation.getLat()));
            put(ApiCallArgumentKeys.LocationApiKeys.LON, String.valueOf(currentLocation.getLon()));
        }});

        ApiRequest apiRequest = objectConstructor.submitAttendanceApiObject(new HashMap<String, Object>() {{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, new BaseResponseActions() {
                @Override
                public void onApiCallSuccess(int i, JSONObject jsonObject) throws JSONException {
                    loggedInUser.setAttendanceInDateTime(new DateTimeUtils().getDefaultDateTimeNow());
                    loggedInUser.setAttendanceStatusToday(AttendanceStatus.SUBMITTED);
                    objectConstructor.getUserRepository().save(loggedInUser);
                    responseActions.onApiCallSuccess(i, jsonObject);
                }
            });
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});

        apiRequest.setProgressDialog(null);
        apiRequest.sendRequest();
    }

    @Override
    public void sendAttendanceOutRequest(final TrackingData currentLocation, final ApiResponseActions responseActions) {
        final ApiDataObject apiDataObject = new ApiDataObjectImpl(new HashMap<String, String>() {{
            put(ApiCallArgumentKeys.CommonArgs.USER_ID, String.valueOf(loggedInUser.getDataId()));
            put(ApiCallArgumentKeys.LocationApiKeys.LAT, String.valueOf(currentLocation.getLat()));
            put(ApiCallArgumentKeys.LocationApiKeys.LON, String.valueOf(currentLocation.getLon()));
        }});

        ApiRequest apiRequest = objectConstructor.punchOutApiObject(new HashMap<String, Object>() {{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, new BaseResponseActions() {
                @Override
                public void onApiCallSuccess(int i, JSONObject jsonObject) throws JSONException {
                    loggedInUser.setAttendanceOutDateTime(new DateTimeUtils().getDefaultDateTimeNow());
                    loggedInUser.setAttendanceStatusToday(AttendanceStatus.SIGNED_OUT_AFTER_SIGN_IN);
                    objectConstructor.getUserRepository().save(loggedInUser);
                    responseActions.onApiCallSuccess(i, jsonObject);
                }
            });
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});

        apiRequest.setProgressDialog(null);
        apiRequest.sendRequest();
    }

    @Override
    public void isSignInRequiredToday(final TypedObserver<Boolean> responseObserver) {
//        getAttendanceStatusOfUser(new TypedObserverImpl<AttendanceStatus>() {
//            @Override
//            public void update(AttendanceStatus attendanceStatus) {
//                if(attendanceStatus == AttendanceStatus.SUBMITTED || attendanceStatus == AttendanceStatus.SIGNED_OUT_AFTER_SIGN_IN){
//                    responseObserver.update(false);
//                } else {
//                    responseObserver.update(true);
//                }
//            }
//        });
    }
}
