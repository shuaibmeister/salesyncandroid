package com.thinknsync.salesync.home.unverifiedOutlets;

import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.route.RouteInterface;

import java.util.ArrayList;
import java.util.List;

public class UnverifiedOutletsController implements UnverifiedOutletsContract.Controller{

    private UnverifiedOutletsContract.View view;

    public UnverifiedOutletsController(UnverifiedOutletsContract.View unverifiedOutletsView) {
        setViewReference(unverifiedOutletsView);
    }

    @Override
    public void setViewReference(UnverifiedOutletsContract.View view) {
        this.view = view;
    }

    @Override
    public List<OutletInterface> getUnverifiedOutlets(int routeId) {
        RouteInterface route = view.getObjectConstructorInstance().getRouteRepository().getRouteWithCondition(true).get(0);
        return getUnverifiedOutletsOfRoute(route);
    }

    private List<OutletInterface> getUnverifiedOutletsOfRoute(RouteInterface route) {
        List<OutletInterface> unverifiedOutlets = new ArrayList<>();
        for (OutletInterface outlet : route.getOutletsOfRoute()){
            if(!outlet.isVerified()){
                unverifiedOutlets.add(outlet);
            }
        }
        return unverifiedOutlets;
    }
}
