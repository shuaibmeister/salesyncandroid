package com.thinknsync.salesync.home.outletCreate;

import com.thinknsync.apilib.ApiResponseView;
import com.thinknsync.salesync.base.BaseController;
import com.thinknsync.salesync.base.BaseView;
import com.thinknsync.salesync.base.viewInterfaces.InteractiveView;
import com.thinknsync.salesync.base.viewInterfaces.ViewInitialization;
import com.thinknsync.salesync.commons.utils.dateTime.TimePickerContract;
import com.thinknsync.salesync.database.records.masterData.MasterData;
import com.thinknsync.salesync.uiElements.formElements.forms.FormContainerContract;

import java.util.List;

public interface NewOutletContract {
    interface View extends BaseView, InteractiveView, ViewInitialization.InitValues,
            FormContainerContract.FormActivity, ApiResponseView {
        String tag = "NewOutletView";
    }

    interface Controller extends BaseController<View>, TimePickerContract.Presenter {
        List<MasterData> getAllOutletTypes();
        List<MasterData> getAllOutletClassifications();
        void sendNewOutletToServer(String ownerName, String ownerPhone, String ownerNid, String ownerDob,
                                   String ownerMarriageDateInput, String outletName, String outletAddress,
                                   MasterData outletType, MasterData outletClassification, String outletPic, String ownerPic);
        void initLocationService();
    }
}
