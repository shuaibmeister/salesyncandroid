package com.thinknsync.salesync.home.unverifiedOutlets;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.thinknsync.listviewadapterhelper.searchable.SearchBoxInit;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.BundleWrapper;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.base.views.BaseBottomNavigation;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.route.RouteInterface;
import com.thinknsync.salesync.outlet.OutletView;

import java.util.HashMap;
import java.util.List;

public class UnverifiedOutletsView extends BaseBottomNavigation implements UnverifiedOutletsContract.View {

    private UnverifiedOutletsContract.Controller controller;
    private EditText searchView;
    private ListView outletList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initValues();
        initViewListeners();
    }

    private void initSearchBox(ArrayAdapter<OutletInterface> adapter) {
        SearchBoxInit.initSearchBox(searchView, adapter);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_unverified_outlet;
    }

    @Override
    public AndroidContextWrapper getContextWrapper() {
        return (AndroidContextWrapper) getObjectConstructorInstance().getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, UnverifiedOutletsView.this);
        }});
    }

    @Override
    public void initController() {
        controller = new UnverifiedOutletsController(this);
    }

    @Override
    public void initViewListeners() {
        outletList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                OutletInterface outletClicked = (OutletInterface) parent.getAdapter().getItem(position);
                final Bundle b = new Bundle();
                b.putInt(OutletInterface.key_outlet_id, outletClicked.getDataId());
                startActivity(OutletView.class, (BundleWrapper) getObjectConstructorInstance().getBundleWrapper(new HashMap<String, Object>(){{
                    put(BundleWrapper.tag, b);
                }}));
                setActivitySlideFromRightSideAnimation();
            }
        });
    }

    @Override
    public void initView() {
        searchView = findViewById(R.id.search_text);
        outletList = findViewById(R.id.unverified_outlet_list);
    }

    @Override
    public void initValues() {
        if(getIntent().getExtras() != null){
            int routeId = getIntent().getExtras().getInt(RouteInterface.key_route_id);
            List<OutletInterface> unverifiedOutlets = controller.getUnverifiedOutlets(routeId);
            ArrayAdapter<OutletInterface> adapter = new UnverifiedOutletsAdapter(this, unverifiedOutlets);
            outletList.setAdapter(adapter);
            initSearchBox(adapter);
        }
    }
}
