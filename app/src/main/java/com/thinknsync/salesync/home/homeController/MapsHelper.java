package com.thinknsync.salesync.home.homeController;

import com.google.android.gms.maps.GoogleMap;
import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiDataObjectImpl;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.logger.Logger;
import com.thinknsync.mapslib.MapsOperation;
import com.thinknsync.mapslib.mapWorks.MapsManager;
import com.thinknsync.mapslib.mapWorks.markers.MarkerInformation;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.positionmanager.TrackingData;
import com.thinknsync.salesync.api.ApiCallArgumentKeys;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.route.RouteInterface;
import com.thinknsync.salesync.database.repositories.outlet.OutletRepositoryInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.home.HomeContract;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class MapsHelper {

    private ObjectConstructor objectConstructor;
    private AndroidContextWrapper contextWrapper;
    private MapsManager mapsManager;
    private Logger logger;
    private MapsDrawHelper mapsMarkerDrawHelper;
    private List<OutletInterface> allOutlets;

    private final int[] default_view_padding = new int[]{150, 150, 150, 270};


    MapsHelper(HomeContract.View view) {
        this.objectConstructor = view.getObjectConstructorInstance();
        this.contextWrapper = (AndroidContextWrapper) view.getContextWrapper();
        logger = view.getObjectConstructorInstance().getLogger();
    }

    void initMapsManager(final HomeContract.View view) {
        mapsManager =  objectConstructor.getGoogleMapsManager(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(MapsOperation.tag, view.getMapsOperationUi());
        }});
        mapsMarkerDrawHelper = new MapsDrawHelper(mapsManager, view);
        setupMapCameraPauseListener();
        mapsManager.setOnMarkerClickAction(new TypedObserverImpl<MarkerInformation>() {
            @Override
            public void update(MarkerInformation markerInformation) {
                view.onMarkerClicked();
            }
        });
        enableDisableMapInteraction(false);
    }

    void enableDisableMapInteraction(boolean isEnabled){
        ((GoogleMap)mapsManager.getMap()).getUiSettings().setAllGesturesEnabled(isEnabled);
    }

    private void setupMapCameraPauseListener() {
        mapsManager.setCameraIdleListener(new TypedObserverImpl() {
            @Override
            public void update(Object o) {
                HashMap<Integer, OutletInterface> outletImages = new HashMap<>();
                for (OutletInterface outlet : allOutlets) {
                    if((outlet.getOwnerImage() == null || outlet.getOutletImage() == null)
                            && mapsManager.isLocationInScreen(outlet.getOutletLocation())){
                        logger.debugLog("outlets in screen and no image:", outlet.getName());
                        outletImages.put(outlet.getDataId(), outlet);
                    }
                }
                if(outletImages.size() > 0) {
                    fetchOutletImagesInBackground(outletImages);
                }

            }
        });
    }

    private void fetchOutletImagesInBackground(final HashMap<Integer, OutletInterface> outletImages) {
        final Collection<Integer> outletInterfaces = outletImages.keySet();

        final ApiDataObject apiDataObject = new ApiDataObjectImpl(new HashMap<String, Integer[]>() {{
            put(ApiCallArgumentKeys.ImageFetchApiKeys.OUTLET_IDS, outletInterfaces.toArray(new Integer[0]));
        }});

        final ApiResponseActions responseActions = new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int i, JSONObject jsonObject) throws JSONException {
                JSONArray outletImageJsonArray = jsonObject.getJSONArray(ApiRequest.ApiFields.KEY_DATA);
                OutletInterface[] outlets = new OutletInterface[outletImageJsonArray.length()];
                OutletRepositoryInterface outletRepository = objectConstructor.getOutletRepository();
                for (int j = 0; j < outletImageJsonArray.length(); j++) {
                    JSONObject outletImageObject = outletImageJsonArray.getJSONObject(j);
                    OutletInterface outlet = outletImages.get(outletImageObject.getInt("id"));
                    outlet.setOutletImage(outletImageObject.getString(OutletInterface.key_outlet_image));
                    outlet.setOwnerImage(outletImageObject.getString(OutletInterface.key_owner_image));
                    outlets[j] = outlet;
                }
                outletRepository.saveOutlets(outlets);
                mapsMarkerDrawHelper.drawOutletMarkers(outlets);
            }
        };

        ApiRequest apiRequest = objectConstructor.getOutletImageFetchApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, responseActions);
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});

        apiRequest.setProgressDialog(null);
        apiRequest.sendRequest();
    }

    void setMapZoomLevel(List<OutletInterface> currentOutlets){
        mapsManager.setZoomLevel(getLocationListOfOutlets(currentOutlets), default_view_padding);
    }

    @NotNull
    private TrackingData[] getLocationListOfOutlets(List<OutletInterface> outletList) {
        final List<TrackingData> locationsOfOutletsList = new ArrayList<>();
        for (int i = 0; i < outletList.size(); i++) {
            if(outletList.get(i).isLocationValid()) {
                locationsOfOutletsList.add(outletList.get(i).getOutletLocation());
            }
        }
        return locationsOfOutletsList.toArray(new TrackingData[0]);
    }

    void gotToSelectedRoute(RouteInterface route) {
        TrackingData[] outletLocations = getLocationListOfOutlets(route.getOutletsOfRoute());
        if(outletLocations.length > 0) {
            mapsManager.setZoomLevel(outletLocations, default_view_padding);
        }
    }

    void addOutletClickDialog() {
        mapsMarkerDrawHelper.addOutletClickDialog();
    }

    void removeOutletClickDialog() {
        mapsMarkerDrawHelper.removeOutletClickDialog();
    }

    void removeMarkers(List<OutletInterface> outletsToKeep){
        List<Integer> outletIds = new ArrayList<>();
        for (OutletInterface outlet : outletsToKeep) {
            outletIds.add(outlet.getDataId());
        }
        mapsMarkerDrawHelper.removeMarkers(outletIds);
    }

    void clickOnMarker(final OutletInterface outlet){
        mapsManager.onMarkerSelected(outlet.getDataId());
    }

    void plotRouteOutlets(List<OutletInterface> currentOutlets) {
        this.allOutlets = currentOutlets;
        mapsMarkerDrawHelper.plotRouteOutlets(allOutlets);
    }

    void redrawOutlet(OutletInterface outletClicked) {
        mapsMarkerDrawHelper.redrawOutlet(outletClicked);
    }

    public void clearMap() {
        mapsManager.clearMap();
    }
}
