package com.thinknsync.salesync.home.homeController;

import android.graphics.Color;

import com.thinknsync.dialogs.DialogInterface;
import com.thinknsync.mapslib.frameworkWrappers.MarketInfoDialog;
import com.thinknsync.mapslib.mapWorks.MapsManager;
import com.thinknsync.mapslib.mapsDrawer.PathDataFetcher;
import com.thinknsync.mapslib.mapsDrawer.PathProperties;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.positionmanager.GpsPositionManager;
import com.thinknsync.positionmanager.TrackingData;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.home.HomeContract;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public class OutletDialogHelper {

    private HomeContract.View view;

    public OutletDialogHelper(HomeContract.View view) {
        this.view = view;
    }


    MarketInfoDialog getOutletClickDialog(final OutletInterface outlet, MapsManager mapsManager){
//        AttendanceManager attendanceManager = view.getObjectConstructorInstance().getAttendanceManager(new HashMap<String, Object>(){{
//            put(AndroidContextWrapper.tag, view.getContextWrapper());
//        }});

//        if(attendanceManager.getAttendanceStatusOfUser() == AttendanceManager.AttendanceStatus.NOT_SUBMITTED) {
//            return getOutletClickNotifierDialog();
//        }

        return getOutletInfoDialog(outlet, mapsManager);
    }

    @NotNull
    MarketInfoDialog<OutletInterface> getOutletInfoDialog(final OutletInterface outlet, final MapsManager mapsManager) {
        final DialogInterface<OutletInterface> clickDialog = view.getObjectConstructorInstance().getOutletInfoDialog(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, view.getContextWrapper().getFrameworkObject());
            put(OutletInterface.tag, outlet);
        }});
        clickDialog.setDialogResponse(new DialogInterface.DialogResponse<OutletInterface>() {
            @Override
            public void onPositiveClicked(OutletInterface infoObject) {
                clickDialog.hideDialog();
                mapsManager.clearAllPaths();
                view.openOutletActivity(outlet);
            }

            @Override
            public void onNegativeClicked(final OutletInterface infoObject) {
                GpsPositionManager positionManager = view.getObjectConstructorInstance().getLocationManager(new HashMap<String, Object>() {{
                    put(AndroidContextWrapper.tag, view.getContextWrapper());
                }});
                final PathProperties pathProperties = new PathProperties();
                pathProperties.setLineColor(Color.BLUE);
                positionManager.getLastLocation(new TypedObserverImpl<TrackingData>() {
                    @Override
                    public void update(TrackingData trackingData) {
                        TrackingData[] sourceAndDestination = new TrackingData[]{trackingData, infoObject.getOutletLocation()};
                        mapsManager.drawPath(sourceAndDestination, PathDataFetcher.NavigationMode.ModeWalking, new TypedObserverImpl<Integer>() {
                            @Override
                            public void update(Integer pathId) {
                                clickDialog.hideDialog();
                            }
                        }, pathProperties);

                    }
                });
            }
        });
        return (MarketInfoDialog<OutletInterface>)clickDialog;
    }

    @NotNull
    MarketInfoDialog<Object> getOutletClickNotifierDialog() {
        final DialogInterface<Object> clickDialog = view.getObjectConstructorInstance().getClickNotificationDialog(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, view.getContextWrapper().getFrameworkObject());
        }});
        clickDialog.setDialogResponse(new DialogInterface.DialogResponse<Object>() {
            @Override
            public void onPositiveClicked(Object infoObject) {

            }

            @Override
            public void onNegativeClicked(Object infoObject) {
                view.outletClickedNoAttendance();
            }
        });

        return (MarketInfoDialog<Object>)clickDialog;
    }
}
