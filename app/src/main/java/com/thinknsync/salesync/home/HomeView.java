package com.thinknsync.salesync.home;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.thinknsync.mapslib.GoogleMapsCallback;
import com.thinknsync.mapslib.MapsOperation;
import com.thinknsync.mapslib.place.PlaceDataObject;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.AndroidViewWrapper;
import com.thinknsync.objectwrappers.BundleWrapper;
import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.positionmanager.TrackingData;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.base.views.BaseWithBottomNavigationBottomSheet;
import com.thinknsync.salesync.commons.utils.animation.Animations;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.route.RouteInterface;
import com.thinknsync.salesync.home.bottomSlidingView.HomeBottomSheetView;
import com.thinknsync.salesync.home.bottomSlidingView.HomeBottomSlidingSheetContract;
import com.thinknsync.salesync.home.bottomSlidingView.RouteListAdapter;
import com.thinknsync.salesync.home.homeController.HomeController;
import com.thinknsync.salesync.home.outletCreate.NewOutletView;
import com.thinknsync.salesync.home.unverifiedOutlets.UnverifiedOutletsView;
import com.thinknsync.salesync.outlet.OutletView;
import com.thinknsync.salesync.uiElements.autocompleteWidget.AutocompleteInitImpl;
import com.thinknsync.salesync.uiElements.autocompleteWidget.AutocompleteInitializer;
import com.thinknsync.salesync.uiElements.autocompleteWidget.AutocompleteAdapter;

import java.util.HashMap;
import java.util.List;

public class HomeView extends BaseWithBottomNavigationBottomSheet implements HomeContract.View {

    private HomeContract.Controller homeController;
    private GoogleMapsCallback googleMapsListener;
    private OutletInterface outletClicked;
    private HomeBottomSlidingSheetContract.HomeBottomSheetView homeBottomView;

    private Button todaysRouteButton, unverifiedOutletsButton, newOutletButton;
    private ListView otherRouteList;
    private View otherRoutesLayout, searchBoxView, outletActionView;

    private Animations animations;

    private final BottomSheetBehavior.BottomSheetCallback sheetCallbackToAdjustMargin = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            if(outletActionView != null && outletActionView.getVisibility() == View.VISIBLE) {
                float slidingViewCurrentHeight = bottomSheet.getHeight() * slideOffset;
                float adjustedButtonLayoutBottomMargin = getContextWrapper().getFrameworkObject().getResources()
                        .getDimension(R.dimen.home_bottom_sliding_view_peek_height) * slideOffset;
                outletActionView.setPadding(0, 0, 0, Math.round(slidingViewCurrentHeight - adjustedButtonLayoutBottomMargin));
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initMaps();
        animations = getObjectConstructorInstance().getAnimationImpl(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, HomeView.this);
        }});
        homeBottomView  = new HomeBottomSheetView(
                getObjectConstructorInstance(), getContextWrapper(), findViewById(R.id.bottom_sheet_home));
        setupBottomDialogView();
        initViewListeners();
    }

    private void setupBottomDialogView() {
        setupBottomSheetWithLayoutView(homeBottomView);
        addToObservers(new TypedObserverImpl<BottomSheetState>() {
            @Override
            public void update(BottomSheetState bottomSheetState) {
                AndroidViewWrapper nameWithBarWrapper = getObjectConstructorInstance().getViewWrapper(new HashMap<String, Object>(){{
                    put(AndroidViewWrapper.tag, findViewById(R.id.name_with_bar_layout));
                }});
                AndroidViewWrapper dashboardWithArrowWrapper = getObjectConstructorInstance().getViewWrapper(new HashMap<String, Object>(){{
                    put(AndroidViewWrapper.tag, findViewById(R.id.dashboard_with_arrow_layout));
                }});
                switch (bottomSheetState){
                    case STATE_EXPANDED:
                        animations.fadeOutElement(dashboardWithArrowWrapper);
                        animations.fadeInElement(nameWithBarWrapper);
                        sheetBehavior.removeBottomSheetCallback(sheetCallbackToAdjustMargin);
                        break;
                    case STATE_DRAGGING:
                        sheetBehavior.addBottomSheetCallback(sheetCallbackToAdjustMargin);
                        homeBottomView.setupAttendanceCard();
                        break;
                    case STATE_COLLAPSED:
                        animations.fadeOutElement(nameWithBarWrapper);
                        animations.fadeInElement(dashboardWithArrowWrapper);
                        homeBottomView.stopAttendanceTimer();
                        otherRouteList.setVisibility(View.GONE);
                        restoreBottomSheetHeightToDefault();
                        sheetBehavior.removeBottomSheetCallback(sheetCallbackToAdjustMargin);
                        if(outletActionView.getVisibility() == View.VISIBLE){
                            searchBoxView.setVisibility(View.VISIBLE);
                        }
                        break;
                }
            }
        });

//        homeBottomView.addToObservers(new TypedObserverImpl<AttendanceManager.AttendanceStatus>() {
//            @Override
//            public void update(final AttendanceManager.AttendanceStatus attendanceStatus) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        switch (attendanceStatus){
//                            case SUBMITTED:
//                                homeController.addOutletClickDialog();
//                                break;
//                            case SIGNED_OUT_AFTER_SIGN_IN:
//                                homeController.removeOutletClickDialog();
//                                break;
//                        }
//                    }
//                });
//            }
//        });
    }

    @Override
    public void initView() {
        setStatusBarColorTransparent();
        todaysRouteButton = findViewById(R.id.go_to_todays_route_button);
        otherRoutesLayout = findViewById(R.id.other_routes_layout);
        otherRouteList = findViewById(R.id.other_route_list);
        searchBoxView = findViewById(R.id.outlet_search_card);
        outletActionView = findViewById(R.id.outlet_actions);
        unverifiedOutletsButton = outletActionView.findViewById(R.id.non_verified_outlet_button);
        newOutletButton = outletActionView.findViewById(R.id.add_outlet_button);
    }

    @Override
    public void initController() {
        homeController = new HomeController(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    public AndroidContextWrapper getContextWrapper() {
        return (AndroidContextWrapper) getObjectConstructorInstance().getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, HomeView.this);
        }});
    }

    private void onRouteSelected(RouteInterface selectedRoute) {
        homeController.setSelectedRoute(selectedRoute);
        toggleBottomSheet();
        initSearchBox(selectedRoute.getOutletsOfRoute());
        outletActionView.setVisibility(View.VISIBLE);
        homeController.removeAllOutletsExceptRouteOutlets(selectedRoute);
        homeController.enableDisableMapInteraction(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                homeController.addOutletClickDialog();
            }
        }).start();
        homeController.gotToSelectedRoute(selectedRoute);
        outletActionView.setPadding(0, 0, 0, 0);
    }

    @Override
    public void initViewListeners() {
        todaysRouteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RouteInterface todaysRoute = homeController.getTodaysRoute();
                onRouteSelected(todaysRoute);
            }
        });

        otherRoutesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBottomSheetHeight(Resources.getSystem().getDisplayMetrics().heightPixels);
                otherRouteList.setVisibility(View.VISIBLE);
                ArrayAdapter<RouteInterface> adapter = new RouteListAdapter(HomeView.this, homeController.getAllOtherRoutes());
                otherRouteList.setAdapter(adapter);
                searchBoxView.setVisibility(View.INVISIBLE);
                otherRouteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        RouteInterface selectedRoute = (RouteInterface)parent.getAdapter().getItem(position);
                        onRouteSelected(selectedRoute);
                    }
                });
            }
        });

        newOutletButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Bundle b = new Bundle();
                b.putInt(RouteInterface.key_route_id, homeController.getSelectedRoute().getDataId());
                startActivity(NewOutletView.class, getObjectConstructorInstance().getBundleWrapper(new HashMap<String, Object>(){{
                    put(BundleWrapper.tag, b);
                }}));
                setActivitySlideFromRightSideAnimation();
            }
        });

        unverifiedOutletsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Bundle b = new Bundle();
                b.putInt(RouteInterface.key_route_id, homeController.getSelectedRoute().getDataId());
                startActivity(UnverifiedOutletsView.class, getObjectConstructorInstance().getBundleWrapper(new HashMap<String, Object>(){{
                    put(BundleWrapper.tag, b);
                }}));
                setActivitySlideFromRightSideAnimation();
            }
        });
    }


    @Override
    public void initSearchBox(List<OutletInterface> allOutletsOfRoute) {
        final ArrayAdapter<OutletInterface> outletAdapter = new AutocompleteAdapter<>(this, allOutletsOfRoute, R.layout.search_outlet_adapter_item);
        AutocompleteInitializer<OutletInterface> autocomplete = new AutocompleteInitImpl<>(searchBoxView);
        autocomplete.setupAutocompleteWithAdapter(outletAdapter);
        autocomplete.addToObservers(new TypedObserverImpl<OutletInterface>() {
            @Override
            public void update(OutletInterface outletInterface) {
                homeController.onOutletSelected(outletInterface);
            }
        });

        AndroidViewWrapper searchBoxViewWrapper = getObjectConstructorInstance().getViewWrapper(new HashMap<String, Object>(){{
            put(AndroidViewWrapper.tag, searchBoxView);
        }});
        animations.setDefaultAnimationDuration(600);
        animations.slideInFromTop(searchBoxViewWrapper);
    }

    @Override
    public void initMaps() {
        googleMapsListener = new GoogleMapsCallback(getContextWrapper(), this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(googleMapsListener);
    }

    @Override
    public void onMapsReady() {
        homeController.initMapsManager();
        initValues();
    }

    private void expandBottomDialog() {
        toggleBottomSheet();
        addToObservers(new TypedObserverImpl<BottomSheetState>() {
            @Override
            public void update(BottomSheetState bottomSheetState) {
                if (bottomSheetState == BottomSheetState.STATE_EXPANDED) {
                    homeBottomView.setupAttendanceCard();
                    homeBottomView.showCardsAnimated();
                }
            }

            @Override
            public void update(ObserverHost<BottomSheetState> observerHost, BottomSheetState bottomSheetState) {
                super.update(observerHost, bottomSheetState);
                observerHost.removeObserver(this);
            }
        });
    }

    @Override
    public void onMapsLoadComplete() {
        expandBottomDialog();
        homeController.setMapZoomLevel();
    }

    @Override
    public void updateCameraIdlePlace(PlaceDataObject placeDataObject) {

    }

    @Override
    public void onLocationReceived(TrackingData trackingData) {
//        System.out.println(trackingData.toString());
    }

    @Override
    public MapsOperation getMapsOperationUi() {
        return googleMapsListener;
    }

    @Override
    public void initValues() {
        homeController.plotRouteOutlets();
        todaysRouteButton.setText(String.format(getString(R.string.go_to_todays_route_label),
                homeController.getTodaysRoute().getName()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(outletClicked != null) {
            homeController.redrawOutlet(outletClicked);
        }
    }

    @Override
    public void onMarkerClicked() {
        if(getCurrentSheetState() == BottomSheetState.STATE_EXPANDED) {
            toggleBottomSheet();
        }
    }

    @Override
    public void openOutletActivity(OutletInterface outlet) {
        outletClicked = outlet;
        final Bundle b = new Bundle();
        b.putInt(OutletInterface.key_outlet_id, outlet.getDataId());
        startActivity(OutletView.class, (BundleWrapper) getObjectConstructorInstance().getBundleWrapper(new HashMap<String, Object>(){{
            put(BundleWrapper.tag, b);
        }}));
        setActivitySlideFromRightSideAnimation();
    }

    @Override
    public void outletClickedNoAttendance() {
        homeBottomView.setTitleMessageAnimated(getString(R.string.attendance_instruction_message));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                homeBottomView.setTitleMessageAnimated(String.format(getString(R.string.welcome_text),
                        homeController.getUserName()));
            }
        }, 5000);
    }
}