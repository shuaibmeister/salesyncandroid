package com.thinknsync.salesync.home.bottomSlidingView;

import android.app.Activity;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.AndroidViewWrapper;
import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.commons.utils.animation.Animations;
import com.thinknsync.salesync.factory.ObjectConstructor;

import java.util.Date;
import java.util.HashMap;

public class HomeBottomSheetView extends ObserverHostImpl<AttendanceManager.AttendanceStatus> implements HomeBottomSlidingSheetContract.HomeBottomSheetView {

    private HomeBottomSlidingSheetContract.AttendanceSheetView attendanceSheetViewImpl;
    private HomeBottomSlidingSheetContract.TargetVsAchievementSheetView targetVsAchievementSheetViewImpl;
    private View bottomSheetView;

    private TextView titleMessageText;
    private View attendanceCardView;

    private HomeBottomSlidingSheetContract.HomeBottomSheetController controller;
    private ObjectConstructor objectConstructor;
    private AndroidContextWrapper contextWrapper;

    public HomeBottomSheetView(ObjectConstructor objectConstructor, AndroidContextWrapper contextWrapper, View bottomSheetView){
        this.bottomSheetView = bottomSheetView;
        this.objectConstructor = objectConstructor;
        this.contextWrapper = contextWrapper;
        initView();
        attendanceSheetViewImpl = new BottomAttendanceSheetView(attendanceCardView, objectConstructor);
        targetVsAchievementSheetViewImpl = new BottomTargetVsAchievementSheetView(
                bottomSheetView.findViewById(R.id.target_vs_achievement_card_view), objectConstructor);
        initController();
        initValues();
        initViewListeners();
    }

    @Override
    public void setAttendanceSubmitted() {
        ((Activity)contextWrapper.getFrameworkObject()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                attendanceSheetViewImpl.setAttendanceSubmitted();
            }
        });
    }

    @Override
    public void setAttendanceSignedOut() {
        ((Activity)contextWrapper.getFrameworkObject()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                attendanceSheetViewImpl.setAttendanceSignedOut();
            }
        });
    }

    @Override
    public void setAttendanceNotSubmitted() {
        ((Activity)contextWrapper.getFrameworkObject()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                attendanceSheetViewImpl.setAttendanceNotSubmitted();
            }
        });
    }

    @Override
    public void setDateTimeNow(final Date dateTime) {
        ((Activity)contextWrapper.getFrameworkObject()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                attendanceSheetViewImpl.setDateTimeNow(dateTime);
            }
        });
    }

    @Override
    public void setAttendanceTimerDateTime(final long durationSec) {
        ((Activity)contextWrapper.getFrameworkObject()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                attendanceSheetViewImpl.setAttendanceTimerDateTime(durationSec);
            }
        });
    }

    @Override
    public void setTargetVsAchievement(double target, double achievement) {
        targetVsAchievementSheetViewImpl.setTargetVsAchievement(target, achievement);
    }

    @Override
    public void setTargetVsAchievementProgressbar(int progress) {
        targetVsAchievementSheetViewImpl.setTargetVsAchievementProgressbar(progress);
    }

    @Override
    public int getBottomSheetLayoutResourceId() {
        return this.bottomSheetView.getId();
    }

    @Override
    public void initValues() {
        controller.setTargetVsAchievementCard();
        titleMessageText.setText(
                String.format(contextWrapper.getFrameworkObject().getString(R.string.welcome_text), controller.getUserName()));
    }

    @Override
    public void setTitleMessageAnimated(final String message) {
        final Animations animations = objectConstructor.getAnimationImpl(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper.getFrameworkObject());
        }});
        final AndroidViewWrapper wrappedView = objectConstructor.getViewWrapper(new HashMap<String, Object>(){{
            put(AndroidViewWrapper.tag, titleMessageText);
        }});
        animations.fadeOutModifyAndFadeIn(wrappedView, new Runnable() {
            @Override
            public void run() {
                ((Activity)contextWrapper.getFrameworkObject()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        titleMessageText.setText(message);
                    }
                });
            }
        });
    }

    @Override
    public void setupAttendanceCard() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                controller.setAttendanceCard();
            }
        }).start();
    }

    @Override
    public void stopAttendanceTimer() {
        controller.stopAttendanceTimer();
    }

    @Override
    public void showCardsAnimated() {
        attendanceSheetViewImpl.showCardsAnimated();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                targetVsAchievementSheetViewImpl.showCardsAnimated();
            }
        }, 200);

        final Animations animations = objectConstructor.getAnimationImpl(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper.getFrameworkObject());
        }});
        animations.setDefaultAnimationDuration(HomeBottomSlidingSheetContract.Animation.FADE_IN_DURATION);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AndroidViewWrapper todaysRouteWrapper = objectConstructor.getViewWrapper(new HashMap<String, Object>(){{
                    put(AndroidViewWrapper.tag, bottomSheetView.findViewById(R.id.go_to_todays_route_button));
                }});
                animations.fadeInElement(todaysRouteWrapper);
            }
        }, 400);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AndroidViewWrapper otherRouteWrapper = objectConstructor.getViewWrapper(new HashMap<String, Object>(){{
                    put(AndroidViewWrapper.tag, bottomSheetView.findViewById(R.id.other_routes_layout));
                }});
                animations.fadeInElement(otherRouteWrapper);
            }
        }, 600);
    }

    @Override
    public void attendanceSubmitInProgress() {
        attendanceSheetViewImpl.attendanceSubmitInProgress();
    }

    @Override
    public void initViewListeners() {
        attendanceCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.showAttendanceDialog();
            }
        });
    }

    @Override
    public void initView() {
        titleMessageText = bottomSheetView.findViewById(R.id.welcome_username_text);
        attendanceCardView = bottomSheetView.findViewById(R.id.attendance_card_view);
    }

    @Override
    public void initController() {
        controller = new HomeBottomSheetController(objectConstructor, contextWrapper, this);
    }

    @Override
    public void onApiCallSuccess(String... strings) {
    }

    @Override
    public void onApiCallFailure(String... strings) {
    }

    @Override
    public void onApiCallError(String... strings) {
        onApiCallFailure(strings);
    }
}
