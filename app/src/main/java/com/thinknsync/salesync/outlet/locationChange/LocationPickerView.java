package com.thinknsync.salesync.outlet.locationChange;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.thinknsync.mapslib.GoogleMapsCallback;
import com.thinknsync.mapslib.MapsOperation;
import com.thinknsync.mapslib.place.PlaceDataObject;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.positionmanager.TrackingData;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.base.views.BaseActivity;

import java.util.HashMap;

public class LocationPickerView extends BaseActivity implements LocationPickerContract.View {

    private LocationPickerContract.Controller controller;
    private GoogleMapsCallback googleMapsListener;
    private PlaceDataObject centerPlace;

    private Button confirmLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initMaps();
        initViewListeners();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_location_picker;
    }

    @Override
    public AndroidContextWrapper getContextWrapper() {
        return (AndroidContextWrapper) getObjectConstructorInstance().getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, LocationPickerView.this);
        }});
    }

    @Override
    public void initController() {
        controller = new LocationPickerController(this);
    }

    @Override
    public void initViewListeners() {
        confirmLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(centerPlace != null){
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra(LocationPickerContract.LOCATION_PICKER_STRING, centerPlace.toString());
                    setResult(Activity.RESULT_OK, resultIntent);
                    onBackPressed();
                }
            }
        });
    }

    @Override
    public void initView() {
        confirmLocation = findViewById(R.id.confirm_location);
    }

    @Override
    public void initMaps() {
        googleMapsListener = new GoogleMapsCallback(getContextWrapper(), this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(googleMapsListener);
    }

    @Override
    public void updateCameraIdlePlace(PlaceDataObject placeDataObject) {
        centerPlace = placeDataObject;
    }

    @Override
    public void onLocationReceived(TrackingData trackingData) {

    }

    @Override
    public MapsOperation<GoogleMap> getMapsOperationUi() {
        return googleMapsListener;
    }

    @Override
    public void onMapsReady() {
        controller.initMapsManager();
    }

    @Override
    public void onMapsLoadComplete() {

    }
}
