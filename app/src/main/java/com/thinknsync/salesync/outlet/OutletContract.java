package com.thinknsync.salesync.outlet;

import com.thinknsync.salesync.base.BaseController;
import com.thinknsync.salesync.base.BaseView;
import com.thinknsync.salesync.base.viewInterfaces.InteractiveView;
import com.thinknsync.salesync.base.viewInterfaces.ViewInitialization;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;

import java.util.Date;
import java.util.List;

public interface OutletContract {
    interface View extends BaseView, InteractiveView, ViewInitialization.InitValues {
        String tag = "OutletView";

        void getOutOfOutlet();
    }

    interface Controller extends BaseController<View> {
        void setOutletFromOutletId(int outletJsonString);
        OutletInterface getSelectedOutlet();
        List<OutletActionEnum> getOutletActions();
        Date getOutletInTime();
    }
}
