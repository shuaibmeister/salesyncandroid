package com.thinknsync.salesync.outlet.bottomSlidingView;

import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.positionmanager.GpsPositionManager;
import com.thinknsync.positionmanager.TrackingData;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.commons.offlineSupport.SendSingleToServer;
import com.thinknsync.salesync.commons.offlineSupport.VerifyOutletToServer;
import com.thinknsync.salesync.commons.utils.dateTime.DateTimePicker;
import com.thinknsync.salesync.database.records.masterData.MasterData;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.repositories.outlet.OutletRepositoryInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class OutletBottomSheetController implements OutletBottomSheetContract.Controller {

    private ObjectConstructor objectConstructor;
    private AndroidContextWrapper contextWrapper;
    private OutletInterface selectedOutlet;
    private OutletBottomSheetContract.View view;
    private boolean setManually = false;
    private TrackingData outletLocation;

    public OutletBottomSheetController(ObjectConstructor objectConstructor, AndroidContextWrapper contextWrapper,
                                       OutletInterface outletInterface, OutletBottomSheetContract.View view) {
        this.objectConstructor = objectConstructor;
        this.contextWrapper = contextWrapper;
        this.selectedOutlet = outletInterface;
        setViewReference(view);
    }

    @Override
    public OutletInterface getSelectedOutlet() {
        return selectedOutlet;
    }

    @Override
    public List<MasterData> getAllOutletTypeMasterData() {
        return objectConstructor.getMasterDataRepository().getMasterDataOfType(MasterData.MasterDataType.OUTLET_TYPE);
    }

    @Override
    public List<MasterData> getAllOutletClassificationMasterData() {
        return objectConstructor.getMasterDataRepository().getMasterDataOfType(MasterData.MasterDataType.OUTLET_CLASSIFICATION);
    }

    @Override
    public void updateOutletInformation(String outletName, String ownerName, String ownerPhone, String ownerDob,
                                        String ownerMarriageDate, String ownerNid, int outletTypeId,
                                        int classificationId, String ownerImageString, String outletImageString) {
        setOutletProperties(outletName, ownerName, ownerPhone, ownerDob, ownerMarriageDate, ownerNid,
                outletTypeId, classificationId, ownerImageString, outletImageString);
        SendSingleToServer<OutletInterface> sendToServer = new VerifyOutletToServer(objectConstructor);
        sendToServer.setData(selectedOutlet);
        sendToServer.sendData(contextWrapper, new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int i, JSONObject jsonObject) throws JSONException {
                OutletRepositoryInterface outletRepositoryInterface = objectConstructor.getOutletRepository();
                outletRepositoryInterface.save(selectedOutlet);
                view.onApiCallSuccess();
            }

            @Override
            public void onApiCallFailure(int i, JSONObject jsonObject) throws JSONException {
                view.onApiCallFailure(jsonObject.getString(ApiRequest.ApiFields.KEY_MESSAGE));
            }
        });
    }

    private void setOutletProperties(String outletName, String ownerName, String ownerPhone, String ownerDob,
                                     String ownerMarriageDate, String ownerNid, int outletTypeId, int outletClassificationId,
                                     String ownerImageString, String outletImageString) {
        if(outletLocation != null) {
            selectedOutlet.setName(outletName);
            selectedOutlet.setOwnerName(ownerName);
            selectedOutlet.setOwnerPhone(ownerPhone);
            selectedOutlet.setOwnerDob(ownerDob);
            selectedOutlet.setOwnerMarriageDate(ownerMarriageDate);
            selectedOutlet.setOwnerNid(ownerNid);
            selectedOutlet.setOutletTypeId(outletTypeId);
            selectedOutlet.setOutletClassificationId(outletClassificationId);
            selectedOutlet.setOwnerImage(ownerImageString);
            selectedOutlet.setOutletImage(outletImageString);
            selectedOutlet.setVerified(true);
            selectedOutlet.setLat(outletLocation.getLat());
            selectedOutlet.setLon(outletLocation.getLon());
        } else {
            view.onApiCallFailure(contextWrapper.getFrameworkObject().getString(R.string.waiting_for_location_message));
        }
    }

    @Override
    public void initLocationService() {
        GpsPositionManager positionManager = objectConstructor.getLocationManager(new HashMap<String, Object>() {{
            put(AndroidContextWrapper.tag, contextWrapper);
        }});

        positionManager.getLastLocation(new TypedObserverImpl<TrackingData>() {
            @Override
            public void update(TrackingData trackingData) {
                if(!setManually) {
                    outletLocation = trackingData;
                }
            }
        });
    }

    @Override
    public void setOutletLocation(double lat, double lon) {
        outletLocation = new TrackingData(lat, lon);
        setManually = true;
    }

    @Override
    public void showTimePicker(TypedObserver<String> observer) {
        DateTimePicker dateTimePicker = objectConstructor.getAndroidDateTimePicker(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper.getFrameworkObject());
        }});
        dateTimePicker.setDateSeparator("-");
        dateTimePicker.allowFutureDates(false);
        dateTimePicker.showDatePicker(observer);
    }

    @Override
    public void setViewReference(OutletBottomSheetContract.View bottomSheetView) {
        this.view = bottomSheetView;
    }
}
