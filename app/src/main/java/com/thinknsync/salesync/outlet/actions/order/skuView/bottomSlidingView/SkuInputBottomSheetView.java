package com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.outlet.actions.order.skuView.SkuSelectionContract;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.promotionAdapters.DiscountAdapter;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.promotionAdapters.PromotionalSkuAdapter;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.promotionAdapters.UpdatePromotionNumber;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.skuInputUtils.SkuInput;
import com.thinknsync.salesync.uiElements.formElements.inputs.ActionButton;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputField;

import java.util.ArrayList;
import java.util.HashMap;

public class SkuInputBottomSheetView implements SkuInputContract.View {

    protected ObjectConstructor objectConstructor;
    protected OutletInterface outlet;
    protected SkuInterface sku;
    protected long outletInDateTime;
    private SkuSelectionContract.SkuInputAddView parentView;

    protected View bottomSheetView;

    protected InputField ctnInput, pcsInput, discountInput;
    protected ActionButton addToCartButton;
    protected TextView totalField;
    private ListView promoSkuListView;

    protected SkuInputContract.Controller controller;
    protected ArrayAdapter promotionAdapter;

    public SkuInputBottomSheetView(SkuSelectionContract.SkuInputAddView parentView, ObjectConstructor objectConstructor, OutletInterface selectedOutlet, SkuInterface sku,
                                   long outletInDateTime, View view) {
        this.parentView = parentView;
        this.objectConstructor = objectConstructor;
        this.outlet = selectedOutlet;
        this.sku = sku;
        this.outletInDateTime = outletInDateTime;
        this.bottomSheetView = view;
        initView();
        initController();
        initViewListeners();
        initValues();
    }

    @Override
    public int getBottomSheetLayoutResourceId() {
        return bottomSheetView.getId();
    }

    @Override
    public void initViewListeners() {
        setTextChangeObserver(ctnInput, new TypedObserverImpl<String>() {
            @Override
            public void update(String input) {
                controller.onQtyInput(Double.parseDouble(input), Double.parseDouble(getInput(pcsInput)));
            }
        });

        setTextChangeObserver(pcsInput, new TypedObserverImpl<String>() {
            @Override
            public void update(String input) {
                controller.onQtyInput(Double.parseDouble(getInput(ctnInput)), Double.parseDouble(input));
            }
        });

        setTextChangeObserver(discountInput, new TypedObserverImpl<String>() {
            @Override
            public void update(String input) {
                controller.onDiscountInputChanged(Double.parseDouble(input));
            }
        });

        addToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String totalValue = totalField.getText().toString();
                if(!totalValue.equals("0")){
                    controller.addItemToCart();
                }
            }
        });
    }

    private void setTextChangeObserver(final InputField inputField, final TypedObserver<String> changeObserver){
        inputField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String input = getInput(inputField);
                changeObserver.update(input);
            }
        });
    }

    private String getInput(InputField inputField){
        return inputField.getInput().equals("") ? "0" : inputField.getInput();
    }

    @Override
    public void initView() {
        ctnInput = bottomSheetView.findViewById(R.id.carton_input);
        pcsInput = bottomSheetView.findViewById(R.id.pcs_input);
        discountInput = bottomSheetView.findViewById(R.id.discount_input);
        totalField = bottomSheetView.findViewById(R.id.total_item_value);
        addToCartButton = bottomSheetView.findViewById(R.id.add_to_cart);
        promoSkuListView = bottomSheetView.findViewById(R.id.promo_sku_list);
    }

    @Override
    public void initValues() {
        resetInputFields();
        setTotalValue(0);
        initPromotionView();
    }

    @Override
    public void initController() {
        controller = new SkuInputBottomSheetController(this, objectConstructor, outlet, sku, outletInDateTime);
    }

    @Override
    public void setTotalValue(double total) {
        totalField.setText(String.format(bottomSheetView.getContext().getString(R.string.total_text), total));
    }

    @Override
    public void setDiscountValue(double discount) {
        discountInput.setText(String.format(bottomSheetView.getContext().getString(R.string.default_price_format), discount));
        setPromotionAdapterValue(discount);
    }

    private void setPromotionAdapterValue(double discount) {
        if(promotionAdapter != null) {
            ((UpdatePromotionNumber) promotionAdapter).updatePromotion(discount);
        }
    }

    @Override
    public void resetInputFields() {
        ctnInput.setText("");
        pcsInput.setText("");
        discountInput.setText("");
        discountInput.setEnabled(!controller.skuHasDiscountPromotion());
    }

    private void initPromotionView(){
        if(controller.skuHasPromotion()) {
            promoSkuListView.setVisibility(View.VISIBLE);
            if (controller.skuHasDiscountPromotion()) {
                promotionAdapter = new DiscountAdapter(bottomSheetView.getContext(), new ArrayList<Double>() {{
                    add(0.0);
                }});
            } else {
                final HashMap<SkuInterface, Integer> promotionSku0QtyMap = controller.getInitialPromotionSkuQtyMap();
                promotionAdapter = new PromotionalSkuAdapter(bottomSheetView.getContext(),
                        new ArrayList<HashMap<SkuInterface, Integer>>(){{
                            add(promotionSku0QtyMap);
                        }});
            }
            promoSkuListView.setAdapter(promotionAdapter);
        } else {
            promoSkuListView.setAdapter(null);
            promotionAdapter = null;
            promoSkuListView.setVisibility(View.GONE);
        }
    }

    @Override
    public void setPromotionSkuListView(final HashMap<SkuInterface, Integer> promoSkuQtyMap) {
        if(promoSkuQtyMap == null) {
            setPromotionAdapterValue(0);
        } else {
            setPromotionAdapterValue(promoSkuQtyMap.values().toArray(new Integer[0])[0]);
        }
    }

    @Override
    public void notifyInputComplete(SkuInput skuInput) {
        parentView.addSkuItem(skuInput);
    }
}
