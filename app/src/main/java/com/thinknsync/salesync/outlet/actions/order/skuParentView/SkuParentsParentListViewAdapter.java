package com.thinknsync.salesync.outlet.actions.order.skuParentView;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.thinknsync.listviewadapterhelper.BaseListViewAdapter;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.base.viewInterfaces.SearchableView;
import com.thinknsync.salesync.database.records.sku.SkuParentInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import java.util.ArrayList;
import java.util.List;

public class SkuParentsParentListViewAdapter extends BaseListViewAdapter<SkuParentInterface> {

    private TypedObserver<SkuParentInterface> itemClickObserver;
    private ObjectConstructor objectConstructor;
    private AndroidContextWrapper contextWrapper;

    private List<SkuParentInterface> allSkuParents;
    private int totalAdapterChildrenAdded;

    private SearchableView<SkuParentInterface> searchableView;

    public SkuParentsParentListViewAdapter(AndroidContextWrapper contextWrapper, List<SkuParentInterface> objects,
                                           TypedObserver<SkuParentInterface> itemClickObserver,
                                           ObjectConstructor objectConstructor, SearchableView<SkuParentInterface> searchableView) {
        super(contextWrapper.getFrameworkObject(), objects);
        this.itemClickObserver = itemClickObserver;
        this.objectConstructor = objectConstructor;
        this.searchableView = searchableView;
        this.contextWrapper = contextWrapper;
        this.allSkuParents = new ArrayList<>();
    }

    @Override
    protected void setValues(View view, SkuParentInterface skuParent) {
        TextView skuParentName = view.findViewById(R.id.sku_parent_title);
        skuParentName.setText(skuParent.getName());

        GridLayoutManager gridLayoutManager = new GridLayoutManager(view.getContext(), 2, GridLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = view.findViewById(R.id.sku_container);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(getSkuParentAdapter(skuParent));
    }

    private SkuParentRecyclerViewAdapter getSkuParentAdapter(SkuParentInterface skuParent){
        allSkuParents.addAll(skuParent.getChildren());
        initSearchBoxIfReady();
        SkuParentRecyclerViewAdapter recyclerViewAdapter = new SkuParentRecyclerViewAdapter(skuParent.getChildren(), objectConstructor, contextWrapper);
        recyclerViewAdapter.addToObservers(itemClickObserver);
        return recyclerViewAdapter;
    }

    private void initSearchBoxIfReady(){
        totalAdapterChildrenAdded++;
        if(totalAdapterChildrenAdded == objects.size()){
            searchableView.initSearchBox(allSkuParents);
        }
    }

    @Override
    protected int getView() {
        return R.layout.sku_parent_list_adapter_layout;
    }
}
