package com.thinknsync.salesync.outlet.bottomSlidingView;

import com.thinknsync.apilib.ApiResponseView;
import com.thinknsync.mapslib.place.PlaceDataObject;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.salesync.base.viewInterfaces.BaseBottomSheetContract;
import com.thinknsync.salesync.base.viewInterfaces.InteractiveView;
import com.thinknsync.salesync.commons.utils.dateTime.TimePickerContract;
import com.thinknsync.salesync.database.records.masterData.MasterData;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.uiElements.expandableView.BooleanEnum;
import com.thinknsync.salesync.uiElements.formElements.forms.FormContainerContract;

import java.util.List;

public interface OutletBottomSheetContract {
    interface View extends BaseBottomSheetContract.BottomSheetView, ApiResponseView, InteractiveView, FormContainerContract.FormActivity,
            TimePickerContract.View {
        String tag = "OutletBottomSheetContractView";

        void resetToInitialState();
        void sendCapturedPicture(FrameworkWrapper imageBitmap, String imageString);
        void sendUpdatedAddress(PlaceDataObject outletAddress);
    }

    interface Controller extends BaseBottomSheetContract.BottomSheetController<View>, TimePickerContract.Presenter {
        OutletInterface getSelectedOutlet();
        List<MasterData> getAllOutletTypeMasterData();
        List<MasterData> getAllOutletClassificationMasterData();
        void updateOutletInformation(String outletName, String ownerName, String ownerPhone, String ownerDob, String ownerMarriageDate, String ownerNid, int outletTypeId, int accountYesNo, String ownerImageString, String outletImageString);
        void initLocationService();
        void setOutletLocation(double lat, double lon);
    }
}
