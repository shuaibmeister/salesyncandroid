package com.thinknsync.salesync.outlet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.thinknsync.mapslib.place.GooglePlaceDataObject;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.BitmapWrapper;
import com.thinknsync.objectwrappers.BundleWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.base.views.BaseWithBottomNavigationBottomSheetAndBackButton;
import com.thinknsync.salesync.commons.osSpecifics.imageCapture.ImageCapture;
import com.thinknsync.salesync.commons.osSpecifics.imageCapture.ImageCaptureImpl;
import com.thinknsync.salesync.commons.utils.ImageUtils;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.outlet.actions.order.skuParentView.SkuParentSelectionView;
import com.thinknsync.salesync.outlet.bottomSlidingView.OutletBottomSheetContract;
import com.thinknsync.salesync.outlet.bottomSlidingView.OutletBottomSheetView;
import com.thinknsync.salesync.outlet.actions.exceptionSubmit.ExceptionBottomSheetContract;
import com.thinknsync.salesync.outlet.actions.exceptionSubmit.ExceptionBottomSheetView;
import com.thinknsync.salesync.outlet.locationChange.LocationPickerContract;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class OutletView extends BaseWithBottomNavigationBottomSheetAndBackButton implements OutletContract.View {

    private OutletContract.Controller controller;

    private View infoButton, reportButton;
    private ImageView ownerImage;
    private TextView outletName, outletAddress;
    private ListView outletActionsList;
    private OutletBottomSheetContract.View bottomSheetViewOutlet;

    private ImageCapture imageCaptureImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initValues();
        imageCaptureImpl = new ImageCaptureImpl(this);
        setValuesInView();
        initViewListeners();
    }

    private void setValuesInView() {
        OutletInterface selectedOutlet = controller.getSelectedOutlet();
        setHeaderTitle(selectedOutlet.getName());
        ownerImage.setImageBitmap(new ImageUtils().getBimapFromBase64Image(selectedOutlet.getOwnerImage()));
        outletName.setText(selectedOutlet.getName());
        outletAddress.setText(selectedOutlet.getOutletAddress());
        outletActionsList.setAdapter(new OutletViewAdapter(this, controller.getOutletActions()));
    }

    @Override
    public void initView() {
        infoButton = findViewById(R.id.outlet_info);
        reportButton = findViewById(R.id.outlet_report);

        ownerImage = findViewById(R.id.outlet_owner_image);
        outletName = findViewById(R.id.outlet_title);
        outletAddress = findViewById(R.id.outlet_address);
        outletActionsList = findViewById(R.id.outlet_action_listview);
    }

    @Override
    public void initController() {
        controller = new OutletController(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_outlet;
    }

    @Override
    public AndroidContextWrapper getContextWrapper() {
        return (AndroidContextWrapper) getObjectConstructorInstance().getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, OutletView.this);
        }});
    }

    @Override
    public void initViewListeners() {
        outletActionsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                OutletActionEnum selectedAction = (OutletActionEnum) adapterView.getAdapter().getItem(i);
                switch (selectedAction){
                    case EXCEPTION:
                        ExceptionBottomSheetContract.View bottomSheetViewException =
                                new ExceptionBottomSheetView(controller.getSelectedOutlet(), controller.getOutletInTime(),
                                        findViewById(R.id.bottom_sheet_exception), OutletView.this);
                        setupBottomSheetWithLayoutView(bottomSheetViewException);
                        toggleBottomSheet();
                        break;
                    case COLLECT_ORDER:
                        final Bundle bundle = new Bundle();
                        bundle.putInt(OutletInterface.key_outlet_id, controller.getSelectedOutlet().getDataId());
                        bundle.putLong(SalesOrderInterface.key_in_date_time, controller.getOutletInTime().getTime());
                        startActivity(SkuParentSelectionView.class, getObjectConstructorInstance().getBundleWrapper(new HashMap<String, Object>(){{
                            put(BundleWrapper.tag, bundle);
                        }}));
                        setActivitySlideFromRightSideAnimation();
                        break;
                    case DELIVERY_PRODUCT:
                        break;
                    case COLLECT_DUE_PAYMENT:
                        break;
                    case COLLECT_DAMAGED_PRODUCT:
                        break;
                }

            }
        });

        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetViewOutlet = new OutletBottomSheetView(getObjectConstructorInstance(),
                        controller.getSelectedOutlet(), imageCaptureImpl, findViewById(R.id.bottom_sheet_outlet));
                setupBottomSheetWithLayoutView(bottomSheetViewOutlet);
                toggleBottomSheet();
            }
        });

        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        addToObservers(new TypedObserverImpl<BottomSheetState>() {
            @Override
            public void update(BottomSheetState bottomSheetState) {
                if(bottomSheetViewOutlet != null && bottomSheetState == BottomSheetState.STATE_COLLAPSED){
                    bottomSheetViewOutlet.resetToInitialState();
                }
            }
        });
    }

    @Override
    public void initValues() {
        if(getIntent().getExtras() != null){
            controller.setOutletFromOutletId(getIntent().getExtras().getInt(OutletInterface.key_outlet_id));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null) {
            if (requestCode == ImageCapture.ImageCaptureCode) {
                FrameworkWrapper bitmapWrapper = getObjectConstructorInstance().getBitmapWrapper(new HashMap<String, Object>(){{
                    put(BitmapWrapper.tag, data.getExtras().get("data"));
                }});
                imageCaptureImpl.processCapturedImage(bitmapWrapper);
                bottomSheetViewOutlet.sendCapturedPicture(bitmapWrapper, imageCaptureImpl.getCapturedImage());
            } else if(requestCode == LocationPickerContract.LOCATION_PICKER_CODE && resultCode == Activity.RESULT_OK){
                try {
                    bottomSheetViewOutlet.sendUpdatedAddress(new GooglePlaceDataObject().fromJson(
                            new JSONObject(data.getStringExtra(LocationPickerContract.LOCATION_PICKER_STRING))));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void getOutOfOutlet() {
        onBackPressed();
    }
}
