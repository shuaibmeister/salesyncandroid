package com.thinknsync.salesync.outlet.locationChange;

import com.thinknsync.mapslib.mapWorks.MapsUiInterface;
import com.thinknsync.salesync.base.BaseController;
import com.thinknsync.salesync.base.BaseView;
import com.thinknsync.salesync.base.viewInterfaces.InteractiveView;

public interface LocationPickerContract {
    int LOCATION_PICKER_CODE = 1000;
    String LOCATION_PICKER_STRING = "CENTER_PLACE";

    interface View extends BaseView, InteractiveView, MapsUiInterface.View {
    }

    interface Controller extends BaseController<View>, MapsUiInterface.Controller<View> {
    }
}
