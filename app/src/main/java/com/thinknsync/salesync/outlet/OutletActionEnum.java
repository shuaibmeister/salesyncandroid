package com.thinknsync.salesync.outlet;

import com.thinknsync.salesync.R;

import java.util.Arrays;
import java.util.List;

public enum OutletActionEnum {
    COLLECT_ORDER(R.string.collect_order),
    DELIVERY_PRODUCT(R.string.deliver_product),
    COLLECT_DAMAGED_PRODUCT(R.string.collect_damaged_product),
    COLLECT_DUE_PAYMENT(R.string.collect_due_payment),
    EXCEPTION(R.string.exception_text);

    private int textId;

    OutletActionEnum(int textId) {
        this.textId = textId;
    }

    public int getTextId() {
        return textId;
    }

    public static List<OutletActionEnum> getAllOutletActionList(){
        return Arrays.asList(values());
    }

    public void processView(){

    }
}
