package com.thinknsync.salesync.outlet.actions.exceptionSubmit;

import android.view.View;
import android.widget.ExpandableListView;

import com.thinknsync.inputvalidation.InputValidation;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.database.records.masterData.MasterData;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.outlet.OutletContract;
import com.thinknsync.salesync.uiElements.expandableView.SingleSelectionExpandedAdapter;
import com.thinknsync.salesync.uiElements.expandableView.SingleSelectionExpandableAdapter;
import com.thinknsync.salesync.uiElements.formElements.forms.FormContainerContract;
import com.thinknsync.salesync.uiElements.formElements.forms.FormContainerViewImpl;
import com.thinknsync.salesync.uiElements.formElements.inputs.ActionButton;
import com.thinknsync.salesync.uiElements.formElements.inputs.ActionView;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputValidationContainer;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ExceptionBottomSheetView implements ExceptionBottomSheetContract.View {

    private ObjectConstructor objectConstructor;
    private OutletInterface selectedOutlet;
    private View bottomSlidingView;

    private OutletContract.View outletMainView;
    private ExceptionBottomSheetContract.Controller controller;
    private Date outletInDateTime;

    private ExpandableListView exceptionDropDown;
    private SingleSelectionExpandableAdapter<MasterData> exceptionDropDownAdapter;
    private ActionButton submitException;

    public ExceptionBottomSheetView(OutletInterface outletInterface, Date outletInDateTime, View view, OutletContract.View outletMainView){
        this.objectConstructor = outletMainView.getObjectConstructorInstance();
        this.selectedOutlet = outletInterface;
        this.bottomSlidingView = view;
        this.outletMainView = outletMainView;
        this.outletInDateTime = outletInDateTime;

        initView();
        initController();
        initViewListeners();
        initValues();
        setupForm(new HashMap<InputValidationContainer, InputValidation.ValidationTypes>(){{
            put(exceptionDropDownAdapter, InputValidation.ValidationTypes.TYPE_MANDATORY);
        }}, submitException);
    }

    @Override
    public int getBottomSheetLayoutResourceId() {
        return bottomSlidingView.getId();
    }

    @Override
    public void initController() {
        AndroidContextWrapper contextWrapper = (AndroidContextWrapper)objectConstructor.getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, bottomSlidingView.getContext());
        }});
        controller = new ExceptionBottomSheetController(objectConstructor, contextWrapper, selectedOutlet, outletInDateTime, this);
    }

    @Override
    public void initViewListeners() {
        submitException.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.sendExceptionToServer(exceptionDropDownAdapter.getSelectedItem());
            }
        });
    }

    @Override
    public void initView() {
        exceptionDropDown = bottomSlidingView.findViewById(R.id.exception_reason_dropdown);
        submitException = bottomSlidingView.findViewById(R.id.submit_exception);
    }

    @Override
    public void initValues() {
        final List<MasterData> exceptionReasons = controller.getAllExceptionReasons();
        exceptionDropDownAdapter = new SingleSelectionExpandedAdapter<>(bottomSlidingView.getContext(), new HashMap<String, List<MasterData>>(){{
            put(bottomSlidingView.getContext().getResources().getString(R.string.cancel_activity_headline_label), exceptionReasons);
        }}, exceptionReasons.get(0));
        exceptionDropDown.setOnChildClickListener(exceptionDropDownAdapter);
        exceptionDropDown.setAdapter(exceptionDropDownAdapter);
        exceptionDropDown.expandGroup(0);
        exceptionDropDown.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });
    }

    @Override
    public void setupForm(HashMap<InputValidationContainer, InputValidation.ValidationTypes> inputAndTypeMap, ActionView actionView) {
        FormContainerContract.Initializer formContainer = new FormContainerViewImpl(inputAndTypeMap, objectConstructor, actionView);
        formContainer.initForm();
    }

    @Override
    public void onApiCallSuccess(String... strings) {
        outletMainView.showShortMessage(strings[0]);
        outletMainView.getOutOfOutlet();
    }

    @Override
    public void onApiCallFailure(String... strings) {
        onApiCallSuccess(strings);
    }

    @Override
    public void onApiCallError(String... strings) {
        onApiCallFailure(strings);
    }
}
