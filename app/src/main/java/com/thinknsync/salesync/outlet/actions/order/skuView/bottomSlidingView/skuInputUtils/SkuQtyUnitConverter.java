package com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.skuInputUtils;

import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.database.records.sku.UomInterface;

import java.util.List;

public class SkuQtyUnitConverter implements SkuInputCtnPcsConverter {

    private SkuInterface sku;

    public SkuQtyUnitConverter(SkuInterface sku) {
        this.sku = sku;
    }

    @Override
    public SkuInterface getSku() {
        return sku;
    }

    @Override
    public void setSku(SkuInterface sku) {
        this.sku = sku;
    }

    @Override
    public double getPcsFromCtn(double orderQtyCtn) {
        return getCtnMultiplicationFactor() * orderQtyCtn;
    }

    @Override
    public double[] getCtnPcsFromPcs(double orderQtyPcs) {
        double[] ctnPcs = new double[]{0, orderQtyPcs};
        double multiplicationFactor = getCtnMultiplicationFactor();
        if(multiplicationFactor > 1){
            ctnPcs[0] = (int)(orderQtyPcs / multiplicationFactor);
            ctnPcs[1] = orderQtyPcs % multiplicationFactor;
        }
        return ctnPcs;
    }

    @Override
    public double getCtnMultiplicationFactor() {
        List<UomInterface> uoms = sku.getSkuUoms();
        for(UomInterface uom : uoms) {
            if(uom.getQty() > 1) {
                return uom.getQty();
            }
        }
        return 1;
    }
}
