package com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.skuInputUtils;

import androidx.annotation.Nullable;

import com.thinknsync.salesync.database.records.promotion.PromotionInterface;
import com.thinknsync.salesync.database.records.sku.SkuInterface;

import java.util.HashMap;

public interface PromotionResolver {
    void setPromotion(PromotionInterface promotion);
    PromotionInterface getPromotion();
    boolean isPromotionApplicable(double inputQty);
    @Nullable
    HashMap<SkuInterface, Integer> getFreeSkuAndQty(double inputQty);
    double getDiscountAmount(double inputQty);
}
