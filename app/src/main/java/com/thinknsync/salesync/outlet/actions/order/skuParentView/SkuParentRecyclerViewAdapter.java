package com.thinknsync.salesync.outlet.actions.order.skuParentView;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiDataObjectImpl;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.listviewadapterhelper.recyclerView.BaseRecyclerViewAdapter;
import com.thinknsync.listviewadapterhelper.recyclerView.BaseViewHolder;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.api.ApiCallArgumentKeys;
import com.thinknsync.salesync.commons.asyncOperation.DiscreetNotifier;
import com.thinknsync.salesync.outlet.actions.order.ImageFetchWithDelay;
import com.thinknsync.salesync.commons.utils.ImageUtils;
import com.thinknsync.salesync.database.records.sku.SkuParentInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class SkuParentRecyclerViewAdapter extends BaseRecyclerViewAdapter<SkuParentInterface, SkuParentRecyclerViewAdapter.SkuParentViewHolder> {

    private ObjectConstructor objectConstructor;
    private AndroidContextWrapper contextWrapper;
    private DiscreetNotifier<HashMap<Integer, SkuParentInterface>> imageFetchDiscreetWithDelay;

    public SkuParentRecyclerViewAdapter(List<SkuParentInterface> objectList, ObjectConstructor objectConstructor, AndroidContextWrapper contextWrapper) {
        super(objectList);
        this.objectConstructor = objectConstructor;
        this.contextWrapper = contextWrapper;
        setupImageFetchWithDelay();
    }

    private void setupImageFetchWithDelay() {
        imageFetchDiscreetWithDelay = new ImageFetchWithDelay<>();
        imageFetchDiscreetWithDelay.setDelay(1500);
        imageFetchDiscreetWithDelay.addToObservers(new TypedObserverImpl<HashMap<Integer, SkuParentInterface>>() {
            @Override
            public void update(HashMap<Integer, SkuParentInterface> skuParentsWithImageMap) {
                fetchParentImagesInBackground(skuParentsWithImageMap);
            }
        });
    }

    @Override
    protected SkuParentViewHolder getViewHolder(View view) {
        return new SkuParentViewHolder(view);
    }

    @Override
    protected int getView() {
        return R.layout.sku_parent_adapter_layout;
    }

    @Override
    protected void setValues(SkuParentViewHolder skuViewHolder, final SkuParentInterface skuParent) {
        Context context = (Context) skuViewHolder.getContextWrapper().getFrameworkObject();
        skuViewHolder.skuParentName.setText(skuParent.getName());
        skuViewHolder.skuCount.setText(String.format(context.getString(R.string.sku_count_text), skuParent.getChildSkus().size()));
        skuViewHolder.skuImage.setImageBitmap(new ImageUtils().getBimapFromBase64Image(skuParent.getImage()));

        skuViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyObservers(skuParent);
            }
        });

        fetchImageAsync(skuParent);
    }

    private void fetchImageAsync(final SkuParentInterface skuParent){
        imageFetchDiscreetWithDelay.startNotificationLoop(new HashMap<Integer, SkuParentInterface>(){{
            put(0, skuParent);
        }});
    }

    private void fetchParentImagesInBackground(final HashMap<Integer, SkuParentInterface> imageFetchIds) {
        final ApiDataObject apiDataObject = new ApiDataObjectImpl(new HashMap<String, Integer[]>() {{
            put(ApiCallArgumentKeys.ImageFetchApiKeys.PRODUCT_IDS, imageFetchIds.keySet().toArray(new Integer[0]));
        }});

        final ApiResponseActions responseActions = new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int i, JSONObject jsonObject) throws JSONException {
                JSONArray productImages = jsonObject.getJSONArray(ApiRequest.ApiFields.KEY_DATA);
                for (int j = 0; j < productImages.length(); j++) {
                    JSONObject productImageJson = productImages.getJSONObject(j);
                    imageFetchIds.get(productImageJson.getInt("id")).setImage(productImageJson
                            .getString(SkuParentInterface.key_image));
                }
                objectConstructor.getSkuRepository().saveSkuParents(imageFetchIds.values().toArray(new SkuParentInterface[0]));
                imageFetchIds.clear();
                notifyDataSetChanged();
            }
        };

        ApiRequest apiRequest = objectConstructor.getProductImageFetchApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, responseActions);
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});

        apiRequest.setProgressDialog(null);
        apiRequest.sendRequest();
    }

    class SkuParentViewHolder extends BaseViewHolder{
        TextView skuParentName, skuCount;
        ImageView skuImage;

        SkuParentViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @Override
        public void initView(FrameworkWrapper frameworkWrapper) {
            View view = (View)frameworkWrapper.getFrameworkObject();
            skuParentName = view.findViewById(R.id.sku_parent_name);
            skuCount = view.findViewById(R.id.sku_count);
            skuImage = view.findViewById(R.id.sku_parent_image);
        }
    }
}
