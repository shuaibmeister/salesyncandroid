package com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.promotionAdapters;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thinknsync.listviewadapterhelper.BaseListViewAdapter;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.commons.utils.ImageUtils;
import com.thinknsync.salesync.database.records.sku.SkuInterface;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PromotionalSkuAdapter extends BaseListViewAdapter<HashMap<SkuInterface, Integer>> implements UpdatePromotionNumber {

    public PromotionalSkuAdapter(Context context, List<HashMap<SkuInterface, Integer>> objects) {
        super(context, objects);
    }

    @Override
    protected void setValues(View view, HashMap<SkuInterface, Integer> skuQtyHashMap) {
        ImageView skuImage = view.findViewById(R.id.sku_image);
        TextView skuName = view.findViewById(R.id.sku_name);
        TextView qty = view.findViewById(R.id.sku_qty);

        Map.Entry<SkuInterface, Integer> entry = skuQtyHashMap.entrySet().iterator().next();
        skuImage.setImageBitmap(new ImageUtils().getBimapFromBase64Image(entry.getKey().getImage()));
        skuName.setText(entry.getKey().getName());
        qty.setText(String.format(getContext().getString(R.string.amount_text_int), entry.getValue()));
    }

    @Override
    protected int getView() {
        return R.layout.cardview_promotion_sku_adapter_layout;
    }

    @Override
    public void updatePromotion(double promotionNumber) {
        Map.Entry<SkuInterface, Integer> entry = objects.get(0).entrySet().iterator().next();
        entry.setValue((int) promotionNumber);
        notifyDataSetChanged();
    }
}
