package com.thinknsync.salesync.outlet;

import com.thinknsync.salesync.commons.utils.DateTimeUtils;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;

import java.util.Date;
import java.util.List;

public class OutletController implements OutletContract.Controller {

    private OutletContract.View outletView;
    private OutletInterface selectedOutlet;
    private Date outletInDateTime;

    public OutletController(OutletContract.View outletView) {
        setViewReference(outletView);
        outletInDateTime = new DateTimeUtils().getDefaultDateTimeNow();
    }

    @Override
    public void setViewReference(OutletContract.View view) {
        this.outletView = view;
    }

    @Override
    public void setOutletFromOutletId(int outletId){
        selectedOutlet = outletView.getObjectConstructorInstance().getOutletRepository().getOutletByOutletId(outletId);
    }

    @Override
    public OutletInterface getSelectedOutlet() {
        return selectedOutlet;
    }

    @Override
    public List<OutletActionEnum> getOutletActions() {
        return OutletActionEnum.getAllOutletActionList();
    }

    @Override
    public Date getOutletInTime() {
        return outletInDateTime;
    }
}
