package com.thinknsync.salesync.outlet.actions.order.skuView;

import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.database.records.sku.SkuParentInterface;

import java.util.List;

public class SkuSelectionController implements SkuSelectionContract.Controller {

    private SkuSelectionContract.View view;
    private OutletInterface outlet;
    private SkuParentInterface skuParent;
    private long outletInDateTime;

    public SkuSelectionController(SkuSelectionContract.View view) {
        setViewReference(view);
    }

    @Override
    public void setViewReference(SkuSelectionContract.View view) {
        this.view = view;
    }

    @Override
    public void setOutletFromId(int outletId) {
        this.outlet = view.getObjectConstructorInstance().getOutletRepository().getOutletByOutletId(outletId);
    }

    public OutletInterface getSelectedOutlet(){
        return this.outlet;
    }

    @Override
    public void setSkuParentFromId(int parentId) {
        this.skuParent = view.getObjectConstructorInstance().getSkuRepository().getSkuParentById(parentId);
    }

    @Override
    public List<SkuInterface> getSkuOfParent() {
        return skuParent.getChildSkus();
    }

    @Override
    public String getSkuParentName(){
        return skuParent.getName();
    }

    @Override
    public long getOutletInDateTime() {
        return outletInDateTime;
    }

    @Override
    public void setOutletInDateTime(long inDateTime) {
        this.outletInDateTime = inDateTime;
    }
}
