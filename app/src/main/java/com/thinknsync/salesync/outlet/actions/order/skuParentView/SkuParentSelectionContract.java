package com.thinknsync.salesync.outlet.actions.order.skuParentView;

import com.thinknsync.salesync.base.BaseController;
import com.thinknsync.salesync.base.BaseView;
import com.thinknsync.salesync.base.viewInterfaces.InteractiveView;
import com.thinknsync.salesync.base.viewInterfaces.SearchableView;
import com.thinknsync.salesync.base.viewInterfaces.ViewInitialization;
import com.thinknsync.salesync.database.records.sku.SkuParentInterface;

import java.util.List;

public interface SkuParentSelectionContract {
    interface View extends BaseView, InteractiveView, SearchableView<SkuParentInterface> {
        String tag = "SkuParentSelectionView";
    }

    interface Controller extends BaseController<View> {
        List<SkuParentInterface> getSkuParentsTopHierarchy();
    }
}
