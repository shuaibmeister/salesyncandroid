package com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView;

import com.thinknsync.salesync.commons.utils.DateTimeUtils;
import com.thinknsync.salesync.database.records.order.SalesOrderDetailsInterface;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.promotion.PromotionInterface;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.database.repositories.order.OrderRepositoryInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.skuInputUtils.SkuInput;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;


public class SkuInputBottomSheetController implements SkuInputContract.Controller {

    private SkuInputContract.View skuInputView;
    //skuInput multiple instance creates, for unknown reason. made static for now. needs further investigation
    protected static SkuInput skuInput;
    private OutletInterface outlet;
    private Date outletInDateTime;
    protected ObjectConstructor objectConstructor;

    public SkuInputBottomSheetController(SkuInputContract.View skuInputBottomSheetView, ObjectConstructor objectConstructor,
                                         OutletInterface outlet, SkuInterface sku, long outletInDateTime) {
        setViewReference(skuInputBottomSheetView);
        this.objectConstructor = objectConstructor;
        this.outlet = outlet;
        this.outletInDateTime = new Date(outletInDateTime);
        skuInput = new SkuInput(sku);
    }

    @Override
    public void setViewReference(SkuInputContract.View bottomSheetView) {
        this.skuInputView = bottomSheetView;
    }

    @Override
    public void onDiscountInputChanged(double input) {
        skuInput.setDiscount(input);
        skuInputView.setTotalValue(skuInput.getTotalWithDiscount());
    }

    @Override
    public void onQtyInput(double inputCtn, double inputPcs) {
        skuInput.setOrderQtyCtnPcs(inputCtn, inputPcs);
        double promoDiscount = skuInput.getDiscount();
        if(skuHasDiscountPromotion()) {
            skuInputView.setDiscountValue(promoDiscount);
        }
        if(skuHasFreeSkuPromotion()) {
            skuInputView.setPromotionSkuListView(skuInput.getPromotionSkuIfApplicable());
        }
        skuInputView.setTotalValue(skuInput.getTotalWithDiscount());
    }

    @Override
    public void addItemToCart() {
        OrderRepositoryInterface orderRepo = objectConstructor.getSalesOrderRepository();
        final SalesOrderInterface salesOrder = getSalesOrderOfOutlet(orderRepo);
        saveOrderWithDetails(orderRepo, salesOrder);

        outlet.setOrders(new ArrayList<SalesOrderInterface>(){{
            add(salesOrder);
        }});
        skuInputView.notifyInputComplete(skuInput);
    }

    @Override
    public boolean skuHasDiscountPromotion() {
        return skuHasPromotion() && skuInput.getSku().getSkuPromotion().getPromotionType() == PromotionInterface.PromotionType.CASH;
    }

    @Override
    public boolean skuHasFreeSkuPromotion() {
        return skuHasPromotion() && skuInput.getSku().getSkuPromotion().getPromotionType() == PromotionInterface.PromotionType.SKU;
    }

    @Override
    public boolean skuHasPromotion() {
        return skuInput.getSku().hasPromotion();
    }

    @Override
    public HashMap<SkuInterface, Integer> getInitialPromotionSkuQtyMap() {
        return new HashMap<SkuInterface, Integer>(){{
            put(skuInput.getSku(), 0);
        }};
    }

    private SalesOrderInterface getSalesOrderOfOutlet(OrderRepositoryInterface orderRepo) {
        if(outlet.getOrders().size() > 0){
            return outlet.getOrders().get(0);
        }
        DateTimeUtils dateTimeUtils = new DateTimeUtils();
        SalesOrderInterface salesOrder = orderRepo.getNewEntity();
        salesOrder.setSr(objectConstructor.getUserRepository().getLoggedInUser());
        salesOrder.setOutlet(outlet);
        salesOrder.setOrderDate(dateTimeUtils.getDefaultDateTimeStringNow());
        salesOrder.setOutletInDateTime(outletInDateTime);
        salesOrder.setOutletOutDateTime(dateTimeUtils.getDefaultDateTimeNow());
        return salesOrder;
    }

    private void saveOrderWithDetails(OrderRepositoryInterface orderRepo, SalesOrderInterface salesOrder) {
        SalesOrderDetailsInterface orderDetails = getOrderDetails(salesOrder.getSalesOrderDetails(), orderRepo);
        if(orderDetails.getId() == 0){
            saveNewSalesOrder(orderDetails, salesOrder, orderRepo);
        } else {
            setSoDetailsQtyDiscountAndSubtotal(orderDetails);
            orderRepo.saveOrderDetails(orderDetails);
        }
    }

    private void setSoDetailsQtyDiscountAndSubtotal(SalesOrderDetailsInterface soDetails) {
        soDetails.setQty(skuInput.getOrderQtyPcs());
        soDetails.setDiscount(skuInput.getDiscount());
        soDetails.setSubTotal(skuInput.getTotal());
    }

    private void saveNewSalesOrder(final SalesOrderDetailsInterface soDetails, SalesOrderInterface salesOrder, OrderRepositoryInterface orderRepo) {
        Date now = new DateTimeUtils().getDefaultDateTimeNow();
        soDetails.setParentOrder(salesOrder);
        soDetails.setSku(skuInput.getSku());
        soDetails.setCreationDateTime(now);
        soDetails.setUpdateDateTime(now);
        setSoDetailsQtyDiscountAndSubtotal(soDetails);
        if(skuHasDiscountPromotion()){
            soDetails.setPromotion(skuInput.getSku().getSkuPromotion());
        }
        salesOrder.setSalesOrderDetails(new ArrayList<SalesOrderDetailsInterface>(){{
            add(soDetails);
        }});
        if(skuHasFreeSkuPromotion()){
            SalesOrderDetailsInterface orderDetails = getPromotionOrderLine(salesOrder, orderRepo, now);
            if(orderDetails != null) {
                salesOrder.getSalesOrderDetails().add(orderDetails);
            }
        }
        orderRepo.save(salesOrder);
    }

    @Nullable
    private SalesOrderDetailsInterface getPromotionOrderLine(SalesOrderInterface salesOrder, OrderRepositoryInterface orderRepo, Date now) {
        SalesOrderDetailsInterface orderDetails = orderRepo.getNewOrderDetails();
        HashMap<SkuInterface, Integer> freeSkuQtyMap = skuInput.getPromotionSkuIfApplicable();
        if(freeSkuQtyMap != null) {
            Map.Entry<SkuInterface, Integer> freeSkuQtyEntryLine = skuInput.getPromotionSkuIfApplicable().entrySet().iterator().next();
            orderDetails.setParentOrder(salesOrder);
            orderDetails.setSku(freeSkuQtyEntryLine.getKey());
            orderDetails.setCreationDateTime(now);
            orderDetails.setUpdateDateTime(now);
            orderDetails.setQty(freeSkuQtyEntryLine.getValue());
            orderDetails.setPromotion(skuInput.getSku().getSkuPromotion());
            return orderDetails;
        }
        return null;
    }

    private SalesOrderDetailsInterface getOrderDetails(List<SalesOrderDetailsInterface> salesOrderDetails, OrderRepositoryInterface orderRepo) {
        for (SalesOrderDetailsInterface details : salesOrderDetails){
            if(details.getSku().getDataId() == skuInput.getSku().getDataId()){
                return details;
            }
        }
        return orderRepo.getNewOrderDetails();
    }
}
