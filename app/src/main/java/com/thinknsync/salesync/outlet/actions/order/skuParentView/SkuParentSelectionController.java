package com.thinknsync.salesync.outlet.actions.order.skuParentView;

import com.thinknsync.salesync.database.records.sku.SkuParentInterface;

import java.util.List;

public class SkuParentSelectionController implements SkuParentSelectionContract.Controller {

    private SkuParentSelectionContract.View orderView;

    public SkuParentSelectionController(SkuParentSelectionContract.View orderView) {
        setViewReference(orderView);
    }

    @Override
    public void setViewReference(SkuParentSelectionContract.View view) {
        this.orderView = view;
    }

    @Override
    public List<SkuParentInterface> getSkuParentsTopHierarchy() {
        return orderView.getObjectConstructorInstance().getSkuRepository().getSkuParentsTopHierarchy();
    }
}
