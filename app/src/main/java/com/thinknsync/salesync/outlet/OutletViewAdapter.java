package com.thinknsync.salesync.outlet;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.thinknsync.listviewadapterhelper.BaseListViewAdapter;
import com.thinknsync.salesync.R;

import java.util.List;

public class OutletViewAdapter extends BaseListViewAdapter<OutletActionEnum> {

    private float defaultCardCornerRadius;
    private float defaultCardElevation;
    private int defaultTextColor;
    private Drawable defaultBackground;

    public OutletViewAdapter(Context context, List<OutletActionEnum> objects) {
        super(context, objects);
        setupDefaultViewParameters();
    }

    private void setupDefaultViewParameters() {
        Resources resources = getContext().getResources();
        defaultCardCornerRadius = resources.getDimension(R.dimen.card_corner_radius);
        defaultCardElevation = resources.getDimension(R.dimen.card_standard_height);
        defaultBackground = resources.getDrawable(R.drawable.rounded_corner_button_very_light_grey);
        defaultTextColor = Color.BLACK;

    }

    @Override
    protected void setValues(View convertView, OutletActionEnum listObject) {
        TextView actionTitle = convertView.findViewById(R.id.action_title);
        View background = convertView.findViewById(R.id.listview_item_layout);
        CardView card = convertView.findViewById(R.id.parent_card);
        ImageView arrowImage = convertView.findViewById(R.id.arrow_imageview);

        actionTitle.setText(getContext().getString(listObject.getTextId()));

        if(listObject == OutletActionEnum.EXCEPTION){
            arrowImage.setVisibility(View.GONE);
            background.setBackground(null);

            card.setRadius(0);
            card.setCardElevation(0);

            actionTitle.setTextColor(getContext().getResources().getColor(R.color.colorCancelActivityRed));
        } else {
            arrowImage.setVisibility(View.VISIBLE);
            background.setBackground(defaultBackground);

            card.setRadius(defaultCardCornerRadius);
            card.setCardElevation(defaultCardElevation);

            actionTitle.setTextColor(defaultTextColor);
        }
    }

    @Override
    protected int getView() {
        return R.layout.cardview_collect_order;
    }
}
