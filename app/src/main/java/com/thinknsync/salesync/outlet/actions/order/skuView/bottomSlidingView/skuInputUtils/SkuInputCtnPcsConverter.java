package com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.skuInputUtils;

import com.thinknsync.salesync.database.records.sku.SkuInterface;

public interface SkuInputCtnPcsConverter {
    SkuInterface getSku();
    void setSku(SkuInterface sku);
    double getPcsFromCtn(double orderQtyCtn);
    double[] getCtnPcsFromPcs(double orderQtypcs);
    double getCtnMultiplicationFactor();
}
