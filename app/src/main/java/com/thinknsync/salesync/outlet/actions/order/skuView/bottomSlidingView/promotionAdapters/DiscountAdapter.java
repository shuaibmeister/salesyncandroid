package com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.promotionAdapters;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.thinknsync.listviewadapterhelper.BaseListViewAdapter;
import com.thinknsync.salesync.R;

import java.util.List;


public class DiscountAdapter extends BaseListViewAdapter<Double> implements UpdatePromotionNumber{

    public DiscountAdapter(Context context, List<Double> objects) {
        super(context, objects);
    }

    @Override
    protected void setValues(View view, Double discountAmount) {
        TextView discount = view.findViewById(R.id.discount_amount);
        discount.setText(String.format(getContext().getString(R.string.amount_text_double), discountAmount));
    }

    @Override
    protected int getView() {
        return R.layout.cardview_promotion_discount_adapter_layout;
    }

    @Override
    public void updatePromotion(double promotionNumber) {
        objects.clear();
        objects.add(promotionNumber);
        notifyDataSetChanged();
    }
}
