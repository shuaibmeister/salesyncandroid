package com.thinknsync.salesync.outlet.actions.order.skuView;

import com.thinknsync.salesync.base.BaseController;
import com.thinknsync.salesync.base.BaseView;
import com.thinknsync.salesync.base.viewInterfaces.ViewInitialization;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.skuInputUtils.SkuInput;

import java.util.List;

public interface SkuSelectionContract {
    interface View extends BaseView, ViewInitialization.InitValues, SkuInputAddView {
        String tag = "SkuSelectionView";
    }

    interface Controller extends BaseController<View> {
        void setOutletFromId(int outletId);
        OutletInterface getSelectedOutlet();
        void setSkuParentFromId(int parentId);
        List<SkuInterface> getSkuOfParent();
        String getSkuParentName();
        long getOutletInDateTime();
        void setOutletInDateTime(long inDateTime);
    }

    interface SkuInputAddView {
        void addSkuItem(SkuInput skuInput);
    }

    interface SkuInputAllView extends SkuInputAddView, SkuInputEditView{
    }

    interface SkuInputEditView {
        void onEditOrderLine(SalesOrderInterface salesOrder, SkuInput skuInput);
        void onRemoveOrderLine(SalesOrderInterface salesOrder, SkuInterface sku);
    }
}
