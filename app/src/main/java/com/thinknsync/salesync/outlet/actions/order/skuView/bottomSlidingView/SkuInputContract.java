package com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView;

import com.thinknsync.salesync.base.viewInterfaces.BaseBottomSheetContract;
import com.thinknsync.salesync.base.viewInterfaces.InteractiveView;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.skuInputUtils.SkuInput;

import java.util.HashMap;

public interface SkuInputContract {
    interface View extends BaseBottomSheetContract.BottomSheetView, InteractiveView {
        String tag = "SkuInputContractView";

        void setTotalValue(double total);
        void setDiscountValue(double discount);
        void resetInputFields();
        void setPromotionSkuListView(HashMap<SkuInterface, Integer> promoSkuQtyMap);
        void notifyInputComplete(SkuInput skuInput);
    }

    interface Controller extends BaseBottomSheetContract.BottomSheetController<View> {
        void onQtyInput(double inputCtn, double inputPcs);
        void onDiscountInputChanged(double input);
        void addItemToCart();
        boolean skuHasDiscountPromotion();
        boolean skuHasFreeSkuPromotion();
        boolean skuHasPromotion();
        HashMap<SkuInterface, Integer> getInitialPromotionSkuQtyMap();
    }
}
