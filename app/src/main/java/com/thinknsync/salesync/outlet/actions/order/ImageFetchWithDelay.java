package com.thinknsync.salesync.outlet.actions.order;

import android.os.Handler;

import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.salesync.commons.asyncOperation.DiscreetNotifier;
import com.thinknsync.salesync.commons.commonInterfaces.HasIdentifier;
import com.thinknsync.salesync.commons.commonInterfaces.HasImage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ImageFetchWithDelay<T extends HasImage & HasIdentifier>  extends ObserverHostImpl<HashMap<Integer, T>>
        implements DiscreetNotifier<HashMap<Integer, T>> {

    private int DELAY = 1000;

    private HashMap<Integer, T> imageFetchIds;
    private List<Integer> alreadyFetched;
    private Handler imageFetchScheduler;

    public ImageFetchWithDelay() {
        imageFetchIds = new HashMap<>();
        alreadyFetched = new ArrayList<>();
        imageFetchScheduler = new Handler();
    }

    @Override
    public void setDelay(int delay){
        this.DELAY = delay;
    }

    @Override
    public void startNotificationLoop(HashMap<Integer, T> object) {
        T imageFetchObject = object.get(0);
        if((imageFetchObject.getImage() == null || imageFetchObject.getImage().equals("false")) &&
                !alreadyFetched.contains(imageFetchObject.getDataId())){
            imageFetchIds.put(imageFetchObject.getDataId(), imageFetchObject);
            alreadyFetched.add(imageFetchObject.getDataId());
        }

        imageFetchScheduler.removeCallbacksAndMessages(null);
        imageFetchScheduler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(imageFetchIds.size() > 0) {
                    notifyObservers(imageFetchIds);
                }
            }
        }, DELAY);
    }
}
