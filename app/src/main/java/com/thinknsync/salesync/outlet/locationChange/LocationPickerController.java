package com.thinknsync.salesync.outlet.locationChange;

import com.thinknsync.mapslib.MapsOperation;
import com.thinknsync.mapslib.mapWorks.MapsManager;
import com.thinknsync.mapslib.mapWorks.mapListeners.MapCameraIdleCenterPlaceFetcher;
import com.thinknsync.mapslib.place.PlaceDataObject;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.observerhost.TypedObserverImpl;

import java.util.HashMap;
import java.util.List;

public class LocationPickerController implements LocationPickerContract.Controller{
    private LocationPickerContract.View view;
    private MapsManager mapsManager;

    public LocationPickerController(LocationPickerContract.View view) {
        setViewReference(view);
    }

    @Override
    public void setViewReference(LocationPickerContract.View view) {
        this.view = view;
    }

    @Override
    public void initMapsManager() {
        mapsManager = view.getObjectConstructorInstance().getGoogleMapsManager(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, view.getContextWrapper());
            put(MapsOperation.tag, view.getMapsOperationUi());
        }});
        setupMapCameraPauseListener();
    }

    private void setupMapCameraPauseListener() {
        ObserverHost<List<PlaceDataObject>> centerLocationFetcher = new MapCameraIdleCenterPlaceFetcher((AndroidContextWrapper) view.getContextWrapper(), view.getMapsOperationUi());
        centerLocationFetcher.addToObservers(new TypedObserverImpl<List<PlaceDataObject>>() {
            @Override
            public void update(List<PlaceDataObject> placeDataObjects) {
                if(placeDataObjects.size() > 0){
                    view.updateCameraIdlePlace(placeDataObjects.get(0));
                }
            }

            @Override
            public void onOperationError(ObserverHost self, Exception e) {
                super.onOperationError(self, e);
                view.showShortMessage("Network error. try again later");
            }
        });
        mapsManager.setCameraIdleListener((TypedObserver) centerLocationFetcher);
    }
}
