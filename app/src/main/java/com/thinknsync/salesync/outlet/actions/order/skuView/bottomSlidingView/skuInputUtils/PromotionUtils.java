package com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.skuInputUtils;

import androidx.annotation.Nullable;

import com.thinknsync.salesync.commons.utils.DateTimeUtils;
import com.thinknsync.salesync.database.records.promotion.PromotionInterface;
import com.thinknsync.salesync.database.records.sku.SkuInterface;

import java.util.HashMap;

public class PromotionUtils implements PromotionResolver{

    private PromotionInterface promotion;

    public PromotionUtils(PromotionInterface promotion) {
        setPromotion(promotion);
    }

    @Override
    public void setPromotion(PromotionInterface promotion) {
        this.promotion = promotion;
    }

    @Override
    public PromotionInterface getPromotion() {
        return this.promotion;
    }

    @Nullable
    @Override
    public HashMap<SkuInterface, Integer> getFreeSkuAndQty(final double inputQty) {
        if(promotion.getPromotionType() == PromotionInterface.PromotionType.SKU && isPromotionApplicable(inputQty)){
            return new HashMap<SkuInterface, Integer>(){{
                put(promotion.getFreeSku(), (int)inputQty/promotion.getSellingQty() * promotion.getFreeSkuQty());
            }};
        }
        return null;
    }

    @Override
    public double getDiscountAmount(double inputQty) {
        if(promotion.getPromotionType() == PromotionInterface.PromotionType.CASH && isPromotionApplicable(inputQty)){
            return (int)inputQty/promotion.getSellingQty() * promotion.getFreeAmount();
        }
        return 0;
    }

    @Override
    public boolean isPromotionApplicable(double inputQty) {
        long dateTodayLong = new DateTimeUtils().getDefaultDateTimeNow().getTime();
        return inputQty >= promotion.getSellingQty() && dateTodayLong >= promotion.getStartDate().getTime()
                && dateTodayLong < promotion.getEndDate().getTime();
    }
}
