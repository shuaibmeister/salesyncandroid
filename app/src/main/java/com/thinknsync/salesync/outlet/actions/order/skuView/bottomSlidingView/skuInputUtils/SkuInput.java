package com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.skuInputUtils;

import com.thinknsync.salesync.database.records.promotion.PromotionInterface;
import com.thinknsync.salesync.database.records.sku.SkuInterface;

import java.util.HashMap;

import javax.annotation.Nullable;

public class SkuInput implements SkuInputCtnPcsConverter {

    private double orderQty;
    private double discount;
    private SkuInputCtnPcsConverter skuCtnPcsConverter;
    private PromotionResolver promotionResolver;

    public SkuInput(SkuInterface sku){
        setSku(sku);
    }

    @Override
    public SkuInterface getSku() {
        return skuCtnPcsConverter.getSku();
    }

    @Override
    public void setSku(SkuInterface sku) {
        skuCtnPcsConverter = new SkuQtyUnitConverter(sku);
        if(sku.hasPromotion()) {
            promotionResolver = new PromotionUtils(sku.getSkuPromotion());
        }
    }

    public double getOrderQtyPcs() {
        return orderQty;
    }

    public void setOrderQtyPcs(double orderQtyPcs) {
        this.orderQty = orderQtyPcs;
        this.discount = getDiscountIfCashPromotion(orderQtyPcs);
    }

    private double getDiscountIfCashPromotion(double orderQtyPcs) {
        if(promotionResolver != null && promotionResolver.getPromotion().getPromotionType() == PromotionInterface.PromotionType.CASH){
            return promotionResolver.getDiscountAmount(orderQtyPcs);
        }
        return discount;
    }

    public void setOrderQtyCtnPcs(double orderQtyCtn, double orderQtyPcs) {
        this.orderQty = getPcsFromCtn(orderQtyCtn) + orderQtyPcs;
        this.discount = getDiscountIfCashPromotion(orderQty);
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getTotal() {
        return orderQty * getSku().getUnitPrice();
    }

    public double getTotalWithDiscount() {
        double total = getTotal() - discount;
        return total < 0 ? 0 : total;
    }

    @Nullable
    public HashMap<SkuInterface, Integer> getPromotionSkuIfApplicable(){
        if(promotionResolver != null){
            return promotionResolver.getFreeSkuAndQty(getOrderQtyPcs());
        }
        return null;
    }

    @Override
    public double getPcsFromCtn(double orderQtyCtn) {
        return skuCtnPcsConverter.getPcsFromCtn(orderQtyCtn);
    }

    @Override
    public double[] getCtnPcsFromPcs(double orderQtyPcs) {
        return skuCtnPcsConverter.getCtnPcsFromPcs(orderQtyPcs);
    }

    @Override
    public double getCtnMultiplicationFactor() {
        return skuCtnPcsConverter.getCtnMultiplicationFactor();
    }
}
