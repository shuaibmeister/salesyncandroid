package com.thinknsync.salesync.outlet.actions.exceptionSubmit;

import com.thinknsync.apilib.ApiResponseView;
import com.thinknsync.salesync.base.viewInterfaces.BaseBottomSheetContract;
import com.thinknsync.salesync.base.viewInterfaces.InteractiveView;
import com.thinknsync.salesync.database.records.masterData.MasterData;
import com.thinknsync.salesync.uiElements.formElements.forms.FormContainerContract;

import java.util.List;

public interface ExceptionBottomSheetContract {
    interface View extends BaseBottomSheetContract.BottomSheetView, ApiResponseView, InteractiveView,
            FormContainerContract.FormActivity {
    }

    interface Controller extends BaseBottomSheetContract.BottomSheetController<View> {
        List<MasterData> getAllExceptionReasons();
        void sendExceptionToServer(MasterData exceptionReason);
    }
}
