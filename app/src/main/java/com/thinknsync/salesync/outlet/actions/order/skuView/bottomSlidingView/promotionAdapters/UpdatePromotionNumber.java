package com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.promotionAdapters;

public interface UpdatePromotionNumber {
    void updatePromotion(double promotionNumber);
}
