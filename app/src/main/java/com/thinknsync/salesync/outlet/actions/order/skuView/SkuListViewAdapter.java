package com.thinknsync.salesync.outlet.actions.order.skuView;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiDataObjectImpl;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.listviewadapterhelper.BaseListViewAdapter;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.api.ApiCallArgumentKeys;
import com.thinknsync.salesync.commons.asyncOperation.DiscreetNotifier;
import com.thinknsync.salesync.database.records.order.SalesOrderDetailsInterface;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.outlet.actions.order.ImageFetchWithDelay;
import com.thinknsync.salesync.commons.utils.ImageUtils;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SkuListViewAdapter extends BaseListViewAdapter<SkuInterface> {

    private ObjectConstructor objectConstructor;
    private AndroidContextWrapper contextWrapper;
    private DiscreetNotifier<HashMap<Integer, SkuInterface>> imageFetchDiscreetWIthDelay;
    private List<Integer> skuIds;

    SkuListViewAdapter(AndroidContextWrapper contextWrapper, List<SkuInterface> objectList, OutletInterface selectedOutlet, ObjectConstructor objectConstructor) {
        super(contextWrapper.getFrameworkObject(), objectList);
        this.objectConstructor = objectConstructor;
        this.contextWrapper = contextWrapper;
        setupImageFetchWithDelay();
        setSelectedSkuIds(selectedOutlet);
    }

    private void setSelectedSkuIds(OutletInterface selectedOutlet) {
        skuIds = new ArrayList<>();
        for(SalesOrderInterface salesOrders : selectedOutlet.getOrders()){
            for(SalesOrderDetailsInterface orderDetails : salesOrders.getSalesOrderDetails()){
                if(!skuIds.contains(orderDetails.getSku().getDataId())){
                    skuIds.add(orderDetails.getSku().getDataId());
                }
            }
        }
    }

    private void setupImageFetchWithDelay() {
        imageFetchDiscreetWIthDelay = new ImageFetchWithDelay<>();
        imageFetchDiscreetWIthDelay.setDelay(1500);
        imageFetchDiscreetWIthDelay.addToObservers(new TypedObserverImpl<HashMap<Integer, SkuInterface>>() {
            @Override
            public void update(HashMap<Integer, SkuInterface> skuParentsWithImageMap) {
                getSkuImages(skuParentsWithImageMap);
            }
        });
    }

    @Override
    protected void setValues(View view, SkuInterface skuInterface) {
        TextView skuName = view.findViewById(R.id.sku_name);
        ImageView skuImage = view.findViewById(R.id.sku_image);
        TextView skuPrice = view.findViewById(R.id.sku_price);
        TextView skuStock = view.findViewById(R.id.sku_stock);
        TextView skuTp = view.findViewById(R.id.sku_tp);
        TextView skuTargetVsAchievement = view.findViewById(R.id.sku_target);

        Context context = view.getContext();
        skuName.setText(skuInterface.getName());
        skuImage.setImageBitmap(new ImageUtils().getBimapFromBase64Image(skuInterface.getImage()));
        skuPrice.setText(String.format(context.getString(R.string.price_text), skuInterface.getUnitPrice()));
        skuStock.setText(String.format(context.getString(R.string.stock_text), skuInterface.getStock()));
        skuTp.setText(String.format(context.getString(R.string.tp_price_text), skuInterface.getTp()));
        skuTargetVsAchievement.setText(String.format(context.getString(R.string.sale_target),
                skuInterface.getTarget(), skuInterface.getAchievement()));

        setSkuState(view, skuInterface);
    }

    private void setSkuState(View view, final SkuInterface sku) {
        View addButton = view.findViewById(R.id.add_button);
        View addedButton = view.findViewById(R.id.added_button);
        View notAvailableButton = view.findViewById(R.id.not_available_button);
        View promotionIcon = view.findViewById(R.id.promotion_icon);
        View priceAndStock = view.findViewById(R.id.price_and_stock);
        View tpAndTarget = view.findViewById(R.id.tp_and_target);
        if (sku.getStock() == 0) {
            addButton.setVisibility(View.GONE);
            addedButton.setVisibility(View.GONE);
            notAvailableButton.setVisibility(View.VISIBLE);
            priceAndStock.setVisibility(View.GONE);
            tpAndTarget.setVisibility(View.GONE);
        } else {
            if(skuIds.contains(sku.getDataId())){
                addedButton.setVisibility(View.VISIBLE);
                addButton.setVisibility(View.GONE);
            } else {
                addedButton.setVisibility(View.GONE);
                addButton.setVisibility(View.VISIBLE);
            }
            notAvailableButton.setVisibility(View.GONE);
            priceAndStock.setVisibility(View.VISIBLE);
            tpAndTarget.setVisibility(View.VISIBLE);
        }

        if(sku.hasPromotion()){
            promotionIcon.setVisibility(View.VISIBLE);
        } else {
            promotionIcon.setVisibility(View.GONE);
        }

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyObservers(sku);
            }
        });

        fetchImageAsync(sku);
    }

    void setSkuSelected(int skuId){
        if(!skuIds.contains(skuId)){
            skuIds.add(skuId);
            notifyDataSetChanged();
        }
    }

    private void fetchImageAsync(final SkuInterface sku) {
        imageFetchDiscreetWIthDelay.startNotificationLoop(new HashMap<Integer, SkuInterface>(){{
            put(0, sku);
        }});

    }

    private void getSkuImages(final HashMap<Integer, SkuInterface> skuMap) {
        final ApiDataObject apiDataObject = new ApiDataObjectImpl(new HashMap<String, Integer[]>() {{
            put(ApiCallArgumentKeys.ImageFetchApiKeys.SKU_IDS, skuMap.keySet().toArray(new Integer[0]));
        }});

        final ApiResponseActions responseActions = new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int i, JSONObject jsonObject) throws JSONException {
                JSONArray skuImages = jsonObject.getJSONArray(ApiRequest.ApiFields.KEY_DATA);
                for (int j = 0; j < skuImages.length(); j++) {
                    JSONObject skuImageJson = skuImages.getJSONObject(j);
                    skuMap.get(skuImageJson.getInt("id")).setImage(skuImageJson
                            .getString(SkuInterface.key_sku_image));
                }
                objectConstructor.getSkuRepository().saveSkus(skuMap.values().toArray(new SkuInterface[0]));
                skuMap.clear();
                notifyDataSetChanged();
            }
        };

        ApiRequest apiRequest = objectConstructor.getSkuImageFetchApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, responseActions);
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});

        apiRequest.setProgressDialog(null);
        apiRequest.sendRequest();
    }

    @Override
    protected int getView() {
        return R.layout.sku_adapter_layout;
    }
}
