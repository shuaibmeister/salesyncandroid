package com.thinknsync.salesync.outlet.bottomSlidingView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.thinknsync.inputvalidation.InputValidation;
import com.thinknsync.mapslib.place.PlaceDataObject;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.commons.osSpecifics.imageCapture.ImageCapture;
import com.thinknsync.salesync.commons.utils.ImageUtils;
import com.thinknsync.salesync.database.records.masterData.MasterData;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.outlet.locationChange.LocationPickerView;
import com.thinknsync.salesync.outlet.locationChange.LocationPickerContract;
import com.thinknsync.salesync.uiElements.expandableView.SingleSelectionExpandableAdapter;
import com.thinknsync.salesync.uiElements.formElements.forms.FormContainerContract;
import com.thinknsync.salesync.uiElements.formElements.forms.FormContainerViewImpl;
import com.thinknsync.salesync.uiElements.formElements.inputs.ActionFloatingButton;
import com.thinknsync.salesync.uiElements.formElements.inputs.ActionView;
import com.thinknsync.salesync.uiElements.formElements.inputs.CircleImageHolder;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputField;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputValidationContainer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OutletBottomSheetView implements OutletBottomSheetContract.View {

    private View bottomView, changeLocationButton, outletImageLayout, outletEditButton;
    private List<View> lineViews;
    private ActionFloatingButton outletEditDoneButton;

    private TextView ownerName;
    private CircleImageHolder ownerPic, outletPic, activeImageView;
    private final HashMap<ImageView, String> imageStringMap = new HashMap<>();
    private TextView ownerNameLabel, ownerPhoneLabel, ownerDobLabel, ownerMarriageDateLabel,
            ownerNidLabel, outletImageLabel;
    private InputField outletAddress, outletName, ownerNameEt, ownerPhoneEt, ownerDobEt, ownerMarriageDateEt, ownerNidEt;

    private ExpandableListView outletTypeFields, outletAccountAvailabilityFields;
    private SingleSelectionExpandableAdapter<MasterData> outletTypeSelectionAdapter;
    private SingleSelectionExpandableAdapter<MasterData> outletClassificationSelectionAdapter;

    private EditText[] allEditTexts;
    private TextView[] allTextViews;
    private int[] textColors;

    private OutletBottomSheetContract.Controller controller;
    private ObjectConstructor objectConstructor;
    private ImageCapture imageCaptureImpl;
    private OutletInterface outletInterface;

    public OutletBottomSheetView(ObjectConstructor objectConstructor, OutletInterface outletInterface,
                                 ImageCapture imageCaptureImpl, View bottomSlidingView) {
        this.bottomView = bottomSlidingView;
        this.outletInterface = outletInterface;
        this.objectConstructor = objectConstructor;
        this.imageCaptureImpl = imageCaptureImpl;
        initController();
        initView();
        initOutletDropdownFields();
        initValues();
        initUiElementArrays();
        initForm();
        initViewListeners();
        controller.initLocationService();
        imageStringMap.put(ownerPic, outletInterface.getOwnerImage());
        imageStringMap.put(outletPic, outletInterface.getOutletImage());
    }

    private void initForm() {
        setupForm(new HashMap<InputValidationContainer, InputValidation.ValidationTypes>(){{
            put(ownerNameEt, InputValidation.ValidationTypes.TYPE_MANDATORY);
            put(ownerPhoneEt, InputValidation.ValidationTypes.TYPE_PHONE);
            put(ownerDobEt, InputValidation.ValidationTypes.TYPE_DATE);
            put(ownerMarriageDateEt, InputValidation.ValidationTypes.TYPE_DATE);
            put(ownerNidEt, InputValidation.ValidationTypes.TYPE_MANDATORY);
            put(outletTypeSelectionAdapter, InputValidation.ValidationTypes.TYPE_MANDATORY);
            put(outletClassificationSelectionAdapter, InputValidation.ValidationTypes.TYPE_MANDATORY);
            put(ownerPic, InputValidation.ValidationTypes.TYPE_MANDATORY);
            put(outletPic, InputValidation.ValidationTypes.TYPE_MANDATORY);
        }}, outletEditDoneButton);
    }

    private void initOutletDropdownFields() {
        setupOutletTypeSingeSelection();
        setupOutletAccountAvailabilitySingeSelection();
    }

    private void setupOutletTypeSingeSelection() {
        final List<MasterData> outletTypeMasterData = controller.getAllOutletTypeMasterData();
        HashMap<String, List<MasterData>> adapterMasterData = new HashMap<String, List<MasterData>>(){{
            put(bottomView.getContext().getString(R.string.outlet_type_label), outletTypeMasterData);
        }};
        outletTypeSelectionAdapter = new SingleSelectionExpandableAdapter<>(bottomView.getContext(), adapterMasterData,
                controller.getSelectedOutlet().getOutletType(objectConstructor.getMasterDataRepository()));
        outletTypeFields.setOnChildClickListener(outletTypeSelectionAdapter);
        outletTypeFields.setAdapter(outletTypeSelectionAdapter);
        outletTypeFields.setEnabled(false);
    }

    private void setupOutletAccountAvailabilitySingeSelection(){
        final List<MasterData> outletClassificationData = controller.getAllOutletClassificationMasterData();
        HashMap<String, List<MasterData>> adapterBooleanData = new HashMap<String, List<MasterData>>(){{
            put(bottomView.getContext().getString(R.string.outlet_classification_label), outletClassificationData);
        }};
        outletClassificationSelectionAdapter = new SingleSelectionExpandableAdapter<>(bottomView.getContext(), adapterBooleanData,
                controller.getSelectedOutlet().getOutletClassification(objectConstructor.getMasterDataRepository()));
        outletAccountAvailabilityFields.setOnChildClickListener(outletClassificationSelectionAdapter);
        outletAccountAvailabilityFields.setAdapter(outletClassificationSelectionAdapter);
        outletAccountAvailabilityFields.setEnabled(false);
    }

    private void initUiElementArrays() {
        allEditTexts = new EditText[]{ownerNameEt, ownerPhoneEt, ownerDobEt, ownerMarriageDateEt, ownerNidEt};
        allTextViews = new TextView[]{ownerNameLabel, ownerPhoneLabel, ownerDobLabel, ownerMarriageDateLabel, ownerNidLabel, outletImageLabel};
        textColors = new int[]{bottomView.getContext().getResources().getColor(R.color.colorTransparent_40Opacity), Color.BLACK};
    }

    @Override
    public int getBottomSheetLayoutResourceId() {
        return bottomView.getId();
    }

    @Override
    public void initController() {
        AndroidContextWrapper contextWrapper = (AndroidContextWrapper)objectConstructor.getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, bottomView.getContext());
        }});
        controller = new OutletBottomSheetController(objectConstructor, contextWrapper, outletInterface, this);
    }

    @Override
    public void initViewListeners() {
        outletEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAllTextsEditableNotEditable(true);
                invertAllUiTextElementColors(true);
                setEditModeVisibility(true);
                modifySingleSelectionEditable(true);
                setOnEditClickListeners();
            }
        });

        outletEditDoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.updateOutletInformation(outletName.getInput(), ownerNameEt.getInput(), ownerPhoneEt.getInput(), ownerDobEt.getInput(),
                        ownerMarriageDateEt.getInput(), ownerNidEt.getInput(),
                        outletTypeSelectionAdapter.getInput().getDataId(), outletClassificationSelectionAdapter.getInput().getDataId(),
                        imageStringMap.get(ownerPic), imageStringMap.get(outletPic));
            }
        });

        changeLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), LocationPickerView.class);
                ((Activity)view.getContext()).startActivityForResult(intent, LocationPickerContract.LOCATION_PICKER_CODE);
            }
        });
    }

    private void setOnEditClickListeners() {
        outletImageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activeImageView = outletPic;
                imageCaptureImpl.openCamera();
            }
        });

        ownerPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activeImageView = ownerPic;
                imageCaptureImpl.openCamera();
            }
        });

        ownerDobEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.showTimePicker(new TypedObserverImpl<String>() {
                    @Override
                    public void update(String s) {
                        ownerDobEt.setText(s);
                    }
                });
            }
        });

        ownerMarriageDateEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.showTimePicker(new TypedObserverImpl<String>() {
                    @Override
                    public void update(String s) {
                        ownerMarriageDateEt.setText(s);
                    }
                });
            }
        });
    }

    private void removeEditClickListeners() {
        ownerDobEt.setOnClickListener(null);
        ownerMarriageDateEt.setOnClickListener(null);
        outletImageLayout.setOnClickListener(null);
        ownerPic.setOnClickListener(null);
    }

    @Override
    public void initValues() {
        OutletInterface outletInterface = controller.getSelectedOutlet();
        ImageUtils imageUtils = new ImageUtils();
        outletAddress.setText(outletInterface.getOutletAddress());
        outletName.setText(outletInterface.getName());
        ownerName.setText(String.format(bottomView.getContext().getString(R.string.owner_text), outletInterface.getOwnerName()));
        ownerPic.setImageBitmap(imageUtils.getBimapFromBase64Image(outletInterface.getOwnerImage()));

        ownerNameEt.setText(outletInterface.getOwnerName());
        ownerPhoneEt.setText(outletInterface.getOwnerPhone());
        ownerDobEt.setText(outletInterface.getOwnerDob());
        ownerMarriageDateEt.setText(outletInterface.getOwnerMarriageDate());
        ownerNidEt.setText(outletInterface.getOwnerNid());
        outletPic.setImageBitmap(imageUtils.getBimapFromBase64Image(outletInterface.getOutletImage()));
    }

    private void setAllTextsEditableNotEditable(boolean isEditable){
        for (EditText editText : allEditTexts) {
            editText.setEnabled(isEditable);
        }
        outletName.setEnabled(isEditable);
    }

    private void invertAllUiTextElementColors(boolean isEditable){
        int textColor = textColors[1];
        int editTextColor = textColors[0];
        if(isEditable){
            textColor = textColors[0];
            editTextColor = textColors[1];
        }
        for (TextView textView : allTextViews) {
            textView.setTextColor(textColor);
        }

        for (EditText editText : allEditTexts) {
            editText.setTextColor(editTextColor);
        }
    }

    private void setEditModeVisibility(boolean isEditing){
        if(isEditing){
            changeLocationButton.setVisibility(View.VISIBLE);
            outletEditButton.setVisibility(View.GONE);
            outletEditDoneButton.setVisibility(View.VISIBLE);
            setLineviewVisibility(View.VISIBLE);
        } else {
            changeLocationButton.setVisibility(View.GONE);
            outletEditButton.setVisibility(View.VISIBLE);
            outletEditDoneButton.setVisibility(View.GONE);
            setLineviewVisibility(View.GONE);
        }
    }

    private void setLineviewVisibility(int visibility) {
        for (View v : lineViews) {
            v.setVisibility(visibility);
        }
    }

    @Override
    public void initView() {
        outletAddress = bottomView.findViewById(R.id.outlet_address);
        changeLocationButton = bottomView.findViewById(R.id.change_location_button);
        outletImageLayout = bottomView.findViewById(R.id.outlet_image_layout);

        outletName = bottomView.findViewById(R.id.outlet_title);
        ownerName = bottomView.findViewById(R.id.owner_name);
        ownerPic = bottomView.findViewById(R.id.outlet_owner_image);

        ownerNameEt = bottomView.findViewById(R.id.owner_name_edit);
        ownerPhoneEt = bottomView.findViewById(R.id.owner_mobile_edit);
        ownerDobEt = bottomView.findViewById(R.id.owner_dob_edit);
        ownerMarriageDateEt = bottomView.findViewById(R.id.owner_marriage_date_edit);
        ownerNidEt = bottomView.findViewById(R.id.owner_nid_edit);

        ownerNameLabel = bottomView.findViewById(R.id.owner_name_label);
        ownerPhoneLabel = bottomView.findViewById(R.id.owner_mobile_label);
        ownerDobLabel = bottomView.findViewById(R.id.owner_dob_label);
        ownerMarriageDateLabel = bottomView.findViewById(R.id.owner_marriage_date_label);
        ownerNidLabel = bottomView.findViewById(R.id.owner_nid_label);
        outletImageLabel = bottomView.findViewById(R.id.outlet_image_label);
        lineViews = getViewsByTag((ViewGroup) bottomView, "line_view");

        outletTypeFields = bottomView.findViewById(R.id.outlet_type_dropdown);
        outletAccountAvailabilityFields = bottomView.findViewById(R.id.outlet_account_dropdown);

        outletPic = bottomView.findViewById(R.id.outlet_image);

        outletEditButton = bottomView.findViewById(R.id.outlet_edit_action);
        outletEditDoneButton = bottomView.findViewById(R.id.outlet_done_action);
    }

    @Override
    public void resetToInitialState() {
        onApiCallSuccess();
    }

    private void modifySingleSelectionEditable(boolean isEditable) {
        outletTypeFields.setEnabled(isEditable);
        outletAccountAvailabilityFields.setEnabled(isEditable);
        outletTypeSelectionAdapter.swapActiveColors();
        outletClassificationSelectionAdapter.swapActiveColors();
    }

    @Override
    public void sendCapturedPicture(FrameworkWrapper imageBitmap, String imageString) {
        imageStringMap.put(activeImageView, imageString);
        activeImageView.setImageBitmap((Bitmap) imageBitmap.getFrameworkObject());
    }

    @Override
    public void sendUpdatedAddress(PlaceDataObject outletAddress) {
        this.outletAddress.setText(outletAddress.getPlaceTitle());
        controller.setOutletLocation(outletAddress.getLat(), outletAddress.getLon());
    }

    @Override
    public void setupForm(HashMap<InputValidationContainer, InputValidation.ValidationTypes> inputAndTypeMap, ActionView actionView) {
        FormContainerContract.Initializer formContainer = new FormContainerViewImpl(inputAndTypeMap, objectConstructor, actionView);
        formContainer.initForm();
    }

    @Override
    public void updateTime(String timeString) {

    }

    private ArrayList<View> getViewsByTag(ViewGroup root, String tag){
        ArrayList<View> views = new ArrayList<>();
        final int childCount = root.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = root.getChildAt(i);
            if (child instanceof ViewGroup) {
                views.addAll(getViewsByTag((ViewGroup) child, tag));
            }

            final Object tagObj = child.getTag();
            if (tagObj != null && tagObj.equals(tag)) {
                views.add(child);
            }

        }
        return views;
    }

    @Override
    public void onApiCallSuccess(String... strings) {
        setAllTextsEditableNotEditable(false);
        invertAllUiTextElementColors(false);
        setEditModeVisibility(false);
        modifySingleSelectionEditable(false);
        outletTypeSelectionAdapter.swapActiveColors();
        outletClassificationSelectionAdapter.swapActiveColors();
        removeEditClickListeners();
    }

    @Override
    public void onApiCallFailure(String... strings) {
        Toast.makeText(bottomView.getContext(), strings[0], Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onApiCallError(String... strings) {
        onApiCallFailure(strings);
    }
}
