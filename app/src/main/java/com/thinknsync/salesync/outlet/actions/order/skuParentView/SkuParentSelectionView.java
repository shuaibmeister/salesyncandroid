package com.thinknsync.salesync.outlet.actions.order.skuParentView;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.BundleWrapper;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.base.views.BaseActivityWithBackButton;
import com.thinknsync.salesync.database.records.sku.SkuParentInterface;
import com.thinknsync.salesync.outlet.actions.order.skuView.SkuSelectionView;
import com.thinknsync.salesync.uiElements.autocompleteWidget.AutocompleteAdapter;
import com.thinknsync.salesync.uiElements.autocompleteWidget.AutocompleteInitImpl;
import com.thinknsync.salesync.uiElements.autocompleteWidget.AutocompleteInitializer;

import java.util.HashMap;
import java.util.List;

public class SkuParentSelectionView extends BaseActivityWithBackButton implements SkuParentSelectionContract.View{

    private SkuParentSelectionContract.Controller controller;

    private ListView skuParentList;
    private View searchBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initController();
        initViewListeners();
    }

    @Override
    public void initView() {
        skuParentList = findViewById(R.id.sku_parent_list);
        searchBox = findViewById(R.id.product_search_card);
        setHeaderTitle(getString(R.string.activity_title));
    }

    @Override
    public void initController() {
        controller = new SkuParentSelectionController(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_sku_parent_select;
    }

    @Override
    public AndroidContextWrapper getContextWrapper() {
        return (AndroidContextWrapper) getObjectConstructorInstance().getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, SkuParentSelectionView.this);
        }});
    }

    @Override
    public void initViewListeners() {
        SkuParentsParentListViewAdapter adapter = new SkuParentsParentListViewAdapter(getContextWrapper(), controller.getSkuParentsTopHierarchy(),
                new TypedObserverImpl<SkuParentInterface>() {
                    @Override
                    public void update(SkuParentInterface skuParent) {
                        goToSkuSelectionView(skuParent);
                    }
                }, getObjectConstructorInstance(), this);
        skuParentList.setAdapter(adapter);
    }

    private void goToSkuSelectionView(SkuParentInterface skuParent) {
        if(getIntent().getExtras() != null){
            final Bundle b = getIntent().getExtras();
            b.putInt(SkuParentInterface.key_parent_id, skuParent.getDataId());
            startActivity(SkuSelectionView.class, getObjectConstructorInstance().getBundleWrapper(new HashMap<String, Object>(){{
                put(BundleWrapper.tag, b);
            }}));
            setActivitySlideFromRightSideAnimation();
        }
    }

    @Override
    public void initSearchBox(List<SkuParentInterface> searchableList) {
        ArrayAdapter<SkuParentInterface> searchAdapter = new AutocompleteAdapter<>(this, searchableList);
        AutocompleteInitializer<SkuParentInterface> autocomplete = new AutocompleteInitImpl<>(searchBox);
        autocomplete.setupAutocompleteWithAdapter(searchAdapter);
        autocomplete.addToObservers(new TypedObserverImpl<SkuParentInterface>() {
            @Override
            public void update(SkuParentInterface skuParent) {
                goToSkuSelectionView(skuParent);
            }
        });
    }
}
