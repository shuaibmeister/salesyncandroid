package com.thinknsync.salesync.outlet.actions.exceptionSubmit;

import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.commons.offlineSupport.SendExceptionToServer;
import com.thinknsync.salesync.commons.offlineSupport.SendSingleToServer;
import com.thinknsync.salesync.commons.utils.DateTimeUtils;
import com.thinknsync.salesync.database.records.masterData.MasterData;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.repositories.masterData.MasterDataRepositoryInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

public class ExceptionBottomSheetController implements ExceptionBottomSheetContract.Controller {

    private ObjectConstructor objectConstructor;
    private AndroidContextWrapper contextWrapper;
    private OutletInterface selectedOutlet;
    private Date outletInTime;
    private ExceptionBottomSheetContract.View view;

    public ExceptionBottomSheetController(ObjectConstructor objectConstructor, AndroidContextWrapper contextWrapper,
                                          OutletInterface outletInterface, Date outletInTime, ExceptionBottomSheetContract.View view) {
        this.objectConstructor = objectConstructor;
        this.contextWrapper = contextWrapper;
        this.selectedOutlet = outletInterface;
        this.outletInTime = outletInTime;
        setViewReference(view);
    }

    @Override
    public void setViewReference(ExceptionBottomSheetContract.View bottomSheetView) {
        this.view = bottomSheetView;
    }

    @Override
    public List<MasterData> getAllExceptionReasons() {
        MasterDataRepositoryInterface masterDataRepo = objectConstructor.getMasterDataRepository();
        return masterDataRepo.getMasterDataOfType(MasterData.MasterDataType.EXCEPTION_REASON);
    }

    @Override
    public void sendExceptionToServer(MasterData exceptionReason) {
        SalesOrderInterface exceptionOrder = getExceptionOrder(exceptionReason);
        SendSingleToServer<SalesOrderInterface> sendExceptionToServer = new SendExceptionToServer(objectConstructor);
        sendExceptionToServer.setData(exceptionOrder);
        sendExceptionToServer.sendData(contextWrapper, new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int i, JSONObject jsonObject) throws JSONException {
                view.onApiCallSuccess(jsonObject.getString(ApiRequest.ApiFields.KEY_MESSAGE));
            }

            @Override
            public void onApiCallFailure(int i, JSONObject jsonObject) throws JSONException {
                view.onApiCallFailure(jsonObject.getString(ApiRequest.ApiFields.KEY_MESSAGE));
            }
        });
    }

    private SalesOrderInterface getExceptionOrder(MasterData exceptionReason) {
        SalesOrderInterface exceptionOrder = objectConstructor.getSalesOrderRepository().getNewEntity();
        exceptionOrder.setSr(objectConstructor.getUserRepository().getLoggedInUser());
        exceptionOrder.setOutlet(selectedOutlet);
        exceptionOrder.setException(exceptionReason);
        exceptionOrder.setOutletInDateTime(outletInTime);
        exceptionOrder.setOutletOutDateTime(new DateTimeUtils().getDefaultDateTimeNow());
        return exceptionOrder;
    }
}
