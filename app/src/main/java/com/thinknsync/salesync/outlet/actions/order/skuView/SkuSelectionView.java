package com.thinknsync.salesync.outlet.actions.order.skuView;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.base.views.BaseWithBottomNavigationBottomSheetAndBackButton;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.database.records.sku.SkuParentInterface;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.skuInputUtils.SkuInput;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.SkuInputBottomSheetView;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.SkuInputContract;

import java.util.HashMap;

public class SkuSelectionView extends BaseWithBottomNavigationBottomSheetAndBackButton implements SkuSelectionContract.View{

    private SkuSelectionContract.Controller controller;

    private TextView skuParentTitle;
    private ListView skuListView;
    private SkuListViewAdapter skuInterfaceArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initValues();
        initListView();
    }

    private void initListView() {
        skuInterfaceArrayAdapter = new SkuListViewAdapter(getContextWrapper(),
                controller.getSkuOfParent(), controller.getSelectedOutlet(), getObjectConstructorInstance());
        skuInterfaceArrayAdapter.addToObservers(new TypedObserverImpl<SkuInterface>() {
            @Override
            public void update(SkuInterface sku) {
                SkuInputContract.View bottomSheetViewSkuInpt = new SkuInputBottomSheetView(SkuSelectionView.this, getObjectConstructorInstance(),
                        controller.getSelectedOutlet(), sku, controller.getOutletInDateTime(), findViewById(R.id.bottom_sheet_sku));
                setupBottomSheetWithLayoutView(bottomSheetViewSkuInpt);
                toggleBottomSheet();
            }
        });
        skuListView.setAdapter(skuInterfaceArrayAdapter);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_sku_select;
    }

    @Override
    public AndroidContextWrapper getContextWrapper() {
        return (AndroidContextWrapper) getObjectConstructorInstance().getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, SkuSelectionView.this);
        }});
    }

    @Override
    public void initController() {
        controller = new SkuSelectionController(this);
    }

    @Override
    public void initView() {
        skuParentTitle = findViewById(R.id.sku_parent_title);
        skuListView = findViewById(R.id.sku_list);
        setTitle(R.string.sku_selection_activity_title);
    }

    @Override
    public void initValues() {
        if(getIntent().getExtras() != null){
            controller.setOutletFromId(getIntent().getExtras().getInt(OutletInterface.key_outlet_id));
            controller.setSkuParentFromId(getIntent().getExtras().getInt(SkuParentInterface.key_parent_id));
            controller.setOutletInDateTime(getIntent().getExtras().getLong(SalesOrderInterface.key_in_date_time));
            skuParentTitle.setText(controller.getSkuParentName());
        }
    }

    @Override
    public void addSkuItem(SkuInput skuInput) {
        skuInterfaceArrayAdapter.setSkuSelected(skuInput.getSku().getDataId());
        toggleBottomSheet();
    }
}
