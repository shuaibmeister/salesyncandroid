package com.thinknsync.salesync.database.repositories.promotion;

import com.thinknsync.salesync.database.records.promotion.PromotionInterface;
import com.thinknsync.salesync.database.repositories.RepositoryPolicy;

import java.util.List;

public interface PromotionRepositoryInterface extends RepositoryPolicy<PromotionInterface> {
    List<PromotionInterface> getAllActivePromotions();
    PromotionInterface getActivePromotionOfSku(int skuId);
    void savePromotions(PromotionInterface[] promotions);
}
