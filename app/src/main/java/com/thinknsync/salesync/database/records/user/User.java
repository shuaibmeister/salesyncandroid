package com.thinknsync.salesync.database.records.user;

import com.thinknsync.salesync.database.records.BaseRecord;
import com.thinknsync.salesync.home.bottomSlidingView.AttendanceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import io.objectbox.annotation.Convert;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class User extends BaseRecord<UserInterface> implements UserInterface {

    @Id(assignable = true)
    private long id;
    protected String name;
    protected int srId;
    private String code;
    private String dbName;
    private String email;
    private String phone;
    private String designation;
    private String address;
    private String userId;
    @Convert(converter = PasswordConverter.class, dbType = String.class)
    private String password;
    protected String userImage;
    private double target;
    private double achievement;
    private double runRate;
    private boolean isLoggedIn;
    private String loginToken;
    @Convert(converter = AttendanceStatusConverter.class, dbType = Integer.class)
    protected AttendanceManager.AttendanceStatus attendanceStatus;
    private Date attendanceInDateTime;
    private Date attendanceOutDateTime;
    private Date lastLoginTime;
    protected Date creationDate;
    protected Date updateDate;

    @Override
    public long getId() {
        return this.id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Date getCreationDateTime() {
        return creationDate;
    }

    @Override
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDate = creationDateTime;
    }

    @Override
    public Date getUpdateDateTime() {
        return updateDate;
    }

    @Override
    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDate = updateDateTime;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setDataId(int id) {
        this.srId = id;
    }

    @Override
    public int getDataId(){
        return srId;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getDbName() {
        return dbName;
    }

    @Override
    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getPhone() {
        return phone;
    }

    @Override
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String getDesignation() {
        return designation;
    }

    @Override
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getImage() {
        return userImage;
    }

    @Override
    public void setImage(String userImage) {
        this.userImage = userImage;
    }

    @Override
    public double getTarget() {
        return target;
    }

    @Override
    public void setTarget(double target) {
        this.target = target;
    }

    @Override
    public double getAchievement() {
        return achievement;
    }

    @Override
    public void setAchievement(double achievement) {
        this.achievement = achievement;
    }

    @Override
    public double getRunRate() {
        return runRate;
    }

    @Override
    public void setRunRate(double runRate) {
        this.runRate = runRate;
    }

    @Override
    public void setIsLoggedIn(boolean loggedIn) {
        this.isLoggedIn = loggedIn;
    }

    @Override
    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    @Override
    public String getLoginToken() {
        return loginToken;
    }

    @Override
    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }

    @Override
    public AttendanceManager.AttendanceStatus getAttendanceStatusToday() {
        return attendanceStatus;
    }

    @Override
    public void setAttendanceStatusToday(AttendanceManager.AttendanceStatus attendanceStatusToday) {
        this.attendanceStatus = attendanceStatusToday;
    }

    @Override
    public Date getAttendanceInDateTime() {
        return attendanceInDateTime;
    }

    @Override
    public void setAttendanceInDateTime(Date attendanceInDateTime) {
        this.attendanceInDateTime = attendanceInDateTime;
    }

    @Override
    public Date getAttendanceOutDateTime() {
        return attendanceOutDateTime;
    }

    @Override
    public void setAttendanceOutDateTime(Date attendanceOutDateTime) {
        this.attendanceOutDateTime = attendanceOutDateTime;
    }

    @Override
    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    @Override
    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    @Override
    public UserInterface fromJson(JSONObject jsonObject) throws JSONException {
        setDates(jsonObject);
        setId(jsonObject.optLong(key_id));
        setName(jsonObject.getString(key_name));
        setCode(jsonObject.getString(key_code));
        setDataId(jsonObject.getInt(key_sr_id));
        setDbName(jsonObject.getString(key_db_name));
        setEmail(jsonObject.getString(key_email));
        setPhone(jsonObject.getString(key_phone));
        setDesignation(jsonObject.getString(key_designation));
        setAddress(jsonObject.getString(key_address));
        setUserId(jsonObject.getString(key_user_id));
        setImage(jsonObject.optString(key_user_image));
        setTarget(jsonObject.getDouble(key_target));
        setAchievement(jsonObject.getDouble(key_achievement));
        setRunRate(jsonObject.getDouble(key_run_rate));
        setIsLoggedIn(jsonObject.optBoolean(key_logged_in));
        setLoginToken(jsonObject.getString(key_token));
        setAttendanceStatusToday(AttendanceManager.AttendanceStatus.getStatusFromId(jsonObject.optInt(key_attendance_status)));
        setAttendanceInDateTime(!jsonObject.optString(key_attendance_in_date_time).equals("") ?
                getDateTimeFromDateTimeString(jsonObject.optString(key_attendance_in_date_time)) : null);
        setAttendanceOutDateTime(!jsonObject.optString(key_attendance_out_date_time).equals("") ?
                getDateTimeFromDateTimeString(jsonObject.optString(key_attendance_out_date_time)) : null);
        return this;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject().put(key_id, getId())
                .put(key_name, getName())
                .put(key_sr_id, getDataId())
                .put(key_code, getCode())
                .put(key_db_name, getDbName())
                .put(key_email, getEmail())
                .put(key_phone, getPhone())
                .put(key_designation, getDesignation())
                .put(key_address, getAddress())
                .put(key_user_id, getUserId())
                .put(key_user_image, getImage())
                .put(key_target, getTarget())
                .put(key_achievement, getAchievement())
                .put(key_run_rate, getRunRate())
                .put(key_logged_in, isLoggedIn())
                .put(key_token, getLoginToken())
                .put(key_attendance_status, getAttendanceStatusToday().getId())
                .put(key_attendance_in_date_time, getDateTimeStringFromDate(getAttendanceInDateTime()))
                .put(key_last_login_datetime, getLastLoginTime());
        return setDatesInJsonObject(jsonObject);
    }
}
