package com.thinknsync.salesync.database.repositories.outlet;


import com.thinknsync.salesync.database.records.DbRecord;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.route.RouteInterface;
import com.thinknsync.salesync.database.repositories.RepositoryPolicy;

import java.util.List;

public interface OutletRepositoryInterface extends RepositoryPolicy<OutletInterface>{
    List<OutletInterface> getVerificationTypedOutletsOfRoute(int routeId, boolean isVerified);
    void saveOutlets(OutletInterface[] outlets);
    OutletInterface getOutletByOutletId(int outletId);
}
