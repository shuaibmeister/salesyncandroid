package com.thinknsync.salesync.database.repositories.order;

import com.thinknsync.salesync.database.records.order.SalesOrderDetailsInterface;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.database.repositories.RepositoryPolicy;

import java.util.HashMap;
import java.util.List;

public interface OrderRepositoryInterface extends RepositoryPolicy<SalesOrderInterface>{
    void saveOrders(SalesOrderInterface[] salesOrders);
    void updateOrderLines(SalesOrderDetailsInterface[] salesOrderDetails);
    void saveOrderDetails(SalesOrderDetailsInterface entity);
    SalesOrderDetailsInterface getNewOrderDetails();
    List<SalesOrderInterface> getUnsyncedOrders();
    void editOrderLine(SalesOrderInterface order, SkuInterface sku, double qty, double discount, double total, HashMap<SkuInterface, Integer> promotionSkuQtyMap);
    void deleteOrder(SalesOrderInterface order);
    void deleteOrders(SalesOrderInterface[] order);
    void deleteOrderLine(SalesOrderDetailsInterface orderLine);
    void deleteOrderLine(SalesOrderInterface order, SkuInterface sku);
}
