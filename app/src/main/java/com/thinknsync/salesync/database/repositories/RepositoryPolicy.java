package com.thinknsync.salesync.database.repositories;

import com.thinknsync.salesync.database.records.DbRecord;

import java.util.List;

public interface RepositoryPolicy<T extends DbRecord<T>>{
    List<T> getAll();
    void save(T entity);
    T getNewEntity();
    void deleteAll();
}
