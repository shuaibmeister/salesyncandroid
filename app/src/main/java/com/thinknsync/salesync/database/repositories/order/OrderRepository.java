package com.thinknsync.salesync.database.repositories.order;

import com.thinknsync.salesync.database.records.order.SalesOrder;
import com.thinknsync.salesync.database.records.order.SalesOrderDetails;
import com.thinknsync.salesync.database.records.order.SalesOrderDetailsInterface;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;
import com.thinknsync.salesync.database.records.order.SalesOrder_;
import com.thinknsync.salesync.database.records.promotion.PromotionInterface;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.database.repositories.BaseRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import io.objectbox.Box;

public class OrderRepository extends BaseRepository implements OrderRepositoryInterface {

    private Box<SalesOrder> orderBox;
    private Box<SalesOrderDetails> orderDetailsBox;

    @Override
    protected void initializeObjectBox() {
        orderBox = getBoxFor(SalesOrder.class);
        orderDetailsBox = getBoxFor(SalesOrderDetails.class);
    }

    @Override
    public void save(SalesOrderInterface entity) {
        saveObject(SalesOrder.class, (SalesOrder)entity);
    }

    @Override
    public void saveOrderDetails(SalesOrderDetailsInterface entity) {
        saveObject(SalesOrderDetails.class, (SalesOrderDetails) entity);
    }

    @Override
    public List<SalesOrderInterface> getAll() {
        return (List<SalesOrderInterface>)(List<?>)orderBox.getAll();
    }

    @Override
    public List<SalesOrderInterface> getUnsyncedOrders() {
        return (List<SalesOrderInterface>)(List<?>)orderBox.query().equal(SalesOrder_.isSynced, false).build().find();
    }

    @Override
    public SalesOrderInterface getNewEntity() {
        return new SalesOrder();
    }

    @Override
    public SalesOrderDetailsInterface getNewOrderDetails() {
        return new SalesOrderDetails();
    }

    @Override
    public void deleteAll() {
        orderBox.removeAll();
    }

    @Override
    public void deleteOrder(SalesOrderInterface order) {
        orderBox.remove((SalesOrder)order);
    }

    @Override
    public void deleteOrders(SalesOrderInterface[] orders) {
        orderBox.remove(Arrays.asList(orders).toArray(new SalesOrder[orders.length]));
    }

    @Override
    public void deleteOrderLine(SalesOrderDetailsInterface orderLine) {
        orderDetailsBox.remove((SalesOrderDetails)orderLine);
    }

    @Override
    public void editOrderLine(SalesOrderInterface order, SkuInterface sku, double qty, double discount,
                              double total, HashMap<SkuInterface, Integer> promotionSkuQtyMap) {
        for(SalesOrderDetailsInterface orderDetails : order.getSalesOrderDetails()){
            if(orderDetails.getSku().getDataId() == sku.getDataId()){
                List<SalesOrderDetailsInterface> orderLinesToSave = new ArrayList<>();
                orderDetails.setQty(qty);
                orderDetails.setDiscount(discount);
                orderDetails.setSubTotal(total);
                orderLinesToSave.add(orderDetails);

                SalesOrderDetailsInterface promoOrderLine = getEditedPromotionOrderLineIfApplicable(orderDetails, sku, promotionSkuQtyMap);
                if(promoOrderLine != null){
                    orderLinesToSave.add(promoOrderLine);
                }
                updateOrderLines(orderLinesToSave.toArray(new SalesOrderDetailsInterface[0]));
                break;
            }
        }
    }

    @Nullable
    private SalesOrderDetailsInterface getEditedPromotionOrderLineIfApplicable(SalesOrderDetailsInterface orderDetails,
                                                                               SkuInterface sku,
                                                                               HashMap<SkuInterface, Integer> promotionSkuQtyMap){
        SalesOrderDetailsInterface promoOrderLine = getExistingPromoOrderLineIfExists(orderDetails.getParentOrder(), sku);
        if(promoOrderLine != null){
            if(promotionSkuQtyMap != null) {
                promoOrderLine.setQty(promotionSkuQtyMap.values().toArray(new Integer[0])[0]);
                return promoOrderLine;
            } else {
                deleteOrderLine(promoOrderLine);
            }
        } else if(promotionSkuQtyMap != null){
            Map.Entry<SkuInterface, Integer> freeSkuQtyEntryLine = promotionSkuQtyMap.entrySet().iterator().next();
            SalesOrderDetailsInterface promoDetails = getNewOrderDetails();
            promoDetails.setParentOrder(orderDetails.getParentOrder());
            promoDetails.setSku(freeSkuQtyEntryLine.getKey());
            promoDetails.setCreationDateTime(orderDetails.getCreationDateTime());
            promoDetails.setUpdateDateTime(orderDetails.getUpdateDateTime());
            promoDetails.setQty(freeSkuQtyEntryLine.getValue());
            promoDetails.setPromotion(sku.getSkuPromotion());
            return promoDetails;
        }
        return null;
    }

    @Override
    public void deleteOrderLine(SalesOrderInterface order, SkuInterface sku) {
        for(SalesOrderDetailsInterface orderDetails : order.getSalesOrderDetails()){
            if(orderDetails.getSku().getDataId() == sku.getDataId()){
                deleteOrderLine(orderDetails);
                order.getSalesOrderDetails().remove(orderDetails);
                SalesOrderDetailsInterface promoOrderLine = getExistingPromoOrderLineIfExists(order, sku);
                if(promoOrderLine != null){
                    deleteOrderLine(promoOrderLine);
                    order.getSalesOrderDetails().remove(promoOrderLine);
                }
                break;
            }
        }

        if(order.getSalesOrderDetails().size() == 0){
            deleteOrder(order);
        }
    }

    @Nullable
    private SalesOrderDetailsInterface getExistingPromoOrderLineIfExists(SalesOrderInterface order, SkuInterface sku) {
        if(sku.hasPromotion() && sku.getSkuPromotion().getPromotionType() == PromotionInterface.PromotionType.SKU){
            for(SalesOrderDetailsInterface promoOrderDetails : order.getSalesOrderDetails()){
                if(promoOrderDetails.getSku().getDataId() == sku.getSkuPromotion().getFreeSku().getDataId()){
                    return promoOrderDetails;
                }
            }
        }
        return null;
    }

    @Override
    public void saveOrders(SalesOrderInterface[] salesOrders){
        SalesOrder[] orders = Arrays.copyOf(salesOrders, salesOrders.length, SalesOrder[].class);
        saveObject(SalesOrder.class, orders);
    }

    @Override
    public void updateOrderLines(SalesOrderDetailsInterface[] salesOrderDetails){
        SalesOrderDetails[] details = Arrays.copyOf(salesOrderDetails, salesOrderDetails.length, SalesOrderDetails[].class);
        updateObject(SalesOrderDetails.class, details);
    }
}
