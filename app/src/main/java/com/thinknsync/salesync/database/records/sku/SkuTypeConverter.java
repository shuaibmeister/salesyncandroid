package com.thinknsync.salesync.database.records.sku;

import io.objectbox.converter.PropertyConverter;

public class SkuTypeConverter implements PropertyConverter<SkuType, Integer> {

    @Override
    public SkuType convertToEntityProperty(Integer databaseValue) {
        if (databaseValue == null) {
            return null;
        }
        for (SkuType skuType : SkuType.values()) {
            if (skuType.getId() == databaseValue) {
                return skuType;
            }
        }
        return SkuType.INTERNAL;
    }

    @Override
    public Integer convertToDatabaseValue(SkuType entityProperty) {
        return entityProperty == null ? null : entityProperty.getId();
    }
}
