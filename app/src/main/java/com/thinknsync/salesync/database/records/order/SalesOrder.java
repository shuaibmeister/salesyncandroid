package com.thinknsync.salesync.database.records.order;

import com.google.gson.JsonObject;
import com.thinknsync.salesync.database.records.BaseRecord;
import com.thinknsync.salesync.database.records.masterData.MasterData;
import com.thinknsync.salesync.database.records.masterData.MasterDataObject;
import com.thinknsync.salesync.database.records.outlet.Outlet;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.promotion.PromotionInterface;
import com.thinknsync.salesync.database.records.route.Route;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.database.records.user.User;
import com.thinknsync.salesync.database.records.user.UserInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;

import io.objectbox.annotation.Backlink;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToMany;
import io.objectbox.relation.ToOne;

@Entity
public class SalesOrder extends BaseRecord<SalesOrderInterface> implements SalesOrderInterface {

    @Id(assignable = true)
    private long id;
    private int dataId;
    protected ToOne<User> sr;
    protected ToOne<Route> route;
    public ToOne<Outlet> outlet;
    protected ToOne<MasterDataObject> exception;
    private Date outletInDateTime;
    private Date outletOutDateTime;
    private boolean isSynced = false;
    @Backlink(to = "parentOrder")
    protected ToMany<SalesOrderDetails> salesOrderDetails;
    private double invoiceDiscount;
    protected Date creationDate;
    protected Date updateDate;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public int getDataId() {
        return dataId;
    }

    @Override
    public void setDataId(int id) {
        this.dataId = id;
    }

    @Override
    public UserInterface getSr() {
        return sr.getTarget();
    }

    @Override
    public void setSr(UserInterface sr) {
        this.sr.setTarget((User)sr);
    }

    @Override
    public OutletInterface getOutlet() {
        return outlet.getTarget();
    }

    @Override
    public void setOutlet(OutletInterface outlet) {
        this.outlet.setTarget((Outlet)outlet);
        this.route.setTarget((Route)outlet.getParentRoute());
        this.setDataId(outlet.getDataId() + dateTimeUtils.getDateIntOfToday());
    }

    @Override
    public String getOrderDate() {
        return dateTimeUtils.getDefaultDateStringFromDate(getOutletInDateTime());
    }

    @Override
    public void setOrderDate(String orderDate) {
        setCreationDateTime(dateTimeUtils.getDateFromDefaultDateTimeString(orderDate));
    }

    @Override
    public MasterData getException() {
        return exception.getTarget();
    }

    @Override
    public void setException(MasterData exception) {
        this.exception.setTarget((MasterDataObject) exception);
    }

    @Override
    public Date getOutletInDateTime() {
        return outletInDateTime;
    }

    @Override
    public void setOutletInDateTime(Date outletInDateTime) {
        this.outletInDateTime = outletInDateTime;
    }

    @Override
    public Date getOutletOutDateTime() {
        return outletOutDateTime;
    }

    @Override
    public void setOutletOutDateTime(Date outletOutDateTime) {
        this.outletOutDateTime = outletOutDateTime;
    }

    @Override
    public boolean isSynced() {
        return isSynced;
    }

    @Override
    public void setSynced(boolean synced) {
        isSynced = synced;
    }

    @Override
    public List<SalesOrderDetailsInterface> getSalesOrderDetails(){
        return (List<SalesOrderDetailsInterface>)(List<?>)salesOrderDetails;
    }

    @Override
    public void setSalesOrderDetails(List<SalesOrderDetailsInterface> salesOrderDetails){
        this.salesOrderDetails.addAll((List<SalesOrderDetails>)(List<?>)salesOrderDetails);
    }

    @Override
    public double getInvoiceDiscount() {
        return invoiceDiscount;
    }

    @Override
    public void setInvoiceDiscount(double invoiceDiscount) {
        this.invoiceDiscount = invoiceDiscount;
    }

    @Override
    public Date getCreationDateTime() {
        return creationDate;
    }

    @Override
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDate = creationDateTime;
    }

    @Override
    public Date getUpdateDateTime() {
        return updateDate;
    }

    @Override
    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDate = updateDateTime;
    }

    @Override
    public double getOrderTotal(){
        double totalOrderValue = 0;
        for (SalesOrderDetailsInterface orderDetails: getSalesOrderDetails()) {
            totalOrderValue += orderDetails.getSubTotalWithDiscount();
        }
        return totalOrderValue;
    }

    @Override
    public double getOrderTotalWithDiscount(){
        return getOrderTotal() - getInvoiceDiscount();
    }

    @Override
    public List<SkuInterface> getUniqueSkus(){
        List<SkuInterface> uniqueSkus = new ArrayList<>();
        for (SalesOrderDetailsInterface orderDetails: getSalesOrderDetails()) {
            if(!uniqueSkus.contains(orderDetails.getSku())){
                uniqueSkus.add(orderDetails.getSku());
            }
        }
        return uniqueSkus;
    }

    @Override
    public int getTotalOrderedQty(){
        int totalQty = 0;
        for (SalesOrderDetailsInterface orderDetails: getSalesOrderDetails()) {
            totalQty += orderDetails.getQty();
        }

        return totalQty;
    }

    @Override
    public SalesOrderInterface fromJson(JSONObject jsonObject) throws JSONException {
        OutletInterface outlet = new Outlet().fromJson(jsonObject.getJSONObject(key_outlet));
        outlet.setParentRoute(new Route().fromJson(jsonObject.getJSONObject(key_route)));

        setDates(jsonObject);
        setId(jsonObject.optLong(key_id));
        setSr(new User().fromJson(jsonObject.getJSONObject(key_sr)));
        setOutlet(outlet);
        setOrderDate(jsonObject.getString(key_order_date));
        setException(new MasterDataObject().fromJson(jsonObject.optJSONObject(key_exception)));
        setSynced(jsonObject.optBoolean(key_is_synced));
        return this;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject().put(key_id, getId())
//                .put(key_sr, sr.getTarget().toJson())
                .put(key_identifier, getDataId())
                .put(key_sr_id, getSr().getDataId())
                .put(key_route, getOutlet().getParentRoute().toString())
                .put(key_route_id, getOutlet().getParentRoute().getDataId())
                .put(key_outlet, getOutlet().toJson())
                .put(key_outlet_id, getOutlet().getDataId())
                .put(key_order_date, getOrderDate())
                .put(key_exception, getException() == null ? new JsonObject() : getException().toJson())
                .put(key_exception_id, getException() == null ? -1 : getException().getDataId())
                .put(key_order_amount, getOrderTotal())
                .put(key_payable_amount, getOrderTotalWithDiscount())
                .put(key_discount, getInvoiceDiscount())
                .put(key_expected_delivery_date, getOrderDate())
                .put(key_in_date_time, getDateTimeStringFromDate(getOutletInDateTime()))
                .put(key_out_date_time, getDateTimeStringFromDate(getOutletOutDateTime()))
                .put(key_is_synced, isSynced());

        JSONArray orderDetailsJsonArray = new JSONArray();
        for (SalesOrderDetailsInterface orderDetails : getSalesOrderDetails()) {
            JSONObject orderDetailsJson = getOrderDetailsJsonArray(orderDetails);
            if(orderDetailsJson != null) {
                orderDetailsJsonArray.put(orderDetailsJson);
            }
        }
        jsonObject.put(key_order_lines, orderDetailsJsonArray);

        return setDatesInJsonObject(jsonObject);
    }

    @Nullable
    private JSONObject getOrderDetailsJsonArray(SalesOrderDetailsInterface orderDetails) throws JSONException {
        if(orderDetails.getPromotion() != null && orderDetails.getPromotion().getPromotionType() ==
                PromotionInterface.PromotionType.SKU){
            return getOfferOrderSkuLineJson(orderDetails);
        }

        if(!(orderDetails.getSku().hasPromotion() && orderDetails.getSku().getSkuPromotion().getPromotionType()
                == PromotionInterface.PromotionType.SKU)){
            return orderDetails.withoutProperty(getOrderLineJsonExcludeKeys());
        }
        return null;
    }

    @Nullable
    private JSONObject getOfferOrderSkuLineJson(SalesOrderDetailsInterface promoOrderLine) throws JSONException {
        for (SalesOrderDetailsInterface orderDetails : getSalesOrderDetails()) {
            if(orderDetails.getSku().getDataId() == promoOrderLine.getPromotion().getSellingSku().getDataId()){
                orderDetails.setPromotion(promoOrderLine.getPromotion());
                JSONObject orderDetailsJson = orderDetails.withoutProperty(getOrderLineJsonExcludeKeys());
                orderDetailsJson.put(PromotionInterface.key_free_sku_qty, promoOrderLine.getQty());
                orderDetailsJson.put(PromotionInterface.key_free_sku_id, promoOrderLine.getSku().getDataId());
                return orderDetailsJson;
            }
        }
        return null;
    }

    private String[] getOrderLineJsonExcludeKeys() {
        return new String[]{
                SalesOrderDetailsInterface.key_sku, SalesOrderDetailsInterface.key_id,
                SalesOrderDetailsInterface.key_promotion, SalesOrderDetailsInterface.key_order,
                SalesOrderDetailsInterface.key_created_at, SalesOrderDetailsInterface.key_updated_at
        };
    }
}
