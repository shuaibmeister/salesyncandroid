package com.thinknsync.salesync.database.repositories.config;

import com.thinknsync.salesync.database.records.DbRecord;
import com.thinknsync.salesync.database.records.config.ConfigInterface;
import com.thinknsync.salesync.database.repositories.RepositoryPolicy;

public interface ConfigRepositoryInterface extends RepositoryPolicy<ConfigInterface> {
    ConfigInterface getConfigOfType(ConfigInterface.ConfigType configType);
    void insertOrUpdateConfig(ConfigInterface.ConfigType configType, String value);
    void insertOrUpdateConfig(String configTypeString, String value);
    void insertOrUpdateConfigs(ConfigInterface[] configs);
}
