package com.thinknsync.salesync.database.records.config;

import com.thinknsync.salesync.database.records.DbRecord;

public interface ConfigInterface extends DbRecord<ConfigInterface> {
    String key_config_type = "config_type";
    String key_value = "value";

    ConfigType getConfigType();
    void setConfigType(ConfigType configType);
    String getValue();
    void setValue(String value);

    enum ConfigType {
        LOCATION_TRACKING_FLAG("location_track"),
        LOCATION_TRACKING_INTERVAL("time_in_minute"),
        FACE_ATTENDANCE_REQUIRED("face_attendance"),
        LAST_SYNC_DATE("last_sync"),
        SESSION_TIMEOUT_FLAG("session_timeout"),
        SESSION_TIMEOUT_INTERVAL("session_timeout_in_minute");

        private final String key;

        ConfigType(String key) {
            this.key = key;
        }

        public String getKey(){
            return this.key;
        }

        public static ConfigType getConfigTypeFromValue(String key){
            for (ConfigType configType : values()) {
                if(configType.getKey().equals(key)){
                    return configType;
                }
            }
            return null;
        }
    }
}
