package com.thinknsync.salesync.database.repositories.outlet;

import com.thinknsync.salesync.database.records.outlet.Outlet;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.outlet.Outlet_;
import com.thinknsync.salesync.database.repositories.BaseRepository;

import java.util.Arrays;
import java.util.List;

import io.objectbox.Box;

public class OutletRepository extends BaseRepository implements OutletRepositoryInterface {

    private Box<Outlet> outletBox;

    @Override
    protected void initializeObjectBox() {
        outletBox = getBoxFor(Outlet.class);
    }

    @Override
    public OutletInterface getOutletByOutletId(int outletId){
        return outletBox.query().equal(Outlet_.outletId, outletId).build().findFirst();
    }
    @Override
    public List<OutletInterface> getVerificationTypedOutletsOfRoute(int routeId, boolean isVerified) {
        return (List<OutletInterface>)(List<?>) outletBox.query().equal(Outlet_.parentRouteId, routeId)
                .equal(Outlet_.isVerified, isVerified).build().find();
    }

    @Override
    public void save(OutletInterface entity) {
        saveObject(Outlet.class, (Outlet)entity);
    }

    @Override
    public List<OutletInterface> getAll() {
        return (List<OutletInterface>)(List<?>)outletBox.getAll();
    }

    @Override
    public Outlet getNewEntity() {
        return new Outlet();
    }

    @Override
    public void deleteAll() {
        outletBox.removeAll();
    }

    @Override
    public void saveOutlets(OutletInterface[] outlets){
        Outlet[] outlets1 = Arrays.copyOf(outlets, outlets.length, Outlet[].class);
        saveObject(Outlet.class, outlets1);
    }

}
