package com.thinknsync.salesync.database.records.order;

import com.thinknsync.salesync.database.records.BaseRecord;
import com.thinknsync.salesync.database.records.promotion.PromotionDefinition;
import com.thinknsync.salesync.database.records.promotion.PromotionInterface;
import com.thinknsync.salesync.database.records.sku.Sku;
import com.thinknsync.salesync.database.records.sku.SkuInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToOne;

@Entity
public class SalesOrderDetails extends BaseRecord<SalesOrderDetailsInterface> implements SalesOrderDetailsInterface {

    @Id(assignable = true)
    private long id;
    protected ToOne<Sku> sku;
    private double qty;
    private double discount;
    private double subTotal = 0;
    protected ToOne<SalesOrder> parentOrder;
    protected ToOne<PromotionDefinition> promotion;
    protected Date creationDate;
    protected Date updateDate;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public double getDiscount() {
        return discount;
    }

    @Override
    public void setDiscount(double discount) {
        this.discount = discount;
    }

    @Override
    public SkuInterface getSku() {
        return sku.getTarget();
    }

    @Override
    public void setSku(SkuInterface sku) {
        this.sku.setTarget((Sku)sku);
    }

    @Override
    public double getQty() {
        return qty;
    }

    @Override
    public void setQty(double qty) {
        this.qty = qty;
    }

    @Override
    public double getSubTotal() {
        return subTotal;
    }

    @Override
    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    @Override
    public double getSubTotalWithDiscount() {
        return getSubTotal() - getDiscount();
    }

    @Override
    public SalesOrderInterface getParentOrder(){
        return parentOrder.getTarget();
    }

    @Override
    public void setParentOrder(SalesOrderInterface parentOrder){
        this.parentOrder.setTarget((SalesOrder) parentOrder);
    }

    @Override
    public PromotionInterface getPromotion() {
        return promotion.getTarget();
    }

    @Override
    public void setPromotion(PromotionInterface promotion) {
        this.promotion.setTarget((PromotionDefinition) promotion);
    }

    @Override
    public Date getCreationDateTime() {
        return creationDate;
    }

    @Override
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDate = creationDateTime;
    }

    @Override
    public Date getUpdateDateTime() {
        return updateDate;
    }

    @Override
    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDate = updateDateTime;
    }

    @Override
    public SalesOrderDetailsInterface fromJson(JSONObject jsonObject) throws JSONException {
        setDates(jsonObject);
        setId(jsonObject.optLong(key_id));
        setParentOrder(new SalesOrder().fromJson(jsonObject.optJSONObject(key_order)));
        setSku(new Sku().fromJson(jsonObject.optJSONObject(key_sku)));
        setQty(jsonObject.getInt(key_qty));
        setDiscount(jsonObject.getDouble(key_discount));
        setSubTotal(jsonObject.getDouble(key_sub_total));
        setPromotion(new PromotionDefinition().fromJson(jsonObject.optJSONObject(key_promotion)));
        return this;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jso = new JSONObject().put(key_id, getId())
//                .put(key_order, getParentOrder().toJson())
                .put(key_order_id, getParentOrder().getId())
                .put(key_sku, getSku().toJson())
                .put(key_sku_id, getSku().getDataId())
                .put(key_unit_price, getSku().getUnitPrice())
                .put(key_qty, getQty())
                .put(key_discount, getDiscount())
                .put(key_sub_total, getSubTotal())
                .put(key_promotion, getPromotion() == null ? new JSONObject() : getPromotion().toJson())
                .put(key_promotion_id, getPromotion() == null ? 0 : getPromotion().getDataId())
                .put("free_sku_id", 0)
                .put("free_sku_qty", 0);

        return setDatesInJsonObject(jso);
    }
}
