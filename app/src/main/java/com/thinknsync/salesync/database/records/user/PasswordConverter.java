package com.thinknsync.salesync.database.records.user;

import android.util.Base64;

import io.objectbox.converter.PropertyConverter;

public class PasswordConverter implements PropertyConverter<String, String> {
    @Override
    public String convertToEntityProperty(String databaseValue) {
        return new String(Base64.decode(databaseValue.getBytes(), Base64.DEFAULT));
    }

    @Override
    public String convertToDatabaseValue(String entityProperty) {
        return Base64.encodeToString(entityProperty.getBytes(), Base64.DEFAULT);
    }
}
