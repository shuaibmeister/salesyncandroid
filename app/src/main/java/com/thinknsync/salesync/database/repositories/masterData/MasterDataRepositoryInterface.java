package com.thinknsync.salesync.database.repositories.masterData;

import com.thinknsync.salesync.database.records.masterData.MasterData;
import com.thinknsync.salesync.database.repositories.RepositoryPolicy;

import java.util.List;

public interface MasterDataRepositoryInterface extends RepositoryPolicy<MasterData>{
    List<MasterData> getMasterDataOfType(MasterData.MasterDataType masterDataType);
    MasterData getMasterDataByIdAndType(int masterDataId, MasterData.MasterDataType masterDataType);
    void saveMasterDataObjects(MasterData[] entities);
}
