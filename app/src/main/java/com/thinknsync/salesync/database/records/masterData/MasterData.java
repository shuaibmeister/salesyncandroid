package com.thinknsync.salesync.database.records.masterData;

import com.thinknsync.salesync.commons.commonInterfaces.HasIdentifier;
import com.thinknsync.salesync.commons.commonInterfaces.HasName;
import com.thinknsync.salesync.database.records.DbRecord;

public interface MasterData extends DbRecord<MasterData>, HasIdentifier, HasName {
    String key_data_id = "id";
    String key_name = "name";

    MasterDataType getType();
    void setType(MasterDataType type);

    enum MasterDataType {
        NONE(0),
        OUTLET_TYPE(1),
        OUTLET_CLASSIFICATION(5),
        PAYMENT_METHOD(2),
        EXCEPTION_REASON(3),
        DISTRIBUTION_HOUSE(4)
        ;

        private final int id;

        MasterDataType(int id) {
            this.id = id;
        }

        public int getId(){
            return id;
        }

        public static MasterDataType getMasterDataTypeFromId(int dataTypeId){
            for (MasterDataType masterData : values()) {
                if(masterData.getId() == dataTypeId){
                    return masterData;
                }
            }
            return NONE;
        }
    }
}
