package com.thinknsync.salesync.database.records.sku;

import com.thinknsync.listviewadapterhelper.searchable.SearchableObject;
import com.thinknsync.salesync.database.records.BaseRecord;
import com.thinknsync.salesync.database.records.masterData.MasterData;
import com.thinknsync.salesync.database.records.masterData.MasterDataObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToOne;

@Entity
public class SkuStock extends BaseRecord<SkuStockInterface> implements SkuStockInterface, SearchableObject {

    @Id(assignable = true)
    private long id;
    protected ToOne<Sku> sku;
    protected ToOne<MasterDataObject> dbMasterData;
    private double ctn;
    private double pcs;
    private String volume;
    protected Date creationDate;
    protected Date updateDate;

    @Override
    public long getId() {
        return this.id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Date getCreationDateTime() {
        return creationDate;
    }

    @Override
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDate = creationDateTime;
    }

    @Override
    public Date getUpdateDateTime() {
        return updateDate;
    }

    @Override
    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDate = updateDateTime;
    }

    @Override
    public SkuInterface getSku() {
        return sku.getTarget();
    }

    @Override
    public void setSku(SkuInterface sku) {
        this.sku.setTarget((Sku)sku);
    }

    @Override
    public MasterData getDbMasterData() {
        return dbMasterData.getTarget();
    }

    @Override
    public void setDbMasterData(MasterData dbMasterData) {
        this.dbMasterData.setTarget((MasterDataObject)dbMasterData);
    }

    @Override
    public String getVolume() {
        return volume;
    }

    @Override
    public void setVolume(String volume) {
        this.volume = volume;
    }

    @Override
    public double getCtn() {
        return ctn;
    }

    @Override
    public void setCtn(double ctn) {
        this.ctn = ctn;
    }

    @Override
    public double getPcs() {
        return pcs;
    }

    @Override
    public void setPcs(double pcs) {
        this.pcs = pcs;
    }

    @Override
    public SkuStockInterface fromJson(JSONObject jsonObject) throws JSONException {
        setDates(jsonObject);
        setCtn(jsonObject.getDouble(key_ctn));
        setPcs(jsonObject.getDouble(key_pcs));
        setVolume(jsonObject.getString(key_volume));

        Sku stockForSku = new Sku();
        stockForSku.setId(jsonObject.getLong(SkuInterface.key_sku_id));
        setSku(stockForSku);

        MasterDataObject dbMaster = new MasterDataObject();
        dbMaster.setId(jsonObject.getLong(key_db_id));
        setDbMasterData(dbMaster);

        return this;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jso = new JSONObject().put(SkuInterface.key_sku_name, getSku().getName())
                .put(SkuInterface.key_sku_code, getSku().getCode())
                .put(key_ctn, getCtn())
                .put(key_pcs, getPcs())
                .put(key_volume, getVolume())
                .put(key_db_name, getDbMasterData().getName())
                .put(SkuInterface.key_sku_id, getSku().getDataId())
                .put(SkuInterface.key_sku_name, getSku().getName())
                .put(key_db_id, getDbMasterData().getDataId());
        return setDatesInJsonObject(jso);
    }

    @Override
    public String[] getSearchableProperties() {
        return new String[]{getSku().getName(), getSku().getCode()};
    }
}
