package com.thinknsync.salesync.database.records.sku;

import com.thinknsync.salesync.database.records.BaseRecord;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;

import io.objectbox.annotation.Backlink;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Unique;
import io.objectbox.relation.ToMany;
import io.objectbox.relation.ToOne;

@Entity
public class SkuParent extends BaseRecord<SkuParentInterface> implements SkuParentInterface {

    @Id(assignable = true)
    private long id;
    @Unique
    protected int identifier;
    protected String name;
    private String image;
    protected ToOne<SkuParent> parent;
    @Backlink(to = "parentSku")
    protected ToMany<Sku> skus;
    @Backlink(to = "parent")
    protected ToMany<SkuParent> children;
    protected Date creationDate;
    protected Date updateDate;

    @Override
    public long getId() {
        return this.id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Date getCreationDateTime() {
        return creationDate;
    }

    @Override
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDate = creationDateTime;
    }

    @Override
    public Date getUpdateDateTime() {
        return updateDate;
    }

    @Override
    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDate = updateDateTime;
    }

    @Override
    public int getDataId() {
        return identifier;
    }

    @Override
    public void setDataId(int parentId) {
        this.identifier = parentId;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String parentName) {
        this.name = parentName;
    }

    @Override
    public String getImage() {
        return image;
    }

    @Override
    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public List<SkuInterface> getChildSkus(){
        return (List<SkuInterface>)(List<?>)skus;
    }

    @Override
    public void setChildSkus(List<SkuInterface> childSkus){
        skus.addAll((List<Sku>)(List<?>)childSkus);
    }

    @Nullable
    @Override
    public SkuParentInterface getParent(){
        return parent.getTarget();
    }

    @Override
    public void setParent(SkuParentInterface skuParentsParent){
        parent.setTarget((SkuParent)skuParentsParent);
    }

    @Override
    public List<SkuParentInterface> getChildren(){
        return (List<SkuParentInterface>)(List<?>)children;
    }

    @Override
    public void setChildren(List<SkuParentInterface> children){
        for(SkuParentInterface child : children){
            child.setParent(this);
        }
        this.children.addAll((List<SkuParent>)(List<?>)children);
    }

    @Override
    public SkuParent fromJson(JSONObject jsonObject) throws JSONException {
        setDates(jsonObject);
        setDataId(jsonObject.has(key_parent_id) ? jsonObject.getInt(key_parent_id) : jsonObject.getInt(key_parents_parent_id));
        setName(jsonObject.has(key_parent_name) ? jsonObject.getString(key_parent_name) : jsonObject.getString(key_parents_parent_name));
        return this;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jso = new JSONObject().put(key_parent_id, getDataId())
                .put(key_parent_name, getName());
        return setDatesInJsonObject(jso);
    }

    @Override
    public String[] getSearchableProperties() {
        return new String[]{getName()};
    }
}
