package com.thinknsync.salesync.database.records;

import com.thinknsync.convertible.BaseConvertible;
import com.thinknsync.salesync.commons.utils.DateTimeUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;


public abstract class BaseRecord<T> extends BaseConvertible<T> implements DbRecord<T> {
    protected DateTimeUtils dateTimeUtils = new DateTimeUtils();

    protected String getDateTimeStringFromDate(Date date){
        return dateTimeUtils.getDefaultDateTimeStringFromDate(date);
    }

    protected Date getDateTimeFromDateTimeString(String dateTime){
        return dateTimeUtils.getDateFromDefaultDateTimeString(dateTime);
    }

    protected Date getDateTimeFromNow(){
        return dateTimeUtils.getDefaultDateTimeNow();
    }

    protected void setDates(JSONObject jsonObject){
        setUpdateDateTime(jsonObject.isNull(key_created_at) ?
                getDateTimeFromNow() : getDateTimeFromDateTimeString(jsonObject.optString(key_created_at)));
        setCreationDateTime(jsonObject.isNull(key_updated_at) ?
                getDateTimeFromNow() : getDateTimeFromDateTimeString(jsonObject.optString(key_updated_at)));
    }

    protected JSONObject setDatesInJsonObject(JSONObject jsonObject) throws JSONException{
        jsonObject.put(key_created_at, getCreationDateTime() == null ?
                getDateTimeStringFromDate(getDateTimeFromNow()) : getDateTimeStringFromDate(getCreationDateTime()));
        jsonObject.put(key_updated_at, getUpdateDateTime() == null ?
                getDateTimeStringFromDate(getDateTimeFromNow()) : getDateTimeStringFromDate(getUpdateDateTime()));
        return jsonObject;
    }
}
