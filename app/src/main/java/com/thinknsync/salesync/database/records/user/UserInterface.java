package com.thinknsync.salesync.database.records.user;

import com.thinknsync.salesync.commons.commonInterfaces.HasCode;
import com.thinknsync.salesync.commons.commonInterfaces.HasIdentifier;
import com.thinknsync.salesync.commons.commonInterfaces.HasImage;
import com.thinknsync.salesync.commons.commonInterfaces.HasName;
import com.thinknsync.salesync.database.records.DbRecord;
import com.thinknsync.salesync.home.bottomSlidingView.AttendanceManager;

import java.util.Date;

public interface UserInterface extends DbRecord<UserInterface>, HasIdentifier, HasName, HasCode, HasImage {
    String tag = "user";

    String key_name = "sr_name";
    String key_sr_id = "sr_id";
    String key_code = "sr_code";
    String key_db_name = "db_name";
    String key_email = "email";
    String key_phone = "mobile";
    String key_designation = "designation";
    String key_address = "address";
    String key_user_id = "user_id";
    String key_user_image = "user_image";
    String key_target = "target";
    String key_achievement = "achievement";
    String key_run_rate = "sr_run_rate";
    String key_logged_in = "logged_in";
    String key_token = "authentication_token";
    String key_attendance_status = "attendance_status";
    String key_attendance_in_date_time = "attendance_date_time";
    String key_attendance_out_date_time = "attendance_out_date_time";
    String key_has_punched_out = "has_punched_out";
    String key_last_login_datetime = "last_login_datetime";

    String getDbName();
    void setDbName(String dbName);
    String getEmail();
    void setEmail(String email);
    String getPhone();
    void setPhone(String phone);
    String getDesignation();
    void setDesignation(String designation);
    String getAddress();
    void setAddress(String address);
    String getUserId();
    void setUserId(String userId);
    String getPassword();
    void setPassword(String password);
    double getTarget();
    void setTarget(double target);
    double getAchievement();
    void setAchievement(double achievement);
    double getRunRate();
    void setRunRate(double runRate);
    void setIsLoggedIn(boolean loggedIn);
    boolean isLoggedIn();
    String getLoginToken();
    void setLoginToken(String loginToken);
    AttendanceManager.AttendanceStatus getAttendanceStatusToday();
    void setAttendanceStatusToday(AttendanceManager.AttendanceStatus attendanceStatusToday);

    Date getAttendanceInDateTime();
    void setAttendanceInDateTime(Date attendanceInDateTime);
    Date getAttendanceOutDateTime();
    void setAttendanceOutDateTime(Date attendanceOutDateTime);
    Date getLastLoginTime();
    void setLastLoginTime(Date lastLoginTime);
}
