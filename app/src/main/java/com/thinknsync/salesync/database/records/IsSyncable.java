package com.thinknsync.salesync.database.records;

public interface IsSyncable {
    String key_is_synced = "is_synced";

    boolean isSynced();
    void setSynced(boolean synced);
}
