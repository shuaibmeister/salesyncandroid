package com.thinknsync.salesync.database.records.route;

import com.thinknsync.salesync.database.records.BaseRecord;
import com.thinknsync.salesync.database.records.outlet.Outlet;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import io.objectbox.annotation.Backlink;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToMany;

@Entity
public class Route extends BaseRecord<RouteInterface> implements RouteInterface{

    @Id(assignable = true)
    private long id;
    protected int routeId;
    protected String routeName;
    private boolean isTodaysRoute;
    @Backlink(to = "parentRoute")
    protected ToMany<Outlet> outlets;
    protected Date creationDate;
    protected Date updateDate;

    @Override
    public long getId() {
        return this.id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Date getCreationDateTime() {
        return creationDate;
    }

    @Override
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDate = creationDateTime;
    }

    @Override
    public Date getUpdateDateTime() {
        return updateDate;
    }

    @Override
    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDate = updateDateTime;
    }

    @Override
    public int getDataId() {
        return routeId;
    }

    @Override
    public void setDataId(int routeId) {
        this.routeId = routeId;
    }

    @Override
    public String getName() {
        return routeName;
    }

    @Override
    public void setName(String routeName) {
        this.routeName = routeName;
    }

    @Override
    public boolean isTodaysRoute() {
        return isTodaysRoute;
    }

    @Override
    public void setTodaysRoute(boolean todaysRoute) {
        isTodaysRoute = todaysRoute;
    }

    @Override
    public List<OutletInterface> getOutletsOfRoute() {
        return (List<OutletInterface>)(List<?>)outlets;
    }

    @Override
    public void setOutletsOfRoute(List<OutletInterface> outletsOfRoute) {
        outlets.addAll((List<Outlet>)(List<?>)outletsOfRoute);
    }

    @Override
    public RouteInterface fromJson(JSONObject jsonObject) throws JSONException {
        setDates(jsonObject);
        setId(jsonObject.optLong(key_id));
        setDataId(jsonObject.getInt(key_route_id));
        setName(jsonObject.getString(key_route_name));
        setTodaysRoute(jsonObject.optBoolean(key_is_todays_route));
        return this;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jso = new JSONObject().put(key_id, getId())
                .put(key_route_id, getDataId())
                .put(key_route_name, getName())
                .put(key_is_todays_route, isTodaysRoute());
        return setDatesInJsonObject(jso);
    }

    @Override
    public String[] getSearchableProperties() {
        return new String[]{key_route_name};
    }
}
