package com.thinknsync.salesync.database.repositories.sku;

import com.thinknsync.salesync.database.records.sku.Sku;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.database.records.sku.SkuParent;
import com.thinknsync.salesync.database.records.sku.SkuParentInterface;
import com.thinknsync.salesync.database.records.sku.SkuParent_;
import com.thinknsync.salesync.database.records.sku.SkuStock;
import com.thinknsync.salesync.database.records.sku.SkuStockInterface;
import com.thinknsync.salesync.database.records.sku.Sku_;
import com.thinknsync.salesync.database.records.sku.Uom;
import com.thinknsync.salesync.database.records.sku.UomInterface;
import com.thinknsync.salesync.database.repositories.BaseRepository;

import java.util.Arrays;
import java.util.List;

import io.objectbox.Box;

public class SkuRepository extends BaseRepository implements SkuRepositoryInterface {

    private Box<Sku> skuBox;
    private Box<SkuParent> skuParentBox;
    private Box<Uom> uomBox;

    @Override
    protected void initializeObjectBox() {
        skuBox = getBoxFor(Sku.class);
        skuParentBox = getBoxFor(SkuParent.class);
        uomBox = getBoxFor(Uom.class);
    }

    @Override
    public List<SkuInterface> getAll() {
        return (List<SkuInterface>)(List<?>)skuBox.getAll();
    }

    @Override
    public void save(SkuInterface sku) {
        saveObject(Sku.class, (Sku) sku);
    }

    @Override
    public SkuInterface getNewEntity() {
        return new Sku();
    }

    @Override
    public SkuParentInterface getNewSkuParent() {
        return new SkuParent();
    }

    @Override
    public SkuStockInterface getNewSkuStock() {
        return new SkuStock();
    }

    @Override
    public UomInterface getNewUom() {
        return new Uom();
    }

    @Override
    public void deleteAll() {
        skuBox.removeAll();
    }

    @Override
    public void saveSkuParent(SkuParentInterface skuParent) {
        saveObject(SkuParent.class, (SkuParent) skuParent);
    }

    @Override
    public void saveUom(UomInterface uom) {
        saveObject(Uom.class, (Uom)uom);
    }

    @Override
    public List<SkuParentInterface> getSkuParents() {
        return (List<SkuParentInterface>)(List<?>)skuParentBox.getAll();
    }

    @Override
    public List<SkuParentInterface> getSkuParentsTopHierarchy() {
        return (List<SkuParentInterface>)(List<?>)skuParentBox.query().equal(SkuParent_.parentId, 0).build().find();
    }

    @Override
    public List<SkuParentInterface> getSkuParentsBottomHierarchy() {
        return (List<SkuParentInterface>)(List<?>)skuParentBox.query().notEqual(SkuParent_.parentId, 0).build().find();
    }

    @Override
    public List<SkuInterface> getSkuOfParent(int parentId) {
        return skuParentBox.query().equal(SkuParent_.identifier, parentId).build().findFirst().getChildSkus();
    }

    @Override
    public SkuInterface getSkuById(int skuId) {
        return skuBox.query().equal(Sku_.skuId, skuId).build().findFirst();
    }

    @Override
    public SkuParentInterface getSkuParentById(int skuParentId) {
        return skuParentBox.query().equal(SkuParent_.identifier, skuParentId).build().findFirst();
    }

    @Override
    public void deleteAllSkuParent() {
        skuParentBox.removeAll();
    }

    @Override
    public void deleteAllUom() {
        uomBox.removeAll();
    }

    @Override
    public void saveSkuParentImage(SkuParentInterface skuParent, String image) {
        skuParent.setImage(image);
        saveSkuParent(skuParent);
    }

    @Override
    public void saveSkus(SkuInterface[] skus){
        Sku[] skus1 = Arrays.copyOf(skus, skus.length, Sku[].class);
        saveObject(Sku.class, skus1);
    }

    @Override
    public void saveSkuStocks(SkuStockInterface[] skuStocks) {
        SkuStock[] skusStocksToSave = Arrays.copyOf(skuStocks, skuStocks.length, SkuStock[].class);
        saveObject(SkuStock.class, skusStocksToSave);
    }

    @Override
    public void saveSkuParents(SkuParentInterface[] skuParents) {
        SkuParent[] skusParents1 = Arrays.copyOf(skuParents, skuParents.length, SkuParent[].class);
        saveObject(SkuParent.class, skusParents1);
    }

    @Override
    public List<SkuInterface> getSkusByDataIds(List<Integer> promotionSkuIds) {
        int[] promotionSkuIdArray = new int[promotionSkuIds.size()];
        for (int i = 0; i < promotionSkuIdArray.length; i++) {
            promotionSkuIdArray[i] = promotionSkuIds.get(i);
        }
        return (List<SkuInterface>)(List<?>) skuBox.query().in(Sku_.skuId, promotionSkuIdArray).build().find();
    }

    @Override
    public void updateSkus(SkuInterface[] skusToUpdate){
        Sku[] skusToSave = Arrays.copyOf(skusToUpdate, skusToUpdate.length, Sku[].class);
        updateObject(Sku.class, skusToSave);
    }
}
