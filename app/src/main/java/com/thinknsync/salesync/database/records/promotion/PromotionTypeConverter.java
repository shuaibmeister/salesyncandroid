package com.thinknsync.salesync.database.records.promotion;

import io.objectbox.converter.PropertyConverter;

public class PromotionTypeConverter implements PropertyConverter<PromotionInterface.PromotionType, Integer> {

    @Override
    public PromotionInterface.PromotionType convertToEntityProperty(Integer databaseValue) {
        if (databaseValue == null) {
            return null;
        }
        for (PromotionInterface.PromotionType promotionType : PromotionInterface.PromotionType.values()) {
            if (promotionType.getId() == databaseValue) {
                return promotionType;
            }
        }
        return PromotionInterface.PromotionType.CASH;
    }

    @Override
    public Integer convertToDatabaseValue(PromotionInterface.PromotionType entityProperty) {
        return entityProperty == null ? null : entityProperty.getId();
    }
}
