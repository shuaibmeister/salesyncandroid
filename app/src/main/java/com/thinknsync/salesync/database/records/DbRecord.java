package com.thinknsync.salesync.database.records;

import com.thinknsync.convertible.Convertible;

import java.util.Date;

public interface DbRecord<T> extends Convertible<T> {
    String key_id = "id";
    String key_created_at = "created_at";
    String key_updated_at = "updated_at";

    long getId();
    void setId(long id);
    Date getCreationDateTime();
    void setCreationDateTime(Date creationDateTime);
    Date getUpdateDateTime();
    void setUpdateDateTime(Date updateDateTime);
}
