package com.thinknsync.salesync.database.repositories.route;


import com.thinknsync.salesync.database.records.DbRecord;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.route.RouteInterface;
import com.thinknsync.salesync.database.repositories.RepositoryPolicy;

import java.util.List;

public interface RouteRepositoryInterface extends RepositoryPolicy<RouteInterface>{
    List<RouteInterface> getRouteWithCondition(boolean todaysOrOthers);
    List<OutletInterface> getOutletsOfRoute(int routeId);
    void saveRoutes(RouteInterface[] routes);
}
