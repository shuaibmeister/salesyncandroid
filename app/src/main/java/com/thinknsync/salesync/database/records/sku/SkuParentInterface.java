package com.thinknsync.salesync.database.records.sku;

import com.thinknsync.listviewadapterhelper.searchable.SearchableObject;
import com.thinknsync.salesync.commons.commonInterfaces.HasIdentifier;
import com.thinknsync.salesync.commons.commonInterfaces.HasImage;
import com.thinknsync.salesync.commons.commonInterfaces.HasName;
import com.thinknsync.salesync.database.records.DbRecord;

import java.util.List;

public interface SkuParentInterface extends DbRecord<SkuParentInterface>, SearchableObject, HasIdentifier, HasName, HasImage {

    String key_parent_id = "parent_id";
    String key_parents_parent_id = "parents_parent_id";
    String key_parent_name = "parent_name";
    String key_parents_parent_name = "parents_parent_name";
    String key_image = "product_image";

    List<SkuInterface> getChildSkus();
    void setChildSkus(List<SkuInterface> childSkus);
    SkuParentInterface getParent();
    void setParent(SkuParentInterface skuParentsParent);
    List<SkuParentInterface> getChildren();
    void setChildren(List<SkuParentInterface> children);
}
