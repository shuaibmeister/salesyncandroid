package com.thinknsync.salesync.database;

import android.content.Context;
import android.util.Log;

import com.thinknsync.salesync.BuildConfig;
import com.thinknsync.salesync.database.records.MyObjectBox;

import io.objectbox.BoxStore;
import io.objectbox.android.AndroidObjectBrowser;

public class DbContext {

    private static BoxStore boxStore;

    public static void init(Context context) {
        boxStore = MyObjectBox.builder()
                .androidContext(context.getApplicationContext())
                .build();
//        if (BuildConfig.DEBUG) {
//            boolean started = new AndroidObjectBrowser(boxStore).start(context.getApplicationContext());
//            Log.i("ObjectBrowser", "Started: " + started);
//        }
    }

    public static BoxStore getStore() { return boxStore; }
}
