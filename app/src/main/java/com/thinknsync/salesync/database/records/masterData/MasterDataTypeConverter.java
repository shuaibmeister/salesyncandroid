package com.thinknsync.salesync.database.records.masterData;

import io.objectbox.converter.PropertyConverter;

public class MasterDataTypeConverter implements PropertyConverter<MasterData.MasterDataType, Integer> {

    @Override
    public MasterData.MasterDataType convertToEntityProperty(Integer databaseValue) {
        if (databaseValue == null) {
            return null;
        }
        for (MasterData.MasterDataType masterDataType : MasterData.MasterDataType.values()) {
            if (masterDataType.getId() == databaseValue) {
                return masterDataType;
            }
        }
        return MasterData.MasterDataType.NONE;
    }

    @Override
    public Integer convertToDatabaseValue(MasterData.MasterDataType entityProperty) {
        return entityProperty == null ? null : entityProperty.getId();
    }
}
