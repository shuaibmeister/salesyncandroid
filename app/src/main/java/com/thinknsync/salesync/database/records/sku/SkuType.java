package com.thinknsync.salesync.database.records.sku;

public enum SkuType {
    INTERNAL(1, "Internal SKU"),
    EXTERNAL(2, "External SKU");

    private int id;
    private String text;

    SkuType(int id, String text) {
        this.id = id;
        this.text= text;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public static SkuType getSkuTypeById(int id){
        for (SkuType skuType : values()) {
            if(skuType.getId() == id){
                return skuType;
            }
        }

        return INTERNAL;
    }
}
