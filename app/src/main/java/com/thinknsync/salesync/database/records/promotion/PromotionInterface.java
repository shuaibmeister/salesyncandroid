package com.thinknsync.salesync.database.records.promotion;

import com.thinknsync.salesync.commons.commonInterfaces.HasIdentifier;
import com.thinknsync.salesync.commons.commonInterfaces.HasName;
import com.thinknsync.salesync.database.records.DbRecord;
import com.thinknsync.salesync.database.records.sku.Sku;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import java.util.Date;

import io.objectbox.relation.ToOne;

public interface PromotionInterface extends DbRecord<PromotionInterface>, HasIdentifier, HasName {

    String tag = "promotion";

    String key_promo_id = "id";
    String key_promo_name = "name";
    String key_promo_description = "description";
    String key_promo_date_from = "date_from";
    String key_promo_date_to = "date_to";
    String key_promo_type_id = "promotion_type_id";

    String key_promo_condition = "promotion_condition";
    String key_promo_selling_sku_id = "selling_sku_id";
    String key_promo_selling_sku_qty = "selling_qty";

    String key_promo_offer = "promotion_offer";
    String key_free_sku_id = "free_sku_id";
    String key_free_sku_qty = "free_sku_qty";
    String key_free_amount = "free_amount";

    String getDescription();
    void setDescription(String description);
    Date getStartDate();
    void setStartDate(Date startDate);
    Date getEndDate();
    void setEndDate(Date endDate);
    PromotionType getPromotionType();
    void setPromotionType(PromotionType promotionType);
    SkuInterface getSellingSku();
    void setSellingSku(SkuInterface sellingSku);
    int getSellingQty();
    void setSellingQty(int sellingQty);
    SkuInterface getFreeSku();
    void setFreeSku(SkuInterface freeSku);
    int getFreeSkuQty();
    void setFreeSkuQty(int freeSkuQty);
    double getFreeAmount();
    void setFreeAmount(double freeAmount);

    enum PromotionType {
        CASH(1, "Cash"),
        SKU(2, "Product");

        private int id;
        private String text;

        PromotionType(int id, String text) {
            this.id = id;
            this.text= text;
        }

        public int getId() {
            return id;
        }

        public String getText() {
            return text;
        }

        public static PromotionType getPromotionTypeById(int id){
            for (PromotionType promoType : PromotionType.values()) {
                if(promoType.getId() == id){
                    return promoType;
                }
            }

            return null;
        }
    }
}
