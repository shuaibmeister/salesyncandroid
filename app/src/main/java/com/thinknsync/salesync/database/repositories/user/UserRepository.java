package com.thinknsync.salesync.database.repositories.user;

import androidx.annotation.Nullable;

import com.thinknsync.salesync.database.records.user.User;
import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.database.records.user.User_;
import com.thinknsync.salesync.database.repositories.BaseRepository;

import java.util.List;

import io.objectbox.Box;

public class UserRepository extends BaseRepository implements UserRepositoryInterface {

    private Box<User> userBox;
    private static UserInterface loggedInUser;

    @Override
    protected void initializeObjectBox() {
        userBox = getBoxFor(User.class);
    }

    @Override
    public UserInterface getLoggedInUser() {
        if(loggedInUser == null) {
            loggedInUser = userBox.query().equal(User_.isLoggedIn, true).build().findFirst();
            if(loggedInUser == null){
                loggedInUser = getNewEntity();
            }
        }
        return loggedInUser;
    }

    @Override
    public UserInterface getUser(){
        UserInterface databaseUser = userBox.query().build().findFirst();
        if(databaseUser == null){
            return getNewEntity();
        }
        return databaseUser;
    }

    @Override
    @Nullable
    public UserInterface getUserIfValid(String username, String pass){
        UserInterface systemUser = getUser();
        if(systemUser.getUserId().equals(username) && systemUser.getPassword().equals(pass)){
            return systemUser;
        }
        return null;
    }

    @Override
    public String getAuthToken() {
        return getLoggedInUser().getLoginToken();
    }

    @Override
    public void save(UserInterface user) {
        loggedInUser = user;
        if(user.getId() == 0) {
            saveObject(User.class, (User) user);
        } else {
            updateObject(User.class, (User) user);
        }
    }

    @Override
    public List<UserInterface> getAll() {
        return (List<UserInterface>)(List<?>)userBox.getAll();
    }

    @Override
    public User getNewEntity() {
        return new User();
    }

    @Override
    public void deleteAll() {
        userBox.removeAll();
    }

    @Override
    public double getTarget(){
        return getLoggedInUser().getTarget();
    }

    @Override
    public double getAchievement(){
        return getLoggedInUser().getAchievement();
    }

    @Override
    public double getRunRate(){
        return getLoggedInUser().getRunRate();
    }
}
