package com.thinknsync.salesync.database.repositories.sku;


import com.thinknsync.salesync.database.records.DbRecord;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.route.RouteInterface;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.database.records.sku.SkuParent;
import com.thinknsync.salesync.database.records.sku.SkuParentInterface;
import com.thinknsync.salesync.database.records.sku.SkuStockInterface;
import com.thinknsync.salesync.database.records.sku.UomInterface;
import com.thinknsync.salesync.database.repositories.RepositoryPolicy;

import org.json.JSONArray;

import java.util.List;

public interface SkuRepositoryInterface extends RepositoryPolicy<SkuInterface>{
    void saveSkuParent(SkuParentInterface skuParent);
    void saveUom(UomInterface uom);
    List<SkuParentInterface> getSkuParents();
    List<SkuParentInterface> getSkuParentsTopHierarchy();
    List<SkuParentInterface> getSkuParentsBottomHierarchy();
    List<SkuInterface> getSkuOfParent(int parentId);
    SkuInterface getSkuById(int skuId);
    SkuParentInterface getNewSkuParent();
    SkuStockInterface getNewSkuStock();
    UomInterface getNewUom();

    SkuParentInterface getSkuParentById(int skuParentId);

    void deleteAllSkuParent();
    void deleteAllUom();
    void saveSkuParentImage(SkuParentInterface skuParent, String image);
    void saveSkus(SkuInterface[] skus);
    void saveSkuStocks(SkuStockInterface[] skuStocks);
    void saveSkuParents(SkuParentInterface[] skuParents);
    List<SkuInterface> getSkusByDataIds(List<Integer> promotionSkuIds);
    void updateSkus(SkuInterface[] skusToUpdate);
}
