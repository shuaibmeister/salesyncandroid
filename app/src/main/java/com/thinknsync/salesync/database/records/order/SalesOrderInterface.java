package com.thinknsync.salesync.database.records.order;

import com.thinknsync.salesync.commons.commonInterfaces.HasIdentifier;
import com.thinknsync.salesync.database.records.DbRecord;
import com.thinknsync.salesync.database.records.IsSyncable;
import com.thinknsync.salesync.database.records.masterData.MasterData;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.route.RouteInterface;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.database.records.user.UserInterface;

import java.util.Date;
import java.util.List;

public interface SalesOrderInterface extends DbRecord<SalesOrderInterface>, HasIdentifier, IsSyncable {

    String tag = "SalesOrder";

    String key_sr = "sr";
    String key_sr_id = "sr_id";
    String key_route = "route";
    String key_route_id = "route_id";
    String key_outlet = "outlet";
    String key_outlet_id = "outlet_id";
    String key_order_date = "order_date";
    String key_expected_delivery_date = "expected_delivery_date";
    String key_exception = "exception";
    String key_exception_id = "exception_id";
    String key_order_amount = "order_amount";
    String key_payable_amount = "payable_amount";
    String key_discount = "invoice_discount";
    String key_in_date_time = "outlet_in_time";
    String key_out_date_time = "outlet_out_time";
    String key_order_lines = "order";

    UserInterface getSr();
    void setSr(UserInterface srId);
    OutletInterface getOutlet();
    void setOutlet(OutletInterface outlet);
    String getOrderDate();
    void setOrderDate(String orderDate);
    MasterData getException();
    void setException(MasterData exception);
    Date getOutletInDateTime();
    void setOutletInDateTime(Date outletInDateTime);
    Date getOutletOutDateTime();
    void setOutletOutDateTime(Date outletOutDateTime);
    List<SalesOrderDetailsInterface> getSalesOrderDetails();
    void setSalesOrderDetails(List<SalesOrderDetailsInterface> salesOrderDetails);
    double getOrderTotal();
    double getInvoiceDiscount();
    void setInvoiceDiscount(double invoiceDiscount);
    double getOrderTotalWithDiscount();
    List<SkuInterface> getUniqueSkus();
    int getTotalOrderedQty();
}
