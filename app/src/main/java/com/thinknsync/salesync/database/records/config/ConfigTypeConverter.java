package com.thinknsync.salesync.database.records.config;

import io.objectbox.converter.PropertyConverter;

public class ConfigTypeConverter implements PropertyConverter<ConfigInterface.ConfigType, String> {

    @Override
    public ConfigInterface.ConfigType convertToEntityProperty(String databaseValue) {
        if (databaseValue == null) {
            return null;
        }
        return ConfigInterface.ConfigType.getConfigTypeFromValue(databaseValue);
    }

    @Override
    public String convertToDatabaseValue(ConfigInterface.ConfigType entityProperty) {
        return entityProperty == null ? null : entityProperty.getKey();
    }
}
