package com.thinknsync.salesync.database.repositories;

import com.thinknsync.salesync.commons.utils.DateTimeUtils;
import com.thinknsync.salesync.database.DbContext;
import com.thinknsync.salesync.database.records.DbRecord;

import java.util.Date;

import io.objectbox.Box;
import io.objectbox.BoxStore;

public abstract class BaseRepository {

    private DateTimeUtils dateUtils;

    public BaseRepository(){
        initializeObjectBox();
        dateUtils = new DateTimeUtils();
    }

    protected abstract void initializeObjectBox();

    protected <ObjectType extends DbRecord> Box<ObjectType> getBoxFor(Class<ObjectType> objectClass){
        BoxStore boxStore = DbContext.getStore();
        return boxStore.boxFor(objectClass);
    }

    protected <ObjectType extends DbRecord> void saveObject(Class<ObjectType> objectClass, ObjectType... objects){
        Date timeNow = dateUtils.getDefaultDateTimeNow();
        for (ObjectType object : objects) {
            object.setCreationDateTime(timeNow);
            object.setUpdateDateTime(timeNow);
        }
        getBoxFor(objectClass).put(objects);
    }

    protected <ObjectType extends DbRecord> void updateObject(Class<ObjectType> objectClass, ObjectType... objects){
        Date timeNow = dateUtils.getDefaultDateTimeNow();
        for (ObjectType object : objects) {
            object.setUpdateDateTime(timeNow);
        }
        getBoxFor(objectClass).put(objects);
    }
}
