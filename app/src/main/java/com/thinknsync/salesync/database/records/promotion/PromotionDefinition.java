package com.thinknsync.salesync.database.records.promotion;

import com.thinknsync.salesync.database.records.BaseRecord;
import com.thinknsync.salesync.database.records.sku.Sku;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import io.objectbox.annotation.Convert;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToOne;

@Entity
public class PromotionDefinition extends BaseRecord<PromotionInterface> implements PromotionInterface {

    @Id(assignable = true)
    private long id;
    protected int promotionId;
    protected String name;
    private String description;
    private Date startDate;
    private Date endDate;
    @Convert(converter = PromotionTypeConverter.class, dbType = Integer.class)
    private PromotionType promotionType;
    protected ToOne<Sku> sellingSku;
    private int sellingQty;
    protected ToOne<Sku> freeSku;
    private int freeSkuQty;
    private double freeAmount;
    protected Date creationDate;
    protected Date updateDate;

    @Override
    public long getId() {
        return this.id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Date getCreationDateTime() {
        return creationDate;
    }

    @Override
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDate = creationDateTime;
    }

    @Override
    public Date getUpdateDateTime() {
        return updateDate;
    }

    @Override
    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDate = updateDateTime;
    }

    @Override
    public int getDataId() {
        return promotionId;
    }

    @Override
    public void setDataId(int promotionId) {
        this.promotionId = promotionId;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Override
    public Date getEndDate() {
        return endDate;
    }

    @Override
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public PromotionType getPromotionType() {
        return promotionType;
    }

    @Override
    public void setPromotionType(PromotionType promotionType) {
        this.promotionType = promotionType;
    }

    @Override
    public SkuInterface getSellingSku() {
        return sellingSku.getTarget();
    }

    @Override
    public void setSellingSku(SkuInterface sellingSku) {
        this.sellingSku.setTarget((Sku) sellingSku);
    }

    @Override
    public SkuInterface getFreeSku() {
        return freeSku.getTarget();
    }

    @Override
    public void setFreeSku(SkuInterface freeSku) {
        this.freeSku.setTarget((Sku) freeSku);
    }

    @Override
    public int getSellingQty() {
        return sellingQty;
    }

    @Override
    public void setSellingQty(int sellingQty) {
        this.sellingQty = sellingQty;
    }

    @Override
    public int getFreeSkuQty() {
        return freeSkuQty;
    }

    @Override
    public void setFreeSkuQty(int freeSkuQty) {
        this.freeSkuQty = freeSkuQty;
    }

    @Override
    public double getFreeAmount() {
        return freeAmount;
    }

    @Override
    public void setFreeAmount(double freeAmount) {
        this.freeAmount = freeAmount;
    }

    @Override
    public PromotionDefinition fromJson(JSONObject jsonObject) throws JSONException {
        setDates(jsonObject);
        setDataId(jsonObject.getInt(key_promo_id));
        setName(jsonObject.getString(key_promo_name));
        setDescription(jsonObject.getString(key_promo_description));
        setStartDate(getDateTimeFromDateString(jsonObject.getString(key_promo_date_from)));
        setEndDate(getDateTimeFromDateString(jsonObject.getString(key_promo_date_to)));
        setPromotionType(PromotionType.getPromotionTypeById(jsonObject.getInt(key_promo_type_id)));
        setSellingQty(jsonObject.getJSONObject(key_promo_condition).getInt(key_promo_selling_sku_qty));
        setFreeSkuQty(jsonObject.getJSONObject(key_promo_offer).optInt(key_free_sku_qty));
        setFreeAmount(jsonObject.getJSONObject(key_promo_offer).optDouble(key_free_amount));
        return this;
    }

    private Date getDateTimeFromDateString(String date){
        return dateTimeUtils.getDateFromDefaultDateString(date);
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jso = new JSONObject().put(key_id, getId())
                .put(key_promo_id, getDataId())
                .put(key_promo_name, getName())
                .put(key_promo_description, getDescription())
                .put(key_promo_date_from, getStartDate())
                .put(key_promo_date_to, getEndDate())
                .put(key_promo_type_id, getPromotionType().getId())
                .put(key_promo_selling_sku_id, getSellingSku().getDataId())
                .put(key_promo_selling_sku_qty, getSellingQty())
//                .put(key_free_sku_id, getFreeSku().getDataId())
                .put(key_free_sku_qty, getFreeSkuQty())
                .put(key_free_amount, getFreeAmount());

        return setDatesInJsonObject(jso);
    }
}
