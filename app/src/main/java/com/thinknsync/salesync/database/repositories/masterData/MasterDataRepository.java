package com.thinknsync.salesync.database.repositories.masterData;

import com.thinknsync.salesync.database.records.masterData.MasterData;
import com.thinknsync.salesync.database.records.masterData.MasterDataObject;
import com.thinknsync.salesync.database.records.masterData.MasterDataObject_;
import com.thinknsync.salesync.database.repositories.BaseRepository;

import java.util.Arrays;
import java.util.List;

import io.objectbox.Box;

public class MasterDataRepository extends BaseRepository implements MasterDataRepositoryInterface {
    private Box<MasterDataObject> masterDataBox;

    @Override
    protected void initializeObjectBox() {
        this.masterDataBox = getBoxFor(MasterDataObject.class);
    }

    @Override
    public List<MasterData> getMasterDataOfType(MasterData.MasterDataType masterDataType){
        return (List<MasterData>)(List<?>)masterDataBox.query().equal(MasterDataObject_.type, masterDataType.getId()).build().find();
    }

    @Override
    public MasterData getMasterDataByIdAndType(int masterDataId, MasterData.MasterDataType masterDataType){
        return masterDataBox.query().equal(MasterDataObject_.dataId, masterDataId)
                .equal(MasterDataObject_.type, masterDataType.getId()).build().findFirst();
    }

    @Override
    public List<MasterData> getAll() {
        return (List<MasterData>)(List<?>)masterDataBox.getAll();
    }

    @Override
    public void save(MasterData entity) {
        saveObject(MasterDataObject.class, (MasterDataObject)entity);
    }

    @Override
    public void saveMasterDataObjects(MasterData[] entities) {
        MasterDataObject[] masterDataObjects = Arrays.copyOf(entities, entities.length, MasterDataObject[].class);
        saveObject(MasterDataObject.class, masterDataObjects);
    }

    @Override
    public MasterData getNewEntity() {
        return new MasterDataObject();
    }

    @Override
    public void deleteAll() {
        masterDataBox.removeAll();
    }
}
