package com.thinknsync.salesync.database.records.sku;

import com.thinknsync.listviewadapterhelper.searchable.SearchableObject;
import com.thinknsync.salesync.commons.commonInterfaces.HasCode;
import com.thinknsync.salesync.commons.commonInterfaces.HasIdentifier;
import com.thinknsync.salesync.commons.commonInterfaces.HasImage;
import com.thinknsync.salesync.commons.commonInterfaces.HasName;
import com.thinknsync.salesync.database.records.DbRecord;
import com.thinknsync.salesync.database.records.promotion.PromotionInterface;

import java.util.List;

public interface SkuInterface extends DbRecord<SkuInterface>, HasIdentifier, HasName, SearchableObject, HasCode, HasImage {
    String tag = "Sku";

    String key_parent_id = "parent_id";
    String key_sku_id = "sku_id";
    String key_sku_name = "sku_name";
    String key_sku_image = "sku_image";
    String key_sku_type = "sku_type";
    String key_unit_price = "unit_price";
    String key_target = "target";
    String key_achievement = "achievement";
    String key_tp = "tp";
    String key_max_tp = "max_tp_price";
    String key_min_tp = "min_tp_price";
    String key_mrp = "mrp";
    String key_stock = "stock";
    String key_sku_code = "sku_code";

    SkuType getSkuType();
    void setSkuType(SkuType skuType);
    double getUnitPrice();
    void setUnitPrice(double unitPrice);
    List<UomInterface> getSkuUoms();
    void setSkuUoms(List<UomInterface> uoms);
    int getTarget();
    void setTarget(int target);
    int getAchievement();
    void setAchievement(int target);
    double getToDateTargetValue();
    double getToDateAchievementValue();
    double getTp();
    double getMaxTp();
    void setMaxTp(double maxTp);
    double getMinTp();
    void setMinTp(double minTp);
    void setTp(double tp);
    double getMrp();
    void setMrp(double mrp);
    int getStock();
    void setStock(int stock);
    SkuParentInterface getSkuParent();
    void setSkuParent(SkuParentInterface skuParent);
    List<SkuStockInterface> getDbWiseSkuStock();
    void setDbWiseSkuStock(List<SkuStockInterface> dbWiseSkuStocks);
    PromotionInterface getSkuPromotion();
    void setSkuPromotion(PromotionInterface promotion);
    boolean hasPromotion();
}
