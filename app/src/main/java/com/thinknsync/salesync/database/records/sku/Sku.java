package com.thinknsync.salesync.database.records.sku;

import com.thinknsync.salesync.database.records.BaseRecord;
import com.thinknsync.salesync.database.records.promotion.PromotionDefinition;
import com.thinknsync.salesync.database.records.promotion.PromotionInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

import io.objectbox.annotation.Backlink;
import io.objectbox.annotation.Convert;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToMany;
import io.objectbox.relation.ToOne;

@Entity
public class Sku extends BaseRecord<SkuInterface> implements SkuInterface {

    @Id(assignable = true)
    private long id;
    protected int skuId;
    protected String skuName;
    protected String skuImage;
    @Convert(converter = SkuTypeConverter.class, dbType = Integer.class)
    private SkuType skuType;
    private double unitPrice;
    private int target;
    private int achievement;
    private double tp;
    private double mrp;
    private double maxTp;
    private double minTp;
    private int stock;
    protected String skuCode;

    protected ToOne<SkuParent> parentSku;
    @Backlink(to = "sku")
    protected ToMany<Uom> uoms;
    @Backlink(to = "sku")
    protected ToMany<SkuStock> dbWiseSkuStock;
    protected ToOne<PromotionDefinition> skuPromotion;
    protected Date creationDate;
    protected Date updateDate;

    @Override
    public long getId() {
        return this.id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Date getCreationDateTime() {
        return creationDate;
    }

    @Override
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDate = creationDateTime;
    }

    @Override
    public Date getUpdateDateTime() {
        return updateDate;
    }

    @Override
    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDate = updateDateTime;
    }

    @Override
    public int getDataId() {
        return skuId;
    }

    @Override
    public void setDataId(int skuId) {
        this.skuId = skuId;
    }

    @Override
    public String getName() {
        return skuName;
    }

    @Override
    public void setName(String skuName) {
        this.skuName = skuName;
    }

    @Override
    public String getImage() {
        return skuImage;
    }

    @Override
    public void setImage(String skuImage) {
        this.skuImage = skuImage;
    }

    @Override
    public SkuType getSkuType(){
        return skuType;
    }

    @Override
    public void setSkuType(SkuType skuType){
        this.skuType = skuType;
    }

    @Override
    public double getUnitPrice() {
        return unitPrice;
    }

    @Override
    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Override
    public int getTarget() {
        return target;
    }

    @Override
    public void setTarget(int target) {
        this.target = target;
    }

    @Override
    public int getAchievement() {
        return achievement;
    }

    @Override
    public void setAchievement(int achievement) {
        this.achievement = achievement;
    }

    @Override
    public double getToDateTargetValue() {
        return getTarget() / getUnitPrice();
    }

    @Override
    public double getToDateAchievementValue() {
        return getAchievement() / getUnitPrice();
    }

    @Override
    public double getTp() {
        return tp;
    }

    @Override
    public void setTp(double tp) {
        this.tp = tp;
    }

    @Override
    public double getMrp() {
        return mrp;
    }

    @Override
    public void setMrp(double mrp) {
        this.mrp = mrp;
    }

    @Override
    public double getMaxTp() {
        return maxTp;
    }

    @Override
    public void setMaxTp(double maxTp) {
        this.maxTp = maxTp;
    }

    @Override
    public double getMinTp() {
        return minTp;
    }

    @Override
    public void setMinTp(double minTp) {
        this.minTp = minTp;
    }

    @Override
    public int getStock() {
        return stock;
    }

    @Override
    public void setStock(int stock) {
        this.stock = stock;
    }

    @Override
    public void setCode(String sku_code) {
        this.skuCode =sku_code;
    }

    @Override
    public String getCode() {
        return skuCode;
    }

    @Override
    public List<UomInterface> getSkuUoms() {
        return (List<UomInterface>)(List<?>)uoms;
    }

    @Override
    public void setSkuUoms(List<UomInterface> uoms) {
        this.uoms.addAll((List<Uom>)(List<?>)uoms);
    }

    @Override
    public SkuParentInterface getSkuParent() {
        return parentSku.getTarget();
    }

    @Override
    public void setSkuParent(SkuParentInterface skuParent) {
        parentSku.setTarget((SkuParent) skuParent);
    }

    @Override
    public List<SkuStockInterface> getDbWiseSkuStock() {
        return (List<SkuStockInterface>)(List<?>)dbWiseSkuStock;
    }

    @Override
    public void setDbWiseSkuStock(List<SkuStockInterface> dbWiseSkuStocks) {
        this.dbWiseSkuStock.addAll((List<SkuStock>)(List<?>)dbWiseSkuStocks);
    }

    @Override
    public PromotionInterface getSkuPromotion() {
        return skuPromotion.getTarget();
    }

    @Override
    public void setSkuPromotion(PromotionInterface promotion) {
        this.skuPromotion.setTarget((PromotionDefinition) promotion);
    }

    @Override
    public boolean hasPromotion(){
        return getSkuPromotion() != null;
    }

    @Override
    public SkuInterface fromJson(JSONObject jsonObject) throws JSONException {
        setDates(jsonObject);
        setDataId(jsonObject.optInt(key_sku_id));
        setName(jsonObject.optString(key_sku_name));
        setImage(jsonObject.optString(key_sku_image));
        setSkuType(SkuType.getSkuTypeById(jsonObject.optInt(key_sku_type)));
        setUnitPrice(jsonObject.optDouble(key_unit_price));
        setTp(jsonObject.optDouble(key_tp));
        setMaxTp(jsonObject.optDouble(key_max_tp));
        setMinTp(jsonObject.optDouble(key_min_tp));
        setMrp(jsonObject.optDouble(key_mrp));
        setTarget(jsonObject.optInt(key_target));
        setAchievement(jsonObject.optInt(key_achievement));
        setStock(jsonObject.optInt(key_stock));
        setCode(jsonObject.optString(key_sku_code));
        return this;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jso = new JSONObject().put(key_id, getId())
                .put(key_parent_id, getSkuParent().getDataId())
                .put(key_sku_id, getDataId())
                .put(key_sku_name, getName())
                .put(key_sku_image, getImage())
                .put(key_sku_type, getSkuType().getId())
                .put(key_unit_price, getUnitPrice())
                .put(key_tp, getTp())
                .put(key_max_tp, getMaxTp())
                .put(key_min_tp, getMinTp())
                .put(key_mrp, getMrp())
                .put(key_stock, getStock())
                .put(key_target, getTarget())
                .put(key_sku_code, getCode())
                .put(key_achievement, getAchievement());
        return setDatesInJsonObject(jso);
    }

    @Override
    public String[] getSearchableProperties() {
        return new String[]{getName(), getCode()};
    }
}
