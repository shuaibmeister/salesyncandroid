package com.thinknsync.salesync.database.repositories.route;

import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.route.Route;
import com.thinknsync.salesync.database.records.route.RouteInterface;
import com.thinknsync.salesync.database.records.route.Route_;
import com.thinknsync.salesync.database.repositories.BaseRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.objectbox.Box;

public class RouteRepository extends BaseRepository implements RouteRepositoryInterface {

    private Box<Route> routeBox;

    @Override
    protected void initializeObjectBox() {
        routeBox = getBoxFor(Route.class);
    }

    @Override
    public List<RouteInterface> getRouteWithCondition(boolean isTodaysRoute) {
        return  (List<RouteInterface>)(List<?>) routeBox.query().equal(Route_.isTodaysRoute, isTodaysRoute).build().find();
    }

    @Override
    public List<OutletInterface> getOutletsOfRoute(int routeId) {
        return routeBox.query().equal(Route_.routeId, routeId).build().findFirst().getOutletsOfRoute();
    }

    @Override
    public RouteInterface getNewEntity() {
        return new Route();
    }

    @Override
    public void deleteAll() {
        routeBox.removeAll();
    }

    @Override
    public List<RouteInterface> getAll() {
        return (List<RouteInterface>)(List<?>)routeBox.getAll();
    }

    @Override
    public void save(RouteInterface route) {
        saveObject(Route.class, (Route)route);
    }

    @Override
    public void saveRoutes(RouteInterface[] routes){
        Route[] routes1 = Arrays.copyOf(routes, routes.length, Route[].class);
        saveObject(Route.class, routes1);
    }
}
