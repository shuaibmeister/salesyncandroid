package com.thinknsync.salesync.database.records.order;

import com.thinknsync.salesync.database.records.DbRecord;
import com.thinknsync.salesync.database.records.promotion.PromotionInterface;
import com.thinknsync.salesync.database.records.sku.SkuInterface;


public interface SalesOrderDetailsInterface extends DbRecord<SalesOrderDetailsInterface> {
    String key_order = "order";
    String key_order_id = "order_id";
    String key_sku = "sku";
    String key_unit_price = "unit_price";
    String key_sku_id = "sku_id";
    String key_qty = "qty";
    String key_sub_total = "sub_total";
    String key_discount = "discount";
    String key_promotion = "promotion";
    String key_promotion_id = "promotion_id";

    SkuInterface getSku();
    void setSku(SkuInterface sku);
    double getQty();
    void setQty(double qty);
    double getDiscount();
    void setDiscount(double discount);
    double getSubTotal();
    void setSubTotal(double subTotal);
    double getSubTotalWithDiscount();
    SalesOrderInterface getParentOrder();
    void setParentOrder(SalesOrderInterface parentOrder);
    PromotionInterface getPromotion();
    void setPromotion(PromotionInterface promotion);
}
