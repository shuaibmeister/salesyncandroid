package com.thinknsync.salesync.database.repositories.user;

import androidx.annotation.Nullable;

import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.database.repositories.RepositoryPolicy;

public interface UserRepositoryInterface extends RepositoryPolicy<UserInterface> {
    UserInterface getLoggedInUser();
    UserInterface getUser();
    @Nullable
    UserInterface getUserIfValid(String username, String pass);
    String getAuthToken();
    double getTarget();
    double getAchievement();
    double getRunRate();
}
