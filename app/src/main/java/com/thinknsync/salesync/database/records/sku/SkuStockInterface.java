package com.thinknsync.salesync.database.records.sku;

import com.thinknsync.salesync.database.records.DbRecord;
import com.thinknsync.salesync.database.records.masterData.MasterData;

public interface SkuStockInterface extends DbRecord<SkuStockInterface> {
    String key_ctn = "ctn";
    String key_pcs = "piece";
    String key_volume="volume";
    String key_db_name="db_name";
    String key_db_id="db_id";

    SkuInterface getSku();
    void setSku(SkuInterface sku);
    MasterData getDbMasterData();
    void setDbMasterData(MasterData dbMasterData);
    String getVolume();
    void setVolume(String volume);
    double getCtn();
    void setCtn(double ctn);
    double getPcs();
    void setPcs(double pcs);
}
