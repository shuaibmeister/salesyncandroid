package com.thinknsync.salesync.database.records.user;

import com.thinknsync.salesync.home.bottomSlidingView.AttendanceManager;

import io.objectbox.converter.PropertyConverter;

public class AttendanceStatusConverter implements PropertyConverter<AttendanceManager.AttendanceStatus, Integer> {
    @Override
    public AttendanceManager.AttendanceStatus convertToEntityProperty(Integer databaseValue) {
        return AttendanceManager.AttendanceStatus.getStatusFromId(databaseValue);
    }

    @Override
    public Integer convertToDatabaseValue(AttendanceManager.AttendanceStatus entityProperty) {
        return entityProperty.getId();
    }
}
