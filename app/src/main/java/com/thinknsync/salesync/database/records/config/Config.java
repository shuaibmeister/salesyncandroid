package com.thinknsync.salesync.database.records.config;

import com.thinknsync.salesync.database.records.BaseRecord;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import io.objectbox.annotation.Convert;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Unique;

@Entity
public class Config extends BaseRecord<ConfigInterface> implements ConfigInterface {

    @Id(assignable = true)
    private long id;
    @Unique
    @Convert(converter = ConfigTypeConverter.class, dbType = String.class)
    private ConfigType configType;
    private String value;
    protected Date creationDate;
    protected Date updateDate;

    public Config(){}

    public Config(ConfigType configType, String configValue){
        this.setConfigType(configType);
        this.setValue(configValue);
    }
    @Override
    public long getId() {
        return this.id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Date getCreationDateTime() {
        return creationDate;
    }

    @Override
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDate = creationDateTime;
    }

    @Override
    public Date getUpdateDateTime() {
        return updateDate;
    }

    @Override
    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDate = updateDateTime;
    }

    @Override
    public ConfigType getConfigType() {
        return configType;
    }

    @Override
    public void setConfigType(ConfigType configType) {
        this.configType = configType;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public ConfigInterface fromJson(JSONObject jsonObject) throws JSONException {
        setDates(jsonObject);
        setConfigType(ConfigType.getConfigTypeFromValue(jsonObject.getString(key_config_type)));
        setValue(jsonObject.getString(key_value));
        return this;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jso = new JSONObject().put(key_config_type, getConfigType().getKey())
                .put(key_value, getValue());
        return setDatesInJsonObject(jso);
    }
}
