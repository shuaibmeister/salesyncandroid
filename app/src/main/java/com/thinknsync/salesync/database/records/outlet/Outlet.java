package com.thinknsync.salesync.database.records.outlet;

import com.thinknsync.positionmanager.TrackingData;
import com.thinknsync.salesync.database.records.BaseRecord;
import com.thinknsync.salesync.database.records.masterData.MasterData;
import com.thinknsync.salesync.database.records.order.SalesOrder;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;
import com.thinknsync.salesync.database.records.route.Route;
import com.thinknsync.salesync.database.records.route.RouteInterface;
import com.thinknsync.salesync.database.repositories.masterData.MasterDataRepositoryInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

import io.objectbox.annotation.Backlink;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToMany;
import io.objectbox.relation.ToOne;

@Entity
public class Outlet extends BaseRecord<OutletInterface> implements OutletInterface {

    @Id(assignable = true)
    private long id;
    protected int outletId;
    protected String outletName;
    private String outletAddress;
    private String outletImage;

    private String ownerPhone;
    private String ownerName;
    private String ownerDob;
    private String ownerMarriageDate;
    private String ownerNid;
    private String ownerImage;
//    private int productLineId;
    private int outletTypeId;
    private int outletClassificationId;
    private double lat;
    private double lon;
    private boolean isVerified;
    public ToOne<Route> parentRoute;
    @Backlink(to = "outlet")
    protected ToMany<SalesOrder> orders;
    private boolean isSynced = false;
    protected Date creationDate;
    protected Date updateDate;

    @Override
    public long getId() {
        return this.id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Date getCreationDateTime() {
        return creationDate;
    }

    @Override
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDate = creationDateTime;
    }

    @Override
    public Date getUpdateDateTime() {
        return updateDate;
    }

    @Override
    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDate = updateDateTime;
    }

    @Override
    public int getDataId() {
        return outletId;
    }

    @Override
    public void setDataId(int outletId) {
        this.outletId = outletId;
    }

    @Override
    public String getName() {
        return outletName;
    }

    @Override
    public void setName(String outletName) {
        this.outletName = outletName;
    }

    @Override
    public String getOutletAddress() {
        return outletAddress;
    }

    @Override
    public void setOutletAddress(String outletAddress) {
        this.outletAddress = outletAddress;
    }

    @Override
    public String getOwnerPhone() {
        return ownerPhone;
    }

    @Override
    public void setOwnerPhone(String ownerPhone) {
        this.ownerPhone = ownerPhone;
    }

    @Override
    public String getOwnerName() {
        return ownerName;
    }

    @Override
    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    @Override
    public String getOwnerDob() {
        return ownerDob;
    }

    @Override
    public void setOwnerDob(String ownerDob) {
        this.ownerDob = ownerDob;
    }

    @Override
    public String getOwnerMarriageDate() {
        return ownerMarriageDate;
    }

    @Override
    public void setOwnerMarriageDate(String ownerMarriageDate) {
        this.ownerMarriageDate = ownerMarriageDate;
    }

    @Override
    public String getOwnerNid() {
        return ownerNid;
    }

    @Override
    public void setOwnerNid(String ownerNid) {
        this.ownerNid = ownerNid;
    }

//    @Override
//    public int getProductLineId() {
//        return productLineId;
//    }
//
//    @Override
//    public void setProductLineId(int productLineId) {
//        this.productLineId = productLineId;
//    }

    @Override
    public int getOutletTypeId() {
        return outletTypeId;
    }

    @Override
    public MasterData getOutletType(MasterDataRepositoryInterface masterDataRepo) {
        return masterDataRepo.getMasterDataByIdAndType(getOutletTypeId(), MasterData.MasterDataType.OUTLET_TYPE);
    }

    @Override
    public void setOutletTypeId(int outletTypeId) {
        this.outletTypeId = outletTypeId;
    }

    @Override
    public int getOutletClassificationId() {
        return outletClassificationId;
    }

    @Override
    public MasterData getOutletClassification(MasterDataRepositoryInterface masterDataRepo){
        return masterDataRepo.getMasterDataByIdAndType(getOutletClassificationId(), MasterData.MasterDataType.OUTLET_CLASSIFICATION);
    }

    @Override
    public void setOutletClassificationId(int outletClassificationId) {
        this.outletClassificationId = outletClassificationId;
    }

    @Override
    public double getLat() {
        return lat;
    }

    @Override
    public void setLat(double lat) {
        this.lat = lat;
    }

    @Override
    public double getLon() {
        return lon;
    }

    @Override
    public void setLon(double lon) {
        this.lon = lon;
    }

    @Override
    public boolean isLocationValid(){
        return getLat() != 0 && getLon() != 0;
    }

    @Override
    public TrackingData getOutletLocation() {
        return new TrackingData(getLat(), getLon());
    }

    @Override
    public void getOutletLocation(TrackingData location) {
        this.setLat(location.getLat());
        this.setLon(location.getLon());
    }

    @Override
    public boolean isVerified() {
        return isVerified;
    }

    @Override
    public void setVerified(boolean verified) {
        isVerified = verified;
    }

    @Override
    public RouteInterface getParentRoute() {
        return parentRoute.getTarget();
    }

    @Override
    public void setParentRoute(RouteInterface parentRoute) {
        this.parentRoute.setTarget((Route) parentRoute);
    }

    @Override
    public String getOutletImage() {
        return outletImage;
    }

    @Override
    public void setOutletImage(String outletImage) {
        this.outletImage = outletImage;
    }

    @Override
    public String getOwnerImage() {
        return ownerImage;
    }

    @Override
    public void setOwnerImage(String ownerImage) {
        this.ownerImage = ownerImage;
    }

    @Override
    public List<SalesOrderInterface> getOrders() {
        return (List<SalesOrderInterface>)(List<?>)orders;
    }

    @Override
    public void setOrders(List<SalesOrderInterface> orders) {
        this.orders.addAll((List<SalesOrder>)(List<?>)orders);;
    }

    @Override
    public boolean isSynced() {
        return isSynced;
    }

    @Override
    public void setSynced(boolean synced) {
        isSynced = synced;
    }

    @Override
    public OutletInterface fromJson(JSONObject jsonObject) throws JSONException {
        setDates(jsonObject);
        setId(jsonObject.optLong(key_id));
        setDataId(jsonObject.getInt(key_outlet_id));
        setName(jsonObject.getString(key_outlet_name));
        setOutletAddress(jsonObject.getString(key_outlet_address));
        setOwnerPhone(jsonObject.getString(key_owner_phone));

        setOwnerName(jsonObject.optString(key_owner_name));
        setOwnerDob(jsonObject.optString(key_owner_dob));
        setOwnerMarriageDate(jsonObject.optString(key_owner_marriage_date));
        setOwnerNid(jsonObject.optString(key_owner_nid));
//        setProductLineId(jsonObject.optInt(key_product_line));
        setOutletTypeId(jsonObject.optInt(key_outlet_type));
        setOutletClassificationId(jsonObject.optInt(key_outlet_classification));
        setLat(jsonObject.has(key_lat) && jsonObject.optString(key_lat).equals("false") ? 0 : jsonObject.optDouble(key_lat));
        setLon(jsonObject.has(key_lon) && jsonObject.optString(key_lon).equals("false") ? 0 : jsonObject.optDouble(key_lon));
        setVerified(jsonObject.optBoolean(key_is_verified));
        setSynced(jsonObject.optBoolean(key_is_synced, isSynced()));
        return this;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jso = new JSONObject().put(key_id, getId())
                .put(key_route, parentRoute.getTarget().toJson())
                .put(key_route_id, getParentRoute().getDataId())
                .put(key_outlet_id, getDataId())
                .put(key_outlet_name, getName())
                .put(key_outlet_address, getOutletAddress())
                .put(key_outlet_image, getOutletImage())
                .put(key_owner_phone, getOwnerPhone())
                .put(key_owner_name, getOwnerName())
                .put(key_owner_dob, getOwnerDob())
                .put(key_owner_marriage_date, getOwnerMarriageDate())
                .put(key_owner_nid, getOwnerNid())
                .put(key_owner_image, getOwnerImage())
//                .put(key_product_line, getProductLineId())
                .put(key_outlet_type, getOutletTypeId())
                .put(key_outlet_classification, getOutletClassificationId())
                .put(key_lat, getLat())
                .put(key_lon, getLon());
        return setDatesInJsonObject(jso);
    }

    @Override
    public String[] getSearchableProperties() {
        return new String[]{getName(), getOutletAddress(), getOwnerPhone()};
    }
}
