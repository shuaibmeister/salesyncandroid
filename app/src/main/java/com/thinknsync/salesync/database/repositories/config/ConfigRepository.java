package com.thinknsync.salesync.database.repositories.config;

import com.thinknsync.salesync.database.records.config.Config;
import com.thinknsync.salesync.database.records.config.Config_;
import com.thinknsync.salesync.database.records.config.ConfigInterface;
import com.thinknsync.salesync.database.repositories.BaseRepository;

import java.util.ArrayList;
import java.util.List;

import io.objectbox.Box;

public class ConfigRepository extends BaseRepository implements ConfigRepositoryInterface {

    private Box<Config> configBox;

    @Override
    protected void initializeObjectBox() {
        configBox = getBoxFor(Config.class);
    }

    @Override
    public ConfigInterface getConfigOfType(ConfigInterface.ConfigType configType){
        return configBox.query().equal(Config_.configType, configType.getKey()).build().findFirst();
    }

    @Override
    public void insertOrUpdateConfig(String configTypeString, String value){
        insertOrUpdateConfig(ConfigInterface.ConfigType.getConfigTypeFromValue(configTypeString), value);
    }

    @Override
    public void insertOrUpdateConfig(ConfigInterface.ConfigType configType, String value){
        Config configRow = getNewEntity();
        configRow.setConfigType(configType);
        configRow.setValue(value);
        insertOrUpdateConfigs(new ConfigInterface[]{configRow});
//        saveObject(Config.class, configRow);
    }

    @Override
    public void insertOrUpdateConfigs(ConfigInterface[] configs){
        List<ConfigInterface> dbConfigs = getAll();
        List<Config> configsToSave = new ArrayList<>();
        for(ConfigInterface config : configs){
            ConfigInterface configToUpdate = getConfigToSave(config, dbConfigs);
            if(configToUpdate != null){
                configsToSave.add((Config) configToUpdate);
            }
        }
        saveObject(Config.class, configsToSave.toArray(new Config[0]));
    }

    private ConfigInterface getConfigToSave(ConfigInterface config, List<ConfigInterface> dbConfigs) {
        for (ConfigInterface dbConfig : dbConfigs) {
            if(dbConfig.getConfigType() == config.getConfigType()){
                if(dbConfig.getValue().equals(config.getValue())){
                    return null;
                }
                dbConfig.setValue(config.getValue());
                return dbConfig;
            }
        }
        return config;
    }

    @Override
    public List<ConfigInterface> getAll() {
        return (List<ConfigInterface>)(List<?>)configBox.getAll();

    }

    @Override
    public void save(ConfigInterface entity) {
        saveObject(Config.class, (Config)entity);
    }

    @Override
    public Config getNewEntity() {
        return new Config();
    }

    @Override
    public void deleteAll() {
        configBox.removeAll();
    }
}
