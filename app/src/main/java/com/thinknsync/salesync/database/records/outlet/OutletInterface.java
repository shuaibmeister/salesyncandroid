package com.thinknsync.salesync.database.records.outlet;

import com.thinknsync.listviewadapterhelper.searchable.SearchableObject;
import com.thinknsync.positionmanager.TrackingData;
import com.thinknsync.salesync.commons.commonInterfaces.HasIdentifier;
import com.thinknsync.salesync.commons.commonInterfaces.HasName;
import com.thinknsync.salesync.database.records.DbRecord;
import com.thinknsync.salesync.database.records.IsSyncable;
import com.thinknsync.salesync.database.records.masterData.MasterData;
import com.thinknsync.salesync.database.records.order.SalesOrder;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;
import com.thinknsync.salesync.database.records.route.RouteInterface;
import com.thinknsync.salesync.database.repositories.masterData.MasterDataRepositoryInterface;

import java.util.List;

public interface OutletInterface extends SearchableObject, HasIdentifier, HasName, DbRecord<OutletInterface>, IsSyncable {

    String tag = "outlet";

    String key_route = "route";
    String key_route_id = "route_id";
    String key_outlet_id = "outlet_id";
    String key_outlet_name = "outlet_name";
    String key_outlet_address = "outlet_address";
    String key_owner_phone = "owner_mobile";

    String key_owner_name = "owner_name";
    String key_owner_dob = "owner_dob";
    String key_owner_marriage_date = "owner_marriage_date";
    String key_owner_nid = "owner_nid";
//    String key_product_line = "product_line_id";
    String key_outlet_type = "outlet_type_id";
    String key_outlet_classification = "outlet_classification_id";
    String key_lat = TrackingData.key_lat;
    String key_lon = TrackingData.key_lon;
    String key_is_verified = "is_verified";
    String key_outlet_image = "image";
    String key_owner_image = "owner_image";

    String getOutletAddress();
    void setOutletAddress(String outletAddress);
    String getOwnerPhone();
    void setOwnerPhone(String ownerPhone);

    String getOwnerName();
    void setOwnerName(String ownerName);
    String getOwnerDob();
    void setOwnerDob(String ownerDob);
    String getOwnerMarriageDate();
    void setOwnerMarriageDate(String ownerMarriageDate);
    String getOwnerNid();
    void setOwnerNid(String ownerNid);
//    int getProductLineId();
//    void setProductLineId(int productLineId);
    int getOutletTypeId();
    MasterData getOutletType(MasterDataRepositoryInterface masterDataRepo);
    void setOutletTypeId(int outletTypeId);
    int getOutletClassificationId();
    MasterData getOutletClassification(MasterDataRepositoryInterface masterDataRepo);
    void setOutletClassificationId(int outletClassificationId);
    double getLat();
    void setLat(double lat);
    double getLon();
    TrackingData getOutletLocation();
    boolean isLocationValid();
    void getOutletLocation(TrackingData location);
    void setLon(double lon);
    boolean isVerified();
    void setVerified(boolean valid);
    String getOutletImage();
    void setOutletImage(String outletImage);
    String getOwnerImage();
    void setOwnerImage(String ownerImage);
    RouteInterface getParentRoute();
    List<SalesOrderInterface> getOrders();
    void setOrders(List<SalesOrderInterface> orders);
    void setParentRoute(RouteInterface parentRoute);
}
