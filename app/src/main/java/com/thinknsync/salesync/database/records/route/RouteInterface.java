package com.thinknsync.salesync.database.records.route;

import com.thinknsync.listviewadapterhelper.searchable.SearchableObject;
import com.thinknsync.salesync.commons.commonInterfaces.HasIdentifier;
import com.thinknsync.salesync.commons.commonInterfaces.HasName;
import com.thinknsync.salesync.database.records.DbRecord;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;

import java.util.List;

public interface RouteInterface extends DbRecord<RouteInterface>, SearchableObject, HasIdentifier, HasName {

    String key_route_id = "route_id";
    String key_route_name = "route_name";
    String key_is_todays_route = "todays_route";

    List<OutletInterface> getOutletsOfRoute();
    void setOutletsOfRoute(List<OutletInterface> outletsOfRoute);
    boolean isTodaysRoute();
    void setTodaysRoute(boolean todaysRoute);
}
