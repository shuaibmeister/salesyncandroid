package com.thinknsync.salesync.database.repositories.promotion;

import com.thinknsync.salesync.commons.utils.DateTimeUtils;
import com.thinknsync.salesync.database.records.promotion.PromotionDefinition;
import com.thinknsync.salesync.database.records.promotion.PromotionDefinition_;
import com.thinknsync.salesync.database.records.promotion.PromotionInterface;
import com.thinknsync.salesync.database.repositories.BaseRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.objectbox.Box;

public class PromotionRepository extends BaseRepository implements PromotionRepositoryInterface {

    private Box<PromotionDefinition> promotionBox;

    @Override
    protected void initializeObjectBox() {
        promotionBox = getBoxFor(PromotionDefinition.class);
    }

    @Override
    public List<PromotionInterface> getAllActivePromotions() {
        return getActiveByDateFilter(getAll());
    }

    @Override
    public PromotionInterface getActivePromotionOfSku(int skuId) {
        List<PromotionInterface> promotions = (List<PromotionInterface>)(List<?>) promotionBox.query()
                .equal(PromotionDefinition_.sellingSkuId, skuId).build().find();
        List<PromotionInterface> activePromos = getActiveByDateFilter(promotions);
        if(activePromos.size() > 0){
            return activePromos.get(0);
        }
        return null;
    }

    private List<PromotionInterface> getActiveByDateFilter(List<PromotionInterface> promotionDefinitions){
        DateTimeUtils dateTimeUtils = new DateTimeUtils();
        int todaysDateInt = dateTimeUtils.getDateIntOfToday();
        List<PromotionInterface> validPromotions = new ArrayList<>();

        for(PromotionInterface promoDef : promotionDefinitions){
            long promoStartDateInt = dateTimeUtils.getDateIntFromDefaultDateString(
                    dateTimeUtils.getDefaultDateTimeStringFromDate(promoDef.getStartDate()));
            long promoEndDateInt = dateTimeUtils.getDateIntFromDefaultDateString(
                    dateTimeUtils.getDefaultDateTimeStringFromDate(promoDef.getEndDate()));
            if(todaysDateInt >= promoStartDateInt && todaysDateInt <= promoEndDateInt){
                validPromotions.add(promoDef);
            }
        }

        return validPromotions;
    }

    @Override
    public List<PromotionInterface> getAll() {
        return (List<PromotionInterface>)(List<?>)promotionBox.getAll();
    }

    @Override
    public void save(PromotionInterface entity) {
        saveObject(PromotionDefinition.class, (PromotionDefinition) entity);
    }

    @Override
    public void savePromotions(PromotionInterface[] promotions) {
        PromotionDefinition[] promotionsToSave = Arrays.copyOf(promotions, promotions.length, PromotionDefinition[].class);
        saveObject(PromotionDefinition.class, promotionsToSave);
    }

    @Override
    public PromotionInterface getNewEntity() {
        return new PromotionDefinition();
    }

    @Override
    public void deleteAll() {
        promotionBox.removeAll();
    }
}
