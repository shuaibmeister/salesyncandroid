package com.thinknsync.salesync.database.records.masterData;

import com.thinknsync.salesync.database.records.BaseRecord;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import io.objectbox.annotation.Convert;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class MasterDataObject extends BaseRecord<MasterData> implements MasterData {

    @Id(assignable = true)
    private long id;
    private int dataId;
    private String name;
    @Convert(converter = MasterDataTypeConverter.class, dbType = Integer.class)
    private MasterDataType type;
    protected Date creationDate;
    protected Date updateDate;

    @Override
    public long getId() {
        return this.id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Date getCreationDateTime() {
        return creationDate;
    }

    @Override
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDate = creationDateTime;
    }

    @Override
    public Date getUpdateDateTime() {
        return updateDate;
    }

    @Override
    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDate = updateDateTime;
    }

    @Override
    public int getDataId() {
        return dataId;
    }

    @Override
    public void setDataId(int id) {
        this.dataId = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public MasterDataType getType() {
        return type;
    }

    @Override
    public void setType(MasterDataType type) {
        this.type = type;
    }

    @Override
    public MasterData fromJson(JSONObject jsonObject) throws JSONException {
        setDates(jsonObject);
        setDataId(jsonObject.optInt(key_data_id));
        setName(jsonObject.getString(key_name));
        return this;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jso = new JSONObject().put(key_data_id, getDataId())
                .put(key_name, getName());
        return setDatesInJsonObject(jso);
    }
}
