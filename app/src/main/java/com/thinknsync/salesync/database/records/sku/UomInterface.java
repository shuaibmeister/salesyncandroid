package com.thinknsync.salesync.database.records.sku;

import com.thinknsync.salesync.commons.commonInterfaces.HasName;
import com.thinknsync.salesync.database.records.DbRecord;

public interface UomInterface extends DbRecord<UomInterface>, HasName {

    String key_sku_id = "sku_id";
    String key_qty = "qty";
    String key_name = "name";

    double getQty();
    void setQty(double qty);
    SkuInterface getUomSku();
    void setSku(SkuInterface sku);
}
