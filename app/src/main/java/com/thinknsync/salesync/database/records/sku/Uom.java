package com.thinknsync.salesync.database.records.sku;

import com.thinknsync.salesync.database.records.BaseRecord;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToOne;

@Entity
public class Uom extends BaseRecord<UomInterface> implements UomInterface {

    @Id(assignable = true)
    long id;
    private double qty;
    private String name;
    protected ToOne<Sku> sku;
    protected Date creationDate;
    protected Date updateDate;

    @Override
    public long getId() {
        return this.id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Date getCreationDateTime() {
        return creationDate;
    }

    @Override
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDate = creationDateTime;
    }

    @Override
    public Date getUpdateDateTime() {
        return updateDate;
    }

    @Override
    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDate = updateDateTime;
    }

    @Override
    public double getQty() {
        return qty;
    }

    @Override
    public void setQty(double qty) {
        this.qty = qty;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public SkuInterface getUomSku() {
        return sku.getTarget();
    }

    @Override
    public void setSku(SkuInterface sku) {
        this.sku.setTarget((Sku)sku);
    }

    @Override
    public UomInterface fromJson(JSONObject jsonObject) throws JSONException {
        setDates(jsonObject);
        setQty(jsonObject.getDouble(key_qty));
        setName(jsonObject.getString(key_name));
        return this;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jso = new JSONObject().put(key_sku_id, getUomSku().getDataId())
                .put(key_qty, getQty())
                .put(key_name, getName());
        return setDatesInJsonObject(jso);
    }
}
