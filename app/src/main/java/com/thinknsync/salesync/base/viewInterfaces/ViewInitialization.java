package com.thinknsync.salesync.base.viewInterfaces;

public interface ViewInitialization {
    interface InitView{
        void initView();
    }

    interface InitValues {
        void initValues();
    }
}
