package com.thinknsync.salesync.base;

import com.thinknsync.salesync.commons.commonInterfaces.ContextHolder;
import com.thinknsync.salesync.base.viewInterfaces.HasViewController;
import com.thinknsync.salesync.base.viewInterfaces.OsUiModifier;
import com.thinknsync.salesync.base.viewInterfaces.UserNotifier;
import com.thinknsync.salesync.base.viewInterfaces.ViewInitialization;
import com.thinknsync.salesync.base.viewInterfaces.ViewsNavigationManager;
import com.thinknsync.salesync.factory.ObjectConstructor;;

public interface BaseView extends HasViewController, UserNotifier, ViewsNavigationManager, OsUiModifier,
        ViewInitialization.InitView, ContextHolder {
    int getLayoutId();
    ObjectConstructor getObjectConstructorInstance();
}
