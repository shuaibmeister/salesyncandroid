package com.thinknsync.salesync.base.viewInterfaces;

import com.thinknsync.objectwrappers.FragmentWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;

public interface ViewsNavigationManager {
    void startActivity(Class<?> activity, FrameworkWrapper... extras);
    void startActivityAndClearAll(Class<?> activity, FrameworkWrapper... extras);
    void startActivityAndClearCurrent(Class<?> activity, FrameworkWrapper... extras);
    void startFragment(FragmentWrapper fragment, int listContainer, String tag);
    void setActivitySlideFromRightSideAnimation();
    void setActivitySlideFromLeftSideAnimation();
}
