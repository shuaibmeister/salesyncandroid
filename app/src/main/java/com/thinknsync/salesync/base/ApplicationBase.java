package com.thinknsync.salesync.base;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;

import com.thinknsync.apilib.ApiContextInitializer;
import com.thinknsync.apilib.AuthTokenContainer;
import com.thinknsync.logger.Logger;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.salesync.commons.osSpecifics.broadcastReceivers.BroadcastListenerContainer;
import com.thinknsync.salesync.database.DbContext;
import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.factory.FactoryStringConstraints;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.factory.factories.AbstractFactoryImpl;
import com.thinknsync.salesync.messageBroker.BrokerTopics;
import com.thinknsync.salesync.messageBroker.PushPullManager;
import com.thinknsync.salesync.messageBroker.PushPullManagerContainer;
import com.thinknsync.salesync.messageBroker.actionListeners.messageListenerActions.ConfigurationJsonArrayMessageActionListener;
import com.thinknsync.salesync.messageBroker.pushPullContents.BrokerSubscribeContent;
import com.thinknsync.salesync.messageBroker.pushPullContents.SubscribeContent;
import com.thinknsync.salesync.commons.osSpecifics.broadcastReceivers.BroadcastListener;
import com.thinknsync.salesync.commons.osSpecifics.frameworkHelper.ApplicationInfo;

import java.util.HashMap;


public class ApplicationBase extends Application implements BroadcastListenerContainer, PushPullManagerContainer {

    private HashMap<BroadcastListener.BroadcastListenerType, BroadcastListener> broadcastListenerDefinition;
    private static ApplicationBase applicationBase;
    private PushPullManager pushPullManager;
    private ObjectConstructor objectConstructor;
    private AndroidContextWrapper applicationContextWrapper;

    @Override
    public void onCreate() {
        super.onCreate();
        DbContext.init(this);

        objectConstructor = (ObjectConstructor) new AbstractFactoryImpl()
                .getObject(FactoryStringConstraints.ObjectType.ObjectConstructor, null);
        initLogger();
        initApiLib();
        broadcastListenerDefinition = new HashMap<>();
        applicationBase = this;
        setupBroadcastReceivers();
        setupPushPullManager();
    }

    private void initApiLib() {
        ApiContextInitializer.init(this);
        UserInterface user = objectConstructor.getUserRepository().getLoggedInUser();
        if(user != null && user.getLoginToken() != null) {
            ((AuthTokenContainer) ApiContextInitializer.getInstance()).setAuthToken(user.getLoginToken());
        }
    }

    private void initLogger() {
        final ApplicationInfo applicationInfo = objectConstructor.getAndroidFrameworkHelper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, new AndroidContextWrapper(getApplicationContext()));
        }});

        objectConstructor.initLogger(new HashMap<String, Object>(){{
            put(Logger.tag, applicationInfo.isDebuggable());
        }});
    }

    private void setupBroadcastReceivers() {
        registerInternetBroadcastReceiver();
        registerScreenOnOffReceiver();
    }

    private void registerInternetBroadcastReceiver() {
        BroadcastListener internetBroadcastListener = objectConstructor.getInternetBroadcastListener();
        registerReceiver(BroadcastListener.BroadcastListenerType.INTERNET_BROADCAST, internetBroadcastListener);
    }

    private void registerScreenOnOffReceiver() {
        BroadcastListener<Boolean> internetBroadcastListener = objectConstructor.getScreenOnOffBroadcastListener();
        registerReceiver(BroadcastListener.BroadcastListenerType.SCREEN_ON_OFF, internetBroadcastListener);
    }

    @Override
    public void registerReceiver(BroadcastListener.BroadcastListenerType listenerType, BroadcastListener broadcastListener) {
        final IntentFilter intentFilter = new IntentFilter();
        for (String listenerString : listenerType.getFilterStrings()){
            intentFilter.addAction(listenerString);
        }
        broadcastListenerDefinition.put(listenerType, broadcastListener);
        registerReceiver((BroadcastReceiver) broadcastListener, intentFilter);
    }

    @Override
    public void deregisterReceiver(BroadcastListener broadcastListener) {
        unregisterReceiver((BroadcastReceiver)broadcastListener);
    }

    @Override
    public BroadcastListener getBroadcastListener(BroadcastListener.BroadcastListenerType listenerType) throws NullPointerException {
        return broadcastListenerDefinition.get(listenerType);
    }

    @Override
    public void addObserverToListener(BroadcastListener.BroadcastListenerType listenerType, TypedObserver observer) {
        try {
            getBroadcastListener(listenerType).addToObservers(observer);
        } catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    @Override
    public void removeObserverFromListener(BroadcastListener.BroadcastListenerType listenerType, TypedObserver observer) {
        try{
            getBroadcastListener(listenerType).removeObserver(observer);
        } catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        for (BroadcastListener listener : broadcastListenerDefinition.values()) {
            unregisterReceiver((BroadcastReceiver)listener);
        }
    }

    public static BroadcastListenerContainer getInstance(){
        return applicationBase;
    }

    @Override
    public void setupPushPullManager() {
        applicationContextWrapper = (AndroidContextWrapper)objectConstructor.getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, getApplicationContext());
        }});
        pushPullManager = objectConstructor.getMqttBrokerClient(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, applicationContextWrapper);
        }});
        subscribeToConfigurationTopic();
    }

    private void subscribeToConfigurationTopic() {
        SubscribeContent subscribeToConfigurationContent = new BrokerSubscribeContent(objectConstructor, BrokerTopics.configurationTopic);
        subscribeToConfigurationContent.addMessageListenerObserver(
                new ConfigurationJsonArrayMessageActionListener(objectConstructor, applicationContextWrapper));

        pushPullManager.setInitialSubscribeTopics(new SubscribeContent[]{subscribeToConfigurationContent});
        pushPullManager.connect();
    }

    @Override
    public PushPullManager getPushPullManager() {
        return pushPullManager;
    }
}
