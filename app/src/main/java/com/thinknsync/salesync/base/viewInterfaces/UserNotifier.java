package com.thinknsync.salesync.base.viewInterfaces;

public interface UserNotifier {
    void showShortMessage(String message);
    void showLongMessage(String message);
    void showMessageWithDialog(String title, String message);
}
