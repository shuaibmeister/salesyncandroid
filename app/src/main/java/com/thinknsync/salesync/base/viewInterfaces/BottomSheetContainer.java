package com.thinknsync.salesync.base.viewInterfaces;

import com.thinknsync.observerhost.ObserverHost;

public interface BottomSheetContainer extends ObserverHost<BottomSheetContainer.BottomSheetState> {
    void setupBottomSheetWithLayoutView(BaseBottomSheetContract.BottomSheetView bottomSheetView);
    void toggleBottomSheet();
    BottomSheetState getCurrentSheetState();
    void restoreBottomSheetHeightToDefault();
    void setBottomSheetHeight(int heightPx);
    void disableBottomSheetDragging();
    void enableBottomSheetDragging();

    enum BottomSheetState {
/*        this is aligned with android bottom sheet constants       */
        STATE_HIDDEN(5),
        STATE_EXPANDED(3),
        STATE_COLLAPSED(4),
        STATE_DRAGGING(1),
        STATE_SETTLING(2),
        STATE_HALF_EXPANDED(6);

        private int id;

        BottomSheetState(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public static BottomSheetState getSheetStateById(int id){
            for (BottomSheetState sheetState : values()){
                if (sheetState.getId() == id){
                    return sheetState;
                }
            }
            return null;
        }
    }
}
