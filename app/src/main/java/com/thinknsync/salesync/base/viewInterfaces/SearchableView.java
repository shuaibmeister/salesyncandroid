package com.thinknsync.salesync.base.viewInterfaces;

import com.thinknsync.listviewadapterhelper.searchable.SearchableObject;

import java.util.List;

public interface SearchableView<T extends SearchableObject> {
    void initSearchBox(List<T> searchableList);
}
