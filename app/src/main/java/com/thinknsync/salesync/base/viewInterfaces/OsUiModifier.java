package com.thinknsync.salesync.base.viewInterfaces;

public interface OsUiModifier {
    void hideStatusBar();
    void showFullScreen();
    void setStatusBarColor(int color);
    void setStatusBarColorTransparent();
}
