package com.thinknsync.salesync.base.views;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.salesync.base.viewInterfaces.BaseBottomSheetContract;
import com.thinknsync.salesync.base.viewInterfaces.BottomSheetContainer;

import java.util.List;

public abstract class BaseWithBottomNavigationBottomSheet extends BaseBottomNavigation implements
        BottomSheetContainer, ObserverHost<BottomSheetContainer.BottomSheetState> {

    protected BottomSheetBehavior<View> sheetBehavior;
    protected BaseBottomSheetContract.BottomSheetView layoutBottomSheet;
    protected View bottomSheetView;
    private int sheetDefaultHeight;
    private ObserverHost<BottomSheetState> observerHost;
    private TypedObserver<BottomSheetState> draggingDisableObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.observerHost = new ObserverHostImpl<>();
    }

    @Override
    public void setupBottomSheetWithLayoutView(BaseBottomSheetContract.BottomSheetView bottomSheetView) {
        this.layoutBottomSheet = bottomSheetView;
        this.bottomSheetView = findViewById(layoutBottomSheet.getBottomSheetLayoutResourceId());
        sheetDefaultHeight = this.bottomSheetView.getLayoutParams().height;
        setupBottomSheet();
    }

    private void setupBottomSheet(){
        try {
            sheetBehavior = BottomSheetBehavior.from(findViewById(layoutBottomSheet.getBottomSheetLayoutResourceId()));
        } catch (IllegalArgumentException e){
            e.printStackTrace();
        }
        setupBottomSheetStateChange();
    }

    private void setupBottomSheetStateChange() {
        /*
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         */
        sheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                notifyObservers(BottomSheetState.getSheetStateById(newState));
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
    }

    protected void setupBottomSheetClickExpand(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleBottomSheet();
            }
        });
    }

    @Override
    public void disableBottomSheetDragging() {
        draggingDisableObject = new TypedObserverImpl<BottomSheetState>() {
            @Override
            public void update(BottomSheetState bottomSheetState) {
                if(bottomSheetState == BottomSheetState.STATE_DRAGGING){
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }
        };
        addToObservers(draggingDisableObject);
    }

    @Override
    public void enableBottomSheetDragging() {
        removeObserver(draggingDisableObject);
        draggingDisableObject = null;
    }

    @Override
    public void toggleBottomSheet() {
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    @Override
    public BottomSheetState getCurrentSheetState() {
        return BottomSheetState.getSheetStateById(sheetBehavior.getState());
    }

    @Override
    public void restoreBottomSheetHeightToDefault() {
        bottomSheetView.getLayoutParams().height = sheetDefaultHeight;
    }

    @Override
    public void setBottomSheetHeight(int height) {
        bottomSheetView.getLayoutParams().height = height;
        bottomSheetView.requestLayout();
    }

    @Override
    public void addToObservers(TypedObserver<BottomSheetState> typedObserver) {
        observerHost.addToObservers(typedObserver);
    }

    @Override
    public void addToObservers(List<TypedObserver<BottomSheetState>> list) {
        observerHost.addToObservers(list);
    }

    @Override
    public void removeObserver(TypedObserver<BottomSheetState> typedObserver) {
        observerHost.removeObserver(typedObserver);
    }

    @Override
    public void clearObservers() {
        observerHost.clearObservers();
    }

    @Override
    public void notifyObservers(BottomSheetState bottomSheetState) {
        observerHost.notifyObservers(bottomSheetState);
    }

    @Override
    public void notifyError(Exception e) {
        observerHost.notifyError(e);
    }
}
