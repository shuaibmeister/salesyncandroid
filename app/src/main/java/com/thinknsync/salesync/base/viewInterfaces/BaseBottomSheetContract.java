package com.thinknsync.salesync.base.viewInterfaces;

public interface BaseBottomSheetContract {
    interface BottomSheetView extends HasViewController, ViewInitialization.InitView, ViewInitialization.InitValues {
        int getBottomSheetLayoutResourceId();
    }

    interface BottomSheetController<V extends BottomSheetView> extends Interactors {
        void setViewReference(V bottomSheetView);
    }
}
