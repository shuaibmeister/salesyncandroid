package com.thinknsync.salesync.base.views;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.thinknsync.salesync.R;
import com.thinknsync.salesync.base.viewInterfaces.ViewHeaderBarContainer;

public abstract class BaseBottomNavigationWithTitleBar extends BaseBottomNavigation implements ViewHeaderBarContainer {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBackButtonAction();
    }

    @Override
    public void setBackButtonAction() {
        View backNavigation = findViewById(R.id.back_navigation);
        backNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void setHeaderTitle(String title) {
        TextView titleText = findViewById(R.id.view_title);
        titleText.setText(title);
    }
}
