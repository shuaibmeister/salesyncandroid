package com.thinknsync.salesync.base.views;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.FragmentWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.salesync.base.BaseView;
import com.thinknsync.salesync.factory.ObjectConstructor;

/**
 * Created by shuaib on 5/13/17.
 */

public abstract class BaseFragment extends Fragment implements BaseView {

    protected View fragmentView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentView = inflater.inflate(getLayoutId(), container, false);
        return fragmentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        initView();
        initController();
    }

    @Override
    public AndroidContextWrapper getContextWrapper() {
        return (AndroidContextWrapper) ((BaseView)getActivity()).getContextWrapper();
    }

    @Override
    public ObjectConstructor getObjectConstructorInstance() {
        return ((BaseView)getActivity()).getObjectConstructorInstance();
    }

    @Override
    public void hideStatusBar() {
        ((BaseView)getActivity()).hideStatusBar();
    }

    @Override
    public void showFullScreen() {
        ((BaseView)getActivity()).showFullScreen();
    }

    @Override
    public void setStatusBarColor(int color) {
        ((BaseView)getActivity()).setStatusBarColor(color);
    }

    @Override
    public void setStatusBarColorTransparent() {
        ((BaseView)getActivity()).setStatusBarColorTransparent();
    }

    @Override
    public void showShortMessage(String message) {
        ((BaseView)getActivity()).showShortMessage(message);
    }

    @Override
    public void showLongMessage(String message) {
        ((BaseView)getActivity()).showLongMessage(message);
    }

    @Override
    public void showMessageWithDialog(String title, String message) {
        ((BaseView)getActivity()).showMessageWithDialog(title, message);
    }

    @Override
    public void startActivity(Class<?> activity, FrameworkWrapper... extras) {
        ((BaseView)getActivity()).startActivity(activity, extras);

    }

    @Override
    public void startActivityAndClearAll(Class<?> activity, FrameworkWrapper... extras) {
        ((BaseView)getActivity()).startActivityAndClearAll(activity, extras);
    }

    @Override
    public void startActivityAndClearCurrent(Class<?> activity, FrameworkWrapper... extras) {
        ((BaseView)getActivity()).startActivityAndClearCurrent(activity, extras);
    }

    @Override
    public void startFragment(FragmentWrapper fragment, int listContainer, String tag) {
        ((BaseView)getActivity()).startFragment(fragment, listContainer, tag);
    }

    @Override
    public void setActivitySlideFromRightSideAnimation() {
        ((BaseView)getActivity()).setActivitySlideFromRightSideAnimation();
    }

    @Override
    public void setActivitySlideFromLeftSideAnimation() {
        ((BaseView)getActivity()).setActivitySlideFromLeftSideAnimation();
    }
}
