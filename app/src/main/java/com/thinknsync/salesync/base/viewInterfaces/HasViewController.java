package com.thinknsync.salesync.base.viewInterfaces;

public interface HasViewController {
    void initController();
}
