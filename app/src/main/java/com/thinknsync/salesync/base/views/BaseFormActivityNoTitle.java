package com.thinknsync.salesync.base.views;

import android.os.Bundle;

import com.thinknsync.inputvalidation.InputValidation;
import com.thinknsync.salesync.uiElements.formElements.forms.FormContainerContract;
import com.thinknsync.salesync.uiElements.formElements.forms.FormContainerViewImpl;
import com.thinknsync.salesync.uiElements.formElements.inputs.ActionView;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputValidationContainer;

import java.util.HashMap;

public abstract class BaseFormActivityNoTitle extends BaseActivity implements FormContainerContract.FormActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initForm();
    }

    protected abstract void initForm();

    @Override
    public void setupForm(HashMap<InputValidationContainer, InputValidation.ValidationTypes> inputAndTypeMap, ActionView actionView) {
        FormContainerContract.Initializer formContainer = new FormContainerViewImpl(inputAndTypeMap, getObjectConstructorInstance(), actionView);
        formContainer.initForm();
    }
}
