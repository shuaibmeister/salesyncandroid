package com.thinknsync.salesync.base.views;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.thinknsync.listviewadapterhelper.ViewPagerAdapter;
import com.thinknsync.salesync.R;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observer;

/**
 * Created by shuaib on 6/15/17.
 */

public abstract class BaseActivityWithViewPagerAndBottomNavigation extends BaseBottomNavigation {

    private ViewPagerAdapter adapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPagerUi();
        initFragments();
    }

    protected abstract void initFragments();

    private void initPagerUi(){
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    protected void addFragmentToPager(HashMap<Fragment, String> fragmentAndTitle){
        Iterator<Map.Entry<Fragment, String>> it = fragmentAndTitle.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Fragment, String> pair = (Map.Entry<Fragment, String>)it.next();
            adapter.addFragment((Fragment) pair.getKey(), (String) pair.getValue());
        }

        adapter.notifyDataSetChanged();
    }

    protected void addFragmentToPager(Fragment fragment, String title){
        adapter.addFragment(fragment, title);
        adapter.notifyDataSetChanged();
    }

    protected void keepFragmentsAlive(int limit){
        viewPager.setOffscreenPageLimit(limit);
    }

    protected void setupPagerOnchangeListener(final Observer[] onPageStateChangedObservers){
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrollStateChanged(int state) {
                runOnPageChange(0, state);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                runOnPageChange(1, position);
            }

            @Override
            public void onPageSelected(int position) {
                runOnPageChange(2, position);
            }

            private void runOnPageChange(int index, int position) {
                if(onPageStateChangedObservers[index] != null) {
                    onPageStateChangedObservers[index].update(null, position);
                }
            }
        });
    }

    @Override
    public void onBackPressed(){
        int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackCount == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
