package com.thinknsync.salesync.base.viewInterfaces;

public interface InteractiveView {
    void initViewListeners();
}
