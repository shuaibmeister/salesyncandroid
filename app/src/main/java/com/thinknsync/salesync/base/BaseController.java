package com.thinknsync.salesync.base;

import com.thinknsync.salesync.base.viewInterfaces.Interactors;

public interface BaseController<V extends BaseView> extends Interactors {
    void setViewReference(V view);
}
