package com.thinknsync.salesync.base.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.thinknsync.dialogs.DialogInterface;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.FragmentWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.base.BaseView;
import com.thinknsync.salesync.commons.osSpecifics.frameworkHelper.ApplicationInfo;
import com.thinknsync.salesync.factory.FactoryStringConstraints;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.factory.factories.AbstractFactoryImpl;

import java.util.HashMap;

public abstract class BaseActivity extends AppCompatActivity implements BaseView {

    private static ObjectConstructor objectConstructor;
    private FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        objectConstructor = (ObjectConstructor) new AbstractFactoryImpl()
                .getObject(FactoryStringConstraints.ObjectType.ObjectConstructor, null);
        setContentView(getLayoutId());
        initView();
        initController();
    }

    @Override
    public void showFullScreen() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    @Override
    public void hideStatusBar() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void setStatusBarColorTransparent(){
        setStatusBarColor(Color.TRANSPARENT);
        showFullScreen();
    }

    @SuppressLint("NewApi")
    @Override
    public void setStatusBarColor(int color) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        ApplicationInfo applicationInfo = objectConstructor.getAndroidFrameworkHelper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, getContextWrapper());
        }});
        if(applicationInfo.isGreaterThan(Build.VERSION_CODES.LOLLIPOP)) {
            getWindow().setStatusBarColor(color);
        }
    }

    @Override
    public void showShortMessage(String message) {
        Toast.makeText((Context)getContextWrapper().getFrameworkObject(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLongMessage(String message) {
        Toast.makeText((Context)getContextWrapper().getFrameworkObject(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMessageWithDialog(String title, String message){
        DialogInterface messageDialog = objectConstructor.getBasicMessageDialog(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, BaseActivity.this);
        }});

        messageDialog.setTitle(title);
        messageDialog.setMessage(message);
        messageDialog.setCancellable(false);
        messageDialog.setPositiveText("Ok");

        messageDialog.showDialog();
    }

    protected String getTextFromEdittext(EditText editText){
        return editText.getText().toString();

    }

    @Override
    public void startActivity(Class<?> activity, FrameworkWrapper... extras) {
        Intent activityIntent = new Intent(this, activity);
        for (FrameworkWrapper b : extras) {
            if(b != null) {
                activityIntent.putExtras((Bundle) b.getFrameworkObject());
            }
        }
        startActivity(activityIntent);
    }

    @Override
    public void startActivityAndClearAll(Class<?> activity, FrameworkWrapper... extras) {
        startActivity(activity, extras);
        finishAffinity();
    }

    @Override
    public void startActivityAndClearCurrent(Class<?> activity, FrameworkWrapper... extras) {
        startActivity(activity, extras);
        finish();
    }

    @Override
    public void startFragment(FragmentWrapper fragment, int listContainer, String tag) {
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(listContainer, fragment.getFrameworkObject()).commit();
    }

    @Override
    public ObjectConstructor getObjectConstructorInstance(){
        return objectConstructor;
    }

    @Override
    public void setActivitySlideFromRightSideAnimation(){
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_right);
    }

    @Override
    public void setActivitySlideFromLeftSideAnimation(){
        overridePendingTransition(R.anim.slide_out_to_right, R.anim.slide_in_from_right);
    }
}
