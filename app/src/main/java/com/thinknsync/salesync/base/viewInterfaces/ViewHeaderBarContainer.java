package com.thinknsync.salesync.base.viewInterfaces;

public interface ViewHeaderBarContainer {
    void setBackButtonAction();
    void setHeaderTitle(String title);
}
