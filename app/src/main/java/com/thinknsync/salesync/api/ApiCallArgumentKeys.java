package com.thinknsync.salesync.api;

/**
 * Created by shuaib on 5/13/17.
 */

public interface ApiCallArgumentKeys {

    interface CommonArgs {
        String USER_ID = "sr_id";
        String TRANSPORT_TYPE = "transport_type";
    }

    interface ImageArgs {
        String USER_IMAGE = "image";
    }

    interface LoginAndRegApiKeys {
        String USERNAME = "username";
        String PASSWORD = "password";
        String DEVICE_IDENTIFIER = "imei";
        String OTP = "temporary_pin";
        String NEW_PIN = "new_password";
    }

    interface LocationApiKeys {
        String LAT = "lat";
        String LON = "lon";
    }

    interface ImageFetchApiKeys {
        String OUTLET_IDS = "outlet_ids";
        String PRODUCT_IDS = "product_ids";
        String SKU_IDS = "sku_ids";
    }

    interface PasswordResetApiKeys {
        String USERNAME = "username";
        String OLD_PASSWORD = "old_password";
        String NEW_PASSWORD = "new_password";
        String RETYPED_PASSWORD = "confirm_password";
    }
}
