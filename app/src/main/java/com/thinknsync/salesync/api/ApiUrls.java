package com.thinknsync.salesync.api;

import com.thinknsync.positionmanager.TrackingData;

public final class ApiUrls {

    private static final String BASE_IP = "13.126.173.102";
    private static final String SECONDARY_MODULE_NAME = "/sale_secondary/";
    private static final String ATTENDANCE_MODULE_NAME = "/face_attendance/";
    private static final String BASE_URL = "http://"+ BASE_IP + ":8084" + SECONDARY_MODULE_NAME + "api/";
    private static final String BASE_ATTENDANCE_URL  = "http://"+ BASE_IP + ":8084" + ATTENDANCE_MODULE_NAME + "api/";

    public static final String LOGIN_URL = BASE_URL + "login";
    public static final String TEMP_PIN_GENERATION_URL = BASE_URL + "temporary_pin";
    public static final String TEMP_PIN_VALIDATE_URL = BASE_URL + "login_with_temporary_pin";
    public static final String PIN_RESET_URL = BASE_URL + "password_reset_with_temporary_pin";
    public static final String TODAYS_ROUTE_URL = BASE_URL + "get_routes";
    public static final String OTHER_ROUTES_URL = BASE_URL + "get_others_route";
    public static final String SKU_FETCH_URL = BASE_URL + "get_sku";
    public static final String EXCEPTION_REASONS_FETCH_URL = BASE_URL + "get_exception_reason";
    public static final String TARGET_VS_ACHIEVEMENT_FETCH_URL = BASE_URL + "get_sr_target_vs_achievement";
    public static final String CM_FETCH_URL = BASE_URL + "get_cm";
    public static final String SEND_ORDER_URL = BASE_URL + "send_order";
    public static final String SEND_ORDER_BULK_URL = BASE_URL + "send_order_bulk";
    public static final String SEND_EXCEPTION_URL = BASE_URL + "send_order_exception";
    public static final String UPDATE_URL = BASE_URL + "check_update";
    public static final String PRODUCT_IMAGE_URL = BASE_URL + "get_product_image";
    public static final String CHANGE_PASSWORD_URL = BASE_URL + "change_password";
    public static final String OUTLET_MASTER_DATA_URL = BASE_URL + "get_new_outlet_master_data";
    public static final String PAYMENT_METHODS_URL = BASE_URL + "get_payment_type";
    public static final String SKU_STOCK_URL = BASE_URL + "get_current_inventory";
    public static final String PROMOTION_DATA_URL = BASE_URL + "get_promotion";
    public static final String RUN_RATE_DARA_URL = BASE_URL + "get_run_rate";
    public static final String NEW_OUTLET_CREATE_URL = BASE_URL + "add_outlet";
    public static final String OUTLET_VERIFY_URL = BASE_URL + "verify_outlet";
    public static final String GET_DATE_TIME_URL = BASE_URL + "get_server_time";
    public static final String GET_SR_LOCATION_SEND_URL = BASE_URL + "send_emp_current_location";
    public static final String GET_CONFIGURATION_URL = BASE_URL + "get_system_config";
    public static final String GET_OUTLET_IMAGES_URL = BASE_URL + "get_outlet_and_owner_image";
    public static final String GET_PRODUCT_IMAGES_URL = BASE_URL + "get_product_image";
    public static final String GET_SKU_IMAGES_URL = BASE_URL + "get_sku_image";
    public static final String GET_OUTLET_INFO_UPDATE_URL = BASE_URL + "get_outlet_and_owner_images";

    public static final String ATTENDANCE_CHECK_URL = BASE_ATTENDANCE_URL + "has_attendance_today";
    public static final String ATTENDANCE_SUBMIT_URL = BASE_ATTENDANCE_URL + "submit";
    public static final String ATTENDANCE_OUT_URL = BASE_ATTENDANCE_URL + "atttendance_out";

    public static final String DAY_WISE_SALES_REPORT_URL = BASE_URL + "get_date_wise_sales";
    public static final String ROUTE_WISE_SALES_REPORT_URL = BASE_URL + "get_route_wise_sales";
    public static final String TARGET_VS_ACHIEVEMENT_REPORT_URL = BASE_URL + "get_sr_sku_wise_target_vs_achievement";
    public static final String DSS_REPORT_URL = BASE_URL + "get_dss";
}
