package com.thinknsync.salesync.cart.order.bottomView.orderConfirm;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import com.thinknsync.salesync.R;
import com.thinknsync.salesync.cart.order.CartOrderContract;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;
import com.thinknsync.salesync.uiElements.formElements.inputs.ActionButton;
import com.thinknsync.salesync.uiElements.formElements.inputs.InputField;

public class OrderSubmitBottomView implements OrderSubmitBottomSheetContract.View {

    private View bottomView;
    private CartOrderContract.View parentView;

    private TextView orderQtyCtn, orderQtyPcs, totalItemValue;
    private InputField discountInput;
    private ActionButton submitOrder;

    private SalesOrderInterface salesOrder;
    private OrderSubmitBottomSheetContract.Controller controller;

    public OrderSubmitBottomView(CartOrderContract.View parentView, SalesOrderInterface salesOrder, View bottomSheetView) {
        this.bottomView = bottomSheetView;
        this.parentView = parentView;
        this.salesOrder = salesOrder;
        initView();
        initController();
        initViewListeners();
        initValues();
    }

    @Override
    public int getBottomSheetLayoutResourceId() {
        return bottomView.getId();
    }

    @Override
    public void initController() {
        controller = new OrderSubmitBottomController(this, salesOrder);
    }

    @Override
    public void initView() {
        orderQtyCtn = bottomView.findViewById(R.id.ctn_count);
        orderQtyPcs = bottomView.findViewById(R.id.pcs_count);
        discountInput = bottomView.findViewById(R.id.discount_input);
        totalItemValue = bottomView.findViewById(R.id.total_item_value);
        submitOrder = bottomView.findViewById(R.id.submit_order);
    }

    @Override
    public void initValues() {
        double[] ctnPcs = controller.getTotalCtnPcsOfOrder();
        orderQtyCtn.setText(String.valueOf(ctnPcs[0]));
        orderQtyPcs.setText(String.valueOf(ctnPcs[1]));
        setOrderTotal(controller.getSalesOrderObject().getOrderTotal());
    }

    private void setOrderTotal(double total){
        totalItemValue.setText(String.valueOf(total));
    }

    @Override
    public void initViewListeners() {
        discountInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String discount = discountInput.getInput().equals("") ? "0" : discountInput.getInput();
                setOrderTotal(controller.getDiscountedTotal(Double.parseDouble(discount)));
            }
        });

        submitOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentView.onOrderConfirmed(controller.getSalesOrderObject());
            }
        });
    }

    @Override
    public void resetDiscount() {
        discountInput.setText("");
    }
}
