package com.thinknsync.salesync.cart.order;

import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.apilib.ErrorWrapper;
import com.thinknsync.salesync.commons.offlineSupport.SendMultipleOrdersToServer;
import com.thinknsync.salesync.commons.offlineSupport.SendMultipleToServer;
import com.thinknsync.salesync.commons.offlineSupport.SendOrderToServer;
import com.thinknsync.salesync.commons.offlineSupport.SendSingleToServer;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.database.repositories.order.OrderRepositoryInterface;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.skuInputUtils.SkuInput;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class CartOrderController implements CartOrderContract.Controller {
    private CartOrderContract.View view;
    private OrderRepositoryInterface orderRepo;
    private List<SalesOrderInterface> unsyncedOrders;

    public CartOrderController(CartOrderContract.View view) {
        setViewReference(view);
        orderRepo = view.getObjectConstructorInstance().getSalesOrderRepository();
    }

    @Override
    public void setViewReference(CartOrderContract.View view) {
        this.view = view;
    }

    @Override
    public List<SalesOrderInterface> getUnSyncedOrders() {
        if(unsyncedOrders == null){
            unsyncedOrders = orderRepo.getAll();
        }
        return unsyncedOrders;
    }

    @Override
    public void sendOrderToServer(final SalesOrderInterface salesOrder) {
        SendSingleToServer<SalesOrderInterface> sendToServer = new SendOrderToServer(view.getObjectConstructorInstance());
        sendToServer.setData(salesOrder);
        sendToServer.sendData(view.getContextWrapper(), new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int i, JSONObject jsonObject) throws JSONException {
                unsyncedOrders.remove(salesOrder);
                view.onApiCallSuccess();
            }

            @Override
            public void onApiCallFailure(int i, JSONObject jsonObject) throws JSONException {
                view.onApiCallFailure();
            }

            @Override
            public void onApiCallError(int reqId, ErrorWrapper error) {
                super.onApiCallError(reqId, error);
                view.onApiCallFailure();
            }
        });
    }

    @Override
    public void sendAllOrders() {
        SendMultipleToServer<SalesOrderInterface> sendAllToServer = new SendMultipleOrdersToServer(view.getObjectConstructorInstance());
        sendAllToServer.setData(getUnSyncedOrders());
        sendAllToServer.sendData(view.getContextWrapper(), new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int i, JSONObject jsonObject) throws JSONException {
                unsyncedOrders.clear();
                view.onBulkOrderSendingSuccess();
            }

            @Override
            public void onApiCallFailure(int i, JSONObject jsonObject) throws JSONException {
                unsyncedOrders = null;
                view.onBulkOrderSendingFailed();
            }

            @Override
            public void onApiCallError(int reqId, ErrorWrapper error) {
                super.onApiCallError(reqId, error);
                view.onBulkOrderSendingFailed();
            }
        });
    }

    @Override
    public void onEditOrderLine(SalesOrderInterface salesOrder, SkuInput skuInput) {
        orderRepo.editOrderLine(salesOrder, skuInput.getSku(), skuInput.getOrderQtyPcs(),
                skuInput.getDiscount(), skuInput.getTotal(), skuInput.getPromotionSkuIfApplicable());
        unsyncedOrders = null;
    }

    @Override
    public void onRemoveOrderLine(SalesOrderInterface salesOrder, SkuInterface sku) {
        orderRepo.deleteOrderLine(salesOrder, sku);
        unsyncedOrders = null;
    }
}
