package com.thinknsync.salesync.cart.order;

import com.thinknsync.apilib.ApiResponseView;
import com.thinknsync.salesync.base.BaseController;
import com.thinknsync.salesync.base.BaseView;
import com.thinknsync.salesync.base.viewInterfaces.InteractiveView;
import com.thinknsync.salesync.database.records.order.SalesOrderDetailsInterface;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;
import com.thinknsync.salesync.outlet.actions.order.skuView.SkuSelectionContract;

import java.util.List;

public interface CartOrderContract {
    interface View extends BaseView, ApiResponseView, InteractiveView, SkuSelectionContract.SkuInputEditView {
        String tag = "CartOrderView";

        void showOrderSummary(SalesOrderInterface salesOrder);
        void onOrderConfirmed(SalesOrderInterface salesOrder);
        void showEditOrderLineUi(SalesOrderDetailsInterface salesOrderDetails);
        void onBulkOrderSendingSuccess();
        void onBulkOrderSendingFailed();
    }

    interface Controller extends BaseController<View>, SkuSelectionContract.SkuInputEditView {
        List<SalesOrderInterface> getUnSyncedOrders();
        void sendOrderToServer(SalesOrderInterface salesOrder);
        void sendAllOrders();
    }
}
