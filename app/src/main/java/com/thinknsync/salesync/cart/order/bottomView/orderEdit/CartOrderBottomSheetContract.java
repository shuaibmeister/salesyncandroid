package com.thinknsync.salesync.cart.order.bottomView.orderEdit;

import com.thinknsync.salesync.commons.commonInterfaces.ContextHolder;
import com.thinknsync.salesync.database.records.order.SalesOrderDetailsInterface;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.SkuInputContract;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.skuInputUtils.SkuInput;

public interface CartOrderBottomSheetContract {
    interface View extends SkuInputContract.View, ContextHolder {
        void removeSkuLine();
    }

    interface Controller extends SkuInputContract.Controller {
        double[] getCtnPcsFromQty();
        SalesOrderDetailsInterface getSalesOrderDetails();
        SkuInput getCurrentLine();
        void setupSkuInputObject();
        void showSkuRemoveConfirmationDialog(String message);
    }
}
