package com.thinknsync.salesync.cart.order;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.thinknsync.listviewadapterhelper.recyclerView.BaseRecyclerViewAdapter;
import com.thinknsync.listviewadapterhelper.recyclerView.BaseViewHolder;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.database.records.order.SalesOrderDetailsInterface;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;

import java.util.List;

public class OutletOrderAdapter extends BaseRecyclerViewAdapter<SalesOrderInterface, OutletOrderAdapter.OutletOrdersViewHolder> {

    private CartOrderContract.View cartView;

    public OutletOrderAdapter(CartOrderContract.View cartView, List<SalesOrderInterface> objects) {
        super(objects);
        this.cartView = cartView;
    }

    @Override
    protected void setValues(OutletOrdersViewHolder outletOrdersViewHolder, SalesOrderInterface salesOrder) {
        outletOrdersViewHolder.outletTitle.setText(salesOrder.getOutlet().getName());
        List<SalesOrderDetailsInterface> orderDetails = salesOrder.getSalesOrderDetails();
        if(orderDetails.size() == 0){
            outletOrdersViewHolder.orderConfirmButton.setVisibility(View.GONE);
        } else {
            setupOrderLineList(outletOrdersViewHolder.orderLineList, orderDetails);
            setupClickListeners(outletOrdersViewHolder.orderConfirmButton, salesOrder);
        }
    }

    private void setupOrderLineList(RecyclerView orderLineList, List<SalesOrderDetailsInterface> salesOrderDetails) {
        OrderLineAdapter orderLineAdapter = new OrderLineAdapter(salesOrderDetails);
        orderLineAdapter.addToObservers(new TypedObserverImpl<SalesOrderDetailsInterface>() {
            @Override
            public void update(SalesOrderDetailsInterface salesOrderDetails) {
                cartView.showEditOrderLineUi(salesOrderDetails);
            }
        });
        orderLineList.setAdapter(orderLineAdapter);
    }

    private void setupClickListeners(TextView orderConfirmButton, final SalesOrderInterface salesOrder) {
        orderConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartView.showOrderSummary(salesOrder);
            }
        });
    }

    @Override
    protected OutletOrdersViewHolder getViewHolder(View view) {
        return new OutletOrdersViewHolder(view);
    }

    @Override
    protected int getView() {
        return R.layout.outlet_order_list_adapter_layout;
    }

    class OutletOrdersViewHolder extends BaseViewHolder {

        TextView outletTitle, orderConfirmButton;
        RecyclerView orderLineList;

        public OutletOrdersViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void initView(FrameworkWrapper frameworkWrapper) {
            View view = (View)frameworkWrapper.getFrameworkObject();
            outletTitle = view.findViewById(R.id.outlet_title);
            orderLineList = view.findViewById(R.id.order_line_list);
            orderConfirmButton = view.findViewById(R.id.order_confirm_button);
        }
    }
}
