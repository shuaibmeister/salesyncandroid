package com.thinknsync.salesync.cart.damage;

import androidx.fragment.app.Fragment;

import com.thinknsync.salesync.R;
import com.thinknsync.salesync.base.views.BaseFragment;

public class CartDamageView extends BaseFragment {
    @Override
    public int getLayoutId() {
        return R.layout.fragment_cart_damage;
    }

    @Override
    public void initController() {

    }

    @Override
    public void initView() {

    }
}
