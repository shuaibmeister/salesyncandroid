package com.thinknsync.salesync.cart;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.base.views.BaseActivityWithViewPagerAndBottomNavigation;
import com.thinknsync.salesync.cart.damage.CartDamageView;
import com.thinknsync.salesync.cart.order.CartOrderView;

import java.util.HashMap;


public class CartViewContainer extends BaseActivityWithViewPagerAndBottomNavigation {


    @Override
    public int getLayoutId() {
        return R.layout.activity_cart;
    }

    @Override
    public void initController() {

    }

    @Override
    public void initView() {

    }

    @Override
    public AndroidContextWrapper getContextWrapper() {
        return (AndroidContextWrapper) getObjectConstructorInstance().getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, CartViewContainer.this);
        }});
    }

    @Override
    protected void initFragments() {
        addFragmentToPager(new CartOrderView(), getString(R.string.order_tab_view_title));
        addFragmentToPager(new CartDamageView(), getString(R.string.damage_tab_view_title));
    }
}
