package com.thinknsync.salesync.cart.order.bottomView.orderEdit;

import com.thinknsync.dialogs.DialogInterface;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.database.records.order.SalesOrderDetailsInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.SkuInputBottomSheetController;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.skuInputUtils.SkuInput;

import java.util.HashMap;


public class CartOrderBottomController extends SkuInputBottomSheetController implements CartOrderBottomSheetContract.Controller{

    private SalesOrderDetailsInterface orderDetails;
    private CartOrderBottomSheetContract.View view;

    public CartOrderBottomController(CartOrderBottomSheetContract.View skuInputBottomSheetView, ObjectConstructor objectConstructor,
                                     SalesOrderDetailsInterface orderDetails) {
        super(skuInputBottomSheetView, objectConstructor, orderDetails.getParentOrder().getOutlet(), orderDetails.getSku(), 0);
        this.view = skuInputBottomSheetView;
        this.orderDetails = orderDetails;
    }

    @Override
    public double[] getCtnPcsFromQty() {
        return skuInput.getCtnPcsFromPcs(orderDetails.getQty());
    }

    @Override
    public SalesOrderDetailsInterface getSalesOrderDetails() {
        return orderDetails;
    }

    @Override
    public SkuInput getCurrentLine() {
        return skuInput;
    }

    @Override
    public void setupSkuInputObject() {
        skuInput.setOrderQtyPcs(orderDetails.getQty());
        skuInput.setDiscount(orderDetails.getDiscount());
    }

    @Override
    public void showSkuRemoveConfirmationDialog(String message) {
        DialogInterface<Boolean> attendanceConfirmationDialog = objectConstructor.getBooleanDialog(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, view.getContextWrapper().getFrameworkObject());
        }});
        attendanceConfirmationDialog.setDialogResponse(new DialogInterface.DialogResponse<Boolean>() {
            @Override
            public void onPositiveClicked(Boolean infoObject) {
                view.removeSkuLine();
            }

            @Override
            public void onNegativeClicked(Boolean infoObject) {

            }
        });
        attendanceConfirmationDialog.setMessage(message);
        attendanceConfirmationDialog.showDialog();
    }
}
