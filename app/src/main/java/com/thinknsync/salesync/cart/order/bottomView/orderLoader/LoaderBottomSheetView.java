package com.thinknsync.salesync.cart.order.bottomView.orderLoader;

import com.thinknsync.salesync.base.viewInterfaces.BaseBottomSheetContract;

public interface LoaderBottomSheetView extends BaseBottomSheetContract.BottomSheetView {
    void startRotatingImageLoader();
    void showContents();
    void showOrderSentSuccess();
    void showOrderSentFailed();
    void resetContents();
}
