package com.thinknsync.salesync.cart.order;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.thinknsync.listviewadapterhelper.recyclerView.BaseRecyclerViewAdapter;
import com.thinknsync.listviewadapterhelper.recyclerView.BaseViewHolder;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.commons.utils.ImageUtils;
import com.thinknsync.salesync.database.records.order.SalesOrderDetailsInterface;
import com.thinknsync.salesync.database.records.promotion.PromotionInterface;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.promotionAdapters.DiscountAdapter;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.promotionAdapters.PromotionalSkuAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrderLineAdapter extends BaseRecyclerViewAdapter<SalesOrderDetailsInterface, OrderLineAdapter.OrderLineViewHolder> {

    public OrderLineAdapter(List<SalesOrderDetailsInterface> objects) {
        super(objects);
    }

    @Override
    protected void setValues(OrderLineViewHolder orderLineViewHolder, final SalesOrderDetailsInterface salesOrderDetails) {
        if(salesOrderDetails.getPromotion() != null && salesOrderDetails.getPromotion().getPromotionType() == PromotionInterface.PromotionType.SKU){
            showPromotionOrderLine(orderLineViewHolder, salesOrderDetails);
        } else {
            showNonPromotionOrderLine(orderLineViewHolder, salesOrderDetails);
        }
    }

    private void showPromotionOrderLine(OrderLineViewHolder orderLineViewHolder, final SalesOrderDetailsInterface salesOrderDetails) {
        final SkuInterface lineSku = salesOrderDetails.getSku();
        Context context = (Context) orderLineViewHolder.getContextWrapper().getFrameworkObject();

        orderLineViewHolder.skuLineView.setVisibility(View.GONE);
        orderLineViewHolder.promotionLine.setVisibility(View.VISIBLE);

        final HashMap<SkuInterface, Integer> freeSkuQtyMap = new HashMap<SkuInterface, Integer>(){{
            put(lineSku, (int)salesOrderDetails.getQty());
        }};
        PromotionalSkuAdapter promotionAdapter = new PromotionalSkuAdapter(context, new ArrayList<HashMap<SkuInterface, Integer>>(){{
            add(freeSkuQtyMap);
        }});
        orderLineViewHolder.promotionLine.setAdapter(promotionAdapter);
    }

    private void showNonPromotionOrderLine(OrderLineViewHolder orderLineViewHolder, SalesOrderDetailsInterface salesOrderDetails) {
        Resources res = ((Context)orderLineViewHolder.getContextWrapper().getFrameworkObject()).getResources();
        final SkuInterface lineSku = salesOrderDetails.getSku();

        orderLineViewHolder.skuLineView.setVisibility(View.VISIBLE);
        orderLineViewHolder.promotionLine.setVisibility(View.GONE);

        orderLineViewHolder.skuImage.setImageBitmap(new ImageUtils().getBimapFromBase64Image(lineSku.getImage()));
        orderLineViewHolder.skuName.setText(lineSku.getName());
        orderLineViewHolder.orderQty.setText(salesOrderDetails.getQty() + " " + lineSku.getSkuUoms().get(0).getName());
        orderLineViewHolder.orderDiscount.setText(String.format(res.getString(R.string.default_price_format), salesOrderDetails.getDiscount()));
        orderLineViewHolder.orderPrice.setText(String.format(res.getString(R.string.default_price_format), salesOrderDetails.getSubTotalWithDiscount()));

        setEditListener(orderLineViewHolder.editAction, salesOrderDetails);
    }

    private void setEditListener(TextView editAction, final SalesOrderDetailsInterface salesOrderDetails) {
        editAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyObservers(salesOrderDetails);
            }
        });
    }

    @Override
    protected OrderLineViewHolder getViewHolder(View view) {
        return new OrderLineViewHolder(view);
    }

    @Override
    protected int getView() {
        return R.layout.order_line_adapter_layout;
    }

    class OrderLineViewHolder extends BaseViewHolder {
        View skuLineView;
        TextView skuName, orderQty, orderDiscount, orderPrice, editAction;
        ImageView skuImage;
        ListView promotionLine;

        public OrderLineViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @Override
        public void initView(FrameworkWrapper frameworkWrapper) {
            View view = (View) frameworkWrapper.getFrameworkObject();
            skuLineView = view.findViewById(R.id.sku_line);
            skuImage = view.findViewById(R.id.sku_image);
            skuName = view.findViewById(R.id.sku_name);
            orderQty = view.findViewById(R.id.order_qty);
            orderDiscount = view.findViewById(R.id.order_discount);
            orderPrice = view.findViewById(R.id.order_price);
            editAction = view.findViewById(R.id.edit_line);

            promotionLine = view.findViewById(R.id.free_sku_line);
        }
    }
}
