package com.thinknsync.salesync.cart.order.bottomView.orderConfirm;

import com.thinknsync.salesync.base.viewInterfaces.BaseBottomSheetContract;
import com.thinknsync.salesync.base.viewInterfaces.InteractiveView;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;

public interface OrderSubmitBottomSheetContract {
    interface View extends BaseBottomSheetContract.BottomSheetView, InteractiveView {
        String tag = "OrderSubmitBottomSheetView";

        void resetDiscount();
    }

    interface Controller extends BaseBottomSheetContract.BottomSheetController<View> {
        SalesOrderInterface getSalesOrderObject();
        void setSalesOrderObject(SalesOrderInterface salesOrder);
        double[] getTotalCtnPcsOfOrder();
        double getDiscountedTotal(double parseDouble);
    }
}
