package com.thinknsync.salesync.cart.order;

import android.os.Handler;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.base.views.BaseFragmentWithBottomSheet;
import com.thinknsync.salesync.cart.order.bottomView.orderLoader.LoaderBottomSheetView;
import com.thinknsync.salesync.cart.order.bottomView.orderLoader.LoaderBottomView;
import com.thinknsync.salesync.cart.order.bottomView.orderConfirm.OrderSubmitBottomSheetContract;
import com.thinknsync.salesync.cart.order.bottomView.orderConfirm.OrderSubmitBottomView;
import com.thinknsync.salesync.cart.order.bottomView.orderEdit.CartOrderBottomView;
import com.thinknsync.salesync.database.records.order.SalesOrderDetailsInterface;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.skuInputUtils.SkuInput;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.SkuInputContract;
import com.thinknsync.salesync.uiElements.formElements.inputs.ActionButton;

public class CartOrderView extends BaseFragmentWithBottomSheet implements CartOrderContract.View {

    private RecyclerView orderList;
    private ActionButton collectAllOrders;

    private CartOrderContract.Controller controller;
    private OutletOrderAdapter orderAdapter;
    private LoaderBottomSheetView bottomSheetViewOrderSendingProgress;

    @Override
    public void onStart() {
        super.onStart();
        setupOrderList();
        initViewListeners();
        setCollectAllOrdersText();
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_cart_order;
    }

    @Override
    public void initController() {
        controller = new CartOrderController(this);
    }

    @Override
    public void initView() {
        orderList = fragmentView.findViewById(R.id.order_list);
        collectAllOrders = fragmentView.findViewById(R.id.submit_all_orders);
    }

    private void setupOrderList() {
        orderAdapter = new OutletOrderAdapter(this, controller.getUnSyncedOrders());
        orderList.setAdapter(orderAdapter);
    }

    private void setCollectAllOrdersText(){
        collectAllOrders.setText(String.format(getString(R.string.bulk_order_confirm_label), controller.getUnSyncedOrders().size()));
    }

    @Override
    public void onApiCallSuccess(String... strings) {
        reviewOrderList();
        bottomSheetViewOrderSendingProgress.showOrderSentSuccess();
        collapseAndEnableBottomLoadingView();
    }

    @Override
    public void onApiCallFailure(String... strings) {
        bottomSheetViewOrderSendingProgress.showOrderSentFailed();
        collapseAndEnableBottomLoadingView();
    }

    private void reviewOrderList(){
        setupOrderList();
        setCollectAllOrdersText();
    }

    @Override
    public void onApiCallError(String... strings) {
        onApiCallFailure(strings);
    }

    private void collapseAndEnableBottomLoadingView(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                toggleBottomSheet();
                enableBottomSheetDragging();
            }
        }, 2000);
    }

    @Override
    public void initViewListeners() {
        collectAllOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoadingBottomSheet();
                controller.sendAllOrders();
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        onApiCallSuccess();
//                    }
//                }, 2500);
            }
        });
    }

    @Override
    public void showOrderSummary(SalesOrderInterface salesOrder){
        final OrderSubmitBottomSheetContract.View bottomSheetViewOrderSummary = new OrderSubmitBottomView(CartOrderView.this,
                salesOrder, getView().findViewById(R.id.bottom_sheet_review));
        addToObservers(new TypedObserverImpl<BottomSheetState>() {
            @Override
            public void update(BottomSheetState bottomSheetState) {
                if(bottomSheetState == BottomSheetState.STATE_COLLAPSED){
                    bottomSheetViewOrderSummary.resetDiscount();
                }
            }
        });
        setupBottomSheetWithLayoutView(bottomSheetViewOrderSummary);
        toggleBottomSheet();
    }

    @Override
    public void onOrderConfirmed(SalesOrderInterface salesOrder) {
        toggleBottomSheet();
        clearObservers();
        showLoadingBottomSheet();
        controller.sendOrderToServer(salesOrder);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                onApiCallSuccess();
//            }
//        }, 2500);
    }

    private void showLoadingBottomSheet(){
        disableBottomSheetDragging();
        bottomSheetViewOrderSendingProgress = new LoaderBottomView(
                getView().findViewById(R.id.bottom_sheet_order_sending), getObjectConstructorInstance());
        setupBottomSheetWithLayoutView(bottomSheetViewOrderSendingProgress);
        toggleBottomSheet();
        bottomSheetViewOrderSendingProgress.resetContents();
        bottomSheetViewOrderSendingProgress.showContents();
        bottomSheetViewOrderSendingProgress.startRotatingImageLoader();
    }

    @Override
    public void showEditOrderLineUi(SalesOrderDetailsInterface salesOrderDetails) {
        SkuInputContract.View bottomSheetViewSku = new CartOrderBottomView(CartOrderView.this, getObjectConstructorInstance(),
                salesOrderDetails, getView().findViewById(R.id.bottom_sheet_sku));
        setupBottomSheetWithLayoutView(bottomSheetViewSku);
        toggleBottomSheet();
    }

    @Override
    public void onBulkOrderSendingSuccess() {
        onApiCallSuccess();
    }

    @Override
    public void onBulkOrderSendingFailed() {
        onApiCallFailure();
        reviewOrderList();
    }

    @Override
    public void onEditOrderLine(SalesOrderInterface salesOrder, SkuInput skuInput) {
//        orderAdapter.onEditOrderLine(salesOrder, skuInput);
        controller.onEditOrderLine(salesOrder, skuInput);
        toggleBottomSheet();
        setupOrderList();
    }

    @Override
    public void onRemoveOrderLine(SalesOrderInterface salesOrder, SkuInterface sku) {
//        orderAdapter.onRemoveOrderLine(salesOrder, sku);
        controller.onRemoveOrderLine(salesOrder, sku);
        setCollectAllOrdersText();
        toggleBottomSheet();
        setupOrderList();
    }
}
