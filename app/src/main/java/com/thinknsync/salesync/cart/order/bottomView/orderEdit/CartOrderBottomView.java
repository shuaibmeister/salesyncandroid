package com.thinknsync.salesync.cart.order.bottomView.orderEdit;


import android.view.View;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.database.records.order.SalesOrderDetailsInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.outlet.actions.order.skuView.SkuSelectionContract;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.SkuInputBottomSheetView;
import com.thinknsync.salesync.uiElements.formElements.inputs.ActionButton;

import java.util.HashMap;


public class CartOrderBottomView extends SkuInputBottomSheetView implements CartOrderBottomSheetContract.View {

    private CartOrderBottomSheetContract.Controller orderViewController;
    private SkuSelectionContract.SkuInputEditView parentEditView;

    private ActionButton editLine, removeLine;

    public CartOrderBottomView(SkuSelectionContract.SkuInputEditView parentView, ObjectConstructor objectConstructor,
                               SalesOrderDetailsInterface orderDetails, View view) {
        super(null, objectConstructor, orderDetails.getParentOrder().getOutlet(), orderDetails.getSku(), 0, view);
        initControllers(orderDetails);
        initValuesLocal();
        resetInputFieldsLocal();
        super.initViewListeners();
        this.parentEditView = parentView;
        setSkuOrderAmountValues();
    }

    @Override
    public void initView() {
        super.initView();
        addToCartButton.setVisibility(View.GONE);
        bottomSheetView.findViewById(R.id.sku_selection_modification_layout).setVisibility(View.VISIBLE);
        editLine = bottomSheetView.findViewById(R.id.update_cart);
        removeLine = bottomSheetView.findViewById(R.id.remove_from_cart);
    }

    @Override
    public void initController() {
    }

    private void initControllers(SalesOrderDetailsInterface orderDetails){
        controller = new CartOrderBottomController(this, objectConstructor, orderDetails);
        orderViewController = (CartOrderBottomSheetContract.Controller)controller;
    }

    @Override
    public void initViewListeners() {
        editLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentEditView.onEditOrderLine(orderViewController.getSalesOrderDetails().getParentOrder(),
                        orderViewController.getCurrentLine());
            }
        });

        removeLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderViewController.showSkuRemoveConfirmationDialog(bottomSheetView.getContext().getString(R.string.remove_from_cart_confirmation_message));
            }
        });
    }

    @Override
    public void initValues() {
    }

    private void initValuesLocal(){
        super.initValues();
    }

    @Override
    public void resetInputFields() {
    }

    private void resetInputFieldsLocal() {
        super.resetInputFields();
    }

    @Override
    public void removeSkuLine(){
        SalesOrderDetailsInterface orderDetails = orderViewController.getSalesOrderDetails();
        parentEditView.onRemoveOrderLine(orderDetails.getParentOrder(), orderDetails.getSku());
    }

    private void setSkuOrderAmountValues(){
        orderViewController.setupSkuInputObject();
        double[] ctnPcs = orderViewController.getCtnPcsFromQty();
        ctnInput.setText(String.valueOf(ctnPcs[0] != 0 ? ctnPcs[0] : ""));
        pcsInput.setText(String.valueOf(ctnPcs[1] != 0 ? ctnPcs[1] : ""));
        discountInput.setText(String.valueOf(orderViewController.getCurrentLine().getDiscount()));
        setTotalValue(orderViewController.getCurrentLine().getTotalWithDiscount());
    }

    @Override
    public AndroidContextWrapper getContextWrapper() {
        return (AndroidContextWrapper) objectConstructor.getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, bottomSheetView.getContext());
        }});
    }
}
