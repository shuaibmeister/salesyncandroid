package com.thinknsync.salesync.cart.order.bottomView.orderConfirm;

import com.thinknsync.salesync.database.records.order.SalesOrderDetailsInterface;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.outlet.actions.order.skuView.bottomSlidingView.skuInputUtils.SkuQtyUnitConverter;

public class OrderSubmitBottomController implements OrderSubmitBottomSheetContract.Controller {

    private OrderSubmitBottomSheetContract.View orderSubmitBottomView;
    private SalesOrderInterface salesOrder;

    public OrderSubmitBottomController(OrderSubmitBottomSheetContract.View orderSubmitBottomView, SalesOrderInterface salesOrder) {
        setViewReference(orderSubmitBottomView);
        this.salesOrder = salesOrder;
    }

    @Override
    public void setViewReference(OrderSubmitBottomSheetContract.View bottomSheetView) {
        this.orderSubmitBottomView = bottomSheetView;
    }

    @Override
    public SalesOrderInterface getSalesOrderObject() {
        return salesOrder;
    }

    @Override
    public void setSalesOrderObject(SalesOrderInterface salesOrder) {
        this.salesOrder = salesOrder;
    }

    @Override
    public double[] getTotalCtnPcsOfOrder() {
        double[] ctnPcs = new double[]{0, 0};
        for (SalesOrderDetailsInterface orderDetails : salesOrder.getSalesOrderDetails()){
            SkuQtyUnitConverter skuQtyUnitConverter = new SkuQtyUnitConverter(orderDetails.getSku());
            double[] ctnPcsOfSku = skuQtyUnitConverter.getCtnPcsFromPcs(orderDetails.getQty());
            ctnPcs[0] += ctnPcsOfSku[0];
            ctnPcs[1] += ctnPcsOfSku[1];
        }
        return ctnPcs;
    }

    @Override
    public double getDiscountedTotal(double discount) {
        salesOrder.setInvoiceDiscount(discount);
        return salesOrder.getOrderTotalWithDiscount();
    }
}
