package com.thinknsync.salesync.cart.order.bottomView.orderLoader;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thinknsync.objectwrappers.AndroidViewWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.commons.utils.animation.AnimationHelper;
import com.thinknsync.salesync.commons.utils.animation.Animations;
import com.thinknsync.salesync.factory.ObjectConstructor;

import java.util.HashMap;

public class LoaderBottomView implements LoaderBottomSheetView {

    private View bottomSheetView, loaderContent;
    private ImageView sheetLoaderImageView, sendSuccessImageView, sendFailedImageView;
    private TextView loaderCaptionText;

    private Animations animations;
    private ObjectConstructor objectConstructor;
    private FrameworkWrapper<View> loaderImageWrapper;

    public LoaderBottomView(View bottomSheetView, ObjectConstructor objectConstructor) {
        this.bottomSheetView = bottomSheetView;
        this.objectConstructor = objectConstructor;
        this.animations = new AnimationHelper(bottomSheetView.getContext());
        initView();
    }

    @Override
    public int getBottomSheetLayoutResourceId() {
        return bottomSheetView.getId();
    }

    @Override
    public void initController() {

    }

    @Override
    public void initView() {
        sheetLoaderImageView = bottomSheetView.findViewById(R.id.loading_image);
        sendSuccessImageView = bottomSheetView.findViewById(R.id.image_complete);
        sendFailedImageView = bottomSheetView.findViewById(R.id.image_failed);
        loaderContent = bottomSheetView.findViewById(R.id.loader_content);
        loaderCaptionText = bottomSheetView.findViewById(R.id.order_send_status_text);

        loaderImageWrapper = objectConstructor.getViewWrapper(new HashMap<String, Object>(){{
            put(AndroidViewWrapper.tag, sheetLoaderImageView);
        }});
    }

    @Override
    public void initValues() {

    }

    @Override
    public void startRotatingImageLoader() {
        animations.rotateViewInfinite(loaderImageWrapper);
    }

    @Override
    public void showContents() {
        FrameworkWrapper<View> viewWrapper = objectConstructor.getViewWrapper(new HashMap<String, Object>(){{
            put(AndroidViewWrapper.tag, loaderContent);
        }});
        animations.setDefaultAnimationDuration(1000);
        animations.fadeInElement(viewWrapper);
    }

    @Override
    public void showOrderSentSuccess() {
        showProgressResult(sendSuccessImageView, bottomSheetView.getContext().getString(R.string.collecting_order_done_label));
    }

    @Override
    public void showOrderSentFailed() {
        showProgressResult(sendFailedImageView, bottomSheetView.getContext().getString(R.string.collecting_order_failed_label));
    }

    @Override
    public void resetContents() {
        loaderCaptionText.setText(bottomSheetView.getContext().getString(R.string.collecting_order_label));
        sheetLoaderImageView.setVisibility(View.VISIBLE);
        sendSuccessImageView.setVisibility(View.GONE);
        sendFailedImageView.setVisibility(View.GONE);
    }

    private void showProgressResult(final View alternateView, final String message){
        FrameworkWrapper<View> locationCaptionWrapper = objectConstructor.getViewWrapper(new HashMap<String, Object>(){{
            put(AndroidViewWrapper.tag, loaderCaptionText);
        }});
        FrameworkWrapper<View> sentSuccessImageWrapper = objectConstructor.getViewWrapper(new HashMap<String, Object>(){{
            put(AndroidViewWrapper.tag, alternateView);
        }});

        animations.setDefaultAnimationDuration(Animations.DEFAULT_FADE_ANIMATION_DURATION);
        animations.fadeOutModifyAndFadeIn(locationCaptionWrapper, new Runnable() {
            @Override
            public void run() {
                loaderCaptionText.setText(message);
            }
        });
        sheetLoaderImageView.clearAnimation();
        animations.fadeOutElement(loaderImageWrapper);
        animations.fadeInElement(sentSuccessImageWrapper);
    }
}
