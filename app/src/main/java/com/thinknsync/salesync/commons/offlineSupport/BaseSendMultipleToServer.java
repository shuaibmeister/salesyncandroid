package com.thinknsync.salesync.commons.offlineSupport;

import android.content.Context;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiDataObjectImpl;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.apilib.ErrorWrapper;
import com.thinknsync.convertible.Convertible;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.salesync.commons.commonInterfaces.HasIdentifier;
import com.thinknsync.salesync.factory.ObjectConstructor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class BaseSendMultipleToServer<T extends Convertible<T> & HasIdentifier> implements SendMultipleToServer<T> {

    protected ObjectConstructor objectConstructor;
    protected List<T> dataObjects;
    protected List<T> saveObjects;

    public BaseSendMultipleToServer(ObjectConstructor objectConstructor){
        this.objectConstructor = objectConstructor;
        saveObjects = new ArrayList<>();
    }

    @Override
    public void setData(List<T> data) {
        this.dataObjects = data;
    }

    @Override
    public void sendData(FrameworkWrapper contextWrapper, ApiResponseActions responseActions) {
        try {
            final ApiDataObject apiDataObject = new ApiDataObjectImpl(new JSONObject()
                    .put(getJsonArrayKey(), getObjectsJsonArray()));
            ApiRequest apiRequest = getSendToServerApiRequest(contextWrapper, responseActions, apiDataObject);
            apiRequest.sendRequest();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected JSONArray getObjectsJsonArray() throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (T dataObject : dataObjects) {
            jsonArray.put(getObjectJson(dataObject));
        }
        return jsonArray;
    }

    protected abstract String getJsonArrayKey();

    protected JSONObject getObjectJson(T dataObject) throws JSONException{
        return dataObject.toJson();
    }

    protected abstract ApiRequest getSendToServerApiRequest(final FrameworkWrapper<Context> contextWrapper, final ApiResponseActions responseActions,
                                                final ApiDataObject apiDataObject);

    protected ApiResponseActions getWrappedResponseActions(final ApiResponseActions responseActions){
        return new ApiResponseActions() {
            @Override
            public void onApiCallSuccess(int i, JSONObject jsonObject) throws JSONException {
                saveObjects.addAll(dataObjects);
                onPostSynced(true);
                responseActions.onApiCallSuccess(i, jsonObject);
            }

            @Override
            public void onApiCallFailure(int i, JSONObject jsonObject) throws JSONException {
                JSONArray data = jsonObject.getJSONArray(ApiRequest.ApiFields.KEY_DATA);
                if(data.length() > 0){
                    long[] failedIds = getFailedObjectIds(data);
                    for (int j = 0; j < dataObjects.size(); j++) {
                        T dataObject = dataObjects.get(j);
                        if(Arrays.binarySearch(failedIds, dataObject.getDataId()) < 0){
                            saveObjects.add(dataObject);
                        }
                    }
                }
                onPostSynced(false);
                responseActions.onApiCallFailure(i, jsonObject);
            }

            @Override
            public void onApiCallError(int i, ErrorWrapper errorWrapper) {
                responseActions.onApiCallError(i, errorWrapper);
            }

            private long[] getFailedObjectIds(JSONArray data) throws JSONException{
                long[] failedIds = new long[data.length()];
                for (int j = 0; j < data.length(); j++) {
                    failedIds[j] = data.getJSONObject(j).getLong(HasIdentifier.key_identifier);
                }
                return failedIds;
            }
        };
    }
}
