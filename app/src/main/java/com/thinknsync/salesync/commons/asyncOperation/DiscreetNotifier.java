package com.thinknsync.salesync.commons.asyncOperation;

import com.thinknsync.observerhost.ObserverHost;

public interface DiscreetNotifier<T> extends ObserverHost<T> {
    void setDelay(int delay);
    void startNotificationLoop(T object);
}
