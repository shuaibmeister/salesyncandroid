package com.thinknsync.salesync.commons.osSpecifics.imageCapture;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;

import com.thinknsync.objectwrappers.BitmapWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.salesync.commons.utils.ImageUtils;

public class ImageCaptureImpl implements ImageCapture {

    private Activity activity;
    private String capturedImage;

    public ImageCaptureImpl(Activity activity){
        this.activity = activity;
    }

    @Override
    public void openCamera() {
        Intent photoPickerIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        activity.startActivityForResult(photoPickerIntent, ImageCaptureCode);
    }

    @Override
    public FrameworkWrapper processCapturedImage(FrameworkWrapper bitmapWrapper) {
        ImageUtils imageUtils = new ImageUtils();
        Bitmap scaledImage = imageUtils.getScaledImage(((BitmapWrapper)bitmapWrapper).getFrameworkObject(), 150, 150);
        capturedImage = imageUtils.encodeToBase64String(scaledImage);
        bitmapWrapper.setFrameworkObject(scaledImage);
        return bitmapWrapper;
    }

    @Override
    public String getCapturedImage() {
        return capturedImage;
    }
}
