package com.thinknsync.salesync.commons.loginAndSessions.fetchServerData;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.convertible.Convertible;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.database.records.promotion.PromotionInterface;
import com.thinknsync.salesync.database.repositories.promotion.PromotionRepositoryInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class FetchPromotionData extends BaseFetchData {

    public FetchPromotionData(ObjectConstructor objectConstructor, AndroidContextWrapper contextWrapper, ApiDataObject apiDataObject) {
        super(objectConstructor, contextWrapper, apiDataObject);
    }

    @Override
    protected String getOnPreExecuteMessage() {
        return contextWrapper.getFrameworkObject().getString(R.string.message_sync_promotion_data_start);
    }

    @Override
    protected String getOnPostExecuteMessage() {
        return contextWrapper.getFrameworkObject().getString(R.string.message_sync_promotion_data_end);
    }

    @Override
    public void executeTask(Object inputObject) {
        onPreExecute();
        syncPromotionData(apiDataObject);
    }

    private void syncPromotionData(final ApiDataObject apiDataObject) {
        final ApiResponseActions responseActions = new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException {
                PromotionRepositoryInterface promoRepo = objectConstructor.getPromotionRepository();
                promoRepo.deleteAll();
                savePromotions(promoRepo, responseJson.getJSONArray(ApiRequest.ApiFields.KEY_DATA));
                checkAndNotifyComplete();
            }

            @Override
            public void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException {
                checkAndNotifyComplete();
            }
        };

        ApiRequest apiRequest = objectConstructor.getPromotionDataApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, responseActions);
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});

        apiRequest.setProgressDialog(null);
        apiRequest.setShouldShowAlertIfNoInternet(false);
        apiRequest.sendRequest();
    }

    private void savePromotions(PromotionRepositoryInterface promoRepo, JSONArray promotionsJsonArray) throws JSONException {
        PromotionInterface[] promotionToSave = new PromotionInterface[promotionsJsonArray.length()];
        for (int i = 0; i < promotionsJsonArray.length(); i++) {
            PromotionInterface promotion = promoRepo.getNewEntity().fromJson(promotionsJsonArray.getJSONObject(i));
            promotionToSave[i] = promotion;
        }
        promoRepo.savePromotions(promotionToSave);
    }
}
