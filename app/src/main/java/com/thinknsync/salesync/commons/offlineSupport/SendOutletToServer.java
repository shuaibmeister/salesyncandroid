package com.thinknsync.salesync.commons.offlineSupport;

import android.content.Context;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SendOutletToServer extends BaseSendSingleToServer<OutletInterface> {

    public SendOutletToServer(ObjectConstructor objectConstructor) {
        super(objectConstructor);
    }

    @Override
    protected JSONObject getObjectJson() throws JSONException {
        JSONObject outletJson = dataObject.withoutProperty(new String[]{
                OutletInterface.key_id, OutletInterface.key_outlet_id, OutletInterface.key_route, OutletInterface.key_is_synced});
        outletJson.put(UserInterface.key_sr_id, objectConstructor.getUserRepository().getLoggedInUser().getDataId());
        return outletJson;
    }

    @Override
    protected ApiRequest getSendToServerApiRequest(final FrameworkWrapper<Context> contextWrapper, final ApiResponseActions responseActions, final ApiDataObject apiDataObject) {
        return objectConstructor.getOutletCreateApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, getWrappedResponseActions(responseActions));
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});
    }

    @Override
    public void onPostSynced(boolean isSynced) {
        objectConstructor.getOutletRepository().save(dataObject);
    }
}
