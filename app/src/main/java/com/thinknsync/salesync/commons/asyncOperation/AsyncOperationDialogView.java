package com.thinknsync.salesync.commons.asyncOperation;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.ProgressDialogWrapper;
import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.salesync.factory.ObjectConstructor;

import java.util.HashMap;

public class AsyncOperationDialogView extends ObserverHostImpl<Object> implements AsyncExecute.View {

    private ProgressDialogWrapper alertDialog;

    public AsyncOperationDialogView(ObjectConstructor objectConstructor, final AndroidContextWrapper contextWrapper) {
        this.alertDialog = (ProgressDialogWrapper) objectConstructor.getNotifiableProgressDialogWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
        }});
    }

    @Override
    public void onAsyncTaskStart(String... progressMessage) {
        alertDialog.setMessage(progressMessage.length > 0 ? progressMessage[0] : "");
        alertDialog.showProgress();
    }

    @Override
    public void onProgressUpdated(int progressValue, String... progressMessage) {
        alertDialog.setMessage(progressMessage.length > 0 ? progressMessage[0] : "");
        alertDialog.setProgressValue(progressValue);
    }

    @Override
    public void onAsyncTaskComplete(String... progressMessage) {
        alertDialog.hideProgress();
        notifyObservers(null);
    }
}
