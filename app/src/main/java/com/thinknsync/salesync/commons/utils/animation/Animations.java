package com.thinknsync.salesync.commons.utils.animation;

import com.thinknsync.objectwrappers.FrameworkWrapper;

public interface Animations {
    int DEFAULT_FADE_ANIMATION_DURATION = 300;

    void fadeInElement(FrameworkWrapper viewWrapper);
    void fadeOutElement(FrameworkWrapper viewWrapper);
    void viewFadeOutAndIn(FrameworkWrapper viewWrapper);
    void fadeOutModifyAndFadeIn(FrameworkWrapper viewWrapper, Runnable viewRunnable);
    void slideInFromTop(FrameworkWrapper viewWrapper);
    void slideOutToTop(FrameworkWrapper viewWrapper);
    void rotateViewInfinite(FrameworkWrapper viewWrapper);
    void rotateView(FrameworkWrapper viewWrapper, int rotationCount);
    int getDefaultAnimationDuration();
    void setDefaultAnimationDuration(int defaultAnimationDuration);
}
