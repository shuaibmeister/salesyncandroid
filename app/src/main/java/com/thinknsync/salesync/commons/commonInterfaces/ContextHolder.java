package com.thinknsync.salesync.commons.commonInterfaces;

import com.thinknsync.objectwrappers.FrameworkWrapper;

public interface ContextHolder {
    FrameworkWrapper getContextWrapper();
}
