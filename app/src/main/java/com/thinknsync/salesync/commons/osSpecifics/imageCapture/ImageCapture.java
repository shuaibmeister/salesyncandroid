package com.thinknsync.salesync.commons.osSpecifics.imageCapture;

import com.thinknsync.objectwrappers.FrameworkWrapper;

public interface ImageCapture {
    int ImageCaptureCode = 1001;

    void openCamera();
    FrameworkWrapper processCapturedImage(FrameworkWrapper bitmapWrapper);
    String getCapturedImage();
}
