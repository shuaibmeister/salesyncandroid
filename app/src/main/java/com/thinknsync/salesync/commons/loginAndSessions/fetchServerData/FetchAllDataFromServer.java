package com.thinknsync.salesync.commons.loginAndSessions.fetchServerData;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiDataObjectImpl;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.commons.asyncOperation.AsyncExecute;
import com.thinknsync.salesync.commons.asyncOperation.AsyncOperationDialogView;
import com.thinknsync.salesync.commons.utils.DateTimeUtils;
import com.thinknsync.salesync.database.records.config.ConfigInterface;
import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.database.repositories.config.ConfigRepositoryInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import java.util.HashMap;

public class FetchAllDataFromServer extends AsyncOperationDialogView implements Runnable {

//    private final int totalSyncCount = 5;
    private final int totalSyncCount = 4;
    private int completedSyncCount = 0;

    private ObjectConstructor objectConstructor;
    private AndroidContextWrapper contextWrapper;

    private ConfigRepositoryInterface configRepository;
    private DateTimeUtils dateUtils;

    public FetchAllDataFromServer(ObjectConstructor objectConstructor, AndroidContextWrapper contextWrapper) {
        super(objectConstructor, contextWrapper);
        this.objectConstructor = objectConstructor;
        this.contextWrapper = contextWrapper;

        configRepository = objectConstructor.getConfigRepository();
        dateUtils = new DateTimeUtils();
    }

    @Override
    public void onAsyncTaskComplete(String... message) {
        completedSyncCount ++;
        onProgressUpdated((int)((double)completedSyncCount / totalSyncCount * 100), message);
        if(completedSyncCount == totalSyncCount){
            configRepository.insertOrUpdateConfig(ConfigInterface.ConfigType.LAST_SYNC_DATE, dateUtils.getDefaultDateTimeStringNow());
            super.onAsyncTaskComplete(message);
        }
    }

    @Override
    public void run() {
        final UserInterface loggedInUser = objectConstructor.getUserRepository().getLoggedInUser();
        final ApiDataObject apiDataObject = new ApiDataObjectImpl(new HashMap<String, String>() {{
            put(UserInterface.key_sr_id, String.valueOf(loggedInUser.getDataId()));

        }});

        AsyncExecute<Object, Void> fetchRouteData = new FetchRouteData(objectConstructor, contextWrapper, apiDataObject);
        fetchRouteData.setViewReference(this);
        fetchRouteData.executeTask(null);

        AsyncExecute<Object, Void> fetchSkuData = new FetchSkuData(objectConstructor, contextWrapper, apiDataObject);
        fetchSkuData.setViewReference(this);
        fetchSkuData.executeTask(null);

        AsyncExecute<Object, Void> fetchMasterData = new FetchMasterData(objectConstructor, contextWrapper, apiDataObject);
        fetchMasterData.setViewReference(this);
        fetchMasterData.executeTask(null);

//        AsyncExecute<Object, Void> fetchPromotionData = new FetchPromotionData(objectConstructor, contextWrapper, apiDataObject);
//        fetchPromotionData.setViewReference(this);
//        fetchPromotionData.executeTask(null);

        AsyncExecute<Object, Void> fetchAttendanceData = new FetchAttendanceData(objectConstructor, contextWrapper, apiDataObject);
        fetchAttendanceData.setViewReference(this);
        fetchAttendanceData.executeTask(null);
    }


//    @Override
//    protected void onProgressUpdate(String... progressValue) {
//        viewReference.onProgressUpdated(progressValue);
//    }
}
