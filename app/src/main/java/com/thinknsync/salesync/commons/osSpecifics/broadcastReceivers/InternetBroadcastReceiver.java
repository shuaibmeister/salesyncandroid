package com.thinknsync.salesync.commons.osSpecifics.broadcastReceivers;

import android.content.Context;
import android.content.Intent;

import com.thinknsync.logger.Logger;
import com.thinknsync.networkutils.NetworkStatusReporter;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.factory.ObjectConstructor;

import java.util.HashMap;

public class InternetBroadcastReceiver extends BaseBroadcastListener<Boolean> {

    private ObjectConstructor objectConstructor;
    private boolean isSyncInProgress;

    public InternetBroadcastReceiver(ObjectConstructor objectConstructor){
        this.objectConstructor = objectConstructor;
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        final AndroidContextWrapper contextWrapper = (AndroidContextWrapper) objectConstructor.getAndroidContextWrapper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, context);
        }});

        NetworkStatusReporter networkUtils = objectConstructor.getNetworkStatusReport(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
        }});

        Logger logger = objectConstructor.getLogger();

        if(networkUtils.isConnected(false)) {
            if (!isSyncInProgress) {
                isSyncInProgress = true;
                notifyObservers(networkUtils.isConnected(false));
                logger.debugLog("connectivity listener", "syncing");
            }
        } else {
            isSyncInProgress = false;
            logger.debugLog("connectivity listener", "connectivity lost");
        }
        logger.debugLog("connectivity listener", networkUtils.getConnectivityStatusString());
    }
}
