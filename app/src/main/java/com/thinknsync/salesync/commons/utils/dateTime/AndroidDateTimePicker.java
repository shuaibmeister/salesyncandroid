package com.thinknsync.salesync.commons.utils.dateTime;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.widget.TimePicker;

import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.salesync.commons.utils.DateTimeUtils;
import com.thinknsync.salesync.commons.utils.TextFormatter;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by shuaib on 7/5/17.
 */

public class AndroidDateTimePicker implements DateTimePicker {

    private String dateSeparator = DateSeparator;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private boolean allowPreviousDates = true;
    private boolean allowFutureDates = true;

    private Context context;
    private Calendar calendar = Calendar.getInstance();

    private DateTimeUtils dateTimeUtils;

    public AndroidDateTimePicker(Context context) {
        this.context = context;
        dateTimeUtils = new DateTimeUtils();
    }

    @Override
    public void setDateSeparator(String separator) {
        this.dateSeparator = separator;
    }

    @Override
    public void allowPreviousDates(boolean allow) {
        this.allowPreviousDates = allow;
    }

    @Override
    public void allowFutureDates(boolean allow) {
        this.allowFutureDates = allow;
    }

    @Override
    public void showDatePicker(final TypedObserver<String> pickerSelectedCallback) {
        setYearMonthDayFromCalendar();
        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(android.widget.DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        calendar.set(year, monthOfYear, dayOfMonth);
                        validateDate();
                        pickerSelectedCallback.update(getDateString());
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    public void showTimePicker(final TypedObserver<String> pickerSelectedCallback) {
        TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);
                        validateDate();
                        pickerSelectedCallback.update(null, getTimeString());
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    private void validateDate() {
        if (!allowPreviousDates && calendar.getTimeInMillis() < Calendar.getInstance().getTimeInMillis()) {
            calendar = Calendar.getInstance();
        }

        if(!allowFutureDates && calendar.getTimeInMillis() > Calendar.getInstance().getTimeInMillis()){
            calendar = Calendar.getInstance();
        }

        setYearMonthDayFromCalendar();
        setHourMinuteFromCalendar();
    }

    private void setHourMinuteFromCalendar() {
        mHour = calendar.get(Calendar.HOUR_OF_DAY);
        mMinute = calendar.get(Calendar.MINUTE);
    }

    private void setYearMonthDayFromCalendar() {
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);
    }

    @Override
    public String getDateString() {
//        return mDay + dateSeparator + (mMonth + 1) + dateSeparator + mYear;
        return mYear + dateSeparator + (mMonth + 1) + dateSeparator + mDay;
    }

    @Override
    public String getTimeString() {
        return dateTimeUtils.getAmPmFormattedTime(calendar.getTime());
    }

    @Override
    public Calendar getCalendar() {
        return calendar;
    }
}
