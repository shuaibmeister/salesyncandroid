package com.thinknsync.salesync.commons.loginAndSessions.fetchServerData;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.apilib.ErrorWrapper;
import com.thinknsync.convertible.Convertible;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.database.records.masterData.MasterData;
import com.thinknsync.salesync.database.records.sku.SkuStockInterface;
import com.thinknsync.salesync.database.repositories.masterData.MasterDataRepositoryInterface;
import com.thinknsync.salesync.database.repositories.sku.SkuRepositoryInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FetchMasterData extends BaseFetchData {

    private MasterDataRepositoryInterface masterDataRepo;
    private List<MasterData> masterDataList;

    public FetchMasterData(ObjectConstructor objectConstructor, AndroidContextWrapper contextWrapper, ApiDataObject apiDataObject) {
        super(objectConstructor, contextWrapper, apiDataObject);
        totalRequestCount = 3;
        masterDataRepo = objectConstructor.getMasterDataRepository();
        masterDataList = new ArrayList<>();
    }

    @Override
    protected String getOnPreExecuteMessage() {
        return contextWrapper.getFrameworkObject().getString(R.string.message_sync_master_data_start);
    }

    @Override
    protected String getOnPostExecuteMessage() {
        return contextWrapper.getFrameworkObject().getString(R.string.message_sync_master_data_end);
    }

    @Override
    public void executeTask(Object inputObject) {
        onPreExecute();
        syncExceptionReason();
        syncOutletMaster();
        syncPaymentMethods();
        syncSkuStocks();
    }

    private void syncExceptionReason() {
        final ApiResponseActions responseActions = new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException {
                addToMasterDataList(responseJson.getJSONArray(ApiRequest.ApiFields.KEY_DATA), MasterData.MasterDataType.EXCEPTION_REASON);
                checkAndNotifyComplete();
            }

            @Override
            public void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException {
                checkAndNotifyComplete();
            }

            @Override
            public void onApiCallError(int i, ErrorWrapper errorWrapper) {
                super.onApiCallError(i, errorWrapper);
                checkAndNotifyComplete();
            }
        };

        ApiRequest apiRequest = objectConstructor.getExceptionReasonsApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, responseActions);
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});

        apiRequest.setProgressDialog(null);
        apiRequest.setShouldShowAlertIfNoInternet(false);
        apiRequest.sendRequest();
    }

    private void syncOutletMaster() {
        final ApiResponseActions responseActions = new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException {
                JSONObject dataJson = responseJson.getJSONObject(ApiRequest.ApiFields.KEY_DATA);
                addToMasterDataList(dataJson.getJSONArray("outlet_type"), MasterData.MasterDataType.OUTLET_TYPE);
                addToMasterDataList(dataJson.getJSONArray("outlet_classification"), MasterData.MasterDataType.OUTLET_CLASSIFICATION);
                addToMasterDataList(dataJson.getJSONArray("db_list"), MasterData.MasterDataType.DISTRIBUTION_HOUSE);
                checkAndNotifyComplete();
            }

            @Override
            public void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException {
                checkAndNotifyComplete();
            }

            @Override
            public void onApiCallError(int i, ErrorWrapper errorWrapper) {
                super.onApiCallError(i, errorWrapper);
                checkAndNotifyComplete();
            }
        };

        ApiRequest apiRequest = objectConstructor.getOutletMasterDataApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, responseActions);
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});

        apiRequest.setProgressDialog(null);
        apiRequest.setShouldShowAlertIfNoInternet(false);
        apiRequest.sendRequest();
    }

    private void syncPaymentMethods() {
        final ApiResponseActions responseActions = new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException {
                addToMasterDataList(responseJson.getJSONArray(ApiRequest.ApiFields.KEY_DATA), MasterData.MasterDataType.PAYMENT_METHOD);
                checkAndNotifyComplete();
            }

            @Override
            public void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException {
                checkAndNotifyComplete();
            }

            @Override
            public void onApiCallError(int i, ErrorWrapper errorWrapper) {
                super.onApiCallError(i, errorWrapper);
                checkAndNotifyComplete();
            }
        };

        ApiRequest apiRequest = objectConstructor.getPaymentMethodsApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, responseActions);
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});

        apiRequest.setProgressDialog(null);
        apiRequest.setShouldShowAlertIfNoInternet(false);
        apiRequest.sendRequest();
    }

    private void syncSkuStocks() {
        final ApiResponseActions responseActions = new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException {
                SkuRepositoryInterface skuRepo = objectConstructor.getSkuRepository();
                JSONArray skuStocksJsonArray = responseJson.getJSONArray(ApiRequest.ApiFields.KEY_DATA);
                SkuStockInterface[] skuStocksToSave = new SkuStockInterface[skuStocksJsonArray.length()];
                for (int i = 0; i < skuStocksJsonArray.length(); i++) {
                    SkuStockInterface skuStock = ((Convertible<SkuStockInterface>)skuRepo.getNewSkuStock())
                            .fromJson(skuStocksJsonArray.getJSONObject(i));
                    skuStocksToSave[i] = skuStock;
                }
                skuRepo.saveSkuStocks(skuStocksToSave);
                checkAndNotifyComplete();
            }

            @Override
            public void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException {
                checkAndNotifyComplete();
            }

            @Override
            public void onApiCallError(int i, ErrorWrapper errorWrapper) {
                super.onApiCallError(i, errorWrapper);
                checkAndNotifyComplete();
            }
        };

        ApiRequest apiRequest = objectConstructor.getSkuStockApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, responseActions);
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});

        apiRequest.setProgressDialog(null);
        apiRequest.setShouldShowAlertIfNoInternet(false);
//        apiRequest.sendRequest();
    }

    private void addToMasterDataList(JSONArray masterDataJsonArray, MasterData.MasterDataType masterDataType) throws JSONException{
        for (int i = 0; i < masterDataJsonArray.length(); i++) {
            MasterData masterData = masterDataRepo.getNewEntity().fromJson(masterDataJsonArray.getJSONObject(i));
            masterData.setType(masterDataType);
            masterDataList.add(masterData);
        }
    }

    @Override
    protected void checkAndNotifyComplete() {
        if(isOperationComplete()){
            masterDataRepo.deleteAll();
            masterDataRepo.saveMasterDataObjects(masterDataList.toArray(new MasterData[0]));
            super.checkAndNotifyComplete();
        }
    }
}
