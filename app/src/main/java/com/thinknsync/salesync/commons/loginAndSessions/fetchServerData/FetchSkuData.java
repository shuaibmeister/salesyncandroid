package com.thinknsync.salesync.commons.loginAndSessions.fetchServerData;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.convertible.Convertible;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.database.records.promotion.PromotionInterface;
import com.thinknsync.salesync.database.records.sku.SkuInterface;
import com.thinknsync.salesync.database.records.sku.SkuParentInterface;
import com.thinknsync.salesync.database.records.sku.UomInterface;
import com.thinknsync.salesync.database.repositories.promotion.PromotionRepositoryInterface;
import com.thinknsync.salesync.database.repositories.sku.SkuRepositoryInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FetchSkuData extends BaseFetchData {

    private SkuRepositoryInterface skuRepository;

    public FetchSkuData(ObjectConstructor objectConstructor, AndroidContextWrapper contextWrapper, ApiDataObject apiDataObject) {
        super(objectConstructor, contextWrapper, apiDataObject);
        this.skuRepository = objectConstructor.getSkuRepository();
    }

    @Override
    protected String getOnPreExecuteMessage() {
        return contextWrapper.getFrameworkObject().getString(R.string.message_sync_sku_data_start);
    }

    @Override
    protected String getOnPostExecuteMessage() {
        return contextWrapper.getFrameworkObject().getString(R.string.message_sync_sku_data_end);
    }

    @Override
    public void executeTask(Object inputObject) {
        onPreExecute();
        syncSkuData(apiDataObject);
    }

    private void syncSkuData(final ApiDataObject apiDataObject) {
        final ApiResponseActions responseActions = new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException {
                JSONArray skuParentsParentArray = responseJson.getJSONArray(ApiRequest.ApiFields.KEY_DATA);
                skuRepository.deleteAll();
                skuRepository.deleteAllSkuParent();
                skuRepository.deleteAllUom();
                saveSkus(skuParentsParentArray);
            }
        };

        ApiRequest apiRequest = objectConstructor.getSkuApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, responseActions);
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});

        apiRequest.setProgressDialog(null);
        apiRequest.setShouldShowAlertIfNoInternet(false);
        apiRequest.sendRequest();
    }

    private void syncPromotionData(final ApiDataObject apiDataObject) {
        final ApiResponseActions responseActions = new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException {
                PromotionRepositoryInterface promoRepo = objectConstructor.getPromotionRepository();
                promoRepo.deleteAll();
                savePromotions(promoRepo, responseJson.getJSONArray(ApiRequest.ApiFields.KEY_DATA));
                checkAndNotifyComplete();
            }

            @Override
            public void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException {
                checkAndNotifyComplete();
            }
        };

        ApiRequest apiRequest = objectConstructor.getPromotionDataApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, responseActions);
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});

        apiRequest.setProgressDialog(null);
        apiRequest.setShouldShowAlertIfNoInternet(false);
        apiRequest.sendRequest();
    }

    private void saveSkus(JSONArray skuParentsParentArray) throws JSONException {
        SkuParentInterface[] skuParentsToSave = new SkuParentInterface[skuParentsParentArray.length()];
        for (int i = 0; i < skuParentsParentArray.length(); i++) {
            SkuParentInterface skuParentsParent = ((Convertible<SkuParentInterface>)skuRepository.getNewSkuParent()).fromJson(skuParentsParentArray.getJSONObject(i));
            JSONArray skuparentsJsonArray = skuParentsParentArray.getJSONObject(i).getJSONArray("parents");
            List<SkuParentInterface> skuParents = getParentsOfSkus(skuparentsJsonArray);
            skuParentsParent.setChildren(skuParents);
            skuParentsToSave[i] = skuParentsParent;
        }
        skuRepository.saveSkuParents(skuParentsToSave);
        syncPromotionData(apiDataObject);
    }

    private List<SkuParentInterface> getParentsOfSkus(JSONArray skuParentsJsonArray) throws JSONException{
        List<SkuParentInterface> skuParents = new ArrayList<>();
        for (int i = 0; i < skuParentsJsonArray.length(); i++) {
            SkuParentInterface skuParent = ((Convertible<SkuParentInterface>)skuRepository.getNewSkuParent()).fromJson(skuParentsJsonArray.getJSONObject(i));
            JSONArray skusJsonArray = skuParentsJsonArray.getJSONObject(i).getJSONArray("skus");
            List<SkuInterface> skus = getSkusOfParent(skusJsonArray);
            skuParent.setChildSkus(skus);
            skuParents.add(skuParent);
        }
        return skuParents;
    }

    private List<SkuInterface> getSkusOfParent(JSONArray skusJsonArray) throws JSONException{
        List<SkuInterface> skus = new ArrayList<>();
        for (int i = 0; i < skusJsonArray.length(); i++) {
            SkuInterface sku = skuRepository.getNewEntity().fromJson(skusJsonArray.getJSONObject(i));
            JSONArray uomArray = skusJsonArray.getJSONObject(i).getJSONArray("uom");
            List<UomInterface> uoms = getUomsOfSku(uomArray);
            sku.setSkuUoms(uoms);
            skus.add(sku);
        }
        return skus;
    }

    private List<UomInterface> getUomsOfSku(JSONArray uomArray) throws JSONException{
        List<UomInterface> uoms = new ArrayList<>();
        for (int i = 0; i < uomArray.length(); i++) {
            UomInterface uom = ((Convertible<UomInterface>)skuRepository.getNewUom()).fromJson(uomArray.getJSONObject(i));
            uoms.add(uom);
        }
        return uoms;
    }

    private void savePromotions(PromotionRepositoryInterface promoRepo, JSONArray promotionsJsonArray) throws JSONException{
        HashMap<Integer, SkuInterface> skuDataIdMap = getPromotionRelatedSkuMap(promotionsJsonArray);
        PromotionInterface[] promotionToSave = new PromotionInterface[promotionsJsonArray.length()];
        List<SkuInterface> skusToUpdate = new ArrayList<>();

        for (int i = 0; i < promotionsJsonArray.length(); i++) {
            JSONObject conditionObject = promotionsJsonArray.getJSONObject(i).getJSONObject(PromotionInterface.key_promo_condition);
            SkuInterface sellingSku = skuDataIdMap.get(conditionObject.getInt(PromotionInterface.key_promo_selling_sku_id));

            PromotionInterface promotion = promoRepo.getNewEntity().fromJson(promotionsJsonArray.getJSONObject(i));
            promotion.setSellingSku(sellingSku);
            sellingSku.setSkuPromotion(promotion);
            if(promotion.getPromotionType() == PromotionInterface.PromotionType.SKU) {
                JSONObject offerObject = promotionsJsonArray.getJSONObject(i).getJSONObject(PromotionInterface.key_promo_offer);
                promotion.setFreeSku(skuDataIdMap.get(offerObject.getInt(PromotionInterface.key_free_sku_id)));
            }

            promotionToSave[i] = promotion;
            skusToUpdate.add(sellingSku);
        }
        promoRepo.savePromotions(promotionToSave);
        skuRepository.updateSkus(skusToUpdate.toArray(new SkuInterface[0]));
    }

    private HashMap<Integer, SkuInterface> getPromotionRelatedSkuMap(JSONArray promotionsJsonArray) throws JSONException {
        List<SkuInterface> promoSkus = getPromotionRelatedSkuList(promotionsJsonArray);
        HashMap<Integer, SkuInterface> skuDataIdMap = new HashMap<>();
        for (SkuInterface sku : promoSkus) {
            skuDataIdMap.put(sku.getDataId(), sku);
        }
        return skuDataIdMap;
    }

    private List<SkuInterface> getPromotionRelatedSkuList(JSONArray promotionsJsonArray) throws JSONException {
        List<Integer> promotionSkuIds = new ArrayList<>();
        for (int i = 0; i < promotionsJsonArray.length(); i++) {
            JSONObject conditionObject = promotionsJsonArray.getJSONObject(i).getJSONObject(PromotionInterface.key_promo_condition);
            PromotionInterface.PromotionType promotionType = PromotionInterface.PromotionType
                    .getPromotionTypeById(promotionsJsonArray.getJSONObject(i).getInt(PromotionInterface.key_promo_type_id));

            promotionSkuIds.add(conditionObject.getInt(PromotionInterface.key_promo_selling_sku_id));
            if(promotionType == PromotionInterface.PromotionType.SKU) {
                JSONObject offerObject = promotionsJsonArray.getJSONObject(i).getJSONObject(PromotionInterface.key_promo_offer);
                promotionSkuIds.add(offerObject.getInt(PromotionInterface.key_free_sku_id));
            }
        }
        return skuRepository.getSkusByDataIds(promotionSkuIds);
    }
}
