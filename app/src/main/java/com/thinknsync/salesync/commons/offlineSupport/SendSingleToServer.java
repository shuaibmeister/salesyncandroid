package com.thinknsync.salesync.commons.offlineSupport;

public interface SendSingleToServer<T> extends OfflineSupportContract.SendToServer, OfflineSupportContract.Syncable,
        OfflineSupportContract.SetSingleData<T> {
}
