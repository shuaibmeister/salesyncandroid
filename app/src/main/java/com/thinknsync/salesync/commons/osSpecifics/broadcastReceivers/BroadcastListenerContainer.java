package com.thinknsync.salesync.commons.osSpecifics.broadcastReceivers;

import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.salesync.commons.osSpecifics.broadcastReceivers.BroadcastListener;

public interface BroadcastListenerContainer {
    void registerReceiver(BroadcastListener.BroadcastListenerType listenerType, BroadcastListener broadcastListener);
    void deregisterReceiver(BroadcastListener broadcastListener);
    BroadcastListener getBroadcastListener(BroadcastListener.BroadcastListenerType listenerType) throws NullPointerException;
    void addObserverToListener(BroadcastListener.BroadcastListenerType listenerType, TypedObserver observer);
    void removeObserverFromListener(BroadcastListener.BroadcastListenerType listenerType, TypedObserver observer);
}
