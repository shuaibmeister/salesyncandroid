package com.thinknsync.salesync.commons.loginAndSessions;

import com.thinknsync.apilib.ApiContextInitializer;
import com.thinknsync.apilib.AuthTokenContainer;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.salesync.commons.configActions.ConfigWiseActions;
import com.thinknsync.salesync.commons.loginAndSessions.fetchServerData.FetchAllDataFromServer;
import com.thinknsync.salesync.commons.utils.DateTimeUtils;
import com.thinknsync.salesync.database.records.config.ConfigInterface;
import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.messageBroker.BrokerTopics;
import com.thinknsync.salesync.messageBroker.PushPullManagerContainer;
import com.thinknsync.salesync.commons.configActions.ConfigAction;

import java.util.HashMap;

public class LoginActions extends ObserverHostImpl<Object> implements LoginLogoutActions.Initialization {

    private ObjectConstructor objectConstructor;
    private AndroidContextWrapper contextWrapper;

    public LoginActions(ObjectConstructor objectConstructor, AndroidContextWrapper contextWrapper) {
        this.objectConstructor = objectConstructor;
        this.contextWrapper = contextWrapper;
    }

    @Override
    public void onSuccessfulLogin(UserInterface user) {
        UserInterface loggedInUser = objectConstructor.getUserRepository().getLoggedInUser();
        initMessageBrokerTopics(loggedInUser);
        initConfigActions();
        setAuthToken();
        getDataFromServer();
    }

    @Override
    public boolean shouldFetchDataFromServer() {
        ConfigInterface lastSyncDateTimeString = objectConstructor.getConfigRepository().getConfigOfType(ConfigInterface.ConfigType.LAST_SYNC_DATE);
        if(lastSyncDateTimeString != null){
            String[] lastSyncDateTimeArray = lastSyncDateTimeString.getValue().split(" ");
            return !lastSyncDateTimeArray[0].equals(new DateTimeUtils().getDefaultDateStringToday());
        }
        return true;
    }

    private void initMessageBrokerTopics(final UserInterface user) {
        PushPullManagerContainer pushPullManagerContainer = objectConstructor.getMasterPushPullContainer();
        BrokerTopics brokerTopics = objectConstructor.getBrokerTopics(new HashMap<String, Object>(){{
            put(UserInterface.tag, user);
        }});
        pushPullManagerContainer.getPushPullManager().addToInitialSubscribeTopics(brokerTopics.getInitialSubscribeList());
    }

    private void initConfigActions() {
        ConfigWiseActions configAction = new ConfigAction(contextWrapper, objectConstructor);
        configAction.doAction();
    }

    private void setAuthToken() {
        UserInterface user = objectConstructor.getUserRepository().getLoggedInUser();
        if(user != null && user.getLoginToken() != null) {
            ((AuthTokenContainer) ApiContextInitializer.getInstance()).setAuthToken(user.getLoginToken());
        }
    }

    private void getDataFromServer() {
        if(shouldFetchDataFromServer()) {
            Runnable asyncGetAllServerData = new FetchAllDataFromServer(objectConstructor, contextWrapper);
            ((ObserverHost<Object>)asyncGetAllServerData).addToObservers(new TypedObserverImpl<Object>() {
                @Override
                public void update(Object o) {
                    notifyObservers(o);
                }
            });
            asyncGetAllServerData.run();
        } else {
            notifyObservers(null);
        }
    }
}
