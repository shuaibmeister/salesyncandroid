package com.thinknsync.salesync.commons.offlineSupport;

public interface SendMultipleToServer<T> extends OfflineSupportContract.SendToServer,
        OfflineSupportContract.Syncable, OfflineSupportContract.SetMultipleData<T> {
}
