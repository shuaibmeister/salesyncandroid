package com.thinknsync.salesync.commons.offlineSupport;

import android.content.Context;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SendExceptionToServer extends SendOrderToServer {

    public SendExceptionToServer(ObjectConstructor objectConstructor) {
        super(objectConstructor);
    }

    @Override
    protected JSONObject getObjectJson() throws JSONException {
        return dataObject.withoutProperty(new String[]{
                SalesOrderInterface.key_id, SalesOrderInterface.key_route, SalesOrderInterface.key_outlet,
                SalesOrderInterface.key_sr, SalesOrderInterface.key_exception, SalesOrderInterface.key_order_lines,
                SalesOrderInterface.key_is_synced, SalesOrderInterface.key_payable_amount
        });
    }

    @Override
    protected ApiRequest getSendToServerApiRequest(final FrameworkWrapper<Context> contextWrapper, final ApiResponseActions responseActions, final ApiDataObject apiDataObject) {
        return objectConstructor.getExceptionSendApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, getWrappedResponseActions(responseActions));
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});
    }
}
