package com.thinknsync.salesync.commons.utils.dateTime;

import com.thinknsync.observerhost.TypedObserver;

import java.util.Calendar;

/**
 * Created by shuaib on 7/5/17.
 */

public interface DateTimePicker {
    String DateSeparator = "/";

    void setDateSeparator(String separator);
    void allowPreviousDates(boolean allow);
    void allowFutureDates(boolean allow);
    void showDatePicker(TypedObserver<String> pickerSelectedCallback);
    void showTimePicker(TypedObserver<String> pickerSelectedCallback);
    String getDateString();
    String getTimeString();
    Calendar getCalendar();
}
