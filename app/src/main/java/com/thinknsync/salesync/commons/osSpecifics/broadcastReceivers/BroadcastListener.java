package com.thinknsync.salesync.commons.osSpecifics.broadcastReceivers;

import com.thinknsync.observerhost.ObserverHost;

public interface BroadcastListener<T> extends ObserverHost<T> {
    enum BroadcastListenerType {
        INTERNET_BROADCAST(1, new String[]{"android.net.conn.CONNECTIVITY_CHANGE"}),
        SCREEN_ON_OFF(2, new String[]{"android.intent.action.SCREEN_ON", "android.intent.action.SCREEN_OFF"}),
        ;

        private int receiverId;
        private String[] filterStrings;

        BroadcastListenerType(int id, String[] filterStrings){
            this.receiverId = id;
            this.filterStrings = filterStrings;
        }

        public String[] getFilterStrings() {
            return filterStrings;
        }

        public int getReceiverId() {
            return receiverId;
        }
    }
}
