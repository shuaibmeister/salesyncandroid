package com.thinknsync.salesync.commons.utils.timer;

import com.thinknsync.observerhost.ObserverHost;

import java.util.concurrent.TimeUnit;

public interface RunTimer extends ObserverHost<Long> {
    TimeUnit DEFAULT_TIME_UNIT = TimeUnit.SECONDS;
    String TIMER_UNIT_OUT_OF_BOUND_MESSAGE = "time unit cannot be greater than day or less than millisecond";

    void setTimeUnit(TimeUnit timeUnit);
    void setTimerLimit(long limit);
    void setTimerInterval(long interval);
    void setTimerStartDelay(long delay);
    void shouldStartInverse(boolean startInverse);
    void setAction(Runnable action);
    void startTimer();
    void stopTimer();
    boolean isTimerRunning();
    void restartTimer();
    long getElapsedTime();
    long getElapsedTime(TimeUnit timeUnit);
    long getRemainingTime();
    long getRemainingTime(TimeUnit timeUnit);
    void noteTimeNow();
    Long[] getNotedTimes();
    Long[] getNotedTimes(TimeUnit timeUnit);
}
