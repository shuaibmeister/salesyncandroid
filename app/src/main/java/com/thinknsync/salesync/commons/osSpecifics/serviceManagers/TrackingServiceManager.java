package com.thinknsync.salesync.commons.osSpecifics.serviceManagers;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;

import com.google.gson.Gson;
import com.thinknsync.androidnotification.NotificationObject;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.positionmanager.GpsPositionManagerService;
import com.thinknsync.positionmanager.TrackingData;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.location.EmployeeTracking;
import com.thinknsync.salesync.commons.osSpecifics.frameworkHelper.ApplicationInfo;

import java.util.HashMap;

public class TrackingServiceManager extends BaseServiceManager<EmployeeTracking, TrackingData> {

    private long minTimeMinute;
    private static TrackingServiceManager trackingServiceManager;

    private TrackingServiceManager(AndroidContextWrapper contextWrapper, ObjectConstructor objectConstructor, int minTimeMinute) {
        super(contextWrapper, objectConstructor);
        this.minTimeMinute = minTimeMinute;
    }

    public static TrackingServiceManager getInstance(AndroidContextWrapper contextWrapper, ObjectConstructor objectConstructor, int minTimeMinute){
        if(trackingServiceManager == null){
            trackingServiceManager = new TrackingServiceManager(contextWrapper, objectConstructor, minTimeMinute);
        }
        return trackingServiceManager;
    }

    @SuppressLint("NewApi")
    public void startService(Class<EmployeeTracking> service) {
        serviceClass = service;
        if(!isServiceRunning()) {
            serviceIntent = getServiceIntent(service);

            ApplicationInfo applicationInfo = objectConstructor.getAndroidFrameworkHelper(new HashMap<String, Object>(){{
                put(AndroidContextWrapper.tag, contextWrapper);
            }});
            if (applicationInfo.isGreaterThanOrEquals(Build.VERSION_CODES.O)) {
                contextWrapper.getFrameworkObject().startForegroundService(serviceIntent);
            } else {
                contextWrapper.getFrameworkObject().startService(serviceIntent);
            }
        }
    }

    private Intent getServiceIntent(Class<EmployeeTracking> service) {
        NotificationObject notificationObject = getServiceNotificationObject();
        Intent serviceIntent = new Intent(contextWrapper.getFrameworkObject(), service);
        serviceIntent.putExtra(NotificationObject.tag, notificationObject.toString());
        serviceIntent.putExtra(GpsPositionManagerService.key_min_time, String.valueOf(minTimeMinute));
        serviceIntent.putExtra(ObjectConstructor.tag, new Gson().toJson(objectConstructor));
        return serviceIntent;
    }

    private NotificationObject getServiceNotificationObject() {
        NotificationObject notificationObject = new NotificationObject();
        notificationObject.setTitle(contextWrapper.getFrameworkObject().getString(R.string.app_name));
        notificationObject.setContent("Sales synchronization");
        notificationObject.setImage(R.mipmap.ic_launcher);
        return notificationObject;
    }
}
