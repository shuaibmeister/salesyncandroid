package com.thinknsync.salesync.commons.osSpecifics.broadcastReceivers;

import android.content.Context;
import android.content.Intent;

public class ScreenOnOffBroadcastReceiver extends BaseBroadcastListener<Boolean> {
    @Override
    public void onReceive(final Context context, Intent intent) {
        if(intent.getAction() != null) {
            if (intent.getAction().equals(BroadcastListenerType.SCREEN_ON_OFF.getFilterStrings()[1])) {
                notifyObservers(false);
            } else if (intent.getAction().equals(BroadcastListenerType.SCREEN_ON_OFF.getFilterStrings()[0])) {
                notifyObservers(true);
            }
        }
    }
}
