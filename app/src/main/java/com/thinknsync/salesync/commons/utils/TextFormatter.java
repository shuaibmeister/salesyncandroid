package com.thinknsync.salesync.commons.utils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by shuaib on 6/13/17.
 */

public final class TextFormatter {
    public String get2DigitDecimal(double digit) {
        DecimalFormat df2 = new DecimalFormat(".##");
        return df2.format(digit);
    }

    public DecimalFormat getNumberFormatter1DecimalDigit() {
        return new DecimalFormat("##.#");
    }
}