package com.thinknsync.salesync.commons.osSpecifics.frameworkHelper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.thinknsync.objectwrappers.AndroidContextWrapper;

public class AndroidHelper implements ApplicationInfo {

    private Context context;

    public AndroidHelper(AndroidContextWrapper contextWrapper) {
        this.context = contextWrapper.getFrameworkObject();
    }

    @Override
    public boolean isDebuggable() {
        return (0 != ((context).getApplicationInfo().flags & android.content.pm.ApplicationInfo.FLAG_DEBUGGABLE));
    }

    @SuppressLint("HardwareIds")
    @Override
    public String getDeviceId() throws SecurityException{
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (isGreaterThanOrEquals(Build.VERSION_CODES.Q)) {
            return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        } else {
            return telephonyManager.getDeviceId();
        }
    }

    @Override
    public String getApplicationVersionName(){
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getVersionInt() {
        return Build.VERSION.SDK_INT;
    }

    @Override
    public boolean isGreaterThan(int versionInt) {
        return getVersionInt() > versionInt;
    }

    @Override
    public boolean isGreaterThanOrEquals(int versionInt) {
        return getVersionInt() >= versionInt;
    }

    @Override
    public String getStringValueFromEnvironment(String key) {
        String value = "";
        try {
            android.content.pm.ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            value = (String) ai.metaData.get(key);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return value;
    }

    @Override
    public void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    @Override
    public float convertPixelsToDp(float px){
        return px / ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    @Override
    public float convertDpToPixel(float dp){
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }
}
