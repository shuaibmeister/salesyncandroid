package com.thinknsync.salesync.commons.loginAndSessions;

import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.salesync.database.records.user.UserInterface;

public interface LoginLogoutActions{
    interface Initialization extends ObserverHost<Object> {
        void onSuccessfulLogin(UserInterface user);
        boolean shouldFetchDataFromServer();
    }

    interface Termination{
        void onLogout();
    }
}
