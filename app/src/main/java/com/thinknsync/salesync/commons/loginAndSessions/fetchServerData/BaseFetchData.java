package com.thinknsync.salesync.commons.loginAndSessions.fetchServerData;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.logger.Logger;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.salesync.commons.asyncOperation.AsyncExecute;
import com.thinknsync.salesync.factory.ObjectConstructor;

abstract class BaseFetchData extends ObserverHostImpl<Void> implements AsyncExecute<Object, Void> {

    protected ApiDataObject apiDataObject;

    protected ObjectConstructor objectConstructor;
    protected AndroidContextWrapper contextWrapper;
    protected Logger logger;

    protected int responseReceivedCount = 0;
    protected int totalRequestCount = 0;
    protected final String TAG = "sync log";

    protected AsyncExecute.View viewReference;

    public BaseFetchData(ObjectConstructor objectConstructor, AndroidContextWrapper contextWrapper,
                         ApiDataObject apiDataObject) {
        this.objectConstructor = objectConstructor;
        this.contextWrapper = contextWrapper;
        this.logger = objectConstructor.getLogger();
        this.apiDataObject = apiDataObject;
    }

    //    @Override
    protected void onPreExecute() {
        viewReference.onAsyncTaskStart(getOnPreExecuteMessage());
    }

    protected abstract String getOnPreExecuteMessage();
    protected abstract String getOnPostExecuteMessage();

    @Override
    public void setViewReference(View viewReference) {
        this.viewReference = viewReference;
    }

    protected void checkAndNotifyComplete() {
        if(isOperationComplete()){
            logger.debugLog(TAG, getOnPostExecuteMessage());
            viewReference.onAsyncTaskComplete(getOnPostExecuteMessage());
        }
    }

    protected boolean isOperationComplete(){
        if(totalRequestCount > 0) {
            responseReceivedCount++;
            return responseReceivedCount >= totalRequestCount;
        }
        return true;
    }
}
