package com.thinknsync.salesync.commons.commonInterfaces;

public interface HasCode {
    String key_code = "code";

    String getCode();
    void setCode(String code);
}
