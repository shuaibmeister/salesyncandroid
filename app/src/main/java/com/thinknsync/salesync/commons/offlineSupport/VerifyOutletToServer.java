package com.thinknsync.salesync.commons.offlineSupport;

import android.content.Context;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class VerifyOutletToServer extends SendOutletToServer {
    public VerifyOutletToServer(ObjectConstructor objectConstructor) {
        super(objectConstructor);
    }

    protected JSONObject getObjectJson() throws JSONException{
        return dataObject.withoutProperty(new String[]{OutletInterface.key_route, OutletInterface.key_route_id,
                OutletInterface.key_is_synced});
    }

    protected ApiRequest getSendToServerApiRequest(final FrameworkWrapper<Context> contextWrapper, final ApiResponseActions responseActions,
                                                 final ApiDataObject apiDataObject){
        return objectConstructor.getOutletUpdateApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, getWrappedResponseActions(responseActions));
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});
    }
}
