package com.thinknsync.salesync.commons.osSpecifics.deviceIdFetcher;

public interface DeviceIdentifierFetchManager {
    void setupDeviceIdentifier();
    String readIdentifierFromDevice();
    boolean hasDeviceIdentifierPermission();
    String getDeviceIdentifierText();
}
