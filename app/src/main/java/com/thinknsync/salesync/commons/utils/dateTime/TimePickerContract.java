package com.thinknsync.salesync.commons.utils.dateTime;

import com.thinknsync.observerhost.TypedObserver;

import java.util.Observer;

/**
 * Created by shuaib on 7/17/17.
 */

public interface TimePickerContract {

    interface View {
        void updateTime(String timeString);
    }

    interface Presenter {
        void showTimePicker(TypedObserver<String> observer);
    }
}
