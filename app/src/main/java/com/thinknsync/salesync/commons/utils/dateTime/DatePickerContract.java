package com.thinknsync.salesync.commons.utils.dateTime;

import java.util.Observer;

/**
 * Created by shuaib on 7/17/17.
 */

public interface DatePickerContract {

    interface View {
        void updateDate(String date);
    }

    interface Listener {
        void getDateFromPicker();
    }

    interface Presenter {
        void showDatePicker(Observer observer);
    }
}
