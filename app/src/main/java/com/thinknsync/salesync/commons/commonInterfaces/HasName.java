package com.thinknsync.salesync.commons.commonInterfaces;

public interface HasName {
    String getName();
    void setName(String name);
}
