package com.thinknsync.salesync.commons.osSpecifics.deviceIdFetcher;

import android.content.Context;

import com.thinknsync.androidpermissions.PermissionManager;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.commons.osSpecifics.frameworkHelper.ApplicationInfo;

import java.util.HashMap;

public class DeviceIdentifierFetchImpl implements DeviceIdentifierFetchManager {

    private String deviceId;

    private ObjectConstructor objectConstructor;
    private FrameworkWrapper<Context> contextWrapper;

    public DeviceIdentifierFetchImpl(ObjectConstructor objectConstructor, FrameworkWrapper<Context> contextWrapper) {
        this.objectConstructor = objectConstructor;
        this.contextWrapper = contextWrapper;
    }

    @Override
    public void setupDeviceIdentifier() {
        if(hasDeviceIdentifierPermission()){
            deviceId = readIdentifierFromDevice();
        }
    }

    @Override
    public boolean hasDeviceIdentifierPermission() {
        PermissionManager permissionManager = objectConstructor.getPermissionHandler(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
        }});
        return permissionManager.isImeiPermissionGranted();
    }

    @Override
    public String readIdentifierFromDevice() {
        ApplicationInfo appInfo = objectConstructor.getAndroidFrameworkHelper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
        }});
        return appInfo.getDeviceId();
    }

    @Override
    public String getDeviceIdentifierText(){
        return deviceId;
    }
}
