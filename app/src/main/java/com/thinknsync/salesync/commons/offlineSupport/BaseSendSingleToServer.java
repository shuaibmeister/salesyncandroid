package com.thinknsync.salesync.commons.offlineSupport;

import android.content.Context;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiDataObjectImpl;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.apilib.ErrorWrapper;
import com.thinknsync.convertible.Convertible;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.salesync.factory.ObjectConstructor;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class BaseSendSingleToServer<T extends Convertible<T>> implements SendSingleToServer<T> {

    protected ObjectConstructor objectConstructor;
    protected T dataObject;

    public BaseSendSingleToServer(ObjectConstructor objectConstructor){
        this.objectConstructor = objectConstructor;
    }

    @Override
    public void setData(T data) {
        this.dataObject = data;
    }

    @Override
    public void sendData(FrameworkWrapper contextWrapper, ApiResponseActions responseActions) {
        try {
            final ApiDataObject apiDataObject = new ApiDataObjectImpl(getObjectJson());
            ApiRequest apiRequest = getSendToServerApiRequest(contextWrapper, responseActions, apiDataObject);
            apiRequest.sendRequest();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected JSONObject getObjectJson() throws JSONException{
        return dataObject.toJson();
    }

    protected abstract ApiRequest getSendToServerApiRequest(final FrameworkWrapper<Context> contextWrapper, final ApiResponseActions responseActions,
                                                final ApiDataObject apiDataObject);


    protected ApiResponseActions getWrappedResponseActions(final ApiResponseActions responseActions){
        return new ApiResponseActions() {
            @Override
            public void onApiCallSuccess(int i, JSONObject jsonObject) throws JSONException {
                onPostSynced(true);
                responseActions.onApiCallSuccess(i, jsonObject);
            }

            @Override
            public void onApiCallFailure(int i, JSONObject jsonObject) throws JSONException {
                responseActions.onApiCallFailure(i, jsonObject);
            }

            @Override
            public void onApiCallError(int i, ErrorWrapper errorWrapper) {
                responseActions.onApiCallError(i, errorWrapper);
            }
        };
    }
}
