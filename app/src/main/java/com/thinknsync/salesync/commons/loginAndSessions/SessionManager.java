package com.thinknsync.salesync.commons.loginAndSessions;

import com.thinknsync.observerhost.TypedObserver;

public interface SessionManager extends TypedObserver<Boolean> {
    double getSessionTimeoutInMinutes();
    void setSessionTimeoutInMinutes(double sessionTimeoutInSeconds);
    boolean isSessionTimedOut();
}
