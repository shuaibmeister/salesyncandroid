package com.thinknsync.salesync.commons.loginAndSessions;

import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.salesync.commons.utils.DateTimeUtils;
import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

public class SessionTimeoutMonitor extends TypedObserverImpl<Boolean> implements SessionManager {

    private ObjectConstructor objectConstructor;
    private Runnable runnableOnSessionTimeout;
    private double sessionTimeoutInSec;

    public SessionTimeoutMonitor(ObjectConstructor objectConstructor, Runnable runnableOnUpdate) {
        this.objectConstructor = objectConstructor;
        runnableOnSessionTimeout = runnableOnUpdate;
    }

    @Override
    public double getSessionTimeoutInMinutes() {
        return sessionTimeoutInSec / 60;
    }

    @Override
    public void setSessionTimeoutInMinutes(double sessionTimeoutInMinutes) {
        sessionTimeoutInSec = sessionTimeoutInMinutes * 60;
    }

    @Override
    public boolean isSessionTimedOut() {
        UserInterface loggedInUser = objectConstructor.getUserRepository().getLoggedInUser();
        return loggedInUser.getLastLoginTime() != null && (new DateTimeUtils().getDefaultDateTimeNow().getTime() >
                loggedInUser.getLastLoginTime().getTime() + sessionTimeoutInSec * 1000);
    }

    @Override
    public void update(Boolean isScreenTurnedOn) {
        if(isSessionTimedOut() && isScreenTurnedOn) {
            runnableOnSessionTimeout.run();
        }
    }
}
