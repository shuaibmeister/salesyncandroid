package com.thinknsync.salesync.commons.asyncOperation;

import android.os.AsyncTask;

import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.observerhost.TypedObserver;

import java.util.List;

public abstract class BaseObservableAsyncExecute<InputParams, ProgressMessage, ReturnType>
        extends AsyncTask<InputParams, ProgressMessage, ReturnType> implements AsyncExecute<InputParams, ReturnType>{

    private ObserverHost<ReturnType> observerHost;
    protected View viewReference;

    public BaseObservableAsyncExecute(){
        observerHost = new ObserverHostImpl<>();
    }

    @Override
    public void setViewReference(View viewReference) {
        this.viewReference = viewReference;
    }

    @Override
    public void addToObservers(TypedObserver<ReturnType> typedObserver) {
        observerHost.addToObservers(typedObserver);
    }

    @Override
    public void addToObservers(List<TypedObserver<ReturnType>> list) {
        observerHost.addToObservers(list);
    }

    @Override
    public void removeObserver(TypedObserver<ReturnType> typedObserver) {
        observerHost.removeObserver(typedObserver);
    }

    @Override
    public void clearObservers() {
        observerHost.clearObservers();
    }

    @Override
    public void notifyObservers(ReturnType object){
        observerHost.notifyObservers(object);
    }

    @Override
    public void notifyError(Exception e) {
        observerHost.notifyError(e);
    }

    @Override
    public void executeTask(InputParams inputObject) {
        if(inputObject == null) {
            execute();
        } else {
            execute(inputObject);
        }
    }
}
