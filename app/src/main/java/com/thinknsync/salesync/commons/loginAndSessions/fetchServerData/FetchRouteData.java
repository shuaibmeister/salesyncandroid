package com.thinknsync.salesync.commons.loginAndSessions.fetchServerData;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.convertible.Convertible;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.database.records.outlet.OutletInterface;
import com.thinknsync.salesync.database.records.route.RouteInterface;
import com.thinknsync.salesync.database.repositories.outlet.OutletRepositoryInterface;
import com.thinknsync.salesync.database.repositories.route.RouteRepositoryInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FetchRouteData extends BaseFetchData {
    public FetchRouteData(ObjectConstructor objectConstructor, AndroidContextWrapper contextWrapper, ApiDataObject apiDataObject) {
        super(objectConstructor, contextWrapper, apiDataObject);
    }

    @Override
    protected String getOnPreExecuteMessage() {
        return contextWrapper.getFrameworkObject().getString(R.string.message_sync_route_data_start);
    }

    @Override
    protected String getOnPostExecuteMessage() {
        return contextWrapper.getFrameworkObject().getString(R.string.message_sync_route_data_end);
    }

    @Override
    public void executeTask(Object inputObject) {
        onPreExecute();
        syncRoutes(apiDataObject);
    }

    private void syncRoutes(final ApiDataObject apiDataObject) {
        ApiRequest apiRequest = objectConstructor.getTodaysRouteApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, getRouteSyncResponseAction(true));
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});


        apiRequest.setProgressDialog(null);
        apiRequest.sendRequest();
    }

    private ApiResponseActions getRouteSyncResponseAction(final boolean todaysRouteSync){
        return new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException {
                JSONArray routes = responseJson.getJSONArray(ApiRequest.ApiFields.KEY_DATA);
                saveRouteData(routes);
            }

            @Override
            public void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException {
//                view.showShortMessage(responseJson.getString(ApiRequest.ApiFields.KEY_MESSAGE));
                checkAndNotifyComplete();
            }
        };
    }

    private void saveRouteData(JSONArray routes) throws JSONException {
        RouteRepositoryInterface routeRepository = objectConstructor.getRouteRepository();
        OutletRepositoryInterface outletRepository = objectConstructor.getOutletRepository();
        RouteInterface[] allRoutes = new RouteInterface[routes.length()];
        for (int i = 0; i < routes.length(); i++) {
            RouteInterface route = routeRepository.getNewEntity().fromJson(routes.getJSONObject(i));
            JSONArray outletsJsonArray = routes.getJSONObject(i).getJSONArray("outlets");
            List<OutletInterface> outletsOfRoute = getOutletsFromJsonArray(outletsJsonArray, outletRepository);
            route.setOutletsOfRoute(outletsOfRoute);
            allRoutes[i] = route;
        }

        routeRepository.deleteAll();
        outletRepository.deleteAll();
        routeRepository.saveRoutes(allRoutes);

        checkAndNotifyComplete();
    }

    private List<OutletInterface> getOutletsFromJsonArray(JSONArray outletsJsonArray, OutletRepositoryInterface outletRepository) throws JSONException {
        List<OutletInterface> outlets = new ArrayList<>();
        for (int j = 0; j < outletsJsonArray.length(); j++) {
            OutletInterface outlet = outletRepository.getNewEntity().fromJson(outletsJsonArray.getJSONObject(j));
            outlets.add(outlet);
        }
        return outlets;
    }
}
