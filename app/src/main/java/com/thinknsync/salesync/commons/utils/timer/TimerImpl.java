package com.thinknsync.salesync.commons.utils.timer;

import com.thinknsync.observerhost.ObserverHostImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class TimerImpl extends ObserverHostImpl<Long> implements RunTimer {

    private TimeUnit timeUnit = DEFAULT_TIME_UNIT;
    private long timeLimit;
    private long executionInterval;
    private long timerStartDelay = 0;

    private Timer timer = new Timer();
    private boolean isTimerRunning = false;
    private boolean startInverse = false;
    private long elapsedTime = 0;
    private List<Long> notedTimeList = new ArrayList<>();

    private Runnable actionRunnable;

    public TimerImpl(long timeLimitSec, long intervalSec) {
        setTimerLimit(timeLimitSec);
        setTimerInterval(intervalSec);
    }

    public TimerImpl(long timeLimit, long interval, TimeUnit timeUnit) {
        setTimerLimit(timeLimit);
        setTimerInterval(interval);
        setTimeUnit(timeUnit);
    }

    @Override
    public void setTimeUnit(TimeUnit timeUnit) {
        if(timeUnit != TimeUnit.MINUTES && timeUnit != TimeUnit.DAYS && timeUnit != TimeUnit.MILLISECONDS
                && timeUnit != TimeUnit.HOURS && timeUnit != TimeUnit.SECONDS) {
            throw new IllegalArgumentException(TIMER_UNIT_OUT_OF_BOUND_MESSAGE);
        }
        this.timeUnit = timeUnit;
    }

    @Override
    public void setTimerLimit(long limit) {
        this.timeLimit = convertToMilliseconds(limit);
    }

    @Override
    public void setTimerInterval(long interval) {
        this.executionInterval = convertToMilliseconds(interval);
    }

    @Override
    public void setTimerStartDelay(long delay) {
        timerStartDelay = convertToMilliseconds(delay);
    }

    @Override
    public void shouldStartInverse(boolean startInverse) {
        this.startInverse = startInverse;
    }

    @Override
    public void setAction(Runnable action) {
        this.actionRunnable = action;
    }

    @Override
    public void startTimer() {
        notifyFirstPointIfDelayInStart();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                elapsedTime += executionInterval;
                notifyObservers(getElapsedTime());
                if(actionRunnable != null){
                    actionRunnable.run();
                }
                if(elapsedTime == timeLimit){
                    stopTimer();
                }
            }
        }, timerStartDelay, executionInterval);
        isTimerRunning = true;
    }

    private void notifyFirstPointIfDelayInStart() {
        if(timerStartDelay > 0) {
            long startTime = 0;
            if(startInverse) {
                startTime = timeLimit;
            }
            notifyObservers(convertToTimeUnit(timeUnit, startTime));
        }
    }

    @Override
    public void stopTimer() {
        timer.cancel();
        timer = new Timer();
        isTimerRunning = false;
    }

    @Override
    public boolean isTimerRunning() {
        return isTimerRunning;
    }

    @Override
    public void restartTimer() {
        stopTimer();
        startTimer();
    }

    @Override
    public long getElapsedTime() {
        return convertToTimeUnit(timeUnit, elapsedTime);
    }

    @Override
    public long getElapsedTime(TimeUnit timeUnit) {
        return convertToTimeUnit(timeUnit, elapsedTime);
    }

    @Override
    public long getRemainingTime() {
        return convertToTimeUnit(timeUnit, timeLimit - elapsedTime);
    }

    @Override
    public long getRemainingTime(TimeUnit timeUnit) {
        return convertToTimeUnit(timeUnit, timeLimit - elapsedTime);
    }

    @Override
    public void noteTimeNow() {
        notedTimeList.add(elapsedTime);
    }

    @Override
    public Long[] getNotedTimes() {
        return getNotedTimes(timeUnit);
    }

    @Override
    public Long[] getNotedTimes(TimeUnit timeUnit) {
        Long[] notedTimesArray = new Long[notedTimeList.size()];
        for (int i = 0; i < notedTimeList.size(); i++) {
            notedTimesArray[i] = convertToTimeUnit(timeUnit, notedTimeList.get(i));
        }
        return notedTimesArray;
    }

    private long convertToMilliseconds(long time){
        switch (timeUnit){
            case DAYS:
                return time * 24 * 60 * 60 * 1000;
            case HOURS:
                return time * 60 * 60 * 1000;
            case MINUTES:
                return time * 60 * 1000;
            case SECONDS:
                return time * 1000;
            case MILLISECONDS:
                return time;
            default:
                throw new IllegalArgumentException(TIMER_UNIT_OUT_OF_BOUND_MESSAGE);
        }
    }

    private long convertToTimeUnit(TimeUnit timeUnit, long time){
        switch (timeUnit){
            case DAYS:
                return time / 1000 / 60 / 60 / 24;
            case HOURS:
                return time / 1000 / 60 / 60;
            case MINUTES:
                return time / 1000 / 60;
            case SECONDS:
                return time / 1000;
            case MILLISECONDS:
                return time;
            default:
                throw new IllegalArgumentException(TIMER_UNIT_OUT_OF_BOUND_MESSAGE);
        }
    }
}
