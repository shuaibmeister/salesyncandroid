package com.thinknsync.salesync.commons.configActions;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.base.viewInterfaces.ViewsNavigationManager;
import com.thinknsync.salesync.commons.loginAndSessions.SessionManager;
import com.thinknsync.salesync.commons.loginAndSessions.SessionTimeoutMonitor;
import com.thinknsync.salesync.commons.osSpecifics.broadcastReceivers.BroadcastListener;
import com.thinknsync.salesync.commons.osSpecifics.broadcastReceivers.BroadcastListenerContainer;
import com.thinknsync.salesync.commons.osSpecifics.serviceManagers.TrackingServiceManager;
import com.thinknsync.salesync.database.records.config.ConfigInterface;
import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.database.repositories.config.ConfigRepositoryInterface;
import com.thinknsync.salesync.database.repositories.user.UserRepositoryInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.location.EmployeeTracking;
import com.thinknsync.salesync.location.TrackingListenerDefaultAction;
import com.thinknsync.salesync.login.pinLogin.PinLoginView;

public class ConfigAction implements ConfigWiseActions {

    private AndroidContextWrapper contextWrapper;
    private ObjectConstructor objectConstructor;

    private TrackingServiceManager trackingServiceManager;
    private ConfigRepositoryInterface configRepo;

    private static SessionManager screenOnOffObserver;

    public ConfigAction(AndroidContextWrapper contextWrapper, ObjectConstructor objectConstructor) {
        this.contextWrapper = contextWrapper;
        this.objectConstructor = objectConstructor;
        this.configRepo = objectConstructor.getConfigRepository();
    }

    @Override
    public void doAction() {
        UserInterface loggedInUser = objectConstructor.getUserRepository().getLoggedInUser();
        if(loggedInUser != null && loggedInUser.isLoggedIn()) {
            implementSessionTimeout();
            manageTrackingService();
        }
    }

    private void implementSessionTimeout() {
        BroadcastListenerContainer broadcastListenerContainer = objectConstructor.getBroadcastListenerContainer();
        ConfigInterface sessionTimeoutConfig = configRepo.getConfigOfType(ConfigInterface.ConfigType.SESSION_TIMEOUT_FLAG);
        if(sessionTimeoutConfig != null) {
            if (Boolean.valueOf(sessionTimeoutConfig.getValue()) && screenOnOffObserver == null) {
                screenOnOffObserver = new SessionTimeoutMonitor(objectConstructor, new Runnable() {
                    @Override
                    public void run() {
                        try {
                            UserRepositoryInterface userRepo = objectConstructor.getUserRepository();
                            UserInterface loggedInUser = userRepo.getLoggedInUser();
                            if (loggedInUser.isLoggedIn()) {
                                loggedInUser.setIsLoggedIn(false);
                                userRepo.save(loggedInUser);
                                ((ViewsNavigationManager) contextWrapper.getFrameworkObject()).startActivity(PinLoginView.class);
                            }
                        } catch (ClassCastException e) {
                            e.printStackTrace();
                        }
                    }
                });
                screenOnOffObserver.setSessionTimeoutInMinutes(
                        Double.valueOf(configRepo.getConfigOfType(ConfigInterface.ConfigType.SESSION_TIMEOUT_INTERVAL).getValue()));
                broadcastListenerContainer.addObserverToListener(BroadcastListener.BroadcastListenerType.SCREEN_ON_OFF, screenOnOffObserver);
            } else if (!Boolean.valueOf(sessionTimeoutConfig.getValue()) && screenOnOffObserver != null) {
                broadcastListenerContainer.removeObserverFromListener(BroadcastListener.BroadcastListenerType.SCREEN_ON_OFF, screenOnOffObserver);
                screenOnOffObserver = null;
            }
        }
    }

    private void manageTrackingService() {
        ConfigInterface locationTrackingConfig = configRepo.getConfigOfType(ConfigInterface.ConfigType.LOCATION_TRACKING_FLAG);
        if(locationTrackingConfig != null) {
            if (Boolean.valueOf(locationTrackingConfig.getValue())) {
                trackingServiceManager = TrackingServiceManager.getInstance(contextWrapper, objectConstructor,
                        Integer.valueOf(configRepo.getConfigOfType(ConfigInterface.ConfigType.LOCATION_TRACKING_INTERVAL).getValue()));
                trackingServiceManager.addToObservers(new TrackingListenerDefaultAction(objectConstructor));
                trackingServiceManager.startService(EmployeeTracking.class);
            } else if (trackingServiceManager != null) {
                trackingServiceManager.stopService();
            }
        }
    }
}
