package com.thinknsync.salesync.commons.osSpecifics.broadcastReceivers;

import android.content.BroadcastReceiver;

import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.observerhost.TypedObserver;

import java.util.List;

public abstract class BaseBroadcastListener<ObserverType> extends BroadcastReceiver
        implements BroadcastListener<ObserverType> {

    protected ObserverHostImpl<ObserverType> observerHost;

    public BaseBroadcastListener(){
        observerHost = new ObserverHostImpl<>();
    }

    @Override
    public void addToObservers(TypedObserver<ObserverType> typedObserver) {
        observerHost.addToObservers(typedObserver);
    }

    @Override
    public void addToObservers(List<TypedObserver<ObserverType>> list) {
        observerHost.addToObservers(list);
    }

    @Override
    public void removeObserver(TypedObserver<ObserverType> typedObserver) {
        observerHost.removeObserver(typedObserver);
    }

    @Override
    public void clearObservers() {
        observerHost.clearObservers();
    }

    @Override
    public void notifyObservers(ObserverType object){
        observerHost.notifyObservers(object);
    }

    @Override
    public void notifyError(Exception e) {
        e.printStackTrace();
    }
}
