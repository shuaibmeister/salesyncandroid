package com.thinknsync.salesync.commons.commonInterfaces;

public interface HasImage {
    String getImage();
    void  setImage(String imageString);
}
