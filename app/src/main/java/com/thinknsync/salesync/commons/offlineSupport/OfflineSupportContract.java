package com.thinknsync.salesync.commons.offlineSupport;

import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.objectwrappers.FrameworkWrapper;

import java.util.List;

public interface OfflineSupportContract {
    interface SendToServer {
        void sendData(FrameworkWrapper contextWrapper, final ApiResponseActions responseActions);
    }

    interface SetMultipleData<T> {
        void setData(List<T> data);
    }

    interface SetSingleData<T> {
        void setData(T data);
    }

    interface Syncable {
        void onPostSynced(boolean isSynced);
    }
}
