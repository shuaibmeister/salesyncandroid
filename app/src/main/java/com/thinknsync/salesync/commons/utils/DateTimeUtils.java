package com.thinknsync.salesync.commons.utils;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public final class DateTimeUtils {
    private final String DATE_FORMAT_DEFAULT = "yyyy-MM-dd";

    public SimpleDateFormat getDefaultDateFormat() {
        return new SimpleDateFormat(DATE_FORMAT_DEFAULT);
    }

    public SimpleDateFormat getDefaultDateTimeFormat() {
        return new SimpleDateFormat(DATE_FORMAT_DEFAULT + " HH:mm:ss");
    }

    public SimpleDateFormat getDefaultDateTimeFormatWithoutSeconds() {
        return new SimpleDateFormat(DATE_FORMAT_DEFAULT + " HH:mm");
    }

    public SimpleDateFormat getDefaultDateTimeFormatAmPm() {
        return new SimpleDateFormat(DATE_FORMAT_DEFAULT + " hh:mm aa");
    }

    public String getDefaultDateStringToday() {
        return getDefaultDateFormat().format(Calendar.getInstance().getTime());
    }

    public String getDefaultDateStringFromString(String dateString) {
        return getDefaultDateFormat().format(dateString);
    }

    public String getDefaultDateTimeStringNow() {
        return getDefaultDateTimeStringFromDate(Calendar.getInstance().getTime());
    }

    public Date getDefaultDateTimeNow() {
        return Calendar.getInstance().getTime();
    }

    public Date get11thHourDateTimeNow() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    public Date getDateFromDefaultDateTimeString(String dateTimeDefaultString) {
        try {
            return getDefaultDateTimeFormat().parse(dateTimeDefaultString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return getDefaultDateTimeNow();
    }

    public Date getDateFromDefaultDateString(String dateDefaultString) {
        try {
            return getDefaultDateFormat().parse(dateDefaultString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return getDefaultDateTimeNow();
    }

    public String getDefaultDateTimeStringFromDate(Date dateTime) {
        return getDefaultDateTimeFormat().format(dateTime.getTime());
    }

    public String getDefaultDateStringFromDate(Date dateTime) {
        return getDefaultDateFormat().format(dateTime.getTime());
    }

    public int getDateIntOfToday() {
        return getDateIntFromDefaultDateString(getDefaultDateStringToday());
    }

    public long getDateTimeIntOfToday() {
        return getDateTimeIntFromDefaultDateTimeString(getDefaultDateTimeStringNow());
    }

    public int getDateIntFromDefaultDateString(String date) {
        return Integer.parseInt(date.replace("-", ""));
    }

    public int getDateIntFromDefaultDate(Date date) {
        return getDateIntFromDefaultDateString(getDefaultDateStringFromDate(date));
    }

    public String getFormattedDate(String date) {
        return date.replace("/", "-");
    }

    public long getDateTimeIntFromDefaultDateTimeString(String dateTime) {
        return Long.parseLong(dateTime.replace("-", "").replace(":", "").replace(" ", ""));
    }

    public  int[] getDayMonthYearArray(String dateTime){
        String [] dateParts = dateTime.split(" ")[0].split("-");
        return new int[]{Integer.parseInt(dateParts[0]), Integer.parseInt(dateParts[1]) + 1,
                Integer.parseInt(dateParts[2])};
    }

    public String getMonthNameFromDateString(String dateTimeString){
        int[] dateParts = getDayMonthYearArray(dateTimeString);
        return getMonthName(dateParts[1]);
    }

    public String getAmPmFormattedTime(Date date){
//        int[] dateParts = getDayMonthYearArray(dateTimeString);
//        return getMonthName(dateParts[1]);
        String[] dateTimeParts = getDefaultDateTimeFormatAmPm().format(date).split(" ");
        return dateTimeParts[1] + " " + dateTimeParts[2];
    }

    public int getDateOnlyFromDate(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DATE);
    }

    public String getMonthName(int monthNumber) {
        return new DateFormatSymbols().getMonths()[monthNumber -1];
//        switch (monthNumber -1) {
//            case 0:
//                return "January";
//            case 1:
//                return "February";
//            case 2:
//                return "March";
//            case 3:
//                return "April";
//            case 4:
//                return "May";
//            case 5:
//                return "June";
//            case 6:
//                return "July";
//            case 7:
//                return "August";
//            case 8:
//                return "September";
//            case 9:
//                return "October";
//            case 10:
//                return "November";
//            case 11:
//                return "December";
//            default:
//                return "";
//
//        }
    }
}
