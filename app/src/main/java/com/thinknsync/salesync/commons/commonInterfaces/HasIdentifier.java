package com.thinknsync.salesync.commons.commonInterfaces;

public interface HasIdentifier {
    String key_identifier = "data_id";

    int getDataId();
    void setDataId(int id);
}
