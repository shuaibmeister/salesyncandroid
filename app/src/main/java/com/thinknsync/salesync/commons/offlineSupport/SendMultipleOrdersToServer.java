package com.thinknsync.salesync.commons.offlineSupport;

import android.content.Context;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.salesync.database.records.order.SalesOrderInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SendMultipleOrdersToServer extends BaseSendMultipleToServer<SalesOrderInterface> {
    public SendMultipleOrdersToServer(ObjectConstructor objectConstructor) {
        super(objectConstructor);
    }

    @Override
    protected String getJsonArrayKey() {
        return SalesOrderInterface.key_order_lines;
    }

    @Override
    protected JSONObject getObjectJson(SalesOrderInterface dataObject) throws JSONException {
        return dataObject.withoutProperty(new String[]{
                SalesOrderInterface.key_id, SalesOrderInterface.key_route, SalesOrderInterface.key_outlet, SalesOrderInterface.key_sr,
                SalesOrderInterface.key_exception, SalesOrderInterface.key_exception_id,
                SalesOrderInterface.key_is_synced, SalesOrderInterface.key_sr,
                SalesOrderInterface.key_created_at, SalesOrderInterface.key_updated_at
        });
    }

    @Override
    protected ApiRequest getSendToServerApiRequest(final FrameworkWrapper<Context> contextWrapper, final ApiResponseActions responseActions, final ApiDataObject apiDataObject) {
        ApiRequest apiRequest = objectConstructor.getBulkOrderSendApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, getWrappedResponseActions(responseActions));
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});
        apiRequest.setProgressDialog(null);
        return apiRequest;
    }

    @Override
    public void onPostSynced(boolean isSynced) {
        objectConstructor.getSalesOrderRepository().deleteOrders(saveObjects.toArray(new SalesOrderInterface[0]));
    }
}
