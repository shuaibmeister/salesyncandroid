package com.thinknsync.salesync.commons.osSpecifics.frameworkHelper;

public interface FrameworkVersionUtils{
    int getVersionInt();
    boolean isGreaterThan(int versionInt);
    boolean isGreaterThanOrEquals(int versionInt);
}
