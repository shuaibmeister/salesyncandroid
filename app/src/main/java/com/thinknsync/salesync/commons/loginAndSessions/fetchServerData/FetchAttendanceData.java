package com.thinknsync.salesync.commons.loginAndSessions.fetchServerData;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.salesync.R;
import com.thinknsync.salesync.commons.utils.DateTimeUtils;
import com.thinknsync.salesync.database.records.user.UserInterface;
import com.thinknsync.salesync.database.repositories.user.UserRepositoryInterface;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.home.bottomSlidingView.AttendanceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class FetchAttendanceData extends BaseFetchData {

    public FetchAttendanceData(ObjectConstructor objectConstructor, AndroidContextWrapper contextWrapper, ApiDataObject apiDataObject) {
        super(objectConstructor, contextWrapper, apiDataObject);
    }

    @Override
    protected String getOnPreExecuteMessage() {
        return contextWrapper.getFrameworkObject().getString(R.string.message_sync_attendance_data_start);
    }

    @Override
    protected String getOnPostExecuteMessage() {
        return contextWrapper.getFrameworkObject().getString(R.string.message_sync_attendance_data_end);
    }

    @Override
    public void executeTask(Object inputObject) {
        onPreExecute();
        syncAttendanceData(apiDataObject);
    }

    private void syncAttendanceData(final ApiDataObject apiDataObject) {
        final UserRepositoryInterface userRepo = objectConstructor.getUserRepository();
        final UserInterface user = userRepo.getLoggedInUser();

        final ApiResponseActions responseActions = new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException {
                JSONObject data = responseJson.getJSONArray(ApiRequest.ApiFields.KEY_DATA).getJSONObject(0);
                DateTimeUtils dateTimeUtils = new DateTimeUtils();

                String attendanceInDateTimeString = data.getString(UserInterface.key_attendance_in_date_time);
                user.setAttendanceInDateTime(dateTimeUtils.getDateFromDefaultDateTimeString(attendanceInDateTimeString));
                user.setAttendanceStatusToday(AttendanceManager.AttendanceStatus.SUBMITTED);

                if(data.getBoolean(UserInterface.key_has_punched_out)){
                    String attendanceOutDateTimeString = data.getString(UserInterface.key_attendance_out_date_time);
                    user.setAttendanceOutDateTime(dateTimeUtils.getDateFromDefaultDateTimeString(attendanceOutDateTimeString));
                    user.setAttendanceStatusToday(AttendanceManager.AttendanceStatus.SIGNED_OUT_AFTER_SIGN_IN);
                }
                userRepo.save(user);
                checkAndNotifyComplete();
            }

            @Override
            public void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException {
                user.setAttendanceStatusToday(AttendanceManager.AttendanceStatus.NOT_SUBMITTED);
                userRepo.save(user);
                checkAndNotifyComplete();
            }
        };

        ApiRequest apiRequest = objectConstructor.getHasAttendanceTodayApiObject(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
            put(ApiDataObject.tag, apiDataObject);
            put(ApiResponseActions.tag, responseActions);
            put(ApiRequest.ApiFields.REQ_ID, 1);
        }});

        apiRequest.setProgressDialog(null);
        apiRequest.sendRequest();
    }
}
