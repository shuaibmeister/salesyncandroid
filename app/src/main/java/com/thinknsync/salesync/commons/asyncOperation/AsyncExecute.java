package com.thinknsync.salesync.commons.asyncOperation;

import com.thinknsync.observerhost.ObserverHost;

public interface AsyncExecute<InputParamType, ObserverNotifyType> extends ObserverHost<ObserverNotifyType> {
    void executeTask(InputParamType inputObject);
    void setViewReference(View viewReference);

    interface View extends ObserverHost<Object>{
        void onAsyncTaskStart(String... progressMessage);
        void onProgressUpdated(int progressValue, String... progressMessage);
        void onAsyncTaskComplete(String... progressMessage);
    }
}
