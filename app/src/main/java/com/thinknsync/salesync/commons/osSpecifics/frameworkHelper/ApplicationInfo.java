package com.thinknsync.salesync.commons.osSpecifics.frameworkHelper;

public interface ApplicationInfo extends FrameworkVersionUtils{
    boolean isDebuggable();
    String getDeviceId();
    String getApplicationVersionName();
    boolean isServiceRunning(Class<?> serviceClass);
    String getStringValueFromEnvironment(String key);
    void hideSoftKeyboard();
    float convertPixelsToDp(float px);
    float convertDpToPixel(float dp);
}
