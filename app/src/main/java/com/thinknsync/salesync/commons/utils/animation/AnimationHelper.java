package com.thinknsync.salesync.commons.utils.animation;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;

import androidx.transition.Slide;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;

import com.thinknsync.objectwrappers.FrameworkWrapper;

public class AnimationHelper implements Animations {

    private Context context;
    private int default_animation_duration = DEFAULT_FADE_ANIMATION_DURATION;

    public AnimationHelper(Context context){
        this.context = context;
    }

    @Override
    public void fadeInElement(final FrameworkWrapper viewWrapper) {
        if(!getView(viewWrapper).isShown()) {
            Animation fadeIn = getFadeInAnimation(default_animation_duration);
            fadeIn.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    getView(viewWrapper).setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            getView(viewWrapper).startAnimation(fadeIn);
        }
    }

    @Override
    public void fadeOutElement(final FrameworkWrapper viewWrapper) {
        if(getView(viewWrapper).isShown()) {
            Animation fadeOut = getFadeOutAnimation(default_animation_duration);
            fadeOut.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    getView(viewWrapper).setVisibility(View.GONE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            getView(viewWrapper).startAnimation(fadeOut);
        }
    }

    @Override
    public void viewFadeOutAndIn(FrameworkWrapper viewWrapper) {
        final Animation out = new AlphaAnimation(1.0f, 0.0f);
        out.setRepeatCount(Animation.RELATIVE_TO_SELF);
        out.setRepeatMode(Animation.REVERSE);
        out.setDuration(default_animation_duration);

        getView(viewWrapper).startAnimation(out);
    }

    @Override
    public void fadeOutModifyAndFadeIn(final FrameworkWrapper viewWrapper, final Runnable viewRunnable) {
        Animation fadeOut = getFadeOutAnimation(default_animation_duration);
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                getView(viewWrapper).setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Animation fadeIn = getFadeInAnimation(default_animation_duration);
                viewRunnable.run();
                getView(viewWrapper).setVisibility(View.VISIBLE);
                getView(viewWrapper).startAnimation(fadeIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        getView(viewWrapper).startAnimation(fadeOut);
    }

    @Override
    public void slideInFromTop(FrameworkWrapper viewWrapper) {
        View view = (View) viewWrapper.getFrameworkObject();
        Transition transition = new Slide(Gravity.TOP);
        transition.setDuration(default_animation_duration);
        transition.addTarget(view);

        TransitionManager.beginDelayedTransition((ViewGroup) view.getParent(), transition);
        view.setVisibility(View.VISIBLE);
    }

    @Override
    public void slideOutToTop(FrameworkWrapper viewWrapper) {
        View view = (View) viewWrapper.getFrameworkObject();
        view.setVisibility(View.VISIBLE);
        view.animate().translationY(-500)
                .setDuration(default_animation_duration);
    }

    @Override
    public void rotateViewInfinite(FrameworkWrapper viewWrapper) {
        View img = (View) viewWrapper.getFrameworkObject();
        final Animation rotate = getRotationAnimation(img.getPivotX(), img.getPivotY(), 3000);
        rotate.setRepeatCount(Animation.INFINITE);
        img.startAnimation(rotate);
    }

    @Override
    public void rotateView(FrameworkWrapper viewWrapper, int rotationCount) {
        View img = (View) viewWrapper.getFrameworkObject();
        final Animation rotate = getRotationAnimation(img.getPivotX(), img.getPivotY(), 3000);
        rotate.setRepeatCount(rotationCount);
        img.startAnimation(rotate);
    }

    @Override
    public int getDefaultAnimationDuration() {
        return default_animation_duration;
    }

    @Override
    public void setDefaultAnimationDuration(int defaultAnimationDuration) {
        this.default_animation_duration = defaultAnimationDuration;
    }

    private Animation getFadeInAnimation(int duration){
        final Animation in = new AlphaAnimation(0.0f, 1.0f);
        in.setDuration(duration);
        return in;
    }

    private Animation getFadeOutAnimation(int duration){
        final Animation out = new AlphaAnimation(1.0f, 0.0f);
        out.setDuration(duration);
        return out;
    }

    private Animation getRotationAnimation(float pivotX, float pivotY, int duration){
        final Animation rotate = new RotateAnimation(0.0f, 360.0f, pivotX, pivotY);
        rotate.setDuration(duration);
        rotate.setInterpolator(new LinearInterpolator());
        return rotate;
    }

    private View getView(FrameworkWrapper viewWrapper){
        return (View)viewWrapper.getFrameworkObject();
    }
}
