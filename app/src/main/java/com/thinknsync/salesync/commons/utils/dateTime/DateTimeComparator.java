package com.thinknsync.salesync.commons.utils.dateTime;

public interface DateTimeComparator {
    void compareDateTimeWithServer();
    void initiateDateTimeChange();
}
