package com.thinknsync.salesync.commons.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

public final class ImageUtils {

    public Bitmap getBimapFromBase64Image(String base64EncodedString) {
        if(isImageStringValidBase64(base64EncodedString)) {
            try {
                byte[] decodedString = Base64.decode(base64EncodedString, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                return decodedByte;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public String encodeToBase64String(Bitmap image) {
        if(image != null) {
            ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOS);
            return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
        }
        return "";
    }

    public Bitmap getScaledImage(Bitmap image, int width, int height){
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public boolean isImageStringValidBase64(String base64Image){
        return base64Image != null && !base64Image.equals("") && !base64Image.equals("false");
    }
}
