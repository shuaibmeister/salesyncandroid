package com.thinknsync.salesync.commons.osSpecifics.serviceManagers;

import android.content.Intent;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.salesync.factory.ObjectConstructor;
import com.thinknsync.salesync.commons.osSpecifics.frameworkHelper.ApplicationInfo;

import java.util.HashMap;

public abstract class BaseServiceManager<ServiceClassType, ObserverNotifyDataType> extends ObserverHostImpl<ObserverNotifyDataType> {

    protected Intent serviceIntent;
    protected ObjectConstructor objectConstructor;
    protected AndroidContextWrapper contextWrapper;
    protected Class<ServiceClassType> serviceClass;

    protected BaseServiceManager(AndroidContextWrapper contextWrapper, ObjectConstructor objectConstructor) {
        this.objectConstructor = objectConstructor;
        this.contextWrapper = contextWrapper;
    }

    public boolean isServiceRunning() {
        if(serviceClass == null){
            return false;
        }

        ApplicationInfo applicationInfo = objectConstructor.getAndroidFrameworkHelper(new HashMap<String, Object>(){{
            put(AndroidContextWrapper.tag, contextWrapper);
        }});
        return applicationInfo.isServiceRunning(serviceClass);
    }

    public void stopService() {
        contextWrapper.getFrameworkObject().stopService(serviceIntent);
        serviceIntent = null;
    }
}
